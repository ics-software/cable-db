/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.openepics.cable.model.util.StringUtil;

import com.google.common.base.Preconditions;

/**
 * This represents an instance of installation package.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Entity
@Table(name = "installation_package")
public class InstallationPackage extends Persistable {

    private static final long serialVersionUID = 3248815346130689290L;

    private String name;
    private String description;
    private String location;
    private Date created;
    private Date modified;
    @Column(name = "cable_coordinator")
    private String cableCoordinator;
    @Column(name = "routing_documentation")
    private String routingDocumentation;
    @Column(name = "installer_cable")
    private String installerCable;
    @Column(name = "installer_connector_a")
    private String installerConnectorA;
    @Column(name = "installer_connector_b")
    private String installerConnectorB;
    // comma separated string with usernames
    @Column(name = "installation_package_leader")
    private String installationPackageLeader;
    private String active;

    @OneToMany(mappedBy="installationPackage", fetch=FetchType.EAGER)
    private List<Cable> cables;

    /**
     * Creates a new empty instance of a installation package. Also serves as constructor for JPA entity.
     */
    public InstallationPackage() {
        setName("");
        setActive(true);
    }

    /**
     * Creates a new instance of a installation package.
     *
     * @param name the name of the installation package
     * @param description the description of the installation package
     * @param location the location of the installation package
     * @param cableCoordinator person(s) intended as cable coordinator(s)
     * @param routingDocumentation link to routing documentation for the installation package
     * @param installerCable person(s), company(ies) intended to install cable
     * @param installerConnectorA person(s), company(ies) intended to install connector A
     * @param installerConnectorB person(s), company(ies) intended to install connector B
     * @param installationPackageLeader person(s) intended to act as leader for installation package
     * @param active if installation package is active
     */
    public InstallationPackage(
            String name, String description, String location, String cableCoordinator, String routingDocumentation,
            String installerCable, String installerConnectorA, String installerConnectorB,
            String installationPackageLeader,
            boolean active) {
        this();
        setName(name);
        setDescription(description);
        setLocation(location);
        setCableCoordinator(cableCoordinator);
        setRoutingDocumentation(routingDocumentation);
        setInstallerCable(installerCable);
        setInstallerConnectorA(installerConnectorA);
        setInstallerConnectorB(installerConnectorB);
        setInstallationPackageLeader(installationPackageLeader);
        setActive(active);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Preconditions.checkNotNull(name);
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getCreated() {
        return created != null ? new Date(created.getTime()) : null;
    }

    public void setCreated(Date created) {
        this.created = created != null ? new Date(created.getTime()) : null;
    }

    public Date getModified() {
        return modified != null ? new Date(modified.getTime()) : null;
    }

    public void setModified(Date modified) {
        this.modified = modified != null ? new Date(modified.getTime()) : null;
    }

    public String getCableCoordinator() {
        return cableCoordinator;
    }

    public void setCableCoordinator(String cableCoordinator) {
        this.cableCoordinator = cableCoordinator;
    }

    public List<String> getCableCoordinators() {
        return StringUtil.splitToList(getCableCoordinator());
    }

    public void setCableCoordinators(List<String> cableCoordinators) {
        setCableCoordinator(StringUtil.joinToString(cableCoordinators));
    }

    public String getRoutingDocumentation() {
        return routingDocumentation;
    }

    public void setRoutingDocumentation(String routingDocumentation) {
        this.routingDocumentation = routingDocumentation;
    }

    public String getInstallerCable() {
        return installerCable;
    }

    public void setInstallerCable(String installerCable) {
        this.installerCable = installerCable;
    }

    public String getInstallerConnectorA() {
        return installerConnectorA;
    }

    public void setInstallerConnectorA(String installerConnectorA) {
        this.installerConnectorA = installerConnectorA;
    }

    public String getInstallerConnectorB() {
        return installerConnectorB;
    }

    public void setInstallerConnectorB(String installerConnectorB) {
        this.installerConnectorB = installerConnectorB;
    }

    public String getInstallationPackageLeader() {
        return installationPackageLeader;
    }

    public void setInstallationPackageLeader(String installationPackageLeader) {
        this.installationPackageLeader = installationPackageLeader;
    }

    public List<String> getInstallationPackageLeaders() {
        return StringUtil.splitToList(getInstallationPackageLeader());
    }

    public void setInstallationPackageLeaders(List<String> installationPackageLeaders) {
        setInstallationPackageLeader(StringUtil.joinToString(installationPackageLeaders));
    }

    public boolean isActive() {
        return "true".equalsIgnoreCase(active);
    }

    public void setActive(boolean status) {
        this.active = status ? "true" : "false";
    }

    public List<Cable> getCables() {
        return cables;
    }

    public void setCables(List<Cable> cables) {
        this.cables = cables;
    }

    public void addCable(Cable cable) {
        cables.add(cable);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((cableCoordinator == null) ? 0 : cableCoordinator.hashCode());
        result = prime * result + ((routingDocumentation == null) ? 0 : routingDocumentation.hashCode());
        result = prime * result + ((installerCable == null) ? 0 : installerCable.hashCode());
        result = prime * result + ((installerConnectorA == null) ? 0 : installerConnectorA.hashCode());
        result = prime * result + ((installerConnectorB == null) ? 0 : installerConnectorB.hashCode());
        result = prime * result + ((installationPackageLeader == null) ? 0 : installationPackageLeader.hashCode());
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InstallationPackage other = (InstallationPackage) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (cableCoordinator == null) {
            if (other.cableCoordinator != null)
                return false;
        } else if (!cableCoordinator.equals(other.cableCoordinator))
            return false;
        if (routingDocumentation == null) {
            if (other.routingDocumentation != null)
                return false;
        } else if (!routingDocumentation.equals(other.routingDocumentation))
            return false;
        if (installerCable == null) {
            if (other.installerCable != null)
                return false;
        } else if (!installerCable.equals(other.installerCable))
            return false;
        if (installerConnectorA == null) {
            if (other.installerConnectorA != null)
                return false;
        } else if (!installerConnectorA.equals(other.installerConnectorA))
            return false;
        if (installerConnectorB == null) {
            if (other.installerConnectorB != null)
                return false;
        } else if (!installerConnectorB.equals(other.installerConnectorB))
            return false;
        if (installationPackageLeader == null) {
            if (other.installationPackageLeader != null)
                return false;
        } else if (!installationPackageLeader.equals(other.installationPackageLeader))
            return false;
        if (active == null) {
            if (other.active != null)
                return false;
        } else if (!active.equals(other.active))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
