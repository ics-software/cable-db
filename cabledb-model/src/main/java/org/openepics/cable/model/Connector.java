/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import java.util.Date;

import javax.persistence.*;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * This represents an instance of connector details.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Entity
public class Connector extends Persistable {

    private static final long serialVersionUID = -7654156206144341815L;

    private String name;
    private String description;
    private String connectorType;
    private Date created;
    private Date modified;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "artifact_id")
    private GenericArtifact dataSheet;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER, mappedBy = "connector")
    @OrderColumn(name = "position")
    private List<ConnectorManufacturer> manufacturers = new ArrayList<>();

    private boolean active = true;

    @Basic
    @Column(name = "assembly_instructions")
    private String assemblyInstructions;

    @Basic
    @Column(name = "link_to_datasheet")
    private String linkToDatasheet;

    /** Constructor for JPA entity. */
    public Connector() {}

    /**
     * Creates a new instance of connector details.
     *
     * @param name
     *            connector name
     * @param created
     *            creation date
     * @param modified
     *            modification date
     */
    public Connector(String name, Date created, Date modified) {
        this(name, null, null, null, created, modified, null,
                null, null);
    }

    /**
     * Creates a new instance of connector details.
     *
     * @param name
     *            connector name
     * @param assemblyInstructions
     *            assembly instructions
     * @param description
     *            connector description
     * @param type
     *            connector type
     * @param created
     *            date when connector details were created
     * @param modified
     *            date when connector details were modified
     * @param dataSheet
     *            data sheet artifact
     * @param manufacturers
     *            the manufacturers
     * @param linkToDatasheet
     *            link to the datasheet
     */
    public Connector(String name, String assemblyInstructions, String description, String type,
            Date created, Date modified, GenericArtifact dataSheet, List<ConnectorManufacturer> manufacturers,
            String linkToDatasheet) {
        Preconditions.checkArgument(name != null && !name.isEmpty());
        Preconditions.checkNotNull(created);
        Preconditions.checkNotNull(modified);

        this.name = name;
        this.description = description;
        this.connectorType = type;
        this.created = new Date(created.getTime());
        this.modified = new Date(modified.getTime());
        this.dataSheet = dataSheet;
        this.manufacturers.clear();
        if (manufacturers != null) {
                this.manufacturers.addAll(manufacturers);
        }

        if(Strings.isNullOrEmpty(assemblyInstructions)){
            this.assemblyInstructions = null;
        } else {
            this.assemblyInstructions = assemblyInstructions;
        }

        if(Strings.isNullOrEmpty(assemblyInstructions)) {
            this.linkToDatasheet = null;
        } else {
            this.linkToDatasheet = linkToDatasheet;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return connectorType;
    }

    public void setType(String connectorType) {
        this.connectorType = connectorType;
    }

    public Date getCreated() {
        return created != null ? new Date(created.getTime()) : null;
    }

    public void setCreated(Date created) {
        this.created = created != null ? new Date(created.getTime()) : null;
    }

    public Date getModified() {
        return modified != null ? new Date(modified.getTime()) : null;
    }

    /**
     * Sets the modified date of the manufacturer details.
     *
     * @param modified
     *            the modified date to set
     */
    public void setModified(Date modified) {
        this.modified = modified != null ? new Date(modified.getTime()) : null;
    }

    public GenericArtifact getDataSheet() {
        return dataSheet;
    }

    public void setDataSheet(GenericArtifact dataSheet) {
        this.dataSheet = dataSheet;
    }

    /** @return true if this connector is in active use, false if it was obsoleted */
    public boolean isActive() {
        return active;
    }

    /**
     * @return the manufacturers
     */
    public List<ConnectorManufacturer> getManufacturers() {
        return manufacturers;
    }

    /**
     * Joins the names of all manufacturers and datasheets of this cable type for filtering/sorting purposes.
     *
     * @param addDatasheet
     *            boolean indicating whether or not to add datasheets to the string.
     * @return a concated string of manufacturer names and datasheets
     */
    public String getManufacturersAsString(boolean addDatasheet) {
        final StringBuilder builder = new StringBuilder();
        int i = 1;
        for (ConnectorManufacturer manufacturer : manufacturers) {
            builder.append(manufacturer.getManufacturer().getName());
            if (addDatasheet && manufacturer.getDatasheet() != null && manufacturer.getDatasheet().getName() != null
                    && !manufacturer.getDatasheet().getName().isEmpty()) {
                builder.append(" (");
                builder.append(manufacturer.getDatasheet().getName());
                builder.append(")");
            }
            if (i < manufacturers.size()) {
                builder.append(", ");
            }
            i++;
        }

        return builder.toString();

    }

    /**
     * @param manufacturers
     *            the manufacturers to set
     */
    public void setManufacturers(List<ConnectorManufacturer> manufacturers) {
        this.manufacturers.clear();
        if (manufacturers != null) {
            this.manufacturers.addAll(manufacturers);
        }
    }

    /**
     * Set the connector active state.
     *
     * @param active
     *            true if this connector is in active use, false if it was obsoleted
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAssemblyInstructions() {
        return assemblyInstructions;
    }

    public void setAssemblyInstructions(String assemblyInstructions) {
        this.assemblyInstructions = assemblyInstructions;
    }

    public String getLinkToDatasheet() {
        return linkToDatasheet;
    }

    public void setLinkToDatasheet(String linkToDatasheet) {
        this.linkToDatasheet = linkToDatasheet;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((connectorType == null) ? 0 : connectorType.hashCode());
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        result = prime * result + ((dataSheet == null) ? 0 : dataSheet.hashCode());
        result = prime * result + ((manufacturers == null) ? 0 : manufacturers.hashCode());
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((assemblyInstructions == null) ? 0 : assemblyInstructions.hashCode());
        result = prime * result + ((linkToDatasheet == null) ? 0 : linkToDatasheet.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        // avoid circular dependency on ConnectorManufacturer
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Connector other = (Connector) obj;
        if (!equalsContent(other))
            return false;
        if (dataSheet == null) {
            if (other.dataSheet != null)
                return false;
        } else if (!dataSheet.equals(other.dataSheet))
            return false;
        if (manufacturers == null) {
            if (other.manufacturers != null)
                return false;
        } else if (!manufacturers.equals(other.manufacturers))
            return false;
        return true;
    }

    public boolean equalsContent(Connector other) {
        // avoid circular dependency on ConnectorManufacturer
        if (other == null)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (connectorType == null) {
            if (other.connectorType != null)
                return false;
        } else if (!connectorType.equals(other.connectorType))
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        if (active != other.active)
            return false;
        if (assemblyInstructions == null) {
            if (other.assemblyInstructions != null)
                return false;
        } else if (!assemblyInstructions.equals(other.assemblyInstructions))
            return false;
        if (linkToDatasheet == null) {
            if (other.linkToDatasheet != null)
                return false;
        } else if (!linkToDatasheet.equals(other.linkToDatasheet))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getName();
    }

}
