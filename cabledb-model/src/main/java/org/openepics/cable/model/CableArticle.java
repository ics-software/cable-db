/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.google.common.base.Preconditions;

/**
 * This represents an approved cable article, i.e. a set of data from ePLAN.
 *
 * @author Lars Johansson
 */
@Entity
public class CableArticle extends Persistable {

    /**
     *
     */
    private static final long serialVersionUID = -6237454235100061864L;

    public static final String N   = "N";
    public static final String Y   = "Y";

    private String manufacturer;
    @Column(name = "external_id")
    private String externalId;
    @Column(name = "erp_number")
    private String erpNumber;
    @Column(name = "iso_class")
    private String isoClass;
    private String description;
    @Column(name = "long_description")
    private String longDescription;
    @Column(name = "model_type")
    private String modelType;
    @Column(name = "bending_radius")
    private Float bendingRadius;
    @Column(name = "outer_diameter")
    private Float outerDiameter;
    @Column(name = "rated_voltage")
    private Float ratedVoltage;
    @Column(name = "weight_per_length")
    private Float weightPerLength;
    @Column(name = "short_name")
    private String shortName;
    @Column(name = "short_name_external_id")
    private String shortNameExternalId;
    @Column(name = "url_chess_part")
    private String urlChessPart;
    private boolean active = true;
    private Date created;
    private Date modified;

    /** Constructor for JPA entity. */
    public CableArticle() {}

    public CableArticle(String manufacturer, String externalId, String erpNumber, String isoClass,
            String description, String longDescription, String modelType,
            Float bendingRadius, Float outerDiameter, Float ratedVoltage, Float weightPerLength,
            String shortName, String shortNameExternalId, String urlChessPart,
            boolean active, Date created, Date modified) {

        // required
        Preconditions.checkNotNull(manufacturer);
        Preconditions.checkNotNull(externalId);
        Preconditions.checkNotNull(erpNumber);
        Preconditions.checkNotNull(shortNameExternalId);
        Preconditions.checkArgument(!manufacturer.isEmpty());
        Preconditions.checkArgument(!externalId.isEmpty());
        Preconditions.checkArgument(!erpNumber.isEmpty());
        Preconditions.checkArgument(!shortNameExternalId.isEmpty());

        Preconditions.checkNotNull(created);
        Preconditions.checkNotNull(modified);

        // if present, non-negative value required
        Preconditions.checkArgument(bendingRadius == null || bendingRadius.floatValue() >= 0);
        Preconditions.checkArgument(outerDiameter == null || outerDiameter.floatValue() >= 0);
        Preconditions.checkArgument(ratedVoltage == null || ratedVoltage.floatValue() >= 0);
        Preconditions.checkArgument(weightPerLength == null || weightPerLength.floatValue() >= 0);

        this.manufacturer = manufacturer;
        this.externalId = externalId;
        this.erpNumber = erpNumber;
        this.isoClass = isoClass;
        this.description = description;
        this.longDescription = longDescription;
        this.modelType = modelType;
        this.bendingRadius = bendingRadius;
        this.outerDiameter = outerDiameter;
        this.ratedVoltage = ratedVoltage;
        this.weightPerLength = weightPerLength;
        this.shortName = shortName;
        this.shortNameExternalId = shortNameExternalId;
        this.urlChessPart = urlChessPart;
        this.active = active;
        this.created = created;
        this.modified = modified;
    }

    /**
     * @return manufacturer.externalId
     */
    public String getName() {
        // intention to return shortNameExternalId
        return shortNameExternalId;
    }

    public String getManufacturer() {
        return manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getExternalId() {
        return externalId;
    }
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
    public String getErpNumber() {
        return erpNumber;
    }
    public void setErpNumber(String erpNumber) {
        this.erpNumber = erpNumber;
    }
    public String getIsoClass() {
        return isoClass;
    }
    public void setIsoClass(String isoClass) {
        this.isoClass = isoClass;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getLongDescription() {
        return longDescription;
    }
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
    public String getModelType() {
        return modelType;
    }
    public void setModelType(String modelType) {
        this.modelType = modelType;
    }
    public Float getBendingRadius() {
        return bendingRadius;
    }
    public void setBendingRadius(Float bendingRadius) {
        this.bendingRadius = bendingRadius;
    }
    public Float getOuterDiameter() {
        return outerDiameter;
    }
    public void setOuterDiameter(Float outerDiameter) {
        this.outerDiameter = outerDiameter;
    }
    public Float getRatedVoltage() {
        return ratedVoltage;
    }
    public void setRatedVoltage(Float ratedVoltage) {
        this.ratedVoltage = ratedVoltage;
    }
    public Float getWeightPerLength() {
        return weightPerLength;
    }
    public void setWeightPerLength(Float weightPerLength) {
        this.weightPerLength = weightPerLength;
    }
    public String getShortName() {
        return shortName;
    }
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public String getShortNameExternalId() {
        return shortNameExternalId;
    }
    public void setShortNameExternalId(String shortNameExternalId) {
        this.shortNameExternalId = shortNameExternalId;
    }
    public String getUrlChessPart() {
        return urlChessPart;
    }
    public void setUrlChessPart(String urlChessPart) {
        this.urlChessPart = urlChessPart;
    }
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
        result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
        result = prime * result + ((erpNumber == null) ? 0 : erpNumber.hashCode());
        result = prime * result + ((isoClass == null) ? 0 : isoClass.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((longDescription == null) ? 0 : longDescription.hashCode());
        result = prime * result + ((modelType == null) ? 0 : modelType.hashCode());
        result = prime * result + ((bendingRadius == null) ? 0 : bendingRadius.hashCode());
        result = prime * result + ((outerDiameter == null) ? 0 : outerDiameter.hashCode());
        result = prime * result + ((ratedVoltage == null) ? 0 : ratedVoltage.hashCode());
        result = prime * result + ((weightPerLength == null) ? 0 : weightPerLength.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        result = prime * result + ((shortNameExternalId == null) ? 0 : shortNameExternalId.hashCode());
        result = prime * result + ((urlChessPart == null) ? 0 : urlChessPart.hashCode());
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((created == null) ? 0 : created.hashCode());
        result = prime * result + ((modified == null) ? 0 : modified.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CableArticle other = (CableArticle) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (manufacturer == null) {
            if (other.manufacturer != null)
                return false;
        } else if (!manufacturer.equals(other.manufacturer))
            return false;
        if (externalId == null) {
            if (other.externalId != null)
                return false;
        } else if (!externalId.equals(other.externalId))
            return false;
        if (erpNumber == null) {
            if (other.erpNumber != null)
                return false;
        } else if (!erpNumber.equals(other.erpNumber))
            return false;
        if (isoClass == null) {
            if (other.isoClass != null)
                return false;
        } else if (!isoClass.equals(other.isoClass))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (longDescription == null) {
            if (other.longDescription != null)
                return false;
        } else if (!longDescription.equals(other.longDescription))
            return false;
        if (modelType == null) {
            if (other.modelType != null)
                return false;
        } else if (!modelType.equals(other.modelType))
            return false;
        if (bendingRadius == null) {
            if (other.bendingRadius != null)
                return false;
        } else if (!bendingRadius.equals(other.bendingRadius))
            return false;
        if (outerDiameter == null) {
            if (other.outerDiameter != null)
                return false;
        } else if (!outerDiameter.equals(other.outerDiameter))
            return false;
        if (ratedVoltage == null) {
            if (other.ratedVoltage != null)
                return false;
        } else if (!ratedVoltage.equals(other.ratedVoltage))
            return false;
        if (weightPerLength == null) {
            if (other.weightPerLength != null)
                return false;
        } else if (!weightPerLength.equals(other.weightPerLength))
            return false;
        if (shortName == null) {
            if (other.shortName != null)
                return false;
        } else if (!shortName.equals(other.shortName))
            return false;
        if (shortNameExternalId == null) {
            if (other.shortNameExternalId != null)
                return false;
        } else if (!shortNameExternalId.equals(other.shortNameExternalId))
            return false;
        if (urlChessPart == null) {
            if (other.urlChessPart != null)
                return false;
        } else if (!urlChessPart.equals(other.urlChessPart))
            return false;
        if (active != other.active)
            return false;
        if (created == null) {
            if (other.created != null)
                return false;
        } else if (!created.equals(other.created))
            return false;
        if (modified == null) {
            if (other.modified != null)
                return false;
        } else if (!modified.equals(other.modified))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getName();
    }

}
