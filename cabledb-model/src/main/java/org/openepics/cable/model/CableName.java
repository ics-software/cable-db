/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import com.google.common.base.Preconditions;

/**
 * This represents a name assigned to a cable instance.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableName {

    public static final Integer NAME_NUMBER_MIN_LENGTH = 6;

    private static final Integer NAME_LENGTH = 9;

    private String system;
    private String subsystem;
    private String cableClass;
    private Integer seqNumber;

    /**
     * Creates a new instance from a string.
     *
     * @param name
     *            the string representation of the cable name
     */
    public CableName(String name) {
        Preconditions.checkArgument(isValidName(name));

        try {
            init(name.substring(0, 1), name.substring(1, 2), name.substring(2, 3),
                    Integer.valueOf(name.substring(3, 9)));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Creates a new instance from attributes.
     *
     * @param system
     *            the system digit
     * @param subsystem
     *            the subsystem digit
     * @param cableClass
     *            the class letter
     * @param seqNumber
     *            the sequential number
     */
    public CableName(String system, String subsystem, String cableClass, Integer seqNumber) {
        init(system, subsystem, cableClass, seqNumber);
    }

    private void init(String system, String subsystem, String cableClass, Integer seqNumber) {
        Preconditions.checkArgument(isValidSystem(system));
        Preconditions.checkArgument(isValidSubsystem(subsystem));
        Preconditions.checkArgument(isValidCableClass(cableClass));

        this.system = system;
        this.subsystem = subsystem;
        this.cableClass = cableClass.toUpperCase();
        this.seqNumber = seqNumber;
    }

    /**
     * Returns true if the given name is a valid representation of a cable name.
     *
     * @param name
     *            the name to check
     * @return true if valid, else false
     */
    public static boolean isValidName(String name) {
        // name length 9 --- 1 + 1 + 1 + 6
        if (name != null && name.length() == NAME_LENGTH) {
            final String system     = name.substring(0, 1);
            final String subsystem  = name.substring(1, 2);
            final String cableClass = name.substring(2, 3);
            final String seqNumber  = name.substring(3, 9);

            // check if valid
            //     seqNumber
            //     system
            //     subsystem
            //     cableClass

            try {
                Integer.valueOf(seqNumber);

                if (isValidSystem(system) && isValidSubsystem(subsystem) && isValidCableClass(cableClass)) {
                    return true;
                }
            } catch (NumberFormatException e) {
                // return false
            }
        }
        return false;
    }

    /**
     * Returns true if the value represents a valid system.
     *
     * @param value
     *            the value
     * @return true if the value represents a valid system, else false
     */
    public static boolean isValidSystem(String value) {
        return value != null && value.length() == 1 && Character.isDigit(value.charAt(0));
    }

    /**
     * Returns true if the value represents a valid subsystem.
     *
     * @param value
     *            the value
     * @return true if the value represents a valid subsystem, else false
     */
    public static boolean isValidSubsystem(String value) {
        return value != null && value.length() == 1
                && (Character.isDigit(value.charAt(0)) || Character.isLetter(value.charAt(0)));
    }

    /**
     * Returns true if the value represents a valid cable class
     *
     * @param value
     *            the value
     * @return true if the value represents a valid cable class, else false
     */
    public static boolean isValidCableClass(String value) {
        return value != null && value.length() == 1 && Character.isLetter(value.charAt(0));
    }

    /** @return the system digit */
    public String getSystem() {
        return system;
    }

    /** @return the subsystem digit */
    public String getSubsystem() {
        return subsystem;
    }

    /** @return the class letter */
    public String getCableClass() {
        return cableClass;
    }

    /** @return the sequential number */
    public Integer getSeqNumber() {
        return seqNumber;
    }

    /**
     * Returns the string representation of the given parameters as a cable name (example 12A012345).
     *
     * @param system
     *            the number system
     * @param subsystem
     *            the number subsystem
     * @param cableClass
     *            the number cable class
     * @param seqNumber
     *            the sequential number
     * @return the string representation
     */
    public static String asString(String system, String subsystem, String cableClass, Integer seqNumber) {
        return system + subsystem + cableClass + String.format("%0" + NAME_NUMBER_MIN_LENGTH + "d", seqNumber);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        result = prime * result + ((subsystem == null) ? 0 : subsystem.hashCode());
        result = prime * result + ((cableClass == null) ? 0 : cableClass.hashCode());
        result = prime * result + ((seqNumber == null) ? 0 : seqNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CableName other = (CableName) obj;
        if (system == null) {
            if (other.system != null)
                return false;
        } else if (!system.equals(other.system))
            return false;
        if (subsystem == null) {
            if (other.subsystem != null)
                return false;
        } else if (!subsystem.equals(other.subsystem))
            return false;
        if (cableClass == null) {
            if (other.cableClass != null)
                return false;
        } else if (!cableClass.equals(other.cableClass))
            return false;
        if (seqNumber == null) {
            if (other.seqNumber != null)
                return false;
        } else if (!seqNumber.equals(other.seqNumber))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return asString(system, subsystem, cableClass, seqNumber);
    }

}
