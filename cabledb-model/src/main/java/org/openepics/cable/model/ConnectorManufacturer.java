/*
 * Copyright (c) 2014 European Spallation Source
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * This represents an instance of a linking of Connector#{@link} and Manufacturer#{@link}.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 */
@Entity
public class ConnectorManufacturer extends Persistable {
    private static final long serialVersionUID = 1901434791259820211L;

    @ManyToOne
    @JoinColumn
    private Connector connector;

    @OneToOne
    private Manufacturer manufacturer;

    private int position;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "artifact_id")
    private GenericArtifact datasheet;

    /** Constructor for JPA entity. */
    public ConnectorManufacturer() {}

    /**
     * Constructs a new instance of linking object for connector and manufacturer.
     *
     * @param connector
     *            connector
     * @param manufacturer
     *            cable type manufacturer
     * @param datasheet
     *            manufacturer datasheet
     * @param position
     *            the position of the manufacturer
     */
    public ConnectorManufacturer(Connector connector, Manufacturer manufacturer, GenericArtifact datasheet,
            int position) {
        super();
        this.connector = connector;
        this.manufacturer = manufacturer;
        this.datasheet = datasheet;
        this.position = position;
    }

    /**
     * @return the connector
     */
    public Connector getConnector() {
        return connector;
    }

    /**
     * @param connector
     *            the connector to set
     */
    public void setConnector(Connector connector) {
        this.connector = connector;
    }

    /**
     * @return the manufacturer
     */
    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer
     *            the manufacturer to set
     */
    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer == null ? this.manufacturer : manufacturer;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(int position) {
        this.position = position;
    }

    public GenericArtifact getDatasheet() {
        return datasheet;
    }

    public void setDatasheet(GenericArtifact datasheet) {
        this.datasheet = datasheet;
    }

    public String getName() {
        return this.manufacturer.getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((connector == null) ? 0 : connector.hashCode());
        result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
        result = prime * result + ((datasheet == null) ? 0 : datasheet.hashCode());
        result = prime * result + position;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        // avoid circular dependency on Connector
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConnectorManufacturer other = (ConnectorManufacturer) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (connector == null) {
            if (other.connector != null)
                return false;
        } else if (!connector.equalsContent(other.connector))
            return false;
        if (manufacturer == null) {
            if (other.manufacturer != null)
                return false;
        } else if (!manufacturer.equals(other.manufacturer))
            return false;
        if (datasheet == null) {
            if (other.datasheet != null)
                return false;
        } else if (!datasheet.equals(other.datasheet))
            return false;
        return position == other.position;
    }

    @Override
    public String toString() {
        return manufacturer.toString();
    }
}
