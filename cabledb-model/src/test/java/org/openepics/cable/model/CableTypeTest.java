/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

/**
 * Tests {@link CableType}.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 *
 */
public class CableTypeTest {

    /**
     * Tests create of <tt>CableType</tt>.
     *
     * @see CableType#CableType()
     */
    @Test
    public void createCableType_noParameters() {
        final CableType cableType = new CableType();

        assertTrue(cableType.isActive());
        assertNull(cableType.getName());
        assertNull(cableType.getInstallationType());
        assertNull(cableType.getDescription());
        assertNull(cableType.getService());
        assertNull(cableType.getVoltage());
        assertNull(cableType.getInsulation());
        assertNull(cableType.getJacket());
        assertNull(cableType.getFlammability());
        assertNull(cableType.getTid());
        assertNull(cableType.getWeight());
        assertNull(cableType.getDiameter());
        assertNotNull(cableType.getManufacturers());
        assertEquals(0, cableType.getManufacturers().size());
        assertNull(cableType.getComments());
        assertNull(cableType.getRevision());
    }

    /**
     * Tests create of <tt>CableType</tt>.
     *
     * @see CableType#CableType(String, InstallationType)
     */
    @Test
    public void createCableType_nameInstallationType() {
        final String name = "name";
        final InstallationType installationType = InstallationType.OUTDOOR;
        final CableType cableType = new CableType(name, installationType);

        assertTrue(cableType.isActive());
        assertEquals(name, cableType.getName());
        assertEquals(installationType, cableType.getInstallationType());
        assertNull(cableType.getDescription());
        assertNull(cableType.getService());
        assertNull(cableType.getVoltage());
        assertNull(cableType.getInsulation());
        assertNull(cableType.getJacket());
        assertNull(cableType.getFlammability());
        assertNull(cableType.getTid());
        assertNull(cableType.getWeight());
        assertNull(cableType.getDiameter());
        assertNotNull(cableType.getManufacturers());
        assertEquals(0, cableType.getManufacturers().size());
        assertNull(cableType.getComments());
        assertNull(cableType.getRevision());
    }

    /**
     * Tests create of <tt>CableType</tt>.
     *
     * @see CableType#CableType(String, InstallationType, String, String, Integer, String, String, String,
     * Float, Float, Float, java.util.List, String, String)
     */
    @Test
    public void createCableType() {

        final String name = "name";
        final InstallationType installationType = InstallationType.OUTDOOR;
        final String description = "description";
        final String service = "service";
        final Integer voltage = 24;
        final String insulation = "insulation";
        final String jacket = "jacket";
        final String flammability = "flammability";
        final Float tid = 30f;
        final Float weight = 3.21f;
        final Float diameter = 1.23f;
        final Manufacturer manufacturer = new Manufacturer("CABLEMAN", "street", "00854", "CABLEMAN@example.org",
                "NEVERLAND", new Date(), new Date());
        final CableTypeManufacturer cableTypeManufacturer = new CableTypeManufacturer(null, manufacturer, null, 0);
        final String comments = "comments";
        final String revision = "revision";

        final CableType cableType = new CableType(name, installationType, description, service, voltage, insulation,
                jacket, flammability, tid, weight, diameter, Arrays.asList(cableTypeManufacturer), comments, revision);

        assertTrue(cableType.isActive());
        assertEquals(name, cableType.getName());
        assertEquals(installationType, cableType.getInstallationType());
        assertEquals(description, cableType.getDescription());
        assertEquals(service, cableType.getService());
        assertEquals(voltage, cableType.getVoltage());
        assertEquals(insulation, cableType.getInsulation());
        assertEquals(jacket, cableType.getJacket());
        assertEquals(flammability, cableType.getFlammability());
        assertEquals(tid, cableType.getTid());
        assertEquals(weight, cableType.getWeight());
        assertEquals(diameter, cableType.getDiameter());
        assertNotNull(cableType.getManufacturers());
        assertEquals(1, cableType.getManufacturers().size());
        assertEquals(manufacturer, cableType.getManufacturers().get(0).getManufacturer());
        assertEquals(comments, cableType.getComments());
        assertEquals(revision, cableType.getRevision());
    }

    /**
     * Tests create of <tt>CableType</tt>; to fail with faulty data (name).
     *
     * @see CableType#CableType(String, InstallationType)
     */
    @Test(expected = IllegalArgumentException.class)
    public void createCableType_noName() {
        final String name = null;
        final InstallationType installationType = InstallationType.OUTDOOR;
        new CableType(name, installationType);
    }

    /**
     * Tests create of <tt>CableType</tt>; to fail with faulty data (installationType).
     *
     * @see CableType#CableType(String, InstallationType)
     */
    @Test(expected = NullPointerException.class)
    public void createCableType_noInstallationType() {
        final String name = "name";
        final InstallationType installationType = null;
        new CableType(name, installationType);
    }

    /**
     * Tests update of <tt>CableType</tt>.
     *
     * @see CableType#CableType(String, InstallationType)
     * @see CableType#setInstallationType(InstallationType)
     */
    @Test
    public void updateCableType() {
        final CableType cableType = new CableType("name", InstallationType.OUTDOOR);
        cableType.setInstallationType(InstallationType.UNKNOWN);
    }

    /**
     * Tests update of <tt>CableType</tt>; to fail with faulty data (installationType).
     *
     * @see CableType#CableType(String, InstallationType)
     * @see CableType#setInstallationType(InstallationType)
     */
    @Test(expected = NullPointerException.class)
    public void updateCableType_noInstallationType() {
        final CableType cableType = new CableType("name", InstallationType.OUTDOOR);
        cableType.setInstallationType(null);
    }

}
