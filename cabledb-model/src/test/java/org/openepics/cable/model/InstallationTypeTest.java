package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link InstallationType}.
 *
 * @author Lars Johansson
 */
public class InstallationTypeTest {

    /**
     * Tests return of display name for <tt>InstallationType</tt.
     *
     * @see InstallationType#getDisplayName()
     * @see InstallationType#INDOOR
     * @see InstallationType#OUTDOOR
     * @see InstallationType#UNKNOWN
     */
    @Test
    public void getDisplayName() {
        assertEquals("INDOOR", InstallationType.INDOOR.getDisplayName());
        assertEquals("OUTDOOR", InstallationType.OUTDOOR.getDisplayName());
        assertEquals("UNKNOWN", InstallationType.UNKNOWN.getDisplayName());
    }

}
