package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

/**
 * Tests {@link Query}.
 *
 * @author Lars Johansson
 */
public class QueryTest {

    /**
     * Tests create of <tt>Query</tt>.
     *
     * @see Query#Query()
     */
    @Test
    public void createQuery_noParameters() {
        final Query query = new Query();

        assertNull(query.getDescription());
        assertNull(query.getOwner());
        assertNull(query.getCreated());
        assertNull(query.getExecuted());
        assertNull(query.getEntityType());
        assertNull(query.getConditions());
    }

    /**
     * Tests create of <tt>Query</tt>.
     *
     * @see Query#Query(String, EntityType, String, Date, List)
     */
    @Test
    public void createQuery() {
        String description = "description";
        String owner = "owner";
        Date created = new Date();
        EntityType entityType = EntityType.CABLE;

        List<QueryCondition> conditions = new ArrayList<>();
        QueryCondition queryCondition = new QueryCondition(null, null, "field", null, "value", null, null, Integer.MIN_VALUE);
        conditions.add(queryCondition);

        final Query query = new Query(description, entityType, owner, created, conditions);

        assertEquals(description, query.getDescription());
        assertEquals(owner, query.getOwner());
        assertEquals(created, query.getCreated());
        assertNull(query.getExecuted());
        assertEquals(EntityType.CABLE, query.getEntityType());
        assertNotNull(query.getConditions());
        assertEquals(1, query.getConditions().size());
        assertEquals("value", query.getConditions().get(0).getValue());
    }

}
