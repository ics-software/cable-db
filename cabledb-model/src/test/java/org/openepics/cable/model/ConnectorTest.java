package org.openepics.cable.model;

import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;

/**
 * Tests {@link Connector}.
 *
 * @author Lars Johansson
 */
public class ConnectorTest {

    /**
     * Tests create of <tt>Connector</tt>.
     *
     * @see Connector#Connector()
     */
    @Test
    public void createConnector_noParameters() {
        final Connector connector = new Connector();

        assertNull(connector.getName());
        assertNull(connector.getDescription());
        assertNull(connector.getType());
        assertNull(connector.getCreated());
        assertNull(connector.getModified());
        assertNull(connector.getDataSheet());

    }

    /**
     * Tests create of <tt>Connector</tt>.
     *
     * @see Connector#Connector(String, java.util.Date, java.util.Date)
     */
    @Test
    public void createConnector_nameDates() {
        String name = "name";
        Date created = new Date();
        Date modified = new Date();

        final Connector connector = new Connector(name, created, modified);

        assertEquals(name, connector.getName());
        assertNull(connector.getDescription());
        assertNull(connector.getType());
        assertEquals(created, connector.getCreated());
        assertEquals(modified, connector.getModified());
        assertNull(connector.getDataSheet());
    }

    /**
     * Tests create of <tt>Connector</tt>.
     *
     * @see Connector(String, String, String, String, java.util.Date, java.util.Date, GenericArtifact)
     */
    @Test
    public void createConnector() {
        String name = "name";
        String assemblyInstruction = "http://google.com";
        String description = "description";
        String type = "type";
        Date created = new Date();
        Date modified = new Date();
        GenericArtifact genericArtifact = new GenericArtifact();
        String name2 = "name2";
        genericArtifact.setName(name2);
        String linkToDatasheet = "https://chess.esss.lu.se/";
        final Manufacturer manufacturer = new Manufacturer("CONNECTORMAN", "street", "00854", "CONNECTORMAN@example.org",
                "NEVERLAND", new Date(), new Date());
        final ConnectorManufacturer connectorManufacturer = new ConnectorManufacturer(null, manufacturer, null, 0);

        final Connector connector = new Connector(name, assemblyInstruction, description, type, created, modified,
                genericArtifact, Arrays.asList(connectorManufacturer), linkToDatasheet);

        assertEquals(name, connector.getName());
        assertEquals(assemblyInstruction, connector.getAssemblyInstructions());
        assertEquals(description, connector.getDescription());
        assertEquals(type, connector.getType());
        assertEquals(created, connector.getCreated());
        assertEquals(modified, connector.getModified());
        assertNotNull(connector.getDataSheet());
        assertEquals(name2, connector.getDataSheet().getName());
        assertNotNull(connector.getManufacturers());
        assertEquals(1, connector.getManufacturers().size());
        assertEquals(manufacturer, connector.getManufacturers().get(0).getManufacturer());
        assertEquals(linkToDatasheet, connector.getLinkToDatasheet());
    }

}
