package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

/**
 * Tests {@link QueryCondition}.
 *
 * @author Lars Johansson
 */
public class QueryConditionTest {

    /**
     * Tests create of <tt>QueryCondition</tt>.
     *
     * @see QueryCondition#QueryCondition()
     */
    @Test
    public void createQueryCondition_noParameters() {
        final QueryCondition queryCondition = new QueryCondition();

        assertNull(queryCondition.getQuery());
        assertNull(queryCondition.getParenthesisOpen());
        assertNull(queryCondition.getField());
        assertNull(queryCondition.getComparisonOperator());
        assertNull(queryCondition.getValue());
        assertNull(queryCondition.getParenthesisClose());
        assertNull(queryCondition.getBooleanOperator());
        assertNull(queryCondition.getPosition());
    }

    /**
     * Tests create of <tt>QueryCondition</tt>.
     *
     * @see QueryCondition#QueryCondition(Query, QueryParenthesis, String, QueryComparisonOperator, String,
     * QueryParenthesis, QueryBooleanOperator, Integer)
     */
    @Test
    public void createQueryCondition() {
        String description = "description";
        String owner = "owner";
        Date created = new Date();
        EntityType entityType = EntityType.CABLE;

        List<QueryCondition> conditions = new ArrayList<>();
        QueryCondition queryCondition =
                new QueryCondition(null, null, "field", null, "value", null, null, Integer.MIN_VALUE);
        conditions.add(queryCondition);

        final Query query = new Query(description, entityType, owner, created, conditions);

        QueryParenthesis parenthesisOpen = QueryParenthesis.OPEN;
        String field = "fieldfield";
        QueryComparisonOperator comparisonOperator = QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO;
        String value = "valuevalue";
        QueryParenthesis parenthesisClose = QueryParenthesis.CLOSE;
        QueryBooleanOperator booleanOperator = QueryBooleanOperator.NOT;
        Integer position = Integer.MAX_VALUE;

        final QueryCondition queryConditionNew = new
                QueryCondition(query, parenthesisOpen, field, comparisonOperator,
                        value, parenthesisClose, booleanOperator, position);

        assertEquals(description, queryConditionNew.getQuery().getDescription());
        assertEquals(QueryParenthesis.OPEN, queryConditionNew.getParenthesisOpen());
        assertEquals(field, queryConditionNew.getField());
        assertEquals(QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO, queryConditionNew.getComparisonOperator());
        assertEquals(value, queryConditionNew.getValue());
        assertEquals(QueryParenthesis.CLOSE, queryConditionNew.getParenthesisClose());
        assertEquals(QueryBooleanOperator.NOT, queryConditionNew.getBooleanOperator());
        assertEquals(Integer.MAX_VALUE, queryConditionNew.getPosition().intValue());

    }

}
