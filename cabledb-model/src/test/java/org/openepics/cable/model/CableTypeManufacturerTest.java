package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Tests {@link CableTypeManufacturer}.
 *
 * @author Lars Johansson
 */
public class CableTypeManufacturerTest {

    /**
     * Tests create of <tt>CableTypeManufacturer</tt>.
     *
     * @see CableTypeManufacturer#CableTypeManufacturer()
     */
    @Test
    public void createCableTypeManufacturer_noParameters() {
        final CableTypeManufacturer cableTypeManufacturer = new CableTypeManufacturer();

        assertNull(cableTypeManufacturer.getCableType());
        assertNull(cableTypeManufacturer.getManufacturer());
        assertEquals(0, cableTypeManufacturer.getPosition());
        assertNull(cableTypeManufacturer.getDatasheet());
        //	assertNull(cableTypeManufacturer.getName());
    }

    /**
     * Tests create of <tt>CableTypeManufacturer</tt>.
     *
     * @see CableTypeManufacturer#CableTypeManufacturer(CableType, Manufacturer, GenericArtifact, int)
     */
    @Test
    public void createCableTypeManufacturer() {
        CableType cableType = new CableType();
        String comments = "comments";
        cableType.setComments(comments);

        String name = "name";
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName(name);

        String description = "description";
        GenericArtifact genericArtifact = new GenericArtifact();
        genericArtifact.setDescription(description);

        Integer position = Integer.MIN_VALUE;

        final CableTypeManufacturer cableTypeManufacturer = new CableTypeManufacturer(cableType, manufacturer, genericArtifact, position);

        assertNotNull(cableTypeManufacturer.getCableType());
        assertEquals(comments, cableTypeManufacturer.getCableType().getComments());
        assertNotNull(cableTypeManufacturer.getManufacturer());
        assertEquals(name, cableTypeManufacturer.getManufacturer().getName());
        assertNotNull(cableTypeManufacturer.getDatasheet());
        assertEquals(description, cableTypeManufacturer.getDatasheet().getDescription());
        assertEquals(Integer.MIN_VALUE, cableTypeManufacturer.getPosition());
    }

}
