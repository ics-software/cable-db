package org.openepics.cable.model.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

/**
 * Purpose to test StringUtil class.
 */
public class StringUtilTest {

    /**
     * Tests create, fill and get string with certain length and 'a' as each character.
     */
    @Test(expected = NegativeArraySizeException.class)
    public void getString_negative() {
        assertEquals("", StringUtil.getString(-1));
    }

    /**
     * Tests create, fill and get string with certain length and 'a' as each character.
     */
    @Test
    public void getString_empty() {
        assertEquals("", StringUtil.getString(0));
    }

    /**
     * Tests create, fill and get string with certain length and 'a' as each character.
     */
    @Test
    public void getString_16() {
        assertEquals("aaaaaaaaaaaaaaaa", StringUtil.getString(16));
    }

    /**
     * Tests create, fill and get string with certain length and 'a' as each character.
     */
    @Test
    public void getString_256() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<256; i++) {
            sb.append("a");
        }

        assertEquals(sb.toString(), StringUtil.getString(256));
    }

    /**
     * Test {@link StringUtil#joinToString(List)}.
     */
    @Test
    public void joinToString() {
        assertNull(StringUtil.joinToString(null));

        String str = StringUtil.joinToString(Collections.<String> emptyList());

        assertNotNull(str);
        assertEquals("", str);

        List<String> elements = new ArrayList<>();
        elements.add("A");
        str = StringUtil.joinToString(elements);

        assertNotNull(str);
        assertEquals("A", str);

        elements.clear();
        elements.add("A");
        elements.add("B");
        elements.add("C");
        str = StringUtil.joinToString(elements);

        assertNotNull(str);
        assertEquals("A, B, C", str);

        elements.clear();
        elements.add(" A");
        elements.add("B");
        elements.add("C ");
        str = StringUtil.joinToString(elements);

        assertNotNull(str);
        assertEquals("A, B, C", str);

        elements.clear();
        elements.add(" A ");
        elements.add(" B ");
        elements.add(" C ");
        str = StringUtil.joinToString(elements);

        assertNotNull(str);
        assertEquals("A ,  B ,  C", str);
    }

    /**
     * Test {@link StringUtil#splitToList(String)}.
     */
    @Test
    public void splitToList() {
        List<String> listNull = StringUtil.splitToList(null);

        assertNotNull(listNull);
        assertEquals(0, listNull.size());

        List<String> listEmpty = StringUtil.splitToList("");

        assertNotNull(listEmpty);
        assertEquals(1, listEmpty.size());
        assertEquals("", listEmpty.get(0));

        List<String> listA = StringUtil.splitToList("A");

        assertNotNull(listA);
        assertEquals(1, listA.size());
        assertEquals("A", listA.get(0));

        List<String> listABC1 = StringUtil.splitToList("A,B,C");
        List<String> listABC2 = StringUtil.splitToList(" A, B, C");
        List<String> listABC3 = StringUtil.splitToList("A ,B ,C");
        List<String> listABC4 = StringUtil.splitToList("A , B , C");
        List<String> listABC5 = StringUtil.splitToList(" A , B , C ");

        assertNotNull(listABC1);
        assertNotNull(listABC2);
        assertNotNull(listABC3);
        assertNotNull(listABC4);
        assertNotNull(listABC5);

        assertEquals(3, listABC1.size());
        assertEquals(3, listABC2.size());
        assertEquals(3, listABC3.size());
        assertEquals(3, listABC4.size());
        assertEquals(3, listABC5.size());

        assertEquals("A", listABC1.get(0));
        assertEquals("B", listABC1.get(1));
        assertEquals("C", listABC1.get(2));

        assertEquals("A", listABC2.get(0));
        assertEquals("B", listABC2.get(1));
        assertEquals("C", listABC2.get(2));

        assertEquals("A", listABC3.get(0));
        assertEquals("B", listABC3.get(1));
        assertEquals("C", listABC3.get(2));

        assertEquals("A", listABC4.get(0));
        assertEquals("B", listABC4.get(1));
        assertEquals("C", listABC4.get(2));

        assertEquals("A", listABC5.get(0));
        assertEquals("B", listABC5.get(1));
        assertEquals("C", listABC5.get(2));
    }

}
