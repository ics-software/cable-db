package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link CableStatus}.
 *
 * @author Lars Johansson
 */
public class CableStatusTest {

    /**
     * Tests return of display name for <tt>CableStatus</tt.
     *
     * @see CableStatus#getDisplayName()
     * @see CableStatus#APPROVED
     * @see CableStatus#DELETED
     * @see CableStatus#INSERTED
     * @see CableStatus#ROUTED
     */
    @Test
    public void getDisplayName() {
        assertEquals("APPROVED", CableStatus.APPROVED.getDisplayName());
        assertEquals("DELETED", CableStatus.DELETED.getDisplayName());
        assertEquals("INSERTED", CableStatus.INSERTED.getDisplayName());
        assertEquals("ROUTED", CableStatus.ROUTED.getDisplayName());
    }

    /**
     * Tests conversion of string value into <tt>CableStatus</tt>.
     *
     * @see CableStatus#convertToCableStatus(String)
     * @see CableStatus#APPROVED
     * @see CableStatus#DELETED
     * @see CableStatus#INSERTED
     * @see CableStatus#ROUTED
     */
    @Test
    public void convertToCableStatus() {
        assertEquals(CableStatus.APPROVED, CableStatus.convertToCableStatus("APPROVED"));
        assertEquals(CableStatus.DELETED, CableStatus.convertToCableStatus("DELETED"));
        assertEquals(CableStatus.INSERTED, CableStatus.convertToCableStatus("INSERTED"));
        assertEquals(CableStatus.ROUTED, CableStatus.convertToCableStatus("ROUTED"));
        assertEquals(null, CableStatus.convertToCableStatus(null));
    }

}
