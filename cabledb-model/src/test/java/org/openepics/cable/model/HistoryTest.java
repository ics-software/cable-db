package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;

/**
 * Tests {@link History}.
 *
 * @author Lars Johansson
 */
public class HistoryTest {

    /**
     * Tests create of <tt>History</tt>.
     *
     * @see History#History()
     */
    @Test
    public void createHistory_noParameters() {
        final History history = new History();

        assertNull(history.getTimestamp());
        assertNull(history.getOperation());
        assertNull(history.getUserId());
        assertNull(history.getEntityName());
        assertNull(history.getEntityType());
        assertNull(history.getEntityId());
        assertNull(history.getEntry());
    }

    /**
     * Tests create of <tt>History</tt>.
     *
     * @see History#History(java.util.Date, EntityTypeOperation, String, String, EntityType, Long, String)
     */
    @Test
    public void createHistory() {
      Date timestamp = new Date();
      EntityTypeOperation operation = EntityTypeOperation.DELETE;
      String userId = "userId";
      String entityName = "entityName";
      EntityType entityType = EntityType.CONNECTOR;
      Long entityId = Long.MIN_VALUE;
      String entry = "entry";

      final History history = new History(timestamp, operation, userId, entityName, entityType, entityId, entry);

      assertEquals(timestamp, history.getTimestamp());
      assertEquals(EntityTypeOperation.DELETE, history.getOperation());
      assertEquals(userId, history.getUserId());
      assertEquals(entityName, history.getEntityName());
      assertEquals(EntityType.CONNECTOR, history.getEntityType());
      assertEquals(Long.MIN_VALUE, history.getEntityId().longValue());
      assertEquals(entry, history.getEntry());
    }

}
