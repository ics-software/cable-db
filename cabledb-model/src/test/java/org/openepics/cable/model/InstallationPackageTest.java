package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests {@link InstallationPackage}.
 *
 * @author Lars Johansson
 */
public class InstallationPackageTest {

    /**
     * Tests create of <tt>InstallationPackage</tt>.
     *
     * @see InstallationPackage#InstallationPackage()
     */
    @Test
    public void createInstallationPackage_noParameters() {
        final InstallationPackage installationPackage = new InstallationPackage();

        assertEquals("", installationPackage.getName());
        assertNull(installationPackage.getDescription());
        assertNull(installationPackage.getLocation());
        assertNull(installationPackage.getCreated());
        assertNull(installationPackage.getModified());
        assertNull(installationPackage.getCableCoordinator());
        assertNull(installationPackage.getRoutingDocumentation());
        assertNull(installationPackage.getInstallerCable());
        assertNull(installationPackage.getInstallerConnectorA());
        assertNull(installationPackage.getInstallerConnectorB());
        assertNull(installationPackage.getInstallationPackageLeader());
        assertTrue(installationPackage.isActive());
    }

    /**
     * Tests create of <tt>InstallationPackage</tt>.
     *
     * @see InstallationPackage#InstallationPackage(String, String, Float, String, String)
     */
    @Test
    public void createInstallationPackage() {
      String name = "name";
      String description = "description";
      String location = "location";
      String cableCoordinator = "cableCoordinator";
      String routingDocumentation = "routingDocumentation";
      String installerCable = "installerCable";
      String installerConnectorA = "installerConnectorA";
      String installerConnectorB = "installerConnectorB";
      String installationPackageLeader = "installationPackageLeader";
      boolean active = true;

      final InstallationPackage installationPackage = new InstallationPackage(
              name, description, location, cableCoordinator, routingDocumentation,
              installerCable, installerConnectorA, installerConnectorB, installationPackageLeader,
              active);

      assertEquals(name, installationPackage.getName());
      assertEquals(description, installationPackage.getDescription());
      assertEquals(location, installationPackage.getLocation());
      assertNull(installationPackage.getCreated());
      assertNull(installationPackage.getModified());
      assertEquals(cableCoordinator, installationPackage.getCableCoordinator());
      assertEquals(routingDocumentation, installationPackage.getRoutingDocumentation());
      assertEquals(installerCable, installationPackage.getInstallerCable());
      assertEquals(installerConnectorA, installationPackage.getInstallerConnectorA());
      assertEquals(installerConnectorB, installationPackage.getInstallerConnectorB());
      assertEquals(installationPackageLeader, installationPackage.getInstallationPackageLeader());
      assertTrue(installationPackage.isActive());
    }

}
