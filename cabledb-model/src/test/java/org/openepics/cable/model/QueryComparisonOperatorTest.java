package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests {@link QueryComparisonOperator}.
 *
 * @author Lars Johansson
 */
public class QueryComparisonOperatorTest {

    /**
     * Tests return of operator for <tt>QueryComparisonOperator</tt.
     *
     * @see QueryComparisonOperator#getOperator()
     * @see QueryComparisonOperator#LESS_THAN
     * @see QueryComparisonOperator#LESS_THAN_OR_EQUAL_TO
     * @see QueryComparisonOperator#EQUAL
     * @see QueryComparisonOperator#GREATER_THAN
     * @see QueryComparisonOperator#GREATER_THAN_OR_EQUAL_TO
     * @see QueryComparisonOperator#NOT_EQUAL
     * @see QueryComparisonOperator#STARTS_WITH
     * @see QueryComparisonOperator#ENDS_WITH
     * @see QueryComparisonOperator#CONTAINS
     * @see QueryComparisonOperator#LIKE
     */
    @Test
    public void getOperator() {
        assertEquals("<", QueryComparisonOperator.LESS_THAN.getOperator());
        assertEquals("<=", QueryComparisonOperator.LESS_THAN_OR_EQUAL_TO.getOperator());
        assertEquals("=", QueryComparisonOperator.EQUAL.getOperator());
        assertEquals(">", QueryComparisonOperator.GREATER_THAN.getOperator());
        assertEquals(">=", QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO.getOperator());
        assertEquals("!=", QueryComparisonOperator.NOT_EQUAL.getOperator());
        assertEquals("starts", QueryComparisonOperator.STARTS_WITH.getOperator());
        assertEquals("ends", QueryComparisonOperator.ENDS_WITH.getOperator());
        assertEquals("contains", QueryComparisonOperator.CONTAINS.getOperator());
        assertEquals("like", QueryComparisonOperator.LIKE.getOperator());
    }

    /**
     * Tests if string operator for <tt>QueryComparisonOperator</tt.
     *
     * @see QueryComparisonOperator#isStringComparisonOperator()
     * @see QueryComparisonOperator#LESS_THAN
     * @see QueryComparisonOperator#LESS_THAN_OR_EQUAL_TO
     * @see QueryComparisonOperator#EQUAL
     * @see QueryComparisonOperator#GREATER_THAN
     * @see QueryComparisonOperator#GREATER_THAN_OR_EQUAL_TO
     * @see QueryComparisonOperator#NOT_EQUAL
     * @see QueryComparisonOperator#STARTS_WITH
     * @see QueryComparisonOperator#ENDS_WITH
     * @see QueryComparisonOperator#CONTAINS
     * @see QueryComparisonOperator#LIKE
     */
    @Test
    public void isStringComparisonOperator() {
        assertFalse(QueryComparisonOperator.LESS_THAN.isStringComparisonOperator());
        assertFalse(QueryComparisonOperator.LESS_THAN_OR_EQUAL_TO.isStringComparisonOperator());
        assertFalse(QueryComparisonOperator.EQUAL.isStringComparisonOperator());
        assertFalse(QueryComparisonOperator.GREATER_THAN.isStringComparisonOperator());
        assertFalse(QueryComparisonOperator.GREATER_THAN_OR_EQUAL_TO.isStringComparisonOperator());
        assertFalse(QueryComparisonOperator.NOT_EQUAL.isStringComparisonOperator());
        assertTrue(QueryComparisonOperator.STARTS_WITH.isStringComparisonOperator());
        assertTrue(QueryComparisonOperator.ENDS_WITH.isStringComparisonOperator());
        assertTrue(QueryComparisonOperator.CONTAINS.isStringComparisonOperator());
        assertTrue(QueryComparisonOperator.LIKE.isStringComparisonOperator());
    }


}
