package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Tests {@link BinaryData}.
 *
 * @author Lars Johansson
 */
public class BinaryDataTest {

    /**
     * Tests create of <tt>BinaryData</tt>.
     *
     * @see BinaryData#BinaryData()
     */
    @Test
    public void createBinaryData_noParameters() {
        final BinaryData binaryData = new BinaryData();

        assertNull(binaryData.getContent());
        assertEquals(0, binaryData.getContentLength());
    }

    /**
     * Tests create of <tt>BinaryData</tt>.
     *
     * @see BinaryData#BinaryData(byte[])
     */
    @Test
    public void createBinaryData() {
        byte[] contentToCreate = new byte[] {1, 1, 2, 3, 5, 8, 13, 21};

        final BinaryData binaryData = new BinaryData(contentToCreate);

        byte[] content = binaryData.getContent();
        assertNotNull(content);
        assertEquals(8, content.length);
        assertEquals(1, content[0]);
        assertEquals(1, content[1]);
        assertEquals(2, content[2]);
        assertEquals(3, content[3]);
        assertEquals(5, content[4]);
        assertEquals(8, content[5]);
        assertEquals(13, content[6]);
        assertEquals(21, content[7]);
        assertEquals(8, binaryData.getContentLength());
    }

}
