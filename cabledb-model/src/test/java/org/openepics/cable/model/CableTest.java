package org.openepics.cable.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.openepics.cable.model.util.StringUtil;

/**
 * Tests {@link Cable}.
 *
 * @author Lars Johansson
 */
public class CableTest {

    /**
     * Tests create of <tt>Cable</tt>.
     *
     * @see Cable#Cable(String, String, String, Integer, java.util.List, java.util.Date, java.util.Date)
     */
    @Test
    public void createCable_parameters() {
        String system = "1";
        String subsystem = "6";
        String cableClass = "B";
        Integer seqNumber = 123654;
        String owner1 = "owner1";
        String owner2 = "owner2";
        List<String> owners = new ArrayList<>();
        owners.add(owner1);
        owners.add(owner2);
        Date created = new Date();
        Date modified = new Date();

        final Cable cable = new Cable(system, subsystem, cableClass, seqNumber, owners, created, modified);

        assertEquals(cableClass, cable.getCableClass());
        assertNull(cable.getCableArticle());
        assertNull(cable.getCableType());
        assertNull(cable.getComments());
        assertNull(cable.getContainer());
        assertEquals(created, cable.getCreated());
        assertNull(cable.getEndpointA());
        assertNull(cable.getEndpointB());
        assertNull(cable.getInstallationBy());
        assertEquals(modified, cable.getModified());
        //	system + subsystem + cableClass + String.format("%06d", seqNumber)
        assertEquals(system + subsystem + cableClass + String.valueOf(seqNumber), cable.getName());
        assertNotNull(cable.getOwners());
        assertEquals(2, cable.getOwners().size());
        assertEquals(owner1, cable.getOwners().get(0));
        assertEquals(owner2, cable.getOwners().get(1));
        assertEquals(owner1 + ", " + owner2, cable.getOwnersString());
        assertNull(cable.getRevision());
        assertNull(cable.getInstallationPackage());
        assertEquals(seqNumber, cable.getSeqNumber());
        assertEquals(CableStatus.INSERTED, cable.getStatus());
        assertEquals(subsystem, cable.getSubsystem());
        assertEquals(system, cable.getSystem());
        assertEquals(Cable.Validity.VALID, cable.getValidity());
        assertNull(cable.getFbsTag());
        assertNull(cable.getChessId());
    }

    /**
     * Tests create of <tt>Cable</tt>.
     *
     * @see Cable#Cable(String, String, String, Integer, java.util.List, CableStatus, CableType, String,
     * Endpoint, Endpoint, java.util.Date, java.util.Date, java.util.List, java.util.Date, java.util.Date,
     * GenericArtifact, CableAutoCalculatedLength, Float, String, String)
     */
    @Test
    public void createCable() {
        String system = "1";
        String subsystem = "6";
        String cableClass = "B";
        Integer seqNumber = Integer.MAX_VALUE;
        String owner1 = "owner1";
        String owner2 = "owner2";
        List<String> owners = new ArrayList<>();
        owners.add(owner1);
        owners.add(owner2);
        CableStatus status = CableStatus.INSERTED;

        CableArticle cableArticle = null;
        final String name = "name";
        final InstallationType installationType = InstallationType.OUTDOOR;
        final CableType cableType = new CableType(name, installationType);

        String container = "container";

        final String deviceA = "device";
        final String buildingA = "building";
        final String rackA = "rack";
        final String labelA = StringUtil.getString(Endpoint.MAX_LABEL_SIZE);
        final Endpoint endpointA = new Endpoint(deviceA, buildingA, rackA, null, labelA);

        final String deviceB = "deviceB";
        final String buildingB = "buildingB";
        final String rackB = "rackB";
        final String labelB = StringUtil.getString(Endpoint.MAX_LABEL_SIZE);
        final Endpoint endpointB = new Endpoint(deviceB, buildingB, rackB, null, labelB);

        Date created = new Date();
        Date modified = new Date();
        InstallationPackage installationPackage = new InstallationPackage();
        Date installationBy = new Date();
        String comments = "comments";
        String revision = "revision";
        String electronicDocumentation = "electronicDocumentation";

        final Cable cable =
                new Cable(system, subsystem, cableClass, seqNumber, owners, status, cableArticle, cableType, container,
                        endpointA, endpointB, created, modified, installationPackage, installationBy,
                        comments, revision, electronicDocumentation);

        assertEquals(cableClass, cable.getCableClass());
        assertNull(cable.getCableArticle());
        assertNotNull(cable.getCableType());
        assertEquals(name, cableType.getName());
        assertEquals(InstallationType.OUTDOOR, cableType.getInstallationType());
        assertEquals(comments, cable.getComments());
        assertEquals(container, cable.getContainer());
        assertEquals(created, cable.getCreated());
        assertEquals(created, cable.getCreated());

        assertNotNull(cable.getEndpointA());
        assertEquals(deviceA, cable.getEndpointA().getDevice());
        assertEquals(buildingA, cable.getEndpointA().getBuilding());
        assertEquals(rackA, cable.getEndpointA().getRack());
        assertEquals(labelA, cable.getEndpointA().getLabel());
        assertNotNull(cable.getEndpointB());
        assertEquals(deviceB, cable.getEndpointB().getDevice());
        assertEquals(buildingB, cable.getEndpointB().getBuilding());
        assertEquals(rackB, cable.getEndpointB().getRack());
        assertEquals(labelB, cable.getEndpointB().getLabel());

        assertEquals(installationBy, cable.getInstallationBy());
        assertEquals(modified, cable.getModified());
        //	system + subsystem + cableClass + String.format("%06d", seqNumber)
        assertEquals(system + subsystem + cableClass + String.valueOf(seqNumber), cable.getName());
        assertNotNull(cable.getOwners());
        assertEquals(2, cable.getOwners().size());
        assertEquals(owner1, cable.getOwners().get(0));
        assertEquals(owner2, cable.getOwners().get(1));
        assertEquals(owner1 + ", " + owner2, cable.getOwnersString());
        assertEquals(revision, cable.getRevision());
        assertNotNull(cable.getInstallationPackage());
        assertEquals(seqNumber, cable.getSeqNumber());
        assertEquals(CableStatus.INSERTED, cable.getStatus());
        assertEquals(subsystem, cable.getSubsystem());
        assertEquals(system, cable.getSystem());
        assertEquals(Cable.Validity.VALID, cable.getValidity());
        assertNull(cable.getFbsTag());
        assertNull(cable.getChessId());
    }

}
