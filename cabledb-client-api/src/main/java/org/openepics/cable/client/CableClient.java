/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.client;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.openepics.cable.client.impl.ClosableResponse;
import org.openepics.cable.client.impl.ResponseException;
import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.CableEndpointsElement;
import org.openepics.cable.jaxb.CableResource;

/**
 * This is CableDB service clientdataType parser that is used to get data from server.
 * <p>
 * All class methods are static.
 * </p>
 *
 * @author <a href="mailto:miha.vitorovic@cosylab.com">Miha Vitorovič</a>
 */

class CableClient implements CableResource {

    private static final Logger LOGGER = Logger.getLogger(CableClient.class.getName());

    private static final String ENCODING_SCHEME = "UTF-8";

    private static final String PATH_CABLE = "cable";
    private static final String PATH_CABLE_ENDPOINTS = "cableEndpoints";

    private static final String NO_DATA_FROM_SERVICE = "Couldn't retrieve data from service at ";
    private static final String NO_RETURN_DATA_FROM_SERVICE_POSSIBLE_PROBLEMS_PARAMETERS =
            "The service didn't return any data. Possible problems with parameters.";

    @Nonnull private final CableDBClient client;

    CableClient(CableDBClient client) { this.client = client; }

    /**
     * Requests a {@link List} of all {@link CableElement}s from the REST service.
     *
     * @param field a list of string cable fields to search using regular expression; if <code>null</code> or empty,
     *      all fields are searched
     * @param regExp the regular expression to use to search the cables; if <code>null</code> or empty,
     *      all cables are returned
     *
     * @throws ResponseException if data couldn't be retrieved
     *
     * @return {@link List} of all {@link CableElement}s
     */
    @Override
    public List<CableElement> getAllCables(List<String> field, String regExp) {
        LOGGER.fine("Invoking getAllCables");

        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        if ((regExp != null) && !regExp.isEmpty()) {
            map.add("regexp", regExp);

            if (field != null) {
                for (final String fieldElement : field) {
                    map.add("field", fieldElement);
                }
            }
        }

        final String url = client.buildUrl(PATH_CABLE);

        List<CableElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            throw new ResponseException(NO_DATA_FROM_SERVICE + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException(NO_RETURN_DATA_FROM_SERVICE_POSSIBLE_PROBLEMS_PARAMETERS);
        return responseData;
    }

    /**
     * Requests a {@link List} of all {@link CableEndpointsElement}s from the REST service.
     *
     * @param newerThan if present, then request for elements newer than given date
     * @return {@link List} of all {@link CableEndpointsElement}s
     */
    public List<CableEndpointsElement> getAllCableEndpoints(Date newerThan) {
        final String url = client.buildUrl(PATH_CABLE_ENDPOINTS);
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();

        if (newerThan != null){
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss z");
            map.add("regexp", df.format(newerThan));
            map.add("field", "Modified");
        }

        List<CableEndpointsElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableEndpointsElement>>() {});
        } catch (Exception e) {
            throw new ResponseException(NO_DATA_FROM_SERVICE + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException(NO_RETURN_DATA_FROM_SERVICE_POSSIBLE_PROBLEMS_PARAMETERS);
        return responseData;
    }

    /**
     * Requests particular {@link CableElement} from the REST service.
     *
     * @param name the name of the desired CableElement
     *
     * @throws ResponseException  if data couldn't be retrieved
     *
     * @return {@link CableElement}
     */
    @Override
    public CableElement getCable(String name) {
        LOGGER.fine("Invoking getCable");

        final String url = client.buildUrl(PATH_CABLE, name);
        try (final ClosableResponse response = client.getResponse(url)) {
            return response.readEntity(CableElement.class);
        } catch (Exception e) {
            throw new ResponseException(NO_DATA_FROM_SERVICE + url + ".", e);
        }
    }

    /**
     * @see CableResource#getAllCablesWithESSName(String)
     */
    @Override
    public List<CableElement> getAllCablesWithESSName(String name) {
        // encode parameter, may be regex
        final String url = client.buildUrl(PATH_CABLE, "essName", encode(name));

        List<CableElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            throw new ResponseException(NO_DATA_FROM_SERVICE + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException(NO_RETURN_DATA_FROM_SERVICE_POSSIBLE_PROBLEMS_PARAMETERS);
        return responseData;
    }

    /**
     * @see CableResource#getAllCablesWithFBSTag(String)
     */
    @Override
    public List<CableElement> getAllCablesWithFBSTag(String fbsTag) {
        // encode parameter, may be regex
        final String url = client.buildUrl(PATH_CABLE, "fbsTag", encode(fbsTag));

        List<CableElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            throw new ResponseException(NO_DATA_FROM_SERVICE + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException(NO_RETURN_DATA_FROM_SERVICE_POSSIBLE_PROBLEMS_PARAMETERS);
        return responseData;
    }

    /**
     * @see CableResource#getAllCablesWithLBSTag(String)
     */
    @Override
    public List<CableElement> getAllCablesWithLBSTag(String lbsTag) {
        // encode parameter, may be regex
        final String url = client.buildUrl(PATH_CABLE, "lbsTag", encode(lbsTag));

        List<CableElement> responseData = null;
        try (final ClosableResponse response = client.getResponse(url)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            throw new ResponseException(NO_DATA_FROM_SERVICE + url + ".", e);
        }
        if (responseData == null)
            throw new ResponseException(NO_RETURN_DATA_FROM_SERVICE_POSSIBLE_PROBLEMS_PARAMETERS);
        return responseData;
    }

    /**
     * Encode a <code>String</code> and return the encoded value.
     *
     * @param s the string to encode
     * @return the encoded value
     *
     * @see EncodingUtility#ENCODING_SCHEME
     * @see URLEncoder#encode(String, String)
     */
    private String encode(String s) {
        try {
            return URLEncoder.encode(s, ENCODING_SCHEME);
        } catch (UnsupportedEncodingException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            return s;
        }
    }

}
