-- routing
--     installation package instead of routing
--     additional columns
-- cable
--     additional column to link to installation package
--     update column for link to installation package
-- history
--     update entitytype to keep history for installation package instead of routing


ALTER TABLE routing RENAME TO installation_package;

ALTER TABLE installation_package ADD installer_cable character varying(255);
ALTER TABLE installation_package ADD installer_connector_a character varying(255);
ALTER TABLE installation_package ADD installer_connector_b character varying(255);
ALTER TABLE installation_package ADD installation_package_leader character varying(255);

ALTER TABLE cable ADD installation_package_id bigint NULL;

UPDATE cable c SET installation_package_id = (
    SELECT CASE WHEN EXISTS (SELECT ip.id FROM installation_package ip WHERE ip."name" = c.routingsstring) THEN
                    (SELECT ip.id FROM installation_package ip WHERE ip."name" = c.routingsstring)
                ELSE NULL
           END
);

UPDATE cable SET installation_package_id = (SELECT DISTINCT entityid FROM history WHERE entityname = 'ACC_FEB_TO_G01')
    WHERE routingsstring = 'ACC_FEB_TO_G01' AND installation_package_id IS NULL;

UPDATE history SET entitytype = 'INSTALLATION_PACKAGE' WHERE entitytype = 'ROUTING';
