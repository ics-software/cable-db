-- attachments/artifacts handled by tables genericartifact/binarydata
-- remove
--     references to artifact table
--         2 fk (3rd no foreign key)
--         3 columns
--     artifact table

ALTER TABLE ONLY cabletypemanufacturer
   DROP CONSTRAINT cabletypemanufacturer_datasheet_fkey;
ALTER TABLE ONLY cable
   DROP CONSTRAINT fk_z2t1gfetj3tg6rtsgsharb00d;

ALTER TABLE cabletypemanufacturer DROP COLUMN datasheet_id;
ALTER TABLE cable DROP COLUMN qualityreport_id;
ALTER TABLE connector DROP COLUMN datasheet_id;

DROP TABLE artifact;
