-- add chess id to tables
--     cable
--     endpoint
-- for purpose of improving batch update cable - chess
--     in which information is retrieved from chess
--     and it is made sure that latest and correct
--     information is put into cable
-- example
--     chess_id = ESS-0401755

ALTER TABLE cable ADD COLUMN chess_id character varying(255);

ALTER TABLE endpoint ADD COLUMN device_chess_id character varying(255);
ALTER TABLE endpoint ADD COLUMN rack_chess_id character varying(255);


