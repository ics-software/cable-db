/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.export;

import java.util.List;

import org.openepics.cable.ui.HistoryUI;

/**
 * Concrete implementation of SimpleTableExporter to export log table to Excel.
 */
public class SimpleLogTableExporter extends SimpleTableExporter {

    private List<HistoryUI> selectedEntityHistory;

    /**
     * Construct a new log table exporter.
     *
     * @param selectedEntityHistory list of selected entity history
     */
    public SimpleLogTableExporter(List<HistoryUI> selectedEntityHistory) {
        this.selectedEntityHistory = selectedEntityHistory;

        setIncludeHeaderRow(true);
    }

    @Override
    protected String getTableName() {
        return "Log";
    }

    @Override
    protected String getFileName() {
        return "cdb_log";
    }

    @Override
    protected void addHeaderRow(ExportTable exportTable) {
        exportTable.addHeaderRow("Timestamp", "User", "Entity Type", "Operation", "Entity name", "Entity ID", "Change");
    }

    @Override
    protected void addData(ExportTable exportTable) {
        final List<HistoryUI> exportData = selectedEntityHistory;
        for (final HistoryUI record : exportData) {
            exportTable.addDataRow(record.getTimestamp(), record.getUser(), record.getEntityType(),
                    record.getOperation().toString(), record.getEntityName(), record.getEntityId(), record.getEntry());
        }
    }

    @Override
    protected int getExcelDataStartRow() {
        // log does not have a template
        return 0;
    }

    public int getEntriesToExportSize() {
        return selectedEntityHistory.size();
    }
}
