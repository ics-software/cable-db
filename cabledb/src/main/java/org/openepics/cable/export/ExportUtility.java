/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.export;

/**
 * Utility class for handling export of data to Excel.
 *
 * @author Lars Johansson
 */
public class ExportUtility {

    /**
     * The item value for the PrimeFaces UI selection of the export file format : CSV
     */
    public static final String FILE_FORMAT_CSV = "CSV";
    /**
     * The item value for the PrimeFaces UI selection of the export file format : Excel
     */
    public static final String FILE_FORMAT_EXCEL = "XLSX";
    /**
     * MIME type / content type to use when initiating download of a CSV file
     */
    public static final String MIME_TYPE_CSV = "text/csv";
    /**
     * MIME type / content type to use when initiating download of an Excel file
     */
    public static final String MIME_TYPE_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    /**
     * This class is not to be instantiated.
     */
    private ExportUtility() {
        throw new IllegalStateException("Utility class");
    }

}
