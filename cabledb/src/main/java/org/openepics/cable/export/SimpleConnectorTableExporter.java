/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.export;

import java.util.List;

import org.openepics.cable.services.dl.ConnectorColumn;
import org.openepics.cable.ui.ConnectorUI;
import org.openepics.cable.util.Utility;

/**
 * Concrete implementation of SimpleTableExporter to export connector table to Excel.
 */
public class SimpleConnectorTableExporter extends SimpleTableExporter {

    private List<ConnectorUI> selectedConnectors;
    private List<ConnectorUI> filteredConnectors;

    /**
     * Construct a new connector table exporter.
     *
     * @param selectedConnectors list of selected connectors
     * @param filteredConnectors list of filtered connectors
     */
    public SimpleConnectorTableExporter(List<ConnectorUI> selectedConnectors, List<ConnectorUI> filteredConnectors) {
        this.selectedConnectors = selectedConnectors;
        this.filteredConnectors = filteredConnectors;

        setIncludeHeaderRow(true);
    }

    @Override
    protected String getTableName() {
        return "Connectors";
    }

    @Override
    protected String getFileName() {
        return "cdb_connectors";
    }

    @Override
    protected void addHeaderRow(ExportTable exportTable) {
        exportTable.addHeaderRow(ConnectorColumn.NAME.getColumnLabel(),
                ConnectorColumn.DESCRIPTION.getColumnLabel(),
                ConnectorColumn.TYPE.getColumnLabel(),
                ConnectorColumn.MANUFACTURERS.getColumnLabel(),
                ConnectorColumn.ASSEMBLY_INSTRUCTIONS.getColumnLabel(),
                ConnectorColumn.LINK_TO_DATASHEET.getColumnLabel());
    }

    @Override
    protected void addData(ExportTable exportTable) {
        final List<ConnectorUI> exportData = Utility.isNullOrEmpty(filteredConnectors) ? selectedConnectors
                : filteredConnectors;
        for (final ConnectorUI record : exportData) {
            exportTable.addDataRow(
                    record.getName(),
                    record.getDescription(),
                    record.getType(),
                    record.getConnector().getManufacturersAsString(false),
                    record.getAssemblyInstructions(),
                    record.getLinkToDatasheet());
        }
    }

    @Override
    protected int getExcelDataStartRow() {
        // Connectors do not have a template
        return 0;
    }
}
