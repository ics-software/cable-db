/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.webservice;

import java.io.InputStream;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * This resource provides Swagger UI.
 *
 * @author Anders Harrisson
 */
@Path("/")
public class SwaggerUIResource {

    private static final String WEB_INF_SWAGGER_UI = "/WEB-INF/swagger-ui/";

    @Inject
    ServletContext context;

    /**
     * Response for Swagger UI.
     *
     * @param uri application and request URI information
     * @param request request information for HTTP servlet
     * @return response for Swagger UI
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getSwaggerUI(@Context UriInfo uri, @Context HttpServletRequest request) {
        return Response.ok(context.getResourceAsStream("/WEB-INF/swagger-ui/index.html")).build();
    }

    /**
     * Part of providing Swagger UI, input stream for Swagger UI javascripts.
     *
     * @param uri application and request URI information
     * @return input stream for Swagger UI javascripts
     */
    @GET
    @Path("{filename:.*\\.js}")
    @Produces("application/javascript")
    public InputStream getSwaggerUIJavascripts(@Context UriInfo uri) {
        return context.getResourceAsStream(WEB_INF_SWAGGER_UI + uri.getPath());
    }

    /**
     * Part of providing Swagger UI, input stream for Swagger UI stylesheets.
     *
     * @param uri application and request URI information
     * @return input stream for Swagger UI stylesheets
     */
    @GET
    @Path("{filename:.*\\.css}")
    @Produces("text/css")
    public InputStream getSwaggerUIStylesheets(@Context UriInfo uri) {
        return context.getResourceAsStream(WEB_INF_SWAGGER_UI + uri.getPath());
    }

    /**
     * Part of providing Swagger UI, input stream for Swagger UI images.
     *
     * @param uri application and request URI information
     * @return input stream for Swagger UI images
     */
    @GET
    @Path("{filename:.*\\.png}")
    @Produces("image/png")
    public InputStream getSwaggerUIImages(@Context UriInfo uri) {
        return context.getResourceAsStream(WEB_INF_SWAGGER_UI + uri.getPath());
    }
}
