/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;

import org.openepics.cable.jaxb.ConnectorElement;
import org.openepics.cable.jaxb.ConnectorResource;
import org.openepics.cable.model.Connector;
import org.openepics.cable.services.ConnectorService;

/**
 * This resource provides bulk approved and specific connector data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("connector")
public class ConnectorResourceImpl implements ConnectorResource {

    @Inject
    private ConnectorService connectorService;

    @Override
    public List<ConnectorElement> getAllConnectors() {
        final List<ConnectorElement> connectorElements = new ArrayList<>();

        for (final Connector connector : connectorService.getConnectors()) {
            if (!connector.isActive())
                continue;
            connectorElements.add(getConnectorElement(connector));
        }
        return connectorElements;
    }

    @Override
    public ConnectorElement getConnector(String name) {
        final Connector connector = connectorService.getConnector(name);
        if (connector == null)
            return null;
        if (!connector.isActive())
            return null;

        return getConnectorElement(connector);
    }

    /**
     * Creates an instance of {@link ConnectorElement} from database model object {@link Connector}.
     *
     * @param connector
     *            the database model object
     * @return the JAXB object
     */
    public static ConnectorElement getConnectorElement(Connector connector) {
        ConnectorElement connectorElement = new ConnectorElement();
        connectorElement.setName(connector.getName());
        connectorElement.setConnectorType(connector.getType());
        connectorElement.setDescription(connector.getDescription());
        connectorElement.setActive(connector.isActive());
        return connectorElement;
    }
}
