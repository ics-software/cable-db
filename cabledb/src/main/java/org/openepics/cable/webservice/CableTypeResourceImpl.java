/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;

import org.openepics.cable.jaxb.CableTypeElement;
import org.openepics.cable.jaxb.CableTypeResource;
import org.openepics.cable.model.CableType;
import org.openepics.cable.services.CableTypeService;

/**
 * This resource provides bulk approved and specific cable type data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("cableType")
public class CableTypeResourceImpl implements CableTypeResource {

    @Inject
    private CableTypeService cableTypeService;

    @Override
    public List<CableTypeElement> getAllCableTypes() {
        final List<CableTypeElement> cableTypeElements = new ArrayList<>();

        for (final CableType cableType : cableTypeService.getAllCableTypes()) {
            if (!cableType.isActive())
                continue;
            cableTypeElements.add(getCableTypeElement(cableType));
        }
        return cableTypeElements;
    }

    @Override
    public CableTypeElement getCableType(String name) {
        final CableType cableType = cableTypeService.getCableTypeByName(name);
        if (cableType == null)
            return null;
        if (!cableType.isActive())
            return null;

        return getCableTypeElement(cableType);
    }

    /**
     * Creates an instance of {@link CableTypeElement} from database model object {@link CableType}.
     *
     * @param cableType
     *            the database model object
     * @return the JAXB object
     */
    public static CableTypeElement getCableTypeElement(CableType cableType) {
        CableTypeElement cableTypeElement = new CableTypeElement();
        cableTypeElement.setName(cableType.getName());
        cableTypeElement.setDescription(cableType.getDescription());
        cableTypeElement.setService(cableType.getService());
        cableTypeElement.setVoltage(cableType.getVoltage());
        cableTypeElement.setInsulation(cableType.getInsulation());
        cableTypeElement.setJacket(cableType.getJacket());
        cableTypeElement.setFlammability(cableType.getFlammability());
        cableTypeElement.setInstallationType(String.valueOf(cableType.getInstallationType()));
        cableTypeElement.setTid(cableType.getTid());
        cableTypeElement.setWeight(cableType.getWeight());
        cableTypeElement.setDiameter(cableType.getDiameter());
        cableTypeElement.setManufacturer(cableType.getManufacturersAsString(false));
        cableTypeElement.setComments(cableType.getComments());
        cableTypeElement.setActive(cableType.isActive());
        cableTypeElement.setRevision(cableType.getRevision());
        return cableTypeElement;
    }
}
