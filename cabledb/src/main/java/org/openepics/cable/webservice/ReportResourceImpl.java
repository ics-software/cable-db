/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.webservice;

import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.dto.NotificationDTO;
import org.openepics.cable.jaxb.ReportResource;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableArticle;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.model.ManufacturerStatus;
import org.openepics.cable.services.CableArticleService;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.HistoryService;
import org.openepics.cable.services.InstallationPackageService;
import org.openepics.cable.services.ManufacturerService;
import org.openepics.cable.services.Names;
import org.openepics.cable.services.UserDirectoryService;
import org.openepics.cable.services.dl.CableColumn;
import org.openepics.cable.services.dl.CableSaver;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.ValidityUtil;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsService;
import org.openepics.cable.util.fbs.FbsUtil;

/**
 * This is implementation of {@link ReportResource} interface.
 *
 * @author Lars Johansson
 */
@Stateless
public class ReportResourceImpl implements ReportResource {

    private static final String NBR                      = "#: ";
    private static final String NBR_APPROVED             = "# approved: ";
    private static final String NBR_ACTIVE               = "# active: ";
    private static final String NBR_CABLES               = "# cables: ";
    private static final String NBR_DELETED              = "# deleted: ";
    private static final String NBR_INACTIVE             = "# inactive: ";
    private static final String NBR_IN_USE               = "# in use: ";
    private static final String NBR_IN_USE_OF_ACTIVE     = "# in use (of active): ";
    private static final String NBR_IN_USE_INVALID       = "# in use (invalid): ";
    private static final String NBR_NOT_IN_USE_OF_ACTIVE = "# not in use (of active): ";

    private static final Logger LOGGER = Logger.getLogger(ReportResourceImpl.class.getName());

    private static final String EMPTY       = "";
    private static final String NEWLINE     = "\n";
    private static final String SPACE       = " ";

    private static final String DIVIDER_32  = "--------------------------------";
    private static final String DIVIDER_64  = DIVIDER_32 + DIVIDER_32;
    private static final String DIVIDER_96  = DIVIDER_32 + DIVIDER_32 + DIVIDER_32;
    private static final String DIVIDER_128 = DIVIDER_32 + DIVIDER_32 + DIVIDER_32 + DIVIDER_32;

    private static final String NO_FBS_DATA_AVAILABLE                  = "No FBS data available from CHESS";
    private static final String RESPONSE_HEADER_CONTENT_DISPOSITION    = "Content-Disposition";
    private static final String RESPONSE_HEADER_FILENAME               = "attachment; filename=\"cdb_cables.xlsx\"";

    private static final String NUMBER_OF_FBS_MAPPINGS                 = "# FBS mappings:            {0}";
    private static final String NUMBER_OF_FBS_MAPPINGS_CABLE_NAME      = "# FBS mappings cable name: {0}";
    private static final String NUMBER_OF_FBS_MAPPINGS_ESS_NAME        = "# FBS mappings ess name:   {0}";
    private static final String NUMBER_OF_FBS_MAPPINGS_ID              = "# FBS mappings id:         {0}";
    private static final String NUMBER_OF_FBS_MAPPINGS_TAG             = "# FBS mappings tag:        {0}";
    private static final String NUMBER_OF_CABLES                       = "# cables:                  {0}";
    private static final String NUMBER_OF_NOTIFICATIONS                = "# notifications:           {0}";
    private static final String REPORT_COULD_NOT_BE_MADE_AVAILABLE     = "{0} Report could not be made available. {1}";

    // unofficial reports
    private static final String REPORT_CABLES_NAME_FBS_TAG_DIFFERENT_CHESS_MAPPING =
            "Report for cables for which name has different mapping in CHESS";
    private static final String REPORT_CABLES_NAME_NO_CHESS_MAPPING                =
            "Report for cables for which name is without mapping in CHESS";
    private static final String REPORT_CABLES_ESS_NAME_NO_FBS_TAG                  =
            "Report for cables for which ESS name but not FBS tag is used";
    private static final String REPORT_CABLES_FBS_TAG_NO_CHESS_MAPPING             =
            "Report for cables for which at least one FBS tag is without mapping in CHESS";

    // official reports
    private static final String REPORT_CABLES_FBS_TAG_WITHOUT              =
            "Report for cables without fbs tag";
    private static final String REPORT_CABLES_FBS_TAG_DUPLICATE            =
            "Report for cables with duplicate fbs tag";
    private static final String REPORT_CABLES_FBS_TAG_NOT_EXIST_IN_CHESS   =
            "Report for cables with fbs tag that does not exist in CHESS";
    private static final String REPORT_CABLES_FBS_TAG_NOT_CORRECT_IN_CHESS =
            "Report for cables with fbs tag that are not correct in CHESS";
    private static final String REPORT_CABLES_CABLE_ARTICLE_WITHOUT        =
            "Report for cables without cable article";
    private static final String REPORT_CABLES_CHESS_INVALID                =
            "Report for cables with invalid mapping to/in CHESS";
    private static final String REPORT_CABLES_INVALID                      =
            "Report for cables marked invalid";

    @Inject
    private CableService cableService;
    @Inject
    private CableArticleService cableArticleService;
    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private InstallationPackageService installationPackageService;
    @Inject
    private ConnectorService connectorService;
    @Inject
    private ManufacturerService manufacturerService;
    @Inject
    private HistoryService historyService;
    @Inject
    private FbsService fbsService;
    @Inject
    private Names names;
    @Inject
    private UserDirectoryService userDirectoryService;

    // ----------------------------------------------------------------------------------------------------
    // Unofficial reports
    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesNameFbsTagDifferentChessMapping(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // find fbs elements
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesNameFbsTagDifferentChessMapping(
                        fbsElements, system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_NAME_FBS_TAG_DIFFERENT_CHESS_MAPPING);
    }
    private ReportCablesNotifications getCablesNameFbsTagDifferentChessMapping(final List<FbsElement> fbsElements,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final List<Cable> cablesToExport = new ArrayList<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // find fbs elements and mappings
        Map<String, FbsElement> mappingsCableNameToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsCableNameToFbsElement = FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS, fbsElements.size());
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_CABLE_NAME, getSize(mappingsCableNameToFbsElement));
        }
        if (mappingsCableNameToFbsElement == null) {
            return new ReportCablesNotifications(cablesToExport, notificationDTOs);
        }

        // set up patterns
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        FbsElement fbsElement = null;
        String fbsElementFbsTag = null;

        // find cables with name and fbs tag, with different mapping in CHESS
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem, installationPackage)) {
            // check patterns
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // find mapping for cable name
            fbsElement = mappingsCableNameToFbsElement.get(cable.getName());
            fbsElementFbsTag  = fbsElement != null ? fbsElement.tag : null;
            boolean isSameFbsTag = StringUtils.equalsIgnoreCase(cable.getFbsTag(), fbsElementFbsTag);

            // check if not same fbs tag
            if (!isSameFbsTag) {
                cablesToExport.add(cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                cable, null, cable.getName(),
                                CableColumn.FBS_TAG));
            }
        }

        // sort cables
        sortByName(cablesToExport);

        return new ReportCablesNotifications(cablesToExport, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesNameNoChessMapping(
            String system, String subsystem, String installationPackage, String owners, String revision) {
        // find fbs elements
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesNameNoChessMapping(fbsElements, system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_NAME_NO_CHESS_MAPPING);
    }
    private ReportCablesNotifications getCablesNameNoChessMapping(List<FbsElement> fbsElements,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final List<Cable> cablesToExport = new ArrayList<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // find fbs elements and mappings
        Map<String, FbsElement> mappingsCableNameToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsCableNameToFbsElement = FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS, fbsElements.size());
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_CABLE_NAME, getSize(mappingsCableNameToFbsElement));
        }
        if (mappingsCableNameToFbsElement == null) {
            return new ReportCablesNotifications(cablesToExport, notificationDTOs);
        }

        // set up patterns
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        FbsElement fbsElement = null;

        // find cables with name without mapping in CHESS
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem, installationPackage)) {
            // check patterns
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // find mapping for cable name
            fbsElement = mappingsCableNameToFbsElement.get(cable.getName());

            // check if no mapping for cable name
            if (fbsElement == null) {
                cablesToExport.add(cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                cable, null, cable.getName(),
                                CableColumn.NAME));
            }
        }

        // sort cables
        sortByName(cablesToExport);

        return new ReportCablesNotifications(cablesToExport, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesEssNameNoFbsTag(
            String system, String subsystem, String installationPackage, String owners, String revision) {
        // get cables, notifications
        ReportCablesNotifications report = getCablesEssNameNoFbsTag(system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_ESS_NAME_NO_FBS_TAG);
    }
    private ReportCablesNotifications getCablesEssNameNoFbsTag(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // set up patterns
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        // find cables for which ESS name but not FBS tag is used
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem, installationPackage)) {
            // check patterns
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            if (!StringUtils.isEmpty(cable.getEndpointA().getDevice()) && StringUtils.isEmpty(cable.getEndpointA().getDeviceFbsTag())) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointA(), cable.getName(),
                                CableColumn.FROM_FBS_TAG));
            }
            if (!StringUtils.isEmpty(cable.getEndpointB().getDevice()) && StringUtils.isEmpty(cable.getEndpointB().getDeviceFbsTag())) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointB(), cable.getName(),
                                CableColumn.TO_FBS_TAG));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesFbsTagNoChessMapping(
            String system, String subsystem, String installationPackage, String owners, String revision) {
        // find fbs elements
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesFbsTagNoChessMapping(fbsElements, system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_FBS_TAG_NO_CHESS_MAPPING);
    }
    private ReportCablesNotifications getCablesFbsTagNoChessMapping(List<FbsElement> fbsElements,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // find fbs elements and mappings
        Map<String, FbsElement> mappingsFbsTagToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsFbsTagToFbsElement = FbsUtil.readMappingsTagToFbsElement(fbsElements);
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS, fbsElements.size());
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_TAG, getSize(mappingsFbsTagToFbsElement));
        }
        if (mappingsFbsTagToFbsElement == null) {
            return new ReportCablesNotifications(new ArrayList<>(), notificationDTOs);
        }

        // set up patterns
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        String fbsTag = null;
        String fromFbsTag = null;
        String fromEnclosureFbsTag = null;
        String toFbsTag = null;
        String toEnclosureFbsTag = null;

        // find cables for which at least one FBS tag is without mapping in CHESS
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem, installationPackage)) {
            // check patterns
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // cable
            //     name - fbs tag
            //     from endpoint
            //         ess name - fbs tag
            //         enclosure ess name - enclosure fbs tag
            //     to endpoint
            //         ess name - fbs tag
            //         enclosure ess name - enclosure fbs tag

            fbsTag = cable.getFbsTag();
            fromFbsTag = cable.getEndpointA().getDeviceFbsTag();
            fromEnclosureFbsTag = cable.getEndpointA().getRackFbsTag();
            toFbsTag = cable.getEndpointB().getDeviceFbsTag();
            toEnclosureFbsTag = cable.getEndpointB().getRackFbsTag();

            // catch notifications
            //     one hit per notification enough to catch all cables
            //     one hit per notification to catch not only cable but all notifications
            if (!StringUtils.isEmpty(fbsTag)
                    && mappingsFbsTagToFbsElement.get(fbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                cable, null, cable.getName(),
                                CableColumn.FBS_TAG));
            }
            if (!StringUtils.isEmpty(fromFbsTag)
                    && mappingsFbsTagToFbsElement.get(fromFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointA(), cable.getName(),
                                CableColumn.FROM_FBS_TAG));
            }
            if (!StringUtils.isEmpty(fromEnclosureFbsTag)
                    && mappingsFbsTagToFbsElement.get(fromEnclosureFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointA(), cable.getName(),
                                CableColumn.FROM_ENCLOSURE_FBS_TAG));
            }
            if (!StringUtils.isEmpty(toFbsTag)
                    && mappingsFbsTagToFbsElement.get(toFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointB(), cable.getName(),
                                CableColumn.TO_FBS_TAG));
            }
            if (!StringUtils.isEmpty(toEnclosureFbsTag)
                    && mappingsFbsTagToFbsElement.get(toEnclosureFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointB(), cable.getName(),
                        CableColumn.TO_ENCLOSURE_FBS_TAG));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesChessInvalid(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // note
        //     exact match
        //         system, subsystem
        //     search (contains, pattern match)
        //         installationpackage, owners, revision

        // find fbs elements
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesChessInvalid(fbsElements, system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_CHESS_INVALID);
    }
    private ReportCablesNotifications getCablesChessInvalid(List<FbsElement> fbsElements,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }
        String installationPackageName = null;

        // find fbs elements and mappings
        Map<String, FbsElement> mappingsIdToFbsElement = null;
        Map<String, FbsElement> mappingsCableNameToFbsElement = null;
        Map<String, FbsElement> mappingsEssNameToFbsElement = null;
        Map<String, FbsElement> mappingsFbsTagToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsIdToFbsElement = FbsUtil.readMappingsIdToFbsElement(fbsElements);
            mappingsCableNameToFbsElement = FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            mappingsEssNameToFbsElement = FbsUtil.readMappingsEssNameToFbsElement(fbsElements);
            mappingsFbsTagToFbsElement = FbsUtil.readMappingsTagToFbsElement(fbsElements);
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS, fbsElements.size());
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_CABLE_NAME, getSize(mappingsCableNameToFbsElement));
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_ESS_NAME, getSize(mappingsEssNameToFbsElement));
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_ID, getSize(mappingsIdToFbsElement));
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_TAG, getSize(mappingsFbsTagToFbsElement));
        }
        if (mappingsIdToFbsElement == null || mappingsCableNameToFbsElement == null || mappingsEssNameToFbsElement == null || mappingsFbsTagToFbsElement == null) {
            LOGGER.log(Level.INFO, NO_FBS_DATA_AVAILABLE);
            return new ReportCablesNotifications(new ArrayList<>(), notificationDTOs);
        }


        // find cables with invalid CHESS mapping
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // cable
            //     name
            //         1 name - fbs tag - chess id
            //     endpoint a
            //         2 ess name - fbs tag - chess id
            //         3 enclosure ess name - enclosure fbs tag - chess id
            //     endpoint b
            //         4 ess name - fbs tag - chess id
            //         5 enclosure ess name - enclosure fbs tag - chess id

            // mystery check
            // 1
            //     ok  - fbs tag but not chess id
            //     nok - chess id but not fbs tag
            //           chess id but not itip chess id
            if (
                        (	!StringUtils.isEmpty(cable.getChessId()) && StringUtils.isEmpty(cable.getFbsTag())	)
                    ||	(	!StringUtils.isEmpty(cable.getChessId()) && mappingsIdToFbsElement.get(cable.getChessId()) == null   )
                ) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        cable, null, cable.getName(),
                        CableColumn.FBS_TAG));
            }
            // 2
            //     nok - chess id, not ess name, not fbs tag
            //           chess id, not itip chess id
            if (
                        (	!StringUtils.isEmpty(cable.getEndpointA().getDeviceChessId()) && StringUtils.isEmpty(cable.getEndpointA().getDevice()) && StringUtils.isEmpty(cable.getEndpointA().getDeviceFbsTag())	)
                    || 	(	!StringUtils.isEmpty(cable.getEndpointA().getDeviceChessId()) && mappingsIdToFbsElement.get(cable.getEndpointA().getDeviceChessId()) == null	)
                    ) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointA(), cable.getName(),
                        CableColumn.FROM_ESS_NAME));
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointA(), cable.getName(),
                        CableColumn.FROM_FBS_TAG));
            }
            // 3
            //     nok - chess id, not enclosure ess name, not enclosure fbs tag
            //           chess id, not itip chess id
            if (
                        (	!StringUtils.isEmpty(cable.getEndpointA().getRackChessId()) && StringUtils.isEmpty(cable.getEndpointA().getRack()) && StringUtils.isEmpty(cable.getEndpointA().getRackFbsTag())	)
                    || 	(	!StringUtils.isEmpty(cable.getEndpointA().getRackChessId()) && mappingsIdToFbsElement.get(cable.getEndpointA().getRackChessId()) == null	)
                    ) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointA(), cable.getName(),
                        CableColumn.FROM_ENCLOSURE_ESS_NAME));
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointA(), cable.getName(),
                        CableColumn.FROM_ENCLOSURE_FBS_TAG));
            }
            // 4
            //     nok - chess id, not ess name, not fbs tag
            //           chess id, not itip chess id
            if (
                        (	!StringUtils.isEmpty(cable.getEndpointB().getDeviceChessId()) && StringUtils.isEmpty(cable.getEndpointB().getDevice()) && StringUtils.isEmpty(cable.getEndpointB().getDeviceFbsTag())	)
                    || 	(	!StringUtils.isEmpty(cable.getEndpointB().getDeviceChessId()) && mappingsIdToFbsElement.get(cable.getEndpointB().getDeviceChessId()) == null	)
                    ) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointB(), cable.getName(),
                        CableColumn.TO_ESS_NAME));
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointB(), cable.getName(),
                        CableColumn.TO_FBS_TAG));
            }
            // 5
            //     nok - chess id, not enclosure ess name, not enclosure fbs tag
            //           chess id, not itip chess id
            if (
                        (	!StringUtils.isEmpty(cable.getEndpointB().getRackChessId()) && StringUtils.isEmpty(cable.getEndpointB().getRack()) && StringUtils.isEmpty(cable.getEndpointB().getRackFbsTag())	)
                    || 	(	!StringUtils.isEmpty(cable.getEndpointB().getRackChessId()) && mappingsIdToFbsElement.get(cable.getEndpointB().getRackChessId()) == null	)
                    ) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointB(), cable.getName(),
                        CableColumn.TO_ENCLOSURE_ESS_NAME));
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointB(), cable.getName(),
                        CableColumn.TO_ENCLOSURE_FBS_TAG));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------
    // Official reports
    // ----------------------------------------------------------------------------------------------------

    @Override
    public String reportAbout() {
        // report metrics about cable db
        //     chess/itip
        //     cables, cable articles, cable types, installation packages, connectors, manufacturers, logs
        //     # entries, in use, not in use, valid, invalid

        // find fbs elements
        // metrics chess (before cable)
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();
        Map<String, FbsElement> mappingsCableNameToFbsElement = null;
        Map<String, FbsElement> mappingsEssNameToFbsElement = null;
        Map<String, FbsElement> mappingsIdToFbsElement = null;
        Map<String, FbsElement> mappingsTagToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsCableNameToFbsElement = FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            mappingsEssNameToFbsElement = FbsUtil.readMappingsEssNameToFbsElement(fbsElements);
            mappingsIdToFbsElement = FbsUtil.readMappingsIdToFbsElement(fbsElements);
            mappingsTagToFbsElement = FbsUtil.readMappingsTagToFbsElement(fbsElements);
        }

        // prepare metrics
        long nbrcables = 0;
        long nbrcablesnotdeleted = 0;
        long nbrcableshasproblem = 0;
        long nbrcablesinvalid = 0;

        Map<String, Long> statusNbrCables = new TreeMap<>();
        Map<String, Long> systemNbrCables = new TreeMap<>();
        Map<String, Long> systemSubsystemNbrCables = new TreeMap<>();
        Map<String, Long> systemSubsystemClazzNbrCables = new TreeMap<>();
        Map<String, Long> clazzNbrCables = new TreeMap<>();
        Map<String, Long> articleNbrCables = new TreeMap<>();
        Map<String, Long> typeNbrCables = new TreeMap<>();
        Map<String, Long> installationPackageNbrCables = new TreeMap<>();
        Map<String, Long> connectorNbrCables = new TreeMap<>();
        Map<String, Long> ownerNbrCables = new TreeMap<>();

        Set<String> cablesNok = new TreeSet<>();
        Map<String, Long> articleNbrCablesNok = new TreeMap<>();
        Map<String, Long> typeNbrCablesNok = new TreeMap<>();
        Map<String, Long> installationPackageNbrCablesNok = new TreeMap<>();
        Map<String, Long> connectorNbrCablesNok = new TreeMap<>();

        for (CableStatus cableStatus : CableStatus.values()) {
            statusNbrCables.put(cableStatus.toString(), 0l);
        }

        int maxlengtharticle = 0;
        int maxlengthtype = 0;
        int maxlengthinstallationpackage = 0;
        int maxlengthconnector = 0;
        int maxlengthowner = 0;

        long nbrcablearticles = 0;
        long nbrcablearticlesactive = 0;
        long nbrcablearticlesinactive = 0;
        long nbrcabletypes = 0;
        long nbrcabletypesactive = 0;
        long nbrcabletypesinactive = 0;
        long nbrinstallationpackages = 0;
        long nbrinstallationpackagesactive = 0;
        long nbrinstallationpackagesinactive = 0;
        long nbrconnectors = 0;
        long nbrconnectorsactive = 0;
        long nbrconnectorsinactive = 0;
        long nbrmanufacturers = 0;
        long nbrmanufacturersapproved = 0;
        long nbrmanufacturersdeleted = 0;
        long nbrlogs = 0;

        // metrics (before cable) cable articles, cable types, installation packages, connectors, manufacturers, logs
        for (CableArticle cableArticle : cableArticleService.getCableArticles()) {
            nbrcablearticles++;
            if (cableArticle.isActive()) {
                articleNbrCables.putIfAbsent(cableArticle.getName(), 0l);
                maxlengtharticle = checkGetMaxLength(cableArticle.getName(), maxlengtharticle);

                nbrcablearticlesactive++;
            } else {
                nbrcablearticlesinactive++;
            }
        }
        for (CableType cableType : cableTypeService.getAllCableTypes()) {
            nbrcabletypes++;
            if (cableType.isActive()) {
                typeNbrCables.putIfAbsent(cableType.getName(), 0l);
                maxlengthtype = checkGetMaxLength(cableType.getName(), maxlengthtype);

                nbrcabletypesactive++;
            } else {
                nbrcabletypesinactive++;
            }
        }
        for (InstallationPackage ip : installationPackageService.getInstallationPackages()) {
            nbrinstallationpackages++;
            if (ip.isActive()) {
                installationPackageNbrCables.putIfAbsent(ip.getName(), 0l);
                maxlengthinstallationpackage = checkGetMaxLength(ip.getName(), maxlengthinstallationpackage);

                nbrinstallationpackagesactive++;
            } else {
                nbrinstallationpackagesinactive++;
            }
        }
        for (Connector connector : connectorService.getConnectors()) {
            nbrconnectors++;
            if (connector.isActive()) {
                connectorNbrCables.putIfAbsent(connector.getName(), 0l);
                maxlengthconnector = checkGetMaxLength(connector.getName(), maxlengthconnector);

                nbrconnectorsactive++;
            } else {
                nbrconnectorsinactive++;
            }
        }

        for (Manufacturer manufacturer : manufacturerService.getManufacturers()) {
            nbrmanufacturers++;
            if (ManufacturerStatus.APPROVED.equals(manufacturer.getStatus())) {
                nbrmanufacturersapproved++;
            } else {
                nbrmanufacturersdeleted++;
            }
        }
        nbrlogs = historyService.getRowCount(new HashMap<String, Object>());

        // metrics cables
        String system;
        String systemSubsystem;
        String systemSubsystemClazz;
        String clazz;
        String article;
        String type;
        String installationPackage;
        String connectorA;
        String connectorB;
        String owner;

        Set<String> essnames = new TreeSet<>();
        Set<String> cablefbstags  = new TreeSet<>();
        Set<String> fbstags  = new TreeSet<>();
        Set<String> lbstags  = new TreeSet<>();

        String essname;
        String fbstag;
        String lbstag;

        // usernames (users, administrators) to be crossmatched with owners to find out owners that are not in usernames
        // cable owners is comma-separated list of usernames - e,g, name, name2
        // split and trim - comma + space
        Set<String> user_usernames = userDirectoryService.getAllUserUsernames();
        Set<String> administrator_usernames = userDirectoryService.getAllAdministratorUsernames();
        Set<String> usernames = new TreeSet<>();
        Set<String> owners = new TreeSet<>();
        for (String user : user_usernames) {
            usernames.add(user);
        }
        for (String administrator : administrator_usernames) {
            usernames.add(administrator);
        }

        // metrics cable
        for (Cable cable : cableService.getCables()) {
            // cables
            nbrcables++;
            addOne(cable.getStatus().toString(), statusNbrCables);
            if (CableStatus.DELETED.equals(cable.getStatus())) {
                continue;
            }
            nbrcablesnotdeleted++;
            if (cable.isHasProblem()) {
                nbrcableshasproblem++;
            }
            if (!cable.isValid()) {
                nbrcablesinvalid++;
                cablesNok.add(cable.getName());
            }

            system = cable.getName().substring(0, 1);
            systemSubsystem = cable.getName().substring(0, 2);
            systemSubsystemClazz = cable.getName().substring(0, 3);
            clazz = cable.getName().substring(2, 3);
            article = cable.getCableArticle() != null ? cable.getCableArticle().getName() : null;
            type = cable.getCableType() != null ? cable.getCableType().getName() : null;
            installationPackage = cable.getInstallationPackage() != null ? cable.getInstallationPackage().getName() : null;
            connectorA = cable.getEndpointA().getConnector() != null ? cable.getEndpointA().getConnector().getName() : null;
            connectorB = cable.getEndpointB().getConnector() != null ? cable.getEndpointB().getConnector().getName() : null;
            owner = cable.getOwnersString();

            // cable owners
            for (String str : owner.split(",")) {
                owners.add(str.trim());
            }

            // note nok maps to keep track of nok entries
            addOne(system, systemNbrCables);
            addOne(systemSubsystem, systemSubsystemNbrCables);
            addOne(systemSubsystemClazz, systemSubsystemClazzNbrCables);
            addOne(clazz, clazzNbrCables);
            addOne(article, articleNbrCables, articleNbrCablesNok);
            addOne(type, typeNbrCables, typeNbrCablesNok);
            addOne(installationPackage, installationPackageNbrCables, installationPackageNbrCablesNok);
            addOne(connectorA, connectorNbrCables, connectorNbrCablesNok);
            addOne(connectorB, connectorNbrCables, connectorNbrCablesNok);
            addOne(owner, ownerNbrCables);
            // see above for maxlengtharticle, maxlengthtype, maxlengthinstallationpackage, maxlengthconnector
            maxlengthowner = checkGetMaxLength(owner, maxlengthowner);

            // ess names
            essname = cable.getEndpointA().getDevice();
            if (!StringUtils.isEmpty(essname)) {
                essnames.add(essname);
            }
            essname = cable.getEndpointA().getRack();
            if (!StringUtils.isEmpty(essname)) {
                essnames.add(essname);
            }
            essname = cable.getEndpointB().getDevice();
            if (!StringUtils.isEmpty(essname)) {
                essnames.add(essname);
            }
            essname = cable.getEndpointB().getRack();
            if (!StringUtils.isEmpty(essname)) {
                essnames.add(essname);
            }

            // cable fbs tags, fbs tags
            fbstag = cable.getFbsTag();
            if (!StringUtils.isEmpty(fbstag)) {
                cablefbstags.add(fbstag);
            }
            if (!StringUtils.isEmpty(fbstag)) {
                fbstags.add(fbstag);
            }
            fbstag = cable.getEndpointA().getDeviceFbsTag();
            if (!StringUtils.isEmpty(fbstag)) {
                fbstags.add(fbstag);
            }
            fbstag = cable.getEndpointA().getRackFbsTag();
            if (!StringUtils.isEmpty(fbstag)) {
                fbstags.add(fbstag);
            }
            fbstag = cable.getEndpointB().getDeviceFbsTag();
            if (!StringUtils.isEmpty(fbstag)) {
                fbstags.add(fbstag);
            }
            fbstag = cable.getEndpointB().getRackFbsTag();
            if (!StringUtils.isEmpty(fbstag)) {
                fbstags.add(fbstag);
            }

            // lbs tags
            lbstag = cable.getEndpointA().getBuilding();
            if (!StringUtils.isEmpty(lbstag)) {
                lbstags.add(lbstag);
            }
            lbstag = cable.getEndpointB().getBuilding();
            if (!StringUtils.isEmpty(lbstag)) {
                lbstags.add(lbstag);
            }
        }

        // prepare report
        maxlengtharticle += 8;
        maxlengthtype += 8;
        maxlengthinstallationpackage += 8;
        maxlengthconnector += 8;
        maxlengthowner += 8;
        int spaceUntilSizeOverview = 35;
        int spaceUntilSizeUsageOverview = 16;

        // find out diff between usernames and owners
        String nonexisting_owners = "";
        for (String o : owners) {
            if (!usernames.contains(o)) {
                nonexisting_owners += ", " + o;
            }
        }
        if (nonexisting_owners.startsWith(",")) {
            nonexisting_owners = nonexisting_owners.substring(1).trim();
        }

        // metrics chess and cable db
        String metricschessfbselements   = NBR + fbsElements.size();
        String metricschesscablename     = NBR + (mappingsCableNameToFbsElement != null ? mappingsCableNameToFbsElement.size() : 0);
        String metricschessessname       = NBR + (mappingsEssNameToFbsElement != null ? mappingsEssNameToFbsElement.size() : 0);
        String metricschessid            = NBR + (mappingsIdToFbsElement != null ? mappingsIdToFbsElement.size() : 0);
        String metricschesstag           = NBR + (mappingsTagToFbsElement != null ? mappingsTagToFbsElement.size() : 0);
        String metricschesscableessnames = NBR_IN_USE + essnames.size();
        String metricschesscablefbstags  = NBR_IN_USE + fbstags.size()  + " of which # cable fbs tags: " + cablefbstags.size();
        String metricschesscablelbstags  = NBR_IN_USE + lbstags.size();

        // metrics cable db overview
        String metricsoverviewcables               = NBR + addSpaceUntilSize(nbrcables, spaceUntilSizeOverview) + "# not deleted: " + nbrcablesnotdeleted + " of which has problem: " + nbrcableshasproblem + " invalid: " + nbrcablesinvalid;
        StringBuilder metricsoverviewcablesstatus = new StringBuilder();
        metricsoverviewcablesstatus.append("   ").append(addSpaceUntilSize(SPACE, spaceUntilSizeOverview));
        for (Entry<String, Long> entry : statusNbrCables.entrySet()) {
            metricsoverviewcablesstatus.append("# " + entry.getKey() + ": " + entry.getValue() + SPACE);
        }
        String metricsoverviewcablearticles        = NBR + addSpaceUntilSize(nbrcablearticles, spaceUntilSizeOverview) + NBR_ACTIVE + nbrcablearticlesactive + SPACE + NBR_INACTIVE + nbrcablearticlesinactive;
        String metricsoverviewcabletypes           = NBR + addSpaceUntilSize(nbrcabletypes, spaceUntilSizeOverview) + NBR_ACTIVE + nbrcabletypesactive + SPACE + NBR_INACTIVE + nbrcabletypesinactive;
        String metricsoverviewinstallationpackages = NBR + addSpaceUntilSize(nbrinstallationpackages, spaceUntilSizeOverview) + NBR_ACTIVE + nbrinstallationpackagesactive + SPACE + NBR_INACTIVE + nbrinstallationpackagesinactive;
        String metricsoverviewconnectors           = NBR + addSpaceUntilSize(nbrconnectors, spaceUntilSizeOverview) + NBR_ACTIVE + nbrconnectorsactive + SPACE + NBR_INACTIVE + nbrconnectorsinactive;
        String metricsoverviewmanufacturers        = NBR + addSpaceUntilSize(nbrmanufacturers, spaceUntilSizeOverview) + NBR_APPROVED + nbrmanufacturersapproved + SPACE + NBR_DELETED + nbrmanufacturersdeleted;
        String metricsoverviewlogs                 = NBR + nbrlogs;

        // metrics cable db usage overview
        String metricsusagecable                   = NBR_IN_USE + nbrcablesnotdeleted + " of which # in use (invalid): " + cablesNok.size();
        String metricsusagesystem                  = NBR_IN_USE + systemNbrCables.size();
        String metricsusagesystemsubsystem         = NBR_IN_USE + systemSubsystemNbrCables.size();
        String metricsusagesystemsubsystemclass    = NBR_IN_USE + systemSubsystemClazzNbrCables.size();
        String metricsusageclass                   = NBR_IN_USE + clazzNbrCables.size();
        String metricsusagecablearticle            = NBR_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueNonZero(articleNbrCables)), spaceUntilSizeUsageOverview)             + NBR_NOT_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueZero(articleNbrCables)), spaceUntilSizeUsageOverview)             + NBR_IN_USE_INVALID + articleNbrCablesNok.size();
        String metricsusagecabletype               = NBR_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueNonZero(typeNbrCables)), spaceUntilSizeUsageOverview)                + NBR_NOT_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueZero(typeNbrCables)), spaceUntilSizeUsageOverview)                + NBR_IN_USE_INVALID + typeNbrCablesNok.size();
        String metricsusageinstallationpackage     = NBR_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueNonZero(installationPackageNbrCables)), spaceUntilSizeUsageOverview) + NBR_NOT_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueZero(installationPackageNbrCables)), spaceUntilSizeUsageOverview) + NBR_IN_USE_INVALID + installationPackageNbrCablesNok.size();
        String metricsusageconnector               = NBR_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueNonZero(connectorNbrCables)), spaceUntilSizeUsageOverview)           + NBR_NOT_IN_USE_OF_ACTIVE + addSpaceUntilSize((getNumberWithValueZero(connectorNbrCables)), spaceUntilSizeUsageOverview)           + NBR_IN_USE_INVALID + connectorNbrCablesNok.size();
        String metricsusageowner                   = NBR_IN_USE + ownerNbrCables.size() + " of which not cable users: " + nonexisting_owners;

        // report header
        StringBuilder sb = new StringBuilder();
        sb.append("About Cable DB").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append(DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date())).append(NEWLINE)
        .append("Note # means number of, in use means active valid").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append("Metrics for CHESS and Cable DB").append(NEWLINE)
        .append("    1) # entries - available, usage - ess names, fbs tags, lbs tags").append(NEWLINE)
        .append("Metrics for Cable DB").append(NEWLINE)
        .append("    1) # entries - overview         - cables, cable articles, cable types, installation packages, connectors, manufacturers, logs").append(NEWLINE)
        .append("    2) # entries - usage, overview  - cable, system, system subsystem, system subsystem class, class, owner, cable article, cable type, installation package, connector").append(NEWLINE)
        .append("    3) # entries - usage, details   - in use     (of active) - system, system subsystem, system subsystem class, class, owner, cable article, cable type, installation package, connector").append(NEWLINE)
        .append("    4) # entries - usage, details   - not in use (of active) - cable article, cable type, installation package, connector").append(NEWLINE)
        .append("    5) # entries - usage, details   - in use     (invalid)   - cable, cable article, cable type, installation package, connector").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE);

        // report chess and cabledb
        sb.append("Metrics for CHESS and Cable DB").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append("1) # entries - available, usage - ess names, fbs tags, lbs tags").append(NEWLINE)
        .append(DIVIDER_96).append(NEWLINE)
        .append("CHESS").append(NEWLINE)
        .append(DIVIDER_64).append(NEWLINE)
        .append("fbs elements                     " + metricschessfbselements).append(NEWLINE)
        .append("cable name                       " + metricschesscablename).append(NEWLINE)
        .append("ess name                         " + metricschessessname).append(NEWLINE)
        .append("id                               " + metricschessid).append(NEWLINE)
        .append("tag                              " + metricschesstag).append(NEWLINE)
        .append(DIVIDER_64).append(NEWLINE)
        .append("Cable DB").append(NEWLINE)
        .append(DIVIDER_64).append(NEWLINE)
        .append("ess names                        " + metricschesscableessnames).append(NEWLINE)
        .append("fbs tags                         " + metricschesscablefbstags).append(NEWLINE)
        .append("lbs tags                         " + metricschesscablelbstags).append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE);

        // report cabledb overview
        sb.append("Metrics for Cable DB").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append("1) # entries - overview - cables, cable articles, cable types, installation packages, connectors, manufacturers, logs").append(NEWLINE)
        .append(DIVIDER_96).append(NEWLINE)
        .append("cables                           " + metricsoverviewcables).append(NEWLINE)
        .append("                                 " + metricsoverviewcablesstatus).append(NEWLINE)
        .append("cable articles                   " + metricsoverviewcablearticles).append(NEWLINE)
        .append("cable types                      " + metricsoverviewcabletypes).append(NEWLINE)
        .append("installation packages            " + metricsoverviewinstallationpackages).append(NEWLINE)
        .append("connectors                       " + metricsoverviewconnectors).append(NEWLINE)
        .append("manufacturers                    " + metricsoverviewmanufacturers).append(NEWLINE)
        .append("logs                             " + metricsoverviewlogs).append(NEWLINE)
        .append(DIVIDER_96).append(NEWLINE);

        // report cabledb usage overview
        sb.append("2) # entries - usage, overview - cable, system, system subsystem, system subsystem class, class, owner, cable article, cable type, installation package, connector").append(NEWLINE)
        .append(DIVIDER_96).append(NEWLINE)
        .append("cable                            " + metricsusagecable).append(NEWLINE)
        .append("system                           " + metricsusagesystem).append(NEWLINE)
        .append("system subsystem                 " + metricsusagesystemsubsystem).append(NEWLINE)
        .append("system subsystem class           " + metricsusagesystemsubsystemclass).append(NEWLINE)
        .append("class                            " + metricsusageclass).append(NEWLINE)
        .append("owner                            " + metricsusageowner).append(NEWLINE)
        .append("cable article                    " + metricsusagecablearticle).append(NEWLINE)
        .append("cable type                       " + metricsusagecabletype).append(NEWLINE)
        .append("installation package             " + metricsusageinstallationpackage).append(NEWLINE)
        .append("connector                        " + metricsusageconnector).append(NEWLINE)
        .append(DIVIDER_96).append(NEWLINE);

        String systemLabel = null;
        String subsystemLabel = null;
        String clazzLabel = null;

        // report cabledb usage details in use (of active)
        // report cabledb usage details not in use (of active)
        sb.append("3) # entries - usage, details - in use     (of active) - system, system subsystem, system subsystem class, class, owner, cable article, cable type, installation package, connector").append(NEWLINE);
        sb.append("4) # entries - usage, details - not in use (of active) - cable article, cable type, installation package, connector").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("system").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : systemNbrCables.entrySet()) {
            systemLabel = CableNumbering.getSystemLabel(entry.getKey());
            sb.append(addSpaceUntilSize(entry.getKey(), 33)
                    + addSpaceUntilSize(NBR_CABLES + entry.getValue(), 38)
                    + systemLabel).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("system subsystem").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : systemSubsystemNbrCables.entrySet()) {
            systemLabel = CableNumbering.getSystemLabel(entry.getKey().substring(0, 1));
            subsystemLabel = CableNumbering.getSubsystemLabel(entry.getKey().substring(0, 1), entry.getKey().substring(1, 2));
            sb.append(addSpaceUntilSize(entry.getKey(), 33)
                    + addSpaceUntilSize(NBR_CABLES + entry.getValue(), 38)
                    + addSpaceUntilSize(systemLabel, 60)
                    + subsystemLabel).append(NEWLINE);

        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("system subsystem class").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : systemSubsystemClazzNbrCables.entrySet()) {
            systemLabel = CableNumbering.getSystemLabel(entry.getKey().substring(0, 1));
            subsystemLabel = CableNumbering.getSubsystemLabel(entry.getKey().substring(0, 1), entry.getKey().substring(1, 2));
            clazzLabel = CableNumbering.getClassLabel(entry.getKey().substring(2,  3));
            sb.append(addSpaceUntilSize(entry.getKey(), 33)
                    + addSpaceUntilSize(NBR_CABLES + entry.getValue(), 38)
                    + addSpaceUntilSize(systemLabel, 60)
                    + addSpaceUntilSize(subsystemLabel, 48)
                    + clazzLabel).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("class").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : clazzNbrCables.entrySet()) {
            clazzLabel = CableNumbering.getClassLabel(entry.getKey().substring(0,  1));
            sb.append(addSpaceUntilSize(entry.getKey(), 33)
                    + addSpaceUntilSize(NBR_CABLES + entry.getValue(), 38)
                    + clazzLabel).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("owner").append("                                  ").append("* one or more owners are not cable users").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : ownerNbrCables.entrySet()) {
            String key = entry.getKey();
            for (String str : key.split(",")) {
                if (!usernames.contains(str.trim())) {
                    key = key + " *";
                    break;
                }
            }
            sb.append(addSpaceUntilSize(key, maxlengthowner) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("article").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : articleNbrCables.entrySet()) {
            if (entry.getValue() == 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengtharticle) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : articleNbrCables.entrySet()) {
            if (entry.getValue() > 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengtharticle) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("type").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : typeNbrCables.entrySet()) {
            if (entry.getValue() == 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthtype) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : typeNbrCables.entrySet()) {
            if (entry.getValue() > 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthtype) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("installation package").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : installationPackageNbrCables.entrySet()) {
            if (entry.getValue() == 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthinstallationpackage) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : installationPackageNbrCables.entrySet()) {
            if (entry.getValue() > 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthinstallationpackage) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("connector").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : connectorNbrCables.entrySet()) {
            if (entry.getValue() == 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthconnector) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : connectorNbrCables.entrySet()) {
            if (entry.getValue() > 0) {
                continue;
            }
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthconnector) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_96).append(NEWLINE);

        // report cabledb usage details in use (invalid)
        sb.append("5) # entries - usage, details - in use (invalid) - cable, cable article, cable type, installation package, connector").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("cable").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (String cableName : cablesNok) {
            sb.append(cableName).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("article").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : articleNbrCablesNok.entrySet()) {
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengtharticle) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("type").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : typeNbrCablesNok.entrySet()) {
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthtype) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("installation package").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : installationPackageNbrCablesNok.entrySet()) {
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthinstallationpackage) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_64).append(NEWLINE);
        sb.append("connector").append(NEWLINE);
        sb.append(DIVIDER_64).append(NEWLINE);
        for (Entry<String, Long> entry : connectorNbrCablesNok.entrySet()) {
            sb.append(addSpaceUntilSize(entry.getKey(), maxlengthconnector) + NBR_CABLES + entry.getValue()).append(NEWLINE);
        }
        sb.append(DIVIDER_128).append(NEWLINE);

        return sb.toString();
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesFbsTagWithout(
            Boolean cableFbsTag, Boolean endpointFbsTag, Boolean enclosureFbsTag,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // note
        //     Boolean may be null
        //         cableFbsTag, endpointFbsTag, enclosureFbsTag
        //     exact match
        //         system, subsystem
        //     search (contains, pattern match)
        //         installationPackage, owners, revision

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesFbsTagWithout(cableFbsTag, endpointFbsTag, enclosureFbsTag,
                        system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_FBS_TAG_WITHOUT);
    }
    private ReportCablesNotifications getCablesFbsTagWithout(
            Boolean cableFbsTag, Boolean endpointFbsTag, Boolean enclosureFbsTag,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        boolean optionCableFbsTag     = Boolean.TRUE.equals(cableFbsTag);
        boolean optionEndpointFbsTag  = Boolean.TRUE.equals(endpointFbsTag);
        boolean optionEnclosureFbsTag = Boolean.TRUE.equals(enclosureFbsTag);

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        String fbsTag = null;
        String fromFbsTag = null;
        String fromEnclosureEssName = null;
        String fromEnclosureFbsTag = null;
        String toFbsTag = null;
        String toEnclosureEssName = null;
        String toEnclosureFbsTag = null;
        String installationPackageName = null;

        // find cables without fbs tag
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // check cable fbs tag
            if (optionCableFbsTag) {
                fbsTag = cable.getFbsTag();

                if (StringUtils.isEmpty(fbsTag)) {
                    cablesToExport.putIfAbsent(cable.getId(), cable);
                    notificationDTOs.add(
                            new NotificationDTO(
                                    cable, null, cable.getName(),
                                    CableColumn.FBS_TAG));
                }
            }

            // check endpoint fbs tags
            if (optionEndpointFbsTag) {
                fromFbsTag = cable.getEndpointA().getDeviceFbsTag();
                toFbsTag = cable.getEndpointB().getDeviceFbsTag();

                if (StringUtils.isEmpty(fromFbsTag)) {
                    cablesToExport.putIfAbsent(cable.getId(), cable);
                    notificationDTOs.add(
                            new NotificationDTO(
                                    null, cable.getEndpointA(), cable.getName(),
                                    CableColumn.FROM_FBS_TAG));
                }
                if (StringUtils.isEmpty(toFbsTag)) {
                    cablesToExport.putIfAbsent(cable.getId(), cable);
                    notificationDTOs.add(
                            new NotificationDTO(
                                    null, cable.getEndpointB(), cable.getName(),
                                    CableColumn.TO_FBS_TAG));
                }
            }

            // check endpoint enclosure fbs tags
            if (optionEnclosureFbsTag) {
                fromEnclosureEssName = cable.getEndpointA().getRack();
                fromEnclosureFbsTag = cable.getEndpointA().getRackFbsTag();
                toEnclosureEssName = cable.getEndpointB().getRack();
                toEnclosureFbsTag = cable.getEndpointB().getRackFbsTag();

                if (!StringUtils.isEmpty(fromEnclosureEssName) && StringUtils.isEmpty(fromEnclosureFbsTag)) {
                    cablesToExport.putIfAbsent(cable.getId(), cable);
                    notificationDTOs.add(
                            new NotificationDTO(
                                    null, cable.getEndpointA(), cable.getName(),
                                    CableColumn.FROM_ENCLOSURE_FBS_TAG));
                }
                if (!StringUtils.isEmpty(toEnclosureEssName) && StringUtils.isEmpty(toEnclosureFbsTag)) {
                    cablesToExport.putIfAbsent(cable.getId(), cable);
                    notificationDTOs.add(new NotificationDTO(
                            null, cable.getEndpointB(), cable.getName(),
                            CableColumn.TO_ENCLOSURE_FBS_TAG));
                }
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesFbsTagDuplicate(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // note
        //     exact match
        //         system, subsystem, installationpackage
        //     search (contains, pattern match)
        //         owners

        // get cables, notifications
        ReportCablesNotifications report = getCablesFbsTagDuplicate(system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_FBS_TAG_DUPLICATE);
    }
    private ReportCablesNotifications getCablesFbsTagDuplicate(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        final Map<String, Long> countCablesPerFbsTag = new HashMap<>();
        String fbsTag = null;
        String installationPackageName = null;

        // find cables with duplicate cable fbs tag
        //     not consider deleted cables
        //     loop through cables twice
        //         1st time - count cables per fbs tag
        //         2nd time - export cables for which fbsTag is used more than once
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // check cable fbs tag
            fbsTag = cable.getFbsTag();
            if (!StringUtils.isEmpty(fbsTag)) {
                Long count = countCablesPerFbsTag.get(fbsTag);
                count = count != null ? count + 1 : 1;
                countCablesPerFbsTag.put(fbsTag, count);
            }
        }

        for (Cable cable : cableService.getCables(system, subsystem)) {
            Long count = countCablesPerFbsTag.get(cable.getFbsTag());
            count = count != null ? count : 0;

            // check duplicate
            //     below will export all cables for duplicate tags
            if (count > 1) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                cable, null, cable.getName(),
                                CableColumn.FBS_TAG));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesFbsTagNotExistInChess(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // note
        //     exact match
        //         system, subsystem, installationpackage
        //     search (contains, pattern match)
        //         owners

        // find fbs elements
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesFbsTagNotExistInChess(fbsElements, system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_FBS_TAG_NOT_EXIST_IN_CHESS);
    }
    private ReportCablesNotifications getCablesFbsTagNotExistInChess(List<FbsElement> fbsElements,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // find fbs elements and mappings
        Map<String, FbsElement> mappingsFbsTagToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsFbsTagToFbsElement = FbsUtil.readMappingsTagToFbsElement(fbsElements);
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS, fbsElements.size());
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_TAG, getSize(mappingsFbsTagToFbsElement));
        }
        if (mappingsFbsTagToFbsElement == null) {
            LOGGER.log(Level.INFO, NO_FBS_DATA_AVAILABLE);
            return new ReportCablesNotifications(new ArrayList<>(), notificationDTOs);
        }

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        String fbsTag = null;
        String fromFbsTag = null;
        String fromEnclosureFbsTag = null;
        String toFbsTag = null;
        String toEnclosureFbsTag = null;
        String installationPackageName = null;

        // find cables with fbs tag that does not exist in CHESS
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // check cable fbs tag
            fbsTag = cable.getFbsTag();
            if (!StringUtils.isEmpty(fbsTag) && mappingsFbsTagToFbsElement.get(fbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                cable, null, cable.getName(),
                                CableColumn.FBS_TAG));
            }

            // check endpoint fbs tags
            fromFbsTag = cable.getEndpointA().getDeviceFbsTag();
            fromEnclosureFbsTag = cable.getEndpointA().getRackFbsTag();
            toFbsTag = cable.getEndpointB().getDeviceFbsTag();
            toEnclosureFbsTag = cable.getEndpointB().getRackFbsTag();

            if (!StringUtils.isEmpty(fromFbsTag) && mappingsFbsTagToFbsElement.get(fromFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointA(), cable.getName(),
                                CableColumn.FROM_FBS_TAG));
            }
            if (!StringUtils.isEmpty(fromEnclosureFbsTag) && mappingsFbsTagToFbsElement.get(fromEnclosureFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointA(), cable.getName(),
                                CableColumn.FROM_ENCLOSURE_FBS_TAG));
            }
            if (!StringUtils.isEmpty(toFbsTag) && mappingsFbsTagToFbsElement.get(toFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                null, cable.getEndpointB(), cable.getName(),
                                CableColumn.TO_FBS_TAG));
            }
            if (!StringUtils.isEmpty(toEnclosureFbsTag) && mappingsFbsTagToFbsElement.get(toEnclosureFbsTag) == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        null, cable.getEndpointB(), cable.getName(),
                        CableColumn.TO_ENCLOSURE_FBS_TAG));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesFbsTagNotCorrectInChess(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // note
        //     exact match
        //         system, subsystem, installationpackage
        //     search (contains, pattern match)
        //         owners

        // find fbs elements
        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesFbsTagNotCorrectInChess(fbsElements, system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_FBS_TAG_NOT_CORRECT_IN_CHESS);
    }
    private ReportCablesNotifications getCablesFbsTagNotCorrectInChess(List<FbsElement> fbsElements,
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // find fbs elements and mappings
        Map<String, FbsElement> mappingsCableNameToFbsElement = null;
        Map<String, FbsElement> mappingsFbsTagToFbsElement = null;
        if (!fbsElements.isEmpty()) {
            mappingsCableNameToFbsElement = FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            mappingsFbsTagToFbsElement = FbsUtil.readMappingsTagToFbsElement(fbsElements);
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS, fbsElements.size());
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_CABLE_NAME, getSize(mappingsCableNameToFbsElement));
            LOGGER.log(Level.INFO, NUMBER_OF_FBS_MAPPINGS_TAG, getSize(mappingsFbsTagToFbsElement));
        }
        if (mappingsCableNameToFbsElement == null || mappingsFbsTagToFbsElement == null) {
            LOGGER.log(Level.INFO, NO_FBS_DATA_AVAILABLE);
            return new ReportCablesNotifications(new ArrayList<>(), notificationDTOs);
        }

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        String name = null;
        String fbsTag = null;
        String chessId = null;
        String installationPackageName = null;
        FbsElement fbsElementCableName = null;
        FbsElement fbsElementFbsTag = null;

        // find cables with fbs tag that are not correct in CHESS (no linking back to same cable attribute)
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // does chess point back to same info as cable name/fbs tag?
            name   = cable.getName();
            fbsTag = cable.getFbsTag();
            chessId = cable.getChessId();
            fbsElementCableName = name   != null ? mappingsCableNameToFbsElement.get(name) : null;
            fbsElementFbsTag = fbsTag != null ? mappingsFbsTagToFbsElement.get(fbsTag) : null;
            boolean name2differentTag = fbsElementCableName != null && !StringUtils.equals(fbsTag, fbsElementCableName.tag);
            boolean tag2differentName = fbsElementFbsTag    != null && !StringUtils.equals(name, fbsElementFbsTag.cableName);
            boolean fbsTagEmptyChessId = !StringUtils.isEmpty(fbsTag) && StringUtils.isEmpty(chessId);

            if (name2differentTag || tag2differentName || fbsTagEmptyChessId) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(
                        new NotificationDTO(
                                cable, null, cable.getName(),
                                CableColumn.FBS_TAG));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesCableArticleWithout(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // note
        //     exact match
        //         system, subsystem, installationpackage
        //     search (contains, pattern match)
        //         owners

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesCableArticleWithout(system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_CABLE_ARTICLE_WITHOUT);
    }
    private ReportCablesNotifications getCablesCableArticleWithout(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }

        String installationPackageName = null;

        // find cables without cable article
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // check if no cable article
            if (cable.getCableArticle() == null) {
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.add(new NotificationDTO(
                        cable, null, cable.getName(),
                        CableColumn.CABLE_ARTICLE));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public Response reportCablesInvalid(String system, String subsystem, String installationPackage, String owners,
            String revision) {

        // note
        //     exact match
        //         system, subsystem
        //     search (contains, pattern match)
        //         installationpackage, owners, revision

        // get cables, notifications
        ReportCablesNotifications report =
                getCablesInvalid(system, subsystem, installationPackage, owners, revision);

        LOGGER.log(Level.INFO, NUMBER_OF_CABLES, report.getCables().size());
        LOGGER.log(Level.INFO, NUMBER_OF_NOTIFICATIONS, report.getNotificationDTOs().size());

        // highlight cells in Excel sheet corresponding to notifications
        final CableSaver cableSaver = new CableSaver(report.getCables(), report.getNotificationDTOs());

        // return report with cables (Excel)
        return getResponse(cableSaver, REPORT_CABLES_INVALID);
    }
    private ReportCablesNotifications getCablesInvalid(
            String system, String subsystem, String installationPackage, String owners, String revision) {

        // track cables, notifications
        final Map<Long, Cable> cablesToExport = new HashMap<>();
        List<NotificationDTO> notificationDTOs = new ArrayList<>();

        // set up patterns
        Pattern patternInstallationPackage = null;
        if (!StringUtils.isEmpty(installationPackage)) {
            patternInstallationPackage = Pattern.compile(installationPackage);
        }
        Pattern patternOwners = null;
        if (!StringUtils.isEmpty(owners)) {
            patternOwners = Pattern.compile(owners);
        }
        Pattern patternRevision = null;
        if (!StringUtils.isEmpty(revision)) {
            patternRevision = Pattern.compile(revision);
        }
        String installationPackageName = null;

        // find cables marked invalid
        //     not consider deleted cables
        for (Cable cable : cableService.getCables(system, subsystem)) {
            // check patterns
            installationPackageName = cable.getInstallationPackage() != null
                    ? Objects.toString(cable.getInstallationPackage().getName(), EMPTY)
                    : EMPTY;
            if (patternInstallationPackage != null && !patternInstallationPackage.matcher(installationPackageName).find()) {
                continue;
            }
            if (patternOwners != null && !patternOwners.matcher(Objects.toString(cable.getOwnersString(), EMPTY)).find()) {
                continue;
            }
            if (patternRevision != null && !patternRevision.matcher(Objects.toString(cable.getRevision(), EMPTY)).find()) {
                continue;
            }

            // find out notifications for cable
            if (!cable.isValid() || !cable.getEndpointA().isValid() || !cable.getEndpointB().isValid()) {
                // find out reasons for invalid cable and add to notificationDTO as comment
                cablesToExport.putIfAbsent(cable.getId(), cable);
                notificationDTOs.addAll(ValidityUtil.getNotificationsForProblem(cable, names));
            }
        }

        // sort cables
        List<Cable> objects = new ArrayList<>();
        objects.addAll(cablesToExport.values());
        sortByName(objects);

        return new ReportCablesNotifications(objects, notificationDTOs);
    }

    // ----------------------------------------------------------------------------------------------------
    // Utility
    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method to get size of map.
     *
     * @param map map whose mappings have string key and FbsElement value
     * @return number of key-value mappings
     */
    private int getSize(Map<String, FbsElement> map) {
        return map != null ? map.size() : 0;
    }

    /**
     * Utility method to sort cables according to cable name
     *
     * @param cables cables
     */
    private void sortByName(List<Cable> cables) {
        // sort cables according to cable name
        Collections.sort(cables, new Comparator<Cable>() {
            @Override
            public int compare(Cable arg0, Cable arg1) {
                return arg0.getName().compareTo(arg1.getName());
            }
        });
    }

    /**
     * Utility method to return report with cables (Excel).
     *
     * @param cableSaver used to generate Excel
     * @param message message to write to log in case report can not be generated
     * @return report with cables (Excel)
     */
    private Response getResponse(CableSaver cableSaver, String message) {
        final InputStream inputStream = cableSaver.save();

        try {
            byte[] bytes = IOUtils.toByteArray(inputStream);

            return Response.ok()
                    .entity(bytes)
                    .header(RESPONSE_HEADER_CONTENT_DISPOSITION, RESPONSE_HEADER_FILENAME)
                    .type(MediaType.APPLICATION_OCTET_STREAM)
                    .build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, String.format(REPORT_COULD_NOT_BE_MADE_AVAILABLE, message, e.getMessage()));
            return null;
        }
    }

    /**
     * Utility class to collect report information, cables and notifications.
     * Intended for internal use only (this class).
     *
     * @author Lars Johansson
     */
    private class ReportCablesNotifications {

        private final List<Cable> cables;
        private final List<NotificationDTO> notificationDTOs;

        ReportCablesNotifications(List<Cable> cables, List<NotificationDTO> notificationDTOs) {
            this.cables = cables;
            this.notificationDTOs = notificationDTOs;
        }

        public List<Cable> getCables() {
            return this.cables;
        }
        public List<NotificationDTO> getNotificationDTOs() {
            return this.notificationDTOs;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Add one to count for key in map.
     *
     * @param key key
     * @param map map
     */
    private void addOne(String key, Map<String, Long> map) {
        addOne(key, map, null);
    }
    /**
     * Add one to count for key in map.
     *
     * @param key key
     * @param map map
     * @param mapNok map nok
     */
    private void addOne(String key, Map<String, Long> map, Map<String, Long> mapNok) {
        if (key == null) {
            return;
        }

        Long no = map.get(key);
        if (mapNok != null && no == null) {
            no = mapNok.get(key);
            no = no != null ? no + 1 : 1;
            mapNok.put(key, no);
        } else {
            no = no != null ? no + 1 : 1;
            map.put(key, no);
        }
    }
    /**
     * Get number of entries with value == 0.
     *
     * @param map map
     * @return number of entries with value == 0
     */
    private Long getNumberWithValueZero(Map<String, Long> map) {
        long count = 0l;
        for (Entry<String, Long> entry : map.entrySet()) {
            if (entry.getValue() == 0) {
                count++;
            }
        }
        return count;
    }
    /**
     * Get number of entries with value > 0.
     *
     * @param map map
     * @return number of entries with value > 0
     */
    private Long getNumberWithValueNonZero(Map<String, Long> map) {
        long count = 0l;
        for (Entry<String, Long> entry : map.entrySet()) {
            if (entry.getValue() > 0) {
                count++;
            }
        }
        return count;
    }
    /**
     * Add space to string (for value) until it has size.
     *
     * @param str value
     * @param size size
     * @return string expanded to size
     */
    private String addSpaceUntilSize(String str, int size ) {
        if (str != null && str.length() < size) {
            StringBuilder sb = new StringBuilder(str);
            while (sb.length() < size)  {
                sb.append(SPACE);
            }
            return sb.toString();
        }
        return str;
    }
    /**
     * Add space to string (for value) until it has size.
     *
     * @param val value
     * @param size size
     * @return string expanded to size
     */
    private String addSpaceUntilSize(long val, int size ) {
        return addSpaceUntilSize(String.valueOf(val), size);
    }
    /**
     * Check, compare and get maxlength for str, either length of str or given int.
     *
     * @param str str
     * @param maxlength maxlength
     * @return maxlength for str, either length of str or given int
     */
    private int checkGetMaxLength(String str, int maxlength) {
        if (str == null || str.length() < maxlength) {
            return maxlength;
        } else {
            return str.length();
        }
    }

}
