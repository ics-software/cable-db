/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;

import org.openepics.cable.jaxb.CableArticleElement;
import org.openepics.cable.jaxb.CableArticleResource;
import org.openepics.cable.model.CableArticle;
import org.openepics.cable.services.CableArticleService;

/**
 * This resource provides bulk approved and specific cable article data.
 *
 * @author Lars Johansson
 */
@Path("cableArticle")
public class CableArticleResourceImpl implements CableArticleResource {

    @Inject
    private CableArticleService cableArticleService;

    @Override
    public List<CableArticleElement> getAllCableArticles() {
        final List<CableArticleElement> cableArticleElements = new ArrayList<>();

        for (final CableArticle cableArticle : cableArticleService.getCableArticles()) {
            if (!cableArticle.isActive())
                continue;
            cableArticleElements.add(getCableArticleElement(cableArticle));
        }
        return cableArticleElements;
    }

    @Override
    public CableArticleElement getCableArticle(String shortNameExternalId) {
        final CableArticle cableArticle =
                cableArticleService.getCableArticleByShortNameExternalId(shortNameExternalId);
        return cableArticle != null ? getCableArticleElement(cableArticle) : null;
    }

    /**
     * Creates an instance of {@link CableArticleElement} from database model object {@link CableArticle}.
     *
     * @param cableArticle
     *            the database model object
     * @return the JAXB object
     */
    protected static CableArticleElement getCableArticleElement(CableArticle cableArticle) {
        CableArticleElement cableArticleElement = new CableArticleElement();
        cableArticleElement.setManufacturer(cableArticle.getManufacturer());
        cableArticleElement.setExternalId(cableArticle.getExternalId());
        cableArticleElement.setErpNumber(cableArticle.getErpNumber());
        cableArticleElement.setIsoClass(cableArticle.getIsoClass());
        cableArticleElement.setDescription(cableArticle.getDescription());
        cableArticleElement.setLongDescription(cableArticle.getLongDescription());
        cableArticleElement.setModelType(cableArticle.getModelType());
        cableArticleElement.setBendingRadius(cableArticle.getBendingRadius());
        cableArticleElement.setOuterDiameter(cableArticle.getOuterDiameter());
        cableArticleElement.setRatedVoltage(cableArticle.getRatedVoltage());
        cableArticleElement.setWeightPerLength(cableArticle.getWeightPerLength());
        cableArticleElement.setShortName(cableArticle.getShortName());
        cableArticleElement.setShortNameExternalId(cableArticle.getShortNameExternalId());
        cableArticleElement.setUrlChessPart(cableArticle.getUrlChessPart());
        cableArticleElement.setActive(cableArticle.isActive());
        cableArticleElement.setCreated(cableArticle.getCreated());
        cableArticleElement.setModified(cableArticle.getModified());
        return cableArticleElement;
    }

}
