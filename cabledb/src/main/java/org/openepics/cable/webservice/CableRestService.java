/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import io.swagger.jaxrs.config.BeanConfig;

import java.util.Set;

import javax.ws.rs.core.Application;

/**
 * This represents the JAX-RS application which hosts all REST resources of the cable database.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@javax.ws.rs.ApplicationPath("/rest")
public class CableRestService extends Application {

    /**
     * Initiation of Swagger description and documentation for Cable REST API .
     */
    public CableRestService() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(System.getProperty("cable.swagger.schemes", "https").split(","));
        beanConfig.setBasePath(System.getProperty("cable.swagger.basepath", "/rest"));
        beanConfig.setResourcePackage("org.openepics.cable.jaxb");
        beanConfig.setScan(true);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return getRestResourceClasses();
    }

    private Set<Class<?>> getRestResourceClasses() {
        return new java.util.HashSet<Class<?>>();
    }
}
