/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.webservice;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.jaxb.CableResource;
import org.openepics.cable.jaxb.EndpointElement;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.dl.CableColumn;

/**
 * This is implementation of {@link CableResource} interface.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableResourceImpl implements CableResource {

    private static final Logger LOGGER = Logger.getLogger(CableResourceImpl.class.getName());

    @Inject
    private CableService cableService;

    @Override
    public List<CableElement> getAllCables(List<String> field, String regExp) {

        LOGGER.fine("regExp " + regExp);

        List<CableColumn> columns = new ArrayList<CableColumn>();
        for (String fieldString : field) {
            CableColumn cableColumn = CableColumn.convertColumnLabel(fieldString);
            if (cableColumn == null) {
                return null;
            }
            columns.add(cableColumn);
        }

        columns.remove(CableColumn.STATUS);

        final List<CableElement> cableElements = new ArrayList<>();
        LOGGER.finest("Retrieving cables by regular expression");
        for (final Cable cable : cableService.getFilteredCablesByRegExp(columns, regExp)) {
            if (cable.getStatus() == CableStatus.DELETED)
                continue;
            cableElements.add(getCableElement(cable));
        }
        LOGGER.finest(" All active cables retrieved");
        return cableElements;
    }

    @Override
    public CableElement getCable(String name) {
        final Cable cable = cableService.getCableByName(name);
        if (cable == null)
            return null;
        if (cable.getStatus() == CableStatus.DELETED)
            return null;

        return getCableElement(cable);
    }

    /**
     * @see CableResource#getAllCablesWithESSName(String)
     */
    @Override
    public List<CableElement> getAllCablesWithESSName(String essName) {
        // note
        //     case sensitive
        //     search
        //     regex

        final List<CableElement> cableElements = new ArrayList<>();
        try {
            Pattern pattern = Pattern.compile(essName);

            // get cables
            // check
            //     non-deleted
            //     ess name is used
            //         endpoint a, b
            //             fields device, rack
            for (final Cable cable : cableService.getCables()) {
                if (cable.getStatus() == CableStatus.DELETED)
                    continue;

                Endpoint endpoint = cable.getEndpointA();
                if ((!StringUtils.isEmpty(endpoint.getDevice()) && pattern.matcher(endpoint.getDevice()).find())
                        || (!StringUtils.isEmpty(endpoint.getRack())   && pattern.matcher(endpoint.getRack()).find())) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
                endpoint = cable.getEndpointB();
                if ((endpoint.getDevice() != null && pattern.matcher(endpoint.getDevice()).find())
                        || (endpoint.getRack()   != null && pattern.matcher(endpoint.getRack()).find())) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return cableElements;
    }

    /**
     * @see CableResource#getAllCablesWithFBSTag(String)
     */
    @Override
    public List<CableElement> getAllCablesWithFBSTag(String fbsTag) {
        // note
        //     case sensitive
        //     search
        //     regex

        final List<CableElement> cableElements = new ArrayList<>();
        try {
            Pattern pattern = Pattern.compile(fbsTag);

            // get cables
            // check
            //     non-deleted
            //     fbs tag is used
            //         cable
            //             field fbs tag
            //         endpoint a, b
            //             fields device fbs tag, rack fbs tag
            for (final Cable cable : cableService.getCables()) {
                if (cable.getStatus() == CableStatus.DELETED)
                    continue;

                if (!StringUtils.isEmpty(cable.getFbsTag()) && pattern.matcher(cable.getFbsTag()).find()) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
                Endpoint endpoint = cable.getEndpointA();
                if ((!StringUtils.isEmpty(endpoint.getDeviceFbsTag())
                        && pattern.matcher(endpoint.getDeviceFbsTag()).find())
                        ||
                    (!StringUtils.isEmpty(endpoint.getRackFbsTag())
                        && pattern.matcher(endpoint.getRackFbsTag()).find())) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
                endpoint = cable.getEndpointB();
                if ((!StringUtils.isEmpty(endpoint.getDeviceFbsTag())
                        && pattern.matcher(endpoint.getDeviceFbsTag()).find())
                        ||
                    (!StringUtils.isEmpty(endpoint.getRackFbsTag())
                        && pattern.matcher(endpoint.getRackFbsTag()).find())) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return cableElements;
    }

    /**
     * @see CableResource#getAllCablesWithLBSTag(String)
     */
    @Override
    public List<CableElement> getAllCablesWithLBSTag(String lbsTag) {
        // note
        //     case sensitive
        //     search
        //     regex

        final List<CableElement> cableElements = new ArrayList<>();
        try {
            Pattern pattern = Pattern.compile(lbsTag);

            // get cables
            // check
            //     non-deleted
            //     lbs tag is used
            //         endpoint a, b
            //             field building
            for (final Cable cable : cableService.getCables()) {
                if (cable.getStatus() == CableStatus.DELETED)
                    continue;

                Endpoint endpoint = cable.getEndpointA();
                if (!StringUtils.isEmpty(endpoint.getBuilding()) && pattern.matcher(endpoint.getBuilding()).find()) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
                endpoint = cable.getEndpointB();
                if (!StringUtils.isEmpty(endpoint.getBuilding()) && pattern.matcher(endpoint.getBuilding()).find()) {
                    cableElements.add(getCableElement(cable));
                    continue;
                }
            }
        } catch (PatternSyntaxException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
        }
        return cableElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Creates an instance of {@link CableElement} from database model object {@link Cable}.
     *
     * @param cable
     *            the database model object
     * @return the JAXB object
     */
    private CableElement getCableElement(Cable cable) {
        CableElement cableElement = new CableElement();
        cableElement.setId(cable.getId());
        cableElement.setName(cable.getName());
        cableElement.setFbsTag(cable.getFbsTag());
        cableElement.setSystem(cable.getSystem());
        cableElement.setSubsystem(cable.getSubsystem());
        cableElement.setCableClass(cable.getCableClass());
        cableElement.setSeqNumber(cable.getSeqNumber());
        cableElement.setOwners(cable.getOwnersString());
        if (cable.getCableArticle() != null) {
            cableElement.setCableArticle(CableArticleResourceImpl.getCableArticleElement(cable.getCableArticle()));
        }
        if (cable.getCableType() != null) {
            cableElement.setCableType(CableTypeResourceImpl.getCableTypeElement(cable.getCableType()));
        }
        cableElement.setContainer(cable.getContainer());
        cableElement.setElectricalDocumentation(cable.getElectricalDocumentation());
        cableElement.setEndpointA(getEndpointElement(cable.getEndpointA()));
        cableElement.setEndpointB(getEndpointElement(cable.getEndpointB()));
        if (cable.getInstallationPackage() != null) {
            cableElement.setInstallationPackage(InstallationPackageResourceImpl.getInstallationPackageElement(cable.getInstallationPackage()));
        }
        cableElement.setInstallationBy(cable.getInstallationBy());
        cableElement.setComments(cable.getComments());
        cableElement.setCreated(cable.getCreated());
        cableElement.setModified(cable.getModified());
        cableElement.setStatus(String.valueOf(cable.getStatus()));
        cableElement.setValidity(String.valueOf(cable.getValidity()));
        cableElement.setRevision(cable.getRevision());
        return cableElement;
    }

    /**
     * Creates an instance of {@link EndpointElement} from database model object {@link Endpoint}.
     *
     * @param endpoint
     *            the database model object
     * @return the JAXB object
     */
    private EndpointElement getEndpointElement(Endpoint endpoint) {
        EndpointElement endpointElement = new EndpointElement();
        endpointElement.setEssName(endpoint.getDevice());
        endpointElement.setFbsTag(endpoint.getDeviceFbsTag());
        endpointElement.setLbsTag(endpoint.getBuilding());
        endpointElement.setEnclosureEssName(endpoint.getRack());
        endpointElement.setEnclosureFbsTag(endpoint.getRackFbsTag());
        endpointElement.setConnector(endpoint.getConnector() != null ? endpoint.getConnector().getName() : "");
        endpointElement.setLabel(endpoint.getLabel());
        endpointElement.setValidity(String.valueOf(endpoint.getValidity()));
        return endpointElement;
    }

}
