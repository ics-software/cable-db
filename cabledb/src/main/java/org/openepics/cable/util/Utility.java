/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.util.StringUtil;

import com.google.common.base.CharMatcher;

/**
 * Contains some utility methods.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
public class Utility {

    public static final String MESSAGE_SUMMARY_SUCCESS = "Success";
    public static final String MESSAGE_SUMMARY_ERROR   = "Error";
    public static final String XLSX_CONTENT_TYPE       =
            "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    private Utility() {}

    /**
     * Utility method used for system validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid system
     */
    public static void isValidSystem(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty() || stringValue.length() != 1) {
            return;
        }
        if (!Character.isDigit(stringValue.charAt(0))) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid system.", null));
        }
    }

    /**
     * Utility method used for subsystem validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid subsystem
     */
    public static void isValidSubsystem(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty() || stringValue.length() != 1) {
            return;
        }
        if (!Character.isDigit(stringValue.charAt(0)) && !Character.isLetter(stringValue.charAt(0))) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Entered value is not a valid subsystem.", null));
        }
    }

    /**
     * Utility method used for cable class validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid cable class
     */
    public static void isValidCableClass(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty() || stringValue.length() != 1) {
            return;
        }
        if (!Character.isLetter(stringValue.charAt(0))) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Entered value is not a valid cable class.", null));
        }
    }

    /**
     * Utility method used for Lbs tag validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid Lbs tag
     */
    public static void isValidLbsTag(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        if (!stringValue.startsWith("+")) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Entered value is not a valid LBS tag.", null));
        }
    }

    /**
     * Utility method used for URL validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid URL
     */
    public static void isURLEntered(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        try {
            new URL(stringValue);
        } catch (MalformedURLException e) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid url.", null));
        }
    }

    /**
     * Utility method for Integer validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid Integer
     */
    public static void isIntegerEntered(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        try {
            Integer.parseInt(stringValue);
        } catch (NumberFormatException ne) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid number.", null));
        }
    }

    /**
     * Utility method used for Double validation.
     *
     * @param value value
     * @throws ValidatorException if value is not valid Double
     */
    public static void isFloatEntered(Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        try {
            Float.parseFloat(stringValue);
        } catch (NumberFormatException ne) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered value is not a valid number", null));
        }
    }

    /**
     * Checks whether a {@link Collection} is <code>null</code> or empty.
     *
     * @param collection
     *            the collection to test
     * @return <code>true</code> if collection is <code>null</code> or empty, <code>false</code> otherwise
     */
    public static boolean isNullOrEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Checks whether a {@link Map} is <code>null</code> or empty.
     *
     * @param map
     *            the map to test
     * @return <code>true</code> if map is <code>null</code> or empty, <code>false</code> otherwise
     */
    public static boolean isNullOrEmpty(final Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    /**
     * Returns <tt>null</tt> if string is empty, otherwise string.
     *
     * @param string string
     * @return <tt>null</tt> if string is empty, otherwise string
     */
    public static String nullIfEmpty(String string) {
        // return null if value is empty - not StringUtils.trimToNull
        return StringUtils.isEmpty(string) ? null : string;
    }

    /**
     * Formats given string by collapsing and trimming all whitespaces.
     *
     * @param string to format
     * @return formatted string or null if string to format is null
     */
    public static String formatWhitespace(String string) {
        return string != null ? CharMatcher.whitespace().trimAndCollapseFrom(string, ' ') : null;
    }

    /**
     * Formats given string by collapsing and trimming all leading and trailing whitespaces.
     * Does not format whitespaces inside string.
     *
     * @param string to format
     * @return formatted string or null if string to format is null
     */
    public static String formatWhitespaceTrimFrom(String string) {
        return string != null ? CharMatcher.whitespace().trimFrom(string) : null;
    }

    /**
     * (Default) Formats string from record value.
     *
     * @param string string
     * @return formats string from record value
     */
    public static String formatRecordValue(String string) {
        // in effect StringUtils.trimToNull and some more
        return nullIfEmpty(formatWhitespace(string));
    }

    /**
     * (Default) Formats string from record value, trimming all leading and trailing whitespaces.
     *
     * @param string string
     * @return formats string from record value
     */
    public static String formatRecordValueTrimFrom(String string) {
        // in effect StringUtils.trimToNull and some more
        return nullIfEmpty(formatWhitespaceTrimFrom(string));
    }

    /**
     * Breaks string if it is too long.
     *
     * @param string
     *            string to format.
     *
     * @return new reformatted string.
     */
    public static String formatOverlayContentText(String string) {
        StringBuilder sb = new StringBuilder(string);
        int i = 0;
        while ((i = sb.indexOf(" ", i + 37)) != -1) {
            sb.replace(i, i + 1, "\n");
        }
        return sb.toString();
    }

    /**
     * Splits a string at "," and returns results as a list
     *
     * @param data
     *            string of data to split
     * @return a list of separated values or null if data to split is null
     */
    public static List<String> splitStringIntoList(String data) {
        return data != null ? Arrays.asList(data.split("\\s*,\\s*")) : null;
    }

    /**
     * Compares objects and returns true if objects are not null and values of objects are equal.
     *
     * <p>
     * Note
     * <ul>
     * <li>exceptions as explained in {@link Float#equals(Object)}
     * <li>at least two objects needed for comparison
     * </ul>
     *
     * @param f variable number of float parameters to compare
     * @return <code>true</code> if objects are not null and values of objects are equal, <code>false</code> otherwise
     *
     * @see Float#equals(Object)
     */
    public static boolean equalsFloat(Float ...f) {
        if (f == null || f.length < 2)
            return false;

        //  one handle/entry in array needed
        Float p = null;
        for (Float v : f) {
            if (v != null) {
                p = v;
                break;
            }
        }
        if (p == null)
            return false;

        //  compare handle/entry with other handles/entries
        for (Float v : f) {
            if (v == p)
                continue;
            else {
                if (!Objects.equals(p, v))
                    return false;
            }
        }

        return true;
    }

    /**
     * Return true if given username is contained in list of usernames for cable freeze bypass.
     *
     * @param username given username
     * @return true if given username is contained in list of usernames for cable freeze bypass
     */
    public static boolean isUsernameInCableFreezeBypassUsernames(String username) {
        final String cablefreezebypassusernames = CableProperties.getInstance().getCableFreezeBypassUsernames();

        boolean found = false;
        for (String name : StringUtil.splitToList(cablefreezebypassusernames)) {
            if (StringUtils.equals(name, username)) {
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     * Converts value to Integer or null if value is null or empty.
     *
     * @param value
     *            the value to be converted to double
     * @return the double value if the value object can be converted to double or null otherwise
     * @throws NumberFormatException
     *             if the value cannot be converted.
     */
    public static Integer toInteger(String value) {
        if (StringUtils.isBlank(value))
            return null;
        return Integer.valueOf(value);
    }

    /**
     * Converts value to Float or null if value is null or empty.
     *
     * @param value
     *            the value to be converted to double
     * @return the double value if the value object can be converted to double or null otherwise
     * @throws NumberFormatException
     *             if the value cannot be converted.
     */
    public static Float toFloat(String value) {
        if (StringUtils.isBlank(value))
            return null;
        return Float.valueOf(value);
    }

    /**
     * Converts value to Date or null if value is null or empty.
     *
     * @param value
     *            the value to be casted to date
     *
     * @return the date if the value object is of type Date
     * @throws ClassCastException
     *             if the value cannot be converted.
     *
     */
    public static Date toDate(Object value) {
        if (value == null)
            return null;
        return (Date) value;
    }

    /**
     * Converts value to Date or null if value is null or empty or cannot be converted.
     *
     * @param value
     *            the value to be casted to date
     *
     * @return the date if the value object is of type Date
     */
    public static Date toDateNull(Object value) {
        if (value == null)
            return null;
        try {
            return (Date) value;
        } catch (ClassCastException e) {
            return null;
        }
    }

    /**
     * Returns upper case if string is not <tt>null</tt>, otherwise <tt>null</tt>.
     *
     * @param string string
     * @return upper case if string is not <tt>null</tt>, otherwise <tt>null</tt>
     */
    public static String upperCase(String string) {
        return string != null ? string.toUpperCase() : null;
    }

}
