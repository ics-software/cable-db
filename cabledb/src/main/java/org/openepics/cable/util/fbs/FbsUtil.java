/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util.fbs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class to convert, extract, list, get FBS tag and associated information.
 *
 * <p>
 * Class does NOT retrieve information.
 *
 * @author Lars Johansson
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsElement
 */
public class FbsUtil {
    private static final String AMBIGUOUS_NAME = "Ambiguous name: ";

    /**
     * This class is not to be instantiated.
     */
    private FbsUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Converts a FbsElement list into a map with entries (key, value - cable name, FBS tag).
     * A cable name can be associated with zero or one FBS tag.
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of cable name-FBS tag pairs
     */
    public static Map<String, String> readMappingsCableNameToFbsTag(List<FbsElement> fbsElements) {
        if (fbsElements == null || fbsElements.isEmpty()) {
            return null;
        }

        final Map<String, List<String>> mapWithMultipleValues = new HashMap<>();
        fbsElements.stream()
            .filter(element -> !StringUtils.isEmpty(element.cableName))
            .forEach(element -> addElementToListInMap(mapWithMultipleValues, element.cableName, element.tag));

        return createTagMap(mapWithMultipleValues);
    }

    /**
     * Converts a FbsElement list into a map with entries (key, value - ESS name, FBS tag).
     * An ESS name can be associated with zero or one FBS tag.
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of ESS name-FBS tag pairs
     */
    public static Map<String, String> readMappingsEssNameToFbsTag(List<FbsElement> fbsElements) {
        if (fbsElements == null || fbsElements.isEmpty()) {
            return null;
        }

        final Map<String, List<String>> mapWithMultipleValues = new HashMap<>();
        fbsElements.stream()
            .filter(element -> !StringUtils.isEmpty(element.essName))
            .forEach(element -> addElementToListInMap(mapWithMultipleValues, element.essName, element.tag));

        return createTagMap(mapWithMultipleValues);
    }

    /**
     * Converts a FbsElement list into a map with entries (key, value - FBS tag, ESS name).
     * An FBS tag can lack association with cable name and ESS name.
     * An FBS tag can be associated with maximum one cable name and ESS name.
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of FBS tag-ESS name pairs
     */
    public static Map<String, String> readMappingsFbsTagToEssName(List<FbsElement> fbsElements) {
        if (fbsElements == null) {
            return null;
        }

        // see readMappingsEssNameToFbsTag
        // similar but NOT SAME
        final Map<String, List<String>> mapWithMultipleValues = new HashMap<>();
        fbsElements.stream()
            .filter(element -> !StringUtils.isEmpty(element.tag))
            .forEach(element -> addElementToListInMap(mapWithMultipleValues, element.tag, element.essName));

        return createTagMap(mapWithMultipleValues);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Converts a FbsElement list into a map with entries (key, value - cable name, FbsElement).
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of cable name - FbsElement pairs
     */
    public static Map<String, FbsElement> readMappingsCableNameToFbsElement(List<FbsElement> fbsElements) {
        if (fbsElements == null) {
            return null;
        }

        // fbsListing from one FBS tag to have one mapping, else ambiguous

        final Map<String, FbsElement> mapWithOneValue = new HashMap<>();
        fbsElements.stream()
                .filter(element -> !StringUtils.isEmpty(element.cableName))
                .forEach(element -> addElementToMap(mapWithOneValue, element.cableName, element));

        return mapWithOneValue;
    }

    /**
     * Converts a FbsElement list into a map with entries (key, value - ESS name, FbsElement).
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of ESS name - FbsElement pairs
     */
    public static Map<String, FbsElement> readMappingsEssNameToFbsElement(List<FbsElement> fbsElements) {
        if (fbsElements == null) {
            return null;
        }

        // fbsListing from one FBS tag to have one mapping, else ambiguous

        final Map<String, FbsElement> mapWithOneValue = new HashMap<>();
        fbsElements.stream()
                .filter(element -> !StringUtils.isEmpty(element.essName))
                .forEach(element -> addElementToMap(mapWithOneValue, element.essName, element));

        return mapWithOneValue;
    }

    /**
     * Converts a FbsElement list into a map with entries (key, value - id, FbsElement).
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of id - FbsElement pairs
     */
    public static Map<String, FbsElement> readMappingsIdToFbsElement(List<FbsElement> fbsElements) {
        if (fbsElements == null) {
            return null;
        }

        // filter on id and at least one of cableName, essName, tag
        // fbsListing from one id to have one mapping, else ambiguous

        final Map<String, FbsElement> mapWithOneValue = new HashMap<>();
        fbsElements.stream()
                // .filter(element -> !StringUtils.isEmpty(element.id))
                .filter(element -> (!StringUtils.isEmpty(element.id)
                        && (!StringUtils.isEmpty(element.cableName)
                                || !StringUtils.isEmpty(element.essName)
                                || !StringUtils.isEmpty(element.tag))))
                .forEach(element -> addElementToMap(mapWithOneValue, element.id, element));

        return mapWithOneValue;
    }

    /**
     * Converts a FbsElement list into a map with entries (key, value - tag, FbsElement).
     *
     * <p>
     * An FBS tag can lack association with cable name and ESS name.
     * An FBS tag can be associated with maximum one cable name and ESS name.
     *
     * @param fbsElements FBS element list retrieved from server
     * @return map of tag - FbsElement pairs
     */
    public static Map<String, FbsElement> readMappingsTagToFbsElement(List<FbsElement> fbsElements) {
        if (fbsElements == null) {
            return null;
        }

        // fbsListing from one FBS tag to have one mapping, else ambiguous

        final Map<String, FbsElement> mapWithOneValue = new HashMap<>();
        fbsElements.stream()
                .filter(element -> !StringUtils.isEmpty(element.tag))
                .forEach(element -> addElementToMap(mapWithOneValue, element.tag, element));

        return mapWithOneValue;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Converts a FbsElement list into a list of FBS tags.
     *
     * @param fbsElements FBS element list retrieved from server
     * @return list of FBS tags
     */
    public static List<String> readTags(List<FbsElement> fbsElements) {
        if (fbsElements == null) {
            return Collections.emptyList();
        }

        return fbsElements.stream().filter(element -> !StringUtils.isEmpty(element.tag))
                .map(element -> element.tag)
                .collect(Collectors.toList());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Extract FbsElement that correspond to given cable name (exact match).
     *
     * @param fbsElements collection of FbsElement
     * @param cableName given cable name to search for
     * @return FbsElement that correspond to given cable name (exact match)
     *
     * @see FbsElement#cableName
     */
    public static FbsElement getFbsElementForCableName(Collection<FbsElement> fbsElements, String cableName) {
        List<FbsElement> elements = getFbsElementsForCableName(fbsElements, cableName);
        return elements != null && !elements.isEmpty() ? elements.get(0) : null;
    }

    /**
     * Extract list of FbsElement that correspond to given cable name (exact match).
     * Expected length of list is 0 or 1.
     *
     * @param fbsElements collection of FbsElement
     * @param cableName given cable name to search for
     * @return list of FbsElement that correspond to given cable name (exact match)
     *
     * @see FbsElement#cableName
     */
    public static List<FbsElement> getFbsElementsForCableName(Collection<FbsElement> fbsElements, String cableName) {
        if (fbsElements == null || StringUtils.isEmpty(cableName)) {
            return Collections.emptyList();
        }

        List<FbsElement> elements = new ArrayList<FbsElement>();
        for (FbsElement fbsElement : fbsElements) {
            if (StringUtils.equals(cableName, fbsElement.cableName)) {
                elements.add(fbsElement);
            }
        }
        return elements;
    }

    /**
     * Extract FbsElement that correspond to given ESS name (exact match).
     *
     * @param fbsElements collection of FbsElement
     * @param essName given ESS name to search for
     * @return FbsElement that correspond to given ESS name (exact match)
     *
     * @see FbsElement#essName
     */
    public static FbsElement getFbsElementForEssName(Collection<FbsElement> fbsElements, String essName) {
        List<FbsElement> elements = getFbsElementsForEssName(fbsElements, essName);
        return elements != null && !elements.isEmpty() ? elements.get(0) : null;
    }

    /**
     * Extract list of FbsElement that correspond to given ESS name (exact match).
     * Expected length of list is 0 or 1.
     *
     * @param fbsElements collection of FbsElement
     * @param essName given ESS name to search for
     * @return list of FbsElement that correspond to given ESS name (exact match)
     *
     * @see FbsElement#essName
     */
    public static List<FbsElement> getFbsElementsForEssName(Collection<FbsElement> fbsElements, String essName) {
        if (fbsElements == null || StringUtils.isEmpty(essName)) {
            return Collections.emptyList();
        }

        List<FbsElement> elements = new ArrayList<FbsElement>();
        for (FbsElement fbsElement : fbsElements) {
            if (StringUtils.equals(essName, fbsElement.essName)) {
                elements.add(fbsElement);
            }
        }
        return elements;
    }

    /**
     * Extract FbsElement that correspond to given id (exact match).
     *
     * @param fbsElements collection of FbsElement
     * @param id given id to search for
     * @return FbsElement that correspond to given id (exact match)
     *
     * @see FbsElement#id
     */
    public static FbsElement getFbsElementForId(Collection<FbsElement> fbsElements, String id) {
        List<FbsElement> elements = getFbsElementsForId(fbsElements, id);
        return elements != null && !elements.isEmpty() ? elements.get(0) : null;
    }

    /**
     * Extract list of FbsElement that correspond to given id (exact match).
     * Expected length of list is 0 or 1.
     *
     * @param fbsElements collection of FbsElement
     * @param id given id to search for
     * @return list of FbsElement that correspond to given id (exact match)
     *
     * @see FbsElement#id
     */
    public static List<FbsElement> getFbsElementsForId(Collection<FbsElement> fbsElements, String id) {
        if (fbsElements == null || StringUtils.isEmpty(id)) {
            return Collections.emptyList();
        }

        List<FbsElement> elements = new ArrayList<FbsElement>();
        for (FbsElement fbsElement : fbsElements) {
            if (StringUtils.equals(id, fbsElement.id)) {
                elements.add(fbsElement);
            }
        }
        return elements;
    }

    /**
     * Extract FbsElement that correspond to given tag (exact match).
     *
     * @param fbsElements collection of FbsElement
     * @param tag given tag to search for
     * @return FbsElement that correspond to given tag (exact match)
     *
     * @see FbsElement#tag
     */
    public static FbsElement getFbsElementForTag(Collection<FbsElement> fbsElements, String tag) {
        List<FbsElement> elements = getFbsElementsForTag(fbsElements, tag);
        return elements != null && !elements.isEmpty() ? elements.get(0) : null;
    }

    /**
     * Extract list of FbsElement that correspond to given tag (exact match).
     * Expected length of list is 0 or 1.
     *
     * @param fbsElements collection of FbsElement
     * @param tag given tag to search for
     * @return list of FbsElement that correspond to given tag (exact match)
     *
     * @see FbsElement#tag
     */
    public static List<FbsElement> getFbsElementsForTag(Collection<FbsElement> fbsElements, String tag) {
        if (fbsElements == null || StringUtils.isEmpty(tag)) {
            return Collections.emptyList();
        }

        List<FbsElement> elements = new ArrayList<FbsElement>();
        for (FbsElement fbsElement : fbsElements) {
            if (StringUtils.equals(tag, fbsElement.tag)) {
                elements.add(fbsElement);
            }
        }
        return elements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Utility method for building mappings like String -> List<String>:
     * cable name -> list of FBS tags, ESS name -> list of FBS tags, FBS tag -> list of ESS names.
     *
     * <p>
     * E.g.
     * <ul>
     * <li>cable name -> FbsElement
     * <li>ESS name   -> FbsElement
     * <li>FBS tag    -> FbsElement
     * </ul>
     *
     * <p>
     * There may be 0, 1 or more values for a key.
     */
    private static void addElementToListInMap(Map<String, List<String>> map, String key, String value) {
        if (!map.containsKey(key)) {
            map.put(key, new ArrayList<>());
        }

        map.get(key).add(value);
    }

    /**
     * Utility method for building mapping String -> FbsElement.
     *
     * <p>
     * E.g.
     * <ul>
     * <li>id         -> FbsElement
     * <li>cable name -> FbsElement
     * <li>ESS name   -> FbsElement
     * <li>FBS tag    -> FbsElement
     * </ul>
     *
     * <p>
     * There may be 0 or 1 value for a key. If key already present in mapping, associated value will be set to null.
     * If key present with <tt>null</tt> value, then it indicates that there is more than one mapping and
     * that it is ambiguous.
     *
     * @see FbsElement
     */
    private static void addElementToMap(Map<String, FbsElement> map, String key, FbsElement value) {
        if (map.containsKey(key)) {
            map.put(key,  null);
        } else {
            map.put(key, value);
        }
    }

    /**
     * Utility method which creates key->value map from key->list of values map. In case of ambiguous key,
     * special value will be inserted to map.
     */
    private static Map<String, String> createTagMap(Map<String, List<String>> mapWithMultipleValues) {
        final Map<String, String> mapWithOneValue = new HashMap<>();

        for (Map.Entry<String, List<String>> entry : mapWithMultipleValues.entrySet()) {
            String cableName = entry.getKey();
            List<String> values = entry.getValue();

            // cableName or essName in fbsListing to have one mapping to tag, else ambiguous
            String value;
            if (values.size() > 1) {
                value = AMBIGUOUS_NAME + String.join(", ", values);
            } else {
                value = values.get(0);
            }

            mapWithOneValue.put(cableName, value);
        }

        return mapWithOneValue;
    }

}
