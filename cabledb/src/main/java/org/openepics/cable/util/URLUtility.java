/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import java.util.regex.Pattern;

/**
 * Utility class for URL handling, e.g. encoding URL.
 *
 * @author Lars Johansson
 *
 * @see EncodingUtility
 */
public class URLUtility {

    /**
     * ? corresponds to Unicode \u003F
     */
    private static final String DELIMITER_PATH_QUERY = "\u003F";
    /**
     * & corresponds to Unicode \u003F
     */
    private static final String DELIMITER_ATTRIBUTES_VALUES = "\u0026";
    /**
     * = corresponds to Unicode \u003D
     */
    private static final String DELIMITER_ATTRIBUTE_VALUE = "\u003D";

    private static final Pattern PATTERN_DELIMITER_PATH_QUERY =
            Pattern.compile("\\" + DELIMITER_PATH_QUERY);
    private static final Pattern PATTERN_DELIMITER_ATTRIBUTES_VALUES =
            Pattern.compile("\\" + DELIMITER_ATTRIBUTES_VALUES);
    private static final Pattern PATTERN_DELIMITER_ATTRIBUTE_VALUE =
            Pattern.compile("\\" + DELIMITER_ATTRIBUTE_VALUE);

    /**
     * This class is not to be instantiated.
     */
    private URLUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Encode a URL and return the encoded value.
     *
     * <br><br>
     * Note that only attribute values are to be encoded.
     *
     * <p>Example 1. For <code>abc.xhtml?attribute1=value1&amp;attribute2=value2</code>,
     * then <code>value1</code> and <code>value2</code> are to be encoded, not other parts of url.
     *
     * <p>Example 2. For <code>cable-types.xhtml?cableTypeName=1C20RG-58HV+ 2C20</code>,
     * then <code>1C20RG-58HV+ 2C20</code> is to be encoded, not other parts of url.
     *
     * @param url a url
     * @return the encoded value
     *
     * @see EncodingUtility#ENCODING_SCHEME
     * @see EncodingUtility#encode(String)
     * @see EncodingUtility#decode(String)
     */
    public static String encodeURL(String url) {
        if (url == null)
            return url;

        // not trim url

        // split path and query
        String[] pathQuery = PATTERN_DELIMITER_PATH_QUERY.split(url, 2);
        if (pathQuery.length != 2)
            return url;

        // start prepare return value
        StringBuilder encodedURL = new StringBuilder();
        encodedURL.append(pathQuery[0]);
        encodedURL.append(DELIMITER_PATH_QUERY);

        // split attribute-value pairs
        String[] attributesValues = PATTERN_DELIMITER_ATTRIBUTES_VALUES.split(pathQuery[1]);

        // loop through attributes_values, encode values
        for (String pair : attributesValues) {
            // split attribute and value
            String[] attributeValue = PATTERN_DELIMITER_ATTRIBUTE_VALUE.split(pair, 2);

            if (attributeValue.length == 1) {
                // not encode attribute
                encodedURL.append(attributeValue[0]);
                encodedURL.append(DELIMITER_ATTRIBUTES_VALUES);
            } else {
                // encode value
              encodedURL.append(attributeValue[0]);
              encodedURL.append(DELIMITER_ATTRIBUTE_VALUE);
              encodedURL.append(EncodingUtility.encode(attributeValue[1]));
              encodedURL.append(DELIMITER_ATTRIBUTES_VALUES);
            }
        }

        // remove trailing delimiter
        int length = encodedURL.length();
        if (encodedURL.substring(length - 1).equals(DELIMITER_ATTRIBUTES_VALUES)) {
            encodedURL.setLength(length - 1);
        }

        return encodedURL.toString();
    }

}
