/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import org.openepics.cable.services.SessionService;
import org.openepics.cable.ui.CableColumnUIConstants;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Class is intended to store values that can/should be connected to user session to restore them when needed
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
@SessionScoped
@Named
public class SessionObject implements Serializable {

    @Inject
    private SessionService sessionService;

    /**
     * Field is intended to store filter dropdown-value for Status on Cables tab
     */
    private String cableStatusFilterValue;
    /**
     * Field is intended to store information about user whether he/she can administer cables
     */
    private boolean isAdministrator;

    @PostConstruct
    private void init() {
        //init value for cable status is NON-DELETED
        cableStatusFilterValue = CableColumnUIConstants.NON_DELETED_DROPDOWN_VALUE;
        //init value for isCableAdministrator from RBAC
        isAdministrator = sessionService.canAdminister();
    }

    public String getCableStatusFilterValue() {
        return cableStatusFilterValue;
    }

    public void setCableStatusFilterValue(String cableStatusFilterValue) {
        this.cableStatusFilterValue = cableStatusFilterValue;
    }

    /**
     * Determines if user can administer cables.
     * @return <code>true</code> if user can administer cables
     * <code>false</code></code> if user can not administer cables
     */
    public boolean canAdminister() {
        return isAdministrator;
    }

}
