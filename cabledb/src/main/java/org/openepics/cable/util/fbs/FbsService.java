/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.util.fbs;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.CableProperties;

import javax.ejb.Stateless;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service bean class for classes which need access to CHESS/ITIP web service.
 *
 * <p>
 * Class use FbsClient for retrieval of data.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsClient
 * @see FbsElement
 * @see FbsUtil
 **/
@Stateless
public class FbsService {

    private static final String COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP = "Could not retrieve information from ITIP.";
    private static final Logger LOGGER = Logger.getLogger(FbsService.class.getName());

    /**
     * Read all FBS data and deserializes it into a {@link FbsElement} list.
     *
     * @return {@link List} of all {@link FbsElement}
     */
    public List<FbsElement> getAllFbsElements() {
        try (FbsClient client = createFbsClient()) {
            return client.getFbsListing();
        } catch (Exception e) {
            LOGGER.log(Level.INFO,COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP + e.getMessage());
        }
        return Collections.emptyList();
    }

    /**
     * Read ESS name corresponding to given FBS tag.
     *
     * <p>
     * FBS data (FbsElement) corresponding to given FBS tag
     * is retrieved from which ESS name is extracted and returned.
     * If no ESS name is found, <tt>null</tt> is returned.
     *
     * @param tag FBS tag
     * @return ESS name corresponding to given FBS tag
     *
     * @see FbsElement
     */
    public String getEssNameForFbsTag(final String tag) {
        if (StringUtils.isNotEmpty(StringUtils.trim(tag))) {
            try (FbsClient client = createFbsClient()) {
                final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
                Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);
                final FbsElement element = tagMappings != null ? tagMappings.get(tag) : null;
                return element != null ? element.essName : null;
            } catch (Exception e) {
                LOGGER.log(Level.INFO,COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP + e.getMessage());
            }
        }
        return null;
    }

    /**
     * Read FBS tag corresponding to given ESS name.
     *
     * <p>
     * FBS data (FbsElement) corresponding to given ESS name
     * is retrieved from which FBS tag is extracted and returned.
     * If no FBS tag is found, <tt>null</tt> is returned.
     *
     * @param essName ESS name
     * @return FBS tag corresponding to given ESS name
     */
    public String getFbsTagForEssName(final String essName) {
        if (StringUtils.isNotEmpty(StringUtils.trim(essName))) {
            try (FbsClient client = createFbsClient()) {
                final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
                Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);
                return (tagMappings != null) ? tagMappings.get(essName) : null;
            } catch (Exception e) {
                LOGGER.log(Level.INFO,COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP + e.getMessage());
            }
        }
        return null;
    }

    /**
     * Read a list of up to <tt>maxResults</tt> FBS tags which starts given FBS tag value.
     *
     * @param tag start of FBS tag
     * @param maxResults maximum number of results
     * @return a list of up to <tt>maxResults</tt> FBS tags which starts given FBS tag value
     */
    public List<String> getFbsTagsForFbsTagStartsWith(String tag, int maxResults) {
        if (StringUtils.isNotEmpty(StringUtils.trim(tag))) {
            try (FbsClient client = createFbsClient()) {
                String tagSearch = tag + "%";
                final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
                return FbsUtil.readTags(fbsElements);
            } catch (Exception e) {
                LOGGER.log(Level.INFO,COULD_NOT_RETRIEVE_INFORMATION_FROM_ITIP + e.getMessage());
            }
        }
        return Collections.emptyList();
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Creates a new {@link FbsClient} instance parameterized with CHESS/ITIP webservice base URL.
     *
     * @return {@link FbsClient} instance
     */
    private FbsClient createFbsClient() {
        return new FbsClient(CableProperties.getInstance().getFbsApplicationURL());
    }

}
