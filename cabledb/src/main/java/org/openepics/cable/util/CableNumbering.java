/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.CableName;

/**
 * <code>CableNumberUI</code> contains cable system, subsystem and class labels. Labels are used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
public class CableNumbering implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String REGEX_SYSTEM_CHARACTERS    = "^[1-9]$";
    private static final String REGEX_SUBSYSTEM_CHARACTERS = "^[0-9A-N]$";
    private static final String REGEX_CLASS_CHARACTERS     = "^[ABCDEFGH]$";

    private static final String[] EMPTY_ARRAY  = new String[0];
    private static final String   EMPTY_STRING = "";

    private static final String[] SYSTEM_CHARACTERS =
            new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

    protected static final String SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL    = "1 (Safety & Environmental)";
    protected static final String SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC = "2 (Normal Conducting Linac)";
    protected static final String SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC  = "3 (Super Conducting Linac)";
    protected static final String SYSTEM_LABEL_4_HEBT_A2T_DMP_L          = "4 (HEBT-A2T DmpL)";
    protected static final String SYSTEM_LABEL_5_TARGET_SYSTEMS          = "5 (Target Systems)";
    protected static final String SYSTEM_LABEL_6_NSS_INSTRUMENTS         = "6 (NSS Instruments)";
    protected static final String SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS =
                                                                           "7 (NSS Additional or parts of instruments)";
    protected static final String SYSTEM_LABEL_8_ICS                     = "8 (ICS)";
    protected static final String SYSTEM_LABEL_9_CRYOGENICS              = "9 (Cryogenics)";

    private static final String[] SUBSYSTEM_CHARACTERS =
            new String[] {
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"};

    // NSS
    protected static final String NUMBER_0_NON_INSTRUMENT_SPECIFIC          = "0 (Non instrument specific)";
    protected static final String NUMBER_1_W1_NMX                           = "1 (W1:NMX)";
    protected static final String NUMBER_2_W2_BEER                          = "2 (W2:BEER)";
    protected static final String NUMBER_3_W3_C_SPEC                        = "3 (W3:C-SPEC)";
    protected static final String NUMBER_4_W4_BIFROST                       = "4 (W4:BIFROST)";
    protected static final String NUMBER_5_W5_MIRACLES                      = "5 (W5:MIRACLES)";
    protected static final String NUMBER_6_W6_MAGIC                         = "6 (W6:MAGIC)";
    protected static final String NUMBER_7_W7_T_REX                         = "7 (W7:T-REX)";
    protected static final String NUMBER_8_W8_HEIMDAL                       = "8 (W8:HEIMDAL)";
    protected static final String NUMBER_9_W11_TEST_BEAM                    = "9 (W11:TEST BEAM)";
    protected static final String NUMBER_A_N1_HR_NSE                        = "A (N1:HR-NSE)";
    protected static final String NUMBER_B_N5_FREIA                         = "B (N5:FREIA)";
    protected static final String NUMBER_C_N7_LOKI                          = "C (N7:LOKI)";
    protected static final String NUMBER_D_N9_SLEIPNR                       = "D (N9:SLEIPNR)";
    protected static final String NUMBER_E_E2_ESTIA                         = "E (E2:ESTIA)";
    protected static final String NUMBER_F_E3_SKADI                         = "F (E3:SKADI)";
    protected static final String NUMBER_G_E5_ANNI                          = "G (E5:ANNI)";
    protected static final String NUMBER_H_E7_VESPA                         = "H (E7:VESPA)";
    protected static final String NUMBER_I_E8_SURFACE_SC                    = "I (E8:SURFACE SC)";
    protected static final String NUMBER_J_E10_SPARE                        = "J (E10:Spare)";
    protected static final String NUMBER_K_E11_VOR                          = "K (E11:VOR)";
    protected static final String NUMBER_L_S2_ODIN                          = "L (S2:ODIN)";
    protected static final String NUMBER_M_S3_DREAM                         = "M (S3:DREAM)";
    protected static final String NUMBER_N_S6_I22                           = "N (S6:I22)";

    // NSS
    protected static final String NUMBER_0_BUNKER_W                         = "0 (Bunker W)";
    protected static final String NUMBER_1_BUNKER_N                         = "1 (Bunker N)";
    protected static final String NUMBER_2_BUNKER_E                         = "2 (Bunker E)";
    protected static final String NUMBER_3_BUNKER_S                         = "3 (Bunker S)";
    protected static final String NUMBER_4_CHOPPER_TEST_CMF_1               = "4 (Chopper Test CMF-1)";
    protected static final String NUMBER_5_CHOPPER_TEST_CMF_2               = "5 (Chopper Test CMF-2)";
    protected static final String NUMBER_6_BUNKER_NW                        = "6 (Bunker NW)";
    protected static final String NUMBER_7_BUNKER_SE                        = "7 (Bunker SE)";

    protected static final String NUMBER_0_ACCP_COMPRESSOR_SYSTEM           = "0 (ACCP compressor system)";
    protected static final String NUMBER_0_ANCILLARY_SYSTEMS                = "0 (Ancillary Systems)";
    protected static final String NUMBER_0_MAGNETS                          = "0 (Magnets)";
    protected static final String NUMBER_0_NOT_DEFINED                      = "0 (not defined)";
    protected static final String NUMBER_0_TIMING                           = "0 (Timing)";
    protected static final String NUMBER_0_TSS                              = "0 (TSS)";
    protected static final String NUMBER_1_ACCP_COLDBOX_SYSTEM              = "1 (ACCP coldbox system)";
    protected static final String NUMBER_1_HELIUM_SYSTEMS                   = "1 (Helium Systems)";
    protected static final String NUMBER_1_I_SRC                            = "1 (ISrc)";
    protected static final String NUMBER_1_MONITORING_SYSTEMS               = "1 (Monitoring Systems)";
    protected static final String NUMBER_1_NOT_DEFINED                      = "1 (not defined)";
    protected static final String NUMBER_1_PSS_ARM                          = "1 (PSS/ARM)";
    protected static final String NUMBER_2_DIAGNOSTIC                       = "2 (Diagnostic)";
    protected static final String NUMBER_2_HVAC                             = "2 (HVAC)";
    protected static final String NUMBER_2_NETWORK                          = "2 (Network)";
    protected static final String NUMBER_2_NOT_DEFINED                      = "2 (not defined)";
    protected static final String NUMBER_2_ODH_SYSTEM                       = "2 (ODH System)";
    protected static final String NUMBER_2_TICP_COMPRESSOR_SYSTEM           = "2 (TICP compressor system)";
    protected static final String NUMBER_3_MODERATOR_REFLECTOR_SYSTEMS      = "3 (Moderator Reflector Systems)";
    protected static final String NUMBER_3_NOT_DEFINED                      = "3 (not defined)";
    protected static final String NUMBER_3_OTHER_TARGET_SAFETY              = "3 (Other Target safety)";
    protected static final String NUMBER_3_PLC                              = "3 (PLC)";
    protected static final String NUMBER_3_TICP_COLDBOX_SYSTEM              = "3 (TICP coldbox system)";
    protected static final String NUMBER_3_VACUUM                           = "3 (Vacuum)";
    protected static final String NUMBER_4_COOLING                          = "4 (Cooling)";
    protected static final String NUMBER_4_IOC                              = "4 (IOC)";
    protected static final String NUMBER_4_MONOLITH_VESSEL_SYSTEMS          = "4 (Monolith Vessel Systems)";
    protected static final String NUMBER_4_NOT_DEFINED                      = "4 (not defined)";
    protected static final String NUMBER_4_OTHER_SAFETY                     = "4 (Other safety)";
    protected static final String NUMBER_4_TMCP_COMPRESSOR_SYSTEM           = "4 (TMCP compressor system)";
    protected static final String NUMBER_5_ENVIRONMENTAL_COMPLIANCE_SYSTEMS = "5 (Environmental compliance systems)";
    protected static final String NUMBER_5_NEUTRON_BEAM_EXTRACTION          = "5 (Neutron Beam Extraction)";
    protected static final String NUMBER_5_NOT_DEFINED                      = "5 (not defined)";
    protected static final String NUMBER_5_RF                               = "5 (RF)";
    protected static final String NUMBER_5_TMCP_COLDBOX_SYSTEM              = "5 (TMCP coldbox system)";
    protected static final String NUMBER_6_CRYOGENICS                       = "6 (Cryogenics)";
    protected static final String NUMBER_6_HELIUM_RECOVERY_STORAGE          = "6 (Helium recovery & storage)";
    protected static final String NUMBER_6_MPS                              = "6 (MPS)";
    protected static final String NUMBER_6_NOT_DEFINED                      = "6 (not defined)";
    protected static final String NUMBER_6_OFF_GAS_RELIEF                   = "6 (Off-gas / Relief)";
    protected static final String NUMBER_7_NITROGEN_SYSTEM                  = "7 (Nitrogen system)";
    protected static final String NUMBER_7_NOT_DEFINED                      = "7 (not defined)";
    protected static final String NUMBER_7_PROTON_BEAM_WINDOW_SYSTEMS       = "7 (Proton Beam Window Systems)";
    protected static final String NUMBER_7_PS                               = "7 (PS)";
    protected static final String NUMBER_7_REMS                             = "7 (REMS)";
    protected static final String NUMBER_8_CNPW                             = "8 (CNPW)";
    protected static final String NUMBER_8_NOT_DEFINED                      = "8 (not defined)";
    protected static final String NUMBER_8_REMOTE_HANDLING_TOOLS            = "8 (Remote Handling Tools)";
    protected static final String NUMBER_9_NOT_DEFINED                      = "9 (not defined)";
    protected static final String NUMBER_9_SHIELDINGS                       = "9 (Shieldings)";
    protected static final String NUMBER_9_SRF                              = "9 (SRF)";
    protected static final String A_NOT_DEFINED                             = "A (not defined)";
    protected static final String A_TARGET_WHEEL_SYSTEMS                    = "A (Target Wheel Systems)";
    protected static final String A_UPS                                     = "A (UPS)";
    protected static final String B_NOT_DEFINED                             = "B (not defined)";
    protected static final String B_VACUUM                                  = "B (Vacuum)";
    protected static final String C_NOT_DEFINED                             = "C (not defined)";
    protected static final String C_WATER_COOLING                           = "C (Water Cooling)";
    protected static final String D_DIAGNOSTICS                             = "D (Diagnostics)";
    protected static final String E_CNPW                                    = "E (CNPW)";

    private static final String[] CLASS_CHARACTERS =
            new String[] { "A", "B", "C", "D", "E", "F", "G", "H" };

    protected static final String CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V =
            "A (Very Low Level Signals (\u2264 10V))";
    protected static final String CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V =
            "B (Signal and Instrumentation (> 10V and \u2264 50V))";
    protected static final String CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC =
            "C (Control Signals (> 50V and \u2264 230V, AC or DC))";
    protected static final String CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE =
            "D (Low Power Low Voltage AC (\u2264 1kV and \u2264 32A per phase))";
    protected static final String CLASS_LABEL_E_DC_POWER =
            "E (DC power)";
    protected static final String CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE =
            "F (High Power Low Voltage AC (\u2264 1 kV and > 32A per phase))";
    protected static final String CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V =
            "G (Medium Voltage AC (> 1 kV and \u2264 50kV))";
    protected static final String CLASS_LABEL_H_POWER_SYSTEM_GROUNDING =
            "H (Power system grounding)";

    private static final String[] SYSTEM_LABELS = new String[] {
            SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL,
            SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC,
            SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC,
            SYSTEM_LABEL_4_HEBT_A2T_DMP_L,
            SYSTEM_LABEL_5_TARGET_SYSTEMS,
            SYSTEM_LABEL_6_NSS_INSTRUMENTS,
            SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
            SYSTEM_LABEL_8_ICS,
            SYSTEM_LABEL_9_CRYOGENICS };

    private static final String[] CLASS_LABELS = new String[] {
            CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V,
            CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V,
            CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC,
            CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE,
            CLASS_LABEL_E_DC_POWER,
            CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE,
            CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V,
            CLASS_LABEL_H_POWER_SYSTEM_GROUNDING };

    private static final String[] SYSTEM_1_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_TSS,
            NUMBER_1_PSS_ARM,
            NUMBER_2_ODH_SYSTEM,
            NUMBER_3_OTHER_TARGET_SAFETY,
            NUMBER_4_OTHER_SAFETY,
            NUMBER_5_ENVIRONMENTAL_COMPLIANCE_SYSTEMS,
            NUMBER_6_MPS,
            NUMBER_7_REMS };

    private static final String[] SYSTEM_2_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_MAGNETS,
            NUMBER_1_I_SRC,
            NUMBER_2_DIAGNOSTIC,
            NUMBER_3_VACUUM,
            NUMBER_4_COOLING,
            NUMBER_5_RF,
            NUMBER_7_PS,
            NUMBER_8_CNPW,
            A_UPS };

    private static final String[] SYSTEM_3_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_MAGNETS,
            NUMBER_2_DIAGNOSTIC,
            NUMBER_3_VACUUM,
            NUMBER_4_COOLING,
            NUMBER_5_RF,
            NUMBER_6_CRYOGENICS,
            NUMBER_7_PS,
            NUMBER_8_CNPW,
            NUMBER_9_SRF,
            A_UPS };

    private static final String[] SYSTEM_4_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_MAGNETS,
            NUMBER_1_MONITORING_SYSTEMS,
            NUMBER_2_DIAGNOSTIC,
            NUMBER_3_VACUUM,
            NUMBER_4_COOLING,
            NUMBER_5_RF,
            NUMBER_7_PS,
            NUMBER_8_CNPW,
            A_UPS };

    private static final String[] SYSTEM_5_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_ANCILLARY_SYSTEMS,
            NUMBER_1_HELIUM_SYSTEMS,
            NUMBER_2_HVAC,
            NUMBER_3_MODERATOR_REFLECTOR_SYSTEMS,
            NUMBER_4_MONOLITH_VESSEL_SYSTEMS,
            NUMBER_5_NEUTRON_BEAM_EXTRACTION,
            NUMBER_6_OFF_GAS_RELIEF,
            NUMBER_7_PROTON_BEAM_WINDOW_SYSTEMS,
            NUMBER_8_REMOTE_HANDLING_TOOLS,
            NUMBER_9_SHIELDINGS,
            A_TARGET_WHEEL_SYSTEMS,
            B_VACUUM,
            C_WATER_COOLING,
            D_DIAGNOSTICS,
            E_CNPW };

    private static final String[] SYSTEM_6_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_NON_INSTRUMENT_SPECIFIC,
            NUMBER_1_W1_NMX,
            NUMBER_2_W2_BEER,
            NUMBER_3_W3_C_SPEC,
            NUMBER_4_W4_BIFROST,
            NUMBER_5_W5_MIRACLES,
            NUMBER_6_W6_MAGIC,
            NUMBER_7_W7_T_REX,
            NUMBER_8_W8_HEIMDAL,
            NUMBER_9_W11_TEST_BEAM,
            NUMBER_A_N1_HR_NSE,
            NUMBER_B_N5_FREIA,
            NUMBER_C_N7_LOKI,
            NUMBER_D_N9_SLEIPNR,
            NUMBER_E_E2_ESTIA,
            NUMBER_F_E3_SKADI,
            NUMBER_G_E5_ANNI,
            NUMBER_H_E7_VESPA,
            NUMBER_I_E8_SURFACE_SC,
            NUMBER_J_E10_SPARE,
            NUMBER_K_E11_VOR,
            NUMBER_L_S2_ODIN,
            NUMBER_M_S3_DREAM,
            NUMBER_N_S6_I22 };

    private static final String[] SYSTEM_7_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_BUNKER_W,
            NUMBER_1_BUNKER_N,
            NUMBER_2_BUNKER_E,
            NUMBER_3_BUNKER_S,
            NUMBER_4_CHOPPER_TEST_CMF_1,
            NUMBER_5_CHOPPER_TEST_CMF_2,
            NUMBER_6_BUNKER_NW,
            NUMBER_7_BUNKER_SE,
            NUMBER_8_CNPW,
            A_UPS };

    private static final String[] SYSTEM_8_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_TIMING,
            NUMBER_2_NETWORK,
            NUMBER_3_PLC,
            NUMBER_4_IOC };

    private static final String[] SYSTEM_9_SUBSYSTEM_LABELS = new String[] {
            NUMBER_0_ACCP_COMPRESSOR_SYSTEM,
            NUMBER_1_ACCP_COLDBOX_SYSTEM,
            NUMBER_2_TICP_COMPRESSOR_SYSTEM,
            NUMBER_3_TICP_COLDBOX_SYSTEM,
            NUMBER_4_TMCP_COMPRESSOR_SYSTEM,
            NUMBER_5_TMCP_COLDBOX_SYSTEM,
            NUMBER_6_HELIUM_RECOVERY_STORAGE,
            NUMBER_7_NITROGEN_SYSTEM,
            NUMBER_8_CNPW,
            A_UPS };

    private static final Map<String, String[]> SYSTEM_SUBSYSTEM_LABELS = new ImmutableMap.Builder<String, String[]>()
            .put(SYSTEM_LABELS[0], SYSTEM_1_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[1], SYSTEM_2_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[2], SYSTEM_3_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[3], SYSTEM_4_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[4], SYSTEM_5_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[5], SYSTEM_6_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[6], SYSTEM_7_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[7], SYSTEM_8_SUBSYSTEM_LABELS)
            .put(SYSTEM_LABELS[8], SYSTEM_9_SUBSYSTEM_LABELS)
            .build();

    // system    - character, label
    // subsystem - character, label
    // class     - character, label
    //
    // character previously called number, letter

    // arrays -------------------------------------------------------------------------------------

    /**
     * Return cable system characters (numbers).
     *
     * @return array of cable system characters (numbers).
     */
    public static String[] getSystemNumbers() {
        return Arrays.copyOf(SYSTEM_CHARACTERS, SYSTEM_CHARACTERS.length);
    }

    /**
     * Return cable subsystem characters (numbers).
     *
     * @return array of cable subsystem characters (numbers)
     */
    public static String[] getSubsystemNumbers() {
        return Arrays.copyOf(SUBSYSTEM_CHARACTERS, SUBSYSTEM_CHARACTERS.length);
    }

    /**
     * Return cable subsystem characters (numbers) given system character(number).
     *
     * @param systemCharacter system character (number)
     * @return cable subsystem characters (numbers) given system character(number)
     */
    public static String[] getSubsystemNumbers(String systemCharacter) {
        if (isValidSystemCharacter(systemCharacter)) {
            String systemLabel = getSystemLabel(systemCharacter);
            List<String> subsystemLabels = Arrays.asList(SYSTEM_SUBSYSTEM_LABELS.get(systemLabel));
            String[] sarray = new String[subsystemLabels.size()];
            for (int i=0; i<subsystemLabels.size(); i++) {
                sarray[i] = subsystemLabels.get(i).substring(0, 1);
            }
            return sarray;
        }
        return EMPTY_ARRAY;
    }

    /**
     * Return cable class characters (letters).
     *
     * @return array of cable class characters (letters)
     */
    public static String[] getClassLetters() {
        return Arrays.copyOf(CLASS_CHARACTERS, CLASS_CHARACTERS.length);
    }

    // system -------------------------------------------------------------------------------------

    /**
     * Return system character (number).
     *
     * @param systemLabel system label
     * @return system character (first character (number))
     */
    public static String getSystemNumber(String systemLabel) {
        // may validate label
        if (StringUtils.isNotEmpty(systemLabel)) {
            String systemCharacter = String.valueOf(systemLabel.charAt(0));
            return isValidSystemCharacter(systemCharacter) ? systemCharacter : EMPTY_STRING;
        }
        return EMPTY_STRING;
    }

    /**
     * Return system label.
     *
     * @param systemCharacter system character (number)
     * @return appropriate system label
     */
    public static String getSystemLabel(String systemCharacter) {
        // validate character
        if (isValidSystemCharacter(systemCharacter)) {
            for (String s : SYSTEM_LABELS) {
                if (s.startsWith(systemCharacter)) {
                    return s;
                }
            }
        }
        return EMPTY_STRING;
    }

    /**
     * Return if system is valid. System may be character (number) or label.
     *
     * @param system system, may be character (number) or label
     * @return true if system is valid otherwise false
     */
    public static boolean isValidSystem(String system) {
        return isValidSystemCharacter(system) || isValidSystemLabel(system);
    }

    /**
     * Return if system character (number) is valid.
     *
     * @param systemCharacter system character (number)
     * @return true if system character (number) is valid otherwise false
     */
    public static boolean isValidSystemCharacter(String systemCharacter) {
        return StringUtils.isNotEmpty(systemCharacter) && systemCharacter.matches(REGEX_SYSTEM_CHARACTERS);
    }

    /**
     * Return if system label is valid.
     *
     * @param systemLabel system label
     * @return true if system label is valid otherwise false
     */
    public static boolean isValidSystemLabel(String systemLabel) {
        if (StringUtils.isNotEmpty(systemLabel)) {
            for (String value : SYSTEM_LABELS) {
                if (StringUtils.equals(systemLabel, value)) {
                    return true;
                }
            }
        }
        return false;
    }

    // subsystem ----------------------------------------------------------------------------------

    /**
     * Return subsystem character (number).
     *
     * @param subsystemLabel subsystem label
     * @return subsystem character (first character (number))
     */
    public static String getSubsystemNumber(String subsystemLabel) {
        // may validate label
        if (StringUtils.isNotEmpty(subsystemLabel)) {
            String subsystemCharacter = String.valueOf(subsystemLabel.charAt(0));
            return isValidSubsystemCharacter(subsystemCharacter) ? subsystemCharacter : EMPTY_STRING;
        }
        return EMPTY_STRING;
    }

    /**
     * Return subsystem label.
     *
     * @param systemCharacter system character (number)
     * @param subsystemCharacter subsystem character (number)
     * @return appropriate subsystem label
     */
    public static String getSubsystemLabel(String systemCharacter, String subsystemCharacter) {
        // validate character
        if (isValidSystemCharacter(systemCharacter)) {
            String[] sarray = null;
            for (int i=0; i<SYSTEM_LABELS.length; i++) {
                if (SYSTEM_LABELS[i].startsWith(systemCharacter)) {
                    switch (i) {
                    case 0:
                        sarray = SYSTEM_1_SUBSYSTEM_LABELS;
                        break;
                    case 1:
                        sarray = SYSTEM_2_SUBSYSTEM_LABELS;
                        break;
                    case 2:
                        sarray = SYSTEM_3_SUBSYSTEM_LABELS;
                        break;
                    case 3:
                        sarray = SYSTEM_4_SUBSYSTEM_LABELS;
                        break;
                    case 4:
                        sarray = SYSTEM_5_SUBSYSTEM_LABELS;
                        break;
                    case 5:
                        sarray = SYSTEM_6_SUBSYSTEM_LABELS;
                        break;
                    case 6:
                        sarray = SYSTEM_7_SUBSYSTEM_LABELS;
                        break;
                    case 7:
                        sarray = SYSTEM_8_SUBSYSTEM_LABELS;
                        break;
                    case 8:
                        sarray = SYSTEM_9_SUBSYSTEM_LABELS;
                        break;
                    default:
                        return EMPTY_STRING;
                    }
                }
                if (sarray != null) {
                    break;
                }
            }

            if (sarray != null) {
                for (String s : sarray) {
                    if (s.startsWith(subsystemCharacter)) {
                        return s;
                    }
                }
            }
        }
        return EMPTY_STRING;
    }

    /**
     * Return if subsystem character (number) is valid.
     *
     * @param subsystemCharacter subsystem character (number)
     * @return true if subsystem character (number) is valid otherwise false
     */
    public static boolean isValidSubsystemCharacter(String subsystemCharacter) {
        return !StringUtils.isEmpty(subsystemCharacter) && subsystemCharacter.matches(REGEX_SUBSYSTEM_CHARACTERS);
    }

  /**
   * Return if system and subsystem labels are valid.
   *
   * @param systemLabel system label
   * @param subsystemLabel subsystem label
   * @return true if system and subsystem labels are valid, otherwise false
   */
  public static boolean areValidSystemAndSubsystemLabels(String systemLabel, String subsystemLabel) {
      // may validate labels
      return Arrays.asList(SYSTEM_LABELS).contains(systemLabel)
              && Arrays.asList(SYSTEM_SUBSYSTEM_LABELS.get(systemLabel)).contains(subsystemLabel);
  }

    // class --------------------------------------------------------------------------------------

    /**
     * Return cable class character (letter).
     *
     * @param cableClassLabel cable class label
     * @return cable class character (first character (letter))
     */
    public static String getCableClassLetter(String cableClassLabel) {
        // may validate label
        if (StringUtils.isNotEmpty(cableClassLabel)) {
            String cableClassCharacter = String.valueOf(cableClassLabel.charAt(0));
            return isValidCableClassCharacter(cableClassCharacter) ? cableClassCharacter : EMPTY_STRING;
        }
        return EMPTY_STRING;
    }

    /**
     * Return cable class label.
     *
     * @param cableClassCharacter cable class character (letter)
     * @return appropriate cable class label
     */
    public static String getClassLabel(String cableClassCharacter) {
        // validate character
        if (isValidCableClassCharacter(cableClassCharacter)) {
            for (String s : CLASS_LABELS) {
                if (s.startsWith(cableClassCharacter)) {
                    return s;
                }
            }
        }
        return EMPTY_STRING;
    }

    /**
     * Return if cable class is valid. Cable class may be character (letter) or label.
     *
     * @param cableClass cable class, may be character (letter) or label
     * @return true if cable class is valid otherwise false
     */
    public static boolean isValidCableClass(String cableClass) {
        return isValidCableClassCharacter(cableClass) || isValidCableClassLabel(cableClass);
    }

    /**
     * Return if cable class character (letter) is valid.
     *
     * @param cableClassCharacter cable class character (letter)
     * @return true if cable class character (letter) is valid otherwise false
     */
    public static boolean isValidCableClassCharacter(String cableClassCharacter) {
        return !StringUtils.isEmpty(cableClassCharacter) && cableClassCharacter.matches(REGEX_CLASS_CHARACTERS);
    }

    /**
     * Return if cable class label is valid.
     *
     * @param cableClassLabel cable class label
     * @return true if cable class label is valid otherwise false
     */
    public static boolean isValidCableClassLabel(String cableClassLabel) {
        if (!StringUtils.isEmpty(cableClassLabel)) {
            for (String value : CLASS_LABELS) {
                if (StringUtils.equals(cableClassLabel, value)) {
                    return true;
                }
            }
        }
        return false;
    }

    // name ---------------------------------------------------------------------------------------

    /**
     * Return if cable name is valid; to be considered as part of check, not sole check.
     * Wrapper to {@link CableName#isValidName(String)}.
     *
     * @param cableName the cable name to check
     * @return true if valid, else false
     *
     * @see {@link CableName#isValidName(String)}
     */
    public static boolean isValidCableName(String cableName) {
        if (!StringUtils.isEmpty(cableName)) {
            return CableName.isValidName(cableName);
        }
        return false;
    }

}
