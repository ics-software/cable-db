/*
 * Copyright (c) 2019 European Spallation Source
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.util;

import java.util.ArrayList;
import java.util.List;
import org.openepics.cable.model.Manufacturer;

/**
 * Utility class for manufacturer handling, e.g. connector manufacturer.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 */
public class ManufacturerUtil {

    /**
     * This class is not to be instantiated.
     */
    private ManufacturerUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Autocomplete method for manufacturers.
     *
     * @param query the query to filter manufacturers
     * @param manufacturers list of manufacturers
     * @param usedManufacturers list of (already) used manufacturers
     * @return all available manufacturers from manufacturer service
     */
    public static List<Manufacturer> completeAvailableManufacturers(
            String query, List<Manufacturer> manufacturers, List<Manufacturer> usedManufacturers) {

        boolean alreadyUsed;
        List<Manufacturer> results = new ArrayList<>();
        for (Manufacturer manufacturer : manufacturers) {
            alreadyUsed = false;
            for (Manufacturer usedManufacturer : usedManufacturers) {
                if (manufacturer.getName().equals(usedManufacturer.getName())) {
                    alreadyUsed = true;
                    break;
                }
            }
            if (!alreadyUsed && manufacturer.getName().toLowerCase().contains(query.toLowerCase())) {
                results.add(manufacturer);
            }

            // limited amount of (foreseen) content,
            // no need to impose limit to number of items in result (MAX_AUTOCOMPLETE)
        }

        return results;
    }
}
