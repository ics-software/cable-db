/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util.fbs;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.util.EncodingUtility;
import org.openepics.cable.util.webservice.ClosableResponse;
import org.openepics.cable.util.webservice.ResponseException;
import org.openepics.cable.util.webservice.WebserviceUtility;

import javax.annotation.Nonnull;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client to ITIP integration platform for retrieval of CHESS data and extraction of information
 * such as cable name, ESS name, CHESS id, FBS tag.
 *
 * <p>
 * ITIP is considered and treated as a web service from which data is retrieved.
 *
 * @author Lars Johansson
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsElement
 */
public class FbsClient implements AutoCloseable {

    private static final Logger LOGGER = Logger.getLogger(FbsClient.class.getName());
    private static final String COULD_NOT_RETRIEVE_RESPONSE = "Could not retrieve data from service at %s.";

    // Note
    //     CHESS
    //         - reference Joakim Meyer
    //
    //     ITIP - integration platform with CHESS information
    //         - reference Deepak Jakkareddy, Staffan Sjöberg
    //
    //     FBS - Facility Breakdown Structure
    //
    //     Features of FBS application URL
    //         parameters
    //             CableName, ESSName, tag, id, max_results
    //         capabilities
    //             lookup
    //                 CableName, ESSName, tag, id
    //             search
    //                 tag. possibly max_results
    //         note
    //             content of FbsElement may be subset of available information
    //             lower case, upper case for parameter names
    //             parameter values without " but possibly with = as it may be in tag value
    //             max_results parameter only together with other parameter, default value 25
    //         examples, parameters
    //             CableName=21F009765
    //             ESSName=TS2-010Row:CnPw-U-006
    //             tag==ESS.INFR
    //             id=ESS-0333364
    //             tag=%ESS.INFR%
    //             tag=%ESS.INFR%&max_results=35

    // Note lower and upper case
    private static final String CABLE_NAME  = "CableName";
    private static final String ESS_NAME    = "ESSName";
    private static final String ID          = "id";
    private static final String TAG         = "tag";
    private static final String MAX_RESULTS = "max_results";

    // REST client for retrieval of FBS data from ITIP; effectively ITIP client
    @Nonnull
    private final Client client = ClientBuilder.newClient();

    private final String url;

    /**
     * Constructor.
     *
     * @param url base URL of FBS webservice.
     */
    public FbsClient(String url) {
        this.url = url;
    }

    @Override
    public void close() {
        client.close();
    }

    /**
     * Retrieve (all) FBS data and deserializes it into a FbsElement list.
     *
     * <p>
     * Note no parameter.
     *
     * @return {@link List} of all {@link FbsElement}
     */
    public List<FbsElement> getFbsListing() {
        return getFbsListing(url, null);
    }

    /**
     * Retrieve FBS data for cable name and deserializes it into a FbsElement list.
     *
     * <p>
     * Note
     * <ul>
     * <li><tt>null</tt> returned if empty or <tt>null</tt> parameter
     * <li>method encodes given parameter
     * </ul>
     *
     * @param cableName cable name
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<FbsElement> getFbsListingForCableName(String cableName) {
        if (StringUtils.isEmpty(StringUtils.trim(cableName))) {
            return null;
        }

        return getFbsListing(url, ImmutableMap.of(CABLE_NAME, EncodingUtility.encode(cableName)));
    }

    /**
     * Retrieve FBS data for ESS name and deserializes it into a FbsElement list.
     *
     * <p>
     * Note
     * <ul>
     * <li><tt>null</tt> returned if empty or <tt>null</tt> parameter
     * <li>method encodes given parameter
     * </ul>
     *
     * @param essName ESS name
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<FbsElement> getFbsListingForEssName(String essName) {
        if (StringUtils.isEmpty(StringUtils.trim(essName))) {
            return null;
        }

        return getFbsListing(url, ImmutableMap.of(ESS_NAME, EncodingUtility.encode(essName)));
    }

    /**
     * Retrieve FBS data for id and deserializes it into a FbsElement list.
     *
     * <p>
     * Note
     * <ul>
     * <li><tt>null</tt> returned if empty or <tt>null</tt> value parameter
     * <li>method encodes given parameter
     * </ul>
     *
     * @param tag FBS tag
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<FbsElement> getFbsListingForId(String id) {
        if (StringUtils.isEmpty(StringUtils.trim(id))) {
            return null;
        }

        return getFbsListing(url, ImmutableMap.of(ID, EncodingUtility.encode(id)));
    }

    /**
     * Retrieve FBS data for FBS tag and deserializes it into a FbsElement list.
     *
     * <p>
     * Note
     * <ul>
     * <li><tt>null</tt> returned if empty or <tt>null</tt> value parameter
     * <li>method encodes given parameter
     * </ul>
     *
     * @param tag FBS tag
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<FbsElement> getFbsListingForTag(String tag) {
        if (StringUtils.isEmpty(StringUtils.trim(tag))) {
            return null;
        }

        return getFbsListing(url, ImmutableMap.of(TAG, EncodingUtility.encode(tag)));
    }

    /**
     * Retrieve FBS data for FBS tag and deserializes it into a FbsElement list of up to <tt>maxResults</tt> elements.
     *
     * <p>
     * Note
     * <ul>
     * <li><tt>null</tt> returned if empty or <tt>null</tt> value parameter
     * <li><tt>null</tt> returned if negative maxResults parameter
     * <li>method encodes given parameter
     * </ul>
     *
     * @param tag query, FBS tag part
     * @param maxResults maximum number of results
     * @return {@link List} of matching {@link FbsElement}
     */
    public List<FbsElement> getFbsListingForTag(String tag, int maxResults) {
        if (StringUtils.isEmpty(StringUtils.trim(tag))) {
            return null;
        }
        if (maxResults < 0) {
            return null;
        }

        return getFbsListing(url, ImmutableMap.of(TAG, EncodingUtility.encode(tag),
                MAX_RESULTS, String.valueOf(maxResults)));
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Retrieve FBS data for given url and parameterized query and deserializes it into a FbsElement list.
     *
     * @param url base URL of FBS webservice
     * @param params map of query parameters (name-value pairs)
     * @return {@link List} of retrieved {@link FbsElement}
     */
    private List<FbsElement> getFbsListing(String url, Map<String, String> params) {
        if (StringUtils.isEmpty(StringUtils.trim(url))) {
            return null;
        }

        UriBuilder uriBuilder = WebserviceUtility.getUriBuilder(url, params);

        long time = System.nanoTime();
        try (final ClosableResponse response = new ClosableResponse(
                client.target(uriBuilder).request(new MediaType[]{MediaType.APPLICATION_JSON_TYPE}).get())) {
            if (response.getStatus() == 200) {
                List<FbsElement> fbsElements = response.readEntity(new GenericType<List<FbsElement>>() {
                });
                if (fbsElements != null) {
                    LOGGER.log(Level.INFO,"FBS service call, count " + fbsElements.size() + ", runtime: "
                            + (System.nanoTime() - time) / 1_000_000 + " ms (URL: " + (uriBuilder != null ?
                            uriBuilder.build().toString() : url) + ")");
                }

                return fbsElements;
            } else {
                throw new ResponseException(String.format(COULD_NOT_RETRIEVE_RESPONSE, url) + " "
                        + response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, String.format(COULD_NOT_RETRIEVE_RESPONSE, url), e);
            throw new ResponseException(String.format(COULD_NOT_RETRIEVE_RESPONSE, url), e);
        }
    }

}
