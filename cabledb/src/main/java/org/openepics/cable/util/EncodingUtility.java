/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Utility class for encoding a value according to encoding scheme.
 *
 * @author Lars Johansson
 *
 * @see EncodingUtility#ENCODING_SCHEME
 */
public class EncodingUtility {

    public static final String ENCODING_SCHEME = "UTF-8";

    /**
     * This class is not to be instantiated.
     */
    private EncodingUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Encode a <code>String</code> and return the encoded value.
     *
     * @param s the string to encode
     * @return the encoded value
     *
     * @see EncodingUtility#ENCODING_SCHEME
     * @see URLEncoder#encode(String, String)
     */
    public static String encode(String s) {
        try {
            return URLEncoder.encode(s, ENCODING_SCHEME);
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    /**
     * Decode a <code>String</code> and return the decoded value.
     *
     * @param s the string to decode
     * @return the decoded value
     *
     * @see EncodingUtility#ENCODING_SCHEME
     * @see URLDecoder#decode(String, String)
     */
    public static String decode(String s) {
        try {
            return URLDecoder.decode(s, ENCODING_SCHEME);
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

}
