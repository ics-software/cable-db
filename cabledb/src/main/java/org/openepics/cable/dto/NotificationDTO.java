/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.dto;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.services.dl.CableColumn;

/**
 * Utility class for handling information that is to be communicated to users through notification.
 * Class is also used for generating reports about content in Cable application.
 *
 * @author Imre Toth <imre.toth@esss.se>
 * @author Lars Johansson
 */
public class NotificationDTO {

    private static final String EMPTY = "(EMPTY)";

    private Cable cable;
    private Endpoint endpoint;
    private String cableName;
    private CableColumn cableColumn;
    private String comment;
    private String oldValue;
    private String newValue;

    /**
     * Constructs a new data transfer object for notification.
     *
     * @param cable cable object
     * @param endpoint endpoint object
     * @param cableName cable name
     * @param cableColumn cable column
     */
    public NotificationDTO(
            Cable cable, Endpoint endpoint, String cableName,
            CableColumn cableColumn) {
        this(cable, endpoint, cableName, cableColumn, null, null, null);
    }

    /**
     * Constructs a new data transfer object for notification.
     *
     * @param cable cable object
     * @param endpoint endpoint object
     * @param cableName cable name
     * @param cableColumn cable column
     * @param comment comment
     */
    public NotificationDTO(
            Cable cable, Endpoint endpoint, String cableName,
            CableColumn cableColumn, String comment) {
        this(cable, endpoint, cableName, cableColumn, comment, null, null);
    }

    /**
     * Constructs a new data transfer object for notification.
     *
     * @param cable cable object
     * @param endpoint endpoint object
     * @param cableName cable name
     * @param cableColumn cable column
     * @param comment comment
     * @param oldValue old value
     * @param newValue new value
     */
    public NotificationDTO(
            Cable cable, Endpoint endpoint, String cableName,
            CableColumn cableColumn, String comment, String oldValue, String newValue) {
        this.cable = cable;
        this.endpoint= endpoint;
        this.cableName = cableName;
        this.cableColumn = cableColumn;
        this.comment = comment;
        this.oldValue = StringUtils.isEmpty(oldValue) ? EMPTY : oldValue;
        this.newValue = StringUtils.isEmpty(newValue) ? EMPTY : newValue;
    }

    public Cable getCable() {
        return cable;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public String getCableName() {
        return cableName;
    }

    public CableColumn getCableColumn() {
        return cableColumn;
    }

    public String getComment() {
        return comment;
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

}
