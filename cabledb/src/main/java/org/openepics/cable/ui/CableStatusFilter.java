/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

/**
 * This represents the types of filters for cable status.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableStatusFilter {
    /** Filters all cables. */
    ALL("All"),
    /** Filters all cables that aren't deleted. */
    NON_DELETED("Non-deleted"),
    /** Filters all deleted cables. */
    DELETED("Deleted"),
    /** Filters all Pulled cables */
    PULLED("Pulled"),
    /** Filters all commissioned cables */
    COMMISSIONED("Commissioned"),
    /** Filters all not in use cables */
    NOT_IN_USE("Not in use");

    private final String displayName;

    private CableStatusFilter(String displayName) {
        this.displayName = displayName;
    }

    /** @return the display name for this filter */
    public String getDisplayName() {
        return displayName;
    }
}
