/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.services.dl.CableArticleColumn;

/**
 * Enum wrapper for CableArticleColumn. Contains information for displaying cable article column,
 * wrapping only columns that are available to be displayed in cable data table.
 *
 * @author Lars Johansson
 */
public enum CableArticleColumnUI {

    MANUFACTURER(CableArticleColumn.MANUFACTURER,
            CableArticleColumnUIConstants.MANUFACTURER_VALUE,
            null,
            CableArticleColumnUIConstants.MANUFACTURER_TOOLTIP,
            CableArticleColumnUIConstants.MANUFACTURER_STYLECLASS,
            CableArticleColumnUIConstants.MANUFACTURER_STYLE,
            CableArticleColumnUIConstants.MANUFACTURER_FILTERMODE,
            CableArticleColumnUIConstants.MANUFACTURER_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    EXTERNAL_ID(CableArticleColumn.EXTERNAL_ID,
            CableArticleColumnUIConstants.EXTERNAL_ID_VALUE,
            null,
            CableArticleColumnUIConstants.EXTERNAL_ID_TOOLTIP,
            CableArticleColumnUIConstants.EXTERNAL_ID_STYLECLASS,
            CableArticleColumnUIConstants.EXTERNAL_ID_STYLE,
            CableArticleColumnUIConstants.EXTERNAL_ID_FILTERMODE,
            CableArticleColumnUIConstants.EXTERNAL_ID_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    ERP_NUMBER(CableArticleColumn.ERP_NUMBER,
            CableArticleColumnUIConstants.ERP_NUMBER_VALUE,
            null,
            CableArticleColumnUIConstants.ERP_NUMBER_TOOLTIP,
            CableArticleColumnUIConstants.ERP_NUMBER_STYLECLASS,
            CableArticleColumnUIConstants.ERP_NUMBER_STYLE,
            CableArticleColumnUIConstants.ERP_NUMBER_FILTERMODE,
            CableArticleColumnUIConstants.ERP_NUMBER_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    ISO_CLASS(CableArticleColumn.ISO_CLASS,
            CableArticleColumnUIConstants.ISO_CLASS_VALUE,
            null,
            CableArticleColumnUIConstants.ISO_CLASS_TOOLTIP,
            CableArticleColumnUIConstants.ISO_CLASS_STYLECLASS,
            CableArticleColumnUIConstants.ISO_CLASS_STYLE,
            CableArticleColumnUIConstants.ISO_CLASS_FILTERMODE,
            CableArticleColumnUIConstants.ISO_CLASS_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    DESCRIPTION(CableArticleColumn.DESCRIPTION,
            CableArticleColumnUIConstants.DESCRIPTION_VALUE,
            null,
            CableArticleColumnUIConstants.DESCRIPTION_TOOLTIP,
            CableArticleColumnUIConstants.DESCRIPTION_STYLECLASS,
            CableArticleColumnUIConstants.DESCRIPTION_STYLE,
            CableArticleColumnUIConstants.DESCRIPTION_FILTERMODE,
            CableArticleColumnUIConstants.DESCRIPTION_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    LONG_DESCRIPTION(CableArticleColumn.LONG_DESCRIPTION,
            CableArticleColumnUIConstants.LONG_DESCRIPTION_VALUE,
            null,
            CableArticleColumnUIConstants.LONG_DESCRIPTION_TOOLTIP,
            CableArticleColumnUIConstants.LONG_DESCRIPTION_STYLECLASS,
            CableArticleColumnUIConstants.LONG_DESCRIPTION_STYLE,
            CableArticleColumnUIConstants.LONG_DESCRIPTION_FILTERMODE,
            CableArticleColumnUIConstants.LONG_DESCRIPTION_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    MODEL_TYPE(CableArticleColumn.MODEL_TYPE,
            CableArticleColumnUIConstants.MODEL_TYPE_VALUE,
            null,
            CableArticleColumnUIConstants.MODEL_TYPE_TOOLTIP,
            CableArticleColumnUIConstants.MODEL_TYPE_STYLECLASS,
            CableArticleColumnUIConstants.MODEL_TYPE_STYLE,
            CableArticleColumnUIConstants.MODEL_TYPE_FILTERMODE,
            CableArticleColumnUIConstants.MODEL_TYPE_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    BENDING_RADIUS(CableArticleColumn.BENDING_RADIUS,
            CableArticleColumnUIConstants.BENDING_RADIUS_VALUE,
            null,
            CableArticleColumnUIConstants.BENDING_RADIUS_TOOLTIP,
            CableArticleColumnUIConstants.BENDING_RADIUS_STYLECLASS,
            CableArticleColumnUIConstants.BENDING_RADIUS_STYLE,
            CableArticleColumnUIConstants.BENDING_RADIUS_FILTERMODE,
            CableArticleColumnUIConstants.BENDING_RADIUS_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    OUTER_DIAMETER(CableArticleColumn.OUTER_DIAMETER,
            CableArticleColumnUIConstants.OUTER_DIAMETER_VALUE,
            null,
            CableArticleColumnUIConstants.OUTER_DIAMETER_TOOLTIP,
            CableArticleColumnUIConstants.OUTER_DIAMETER_STYLECLASS,
            CableArticleColumnUIConstants.OUTER_DIAMETER_STYLE,
            CableArticleColumnUIConstants.OUTER_DIAMETER_FILTERMODE,
            CableArticleColumnUIConstants.OUTER_DIAMETER_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    RATED_VOLTAGE(CableArticleColumn.RATED_VOLTAGE,
            CableArticleColumnUIConstants.RATED_VOLTAGE_VALUE,
            null,
            CableArticleColumnUIConstants.RATED_VOLTAGE_TOOLTIP,
            CableArticleColumnUIConstants.RATED_VOLTAGE_STYLECLASS,
            CableArticleColumnUIConstants.RATED_VOLTAGE_STYLE,
            CableArticleColumnUIConstants.RATED_VOLTAGE_FILTERMODE,
            CableArticleColumnUIConstants.RATED_VOLTAGE_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    WEIGHT_PER_LENGTH(CableArticleColumn.WEIGHT_PER_LENGTH,
            CableArticleColumnUIConstants.WEIGHT_PER_LENGTH_VALUE,
            null,
            CableArticleColumnUIConstants.WEIGHT_PER_LENGTH_TOOLTIP,
            CableArticleColumnUIConstants.WEIGHT_PER_LENGTH_STYLECLASS,
            CableArticleColumnUIConstants.WEIGHT_PER_LENGTH_STYLE,
            CableArticleColumnUIConstants.WEIGHT_PER_LENGTH_FILTERMODE,
            CableArticleColumnUIConstants.WEIGHT_PER_LENGTH_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    SHORT_NAME(CableArticleColumn.SHORT_NAME,
            CableArticleColumnUIConstants.SHORT_NAME_VALUE,
            null,
            CableArticleColumnUIConstants.SHORT_NAME_TOOLTIP,
            CableArticleColumnUIConstants.SHORT_NAME_STYLECLASS,
            CableArticleColumnUIConstants.SHORT_NAME_STYLE,
            CableArticleColumnUIConstants.SHORT_NAME_FILTERMODE,
            CableArticleColumnUIConstants.SHORT_NAME_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    SHORT_NAME_EXTERNAL_ID(CableArticleColumn.SHORT_NAME_EXTERNAL_ID,
            CableArticleColumnUIConstants.SHORT_NAME_EXTERNAL_ID_VALUE,
            null,
            CableArticleColumnUIConstants.SHORT_NAME_EXTERNAL_ID_TOOLTIP,
            CableArticleColumnUIConstants.SHORT_NAME_EXTERNAL_ID_STYLECLASS,
            CableArticleColumnUIConstants.SHORT_NAME_EXTERNAL_ID_STYLE,
            CableArticleColumnUIConstants.SHORT_NAME_EXTERNAL_ID_FILTERMODE,
            CableArticleColumnUIConstants.SHORT_NAME_EXTERNAL_ID_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    URL_CHESS_PART(CableArticleColumn.URL_CHESS_PART,
            CableArticleColumnUIConstants.URL_CHESS_PART_VALUE,
            null,
            CableArticleColumnUIConstants.URL_CHESS_PART_TOOLTIP,
            CableArticleColumnUIConstants.URL_CHESS_PART_STYLECLASS,
            CableArticleColumnUIConstants.URL_CHESS_PART_STYLE,
            CableArticleColumnUIConstants.URL_CHESS_PART_FILTERMODE,
            CableArticleColumnUIConstants.URL_CHESS_PART_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    STATUS(CableArticleColumn.STATUS,
            CableArticleColumnUIConstants.STATUS_VALUE,
            null,
            CableArticleColumnUIConstants.STATUS_TOOLTIP,
            CableArticleColumnUIConstants.STATUS_STYLECLASS,
            CableArticleColumnUIConstants.STATUS_STYLE,
            CableArticleColumnUIConstants.STATUS_FILTERMODE,
            CableArticleColumnUIConstants.STATUS_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private CableArticleColumn parent;
    private String tooltip;
    private String value;
    private String url;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private boolean filterDropdown;
    private CableColumnStyle columnStyle;

    /**
     * InstallationPackageColumnUI constructor decides what labels in column to display according to property.
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param url
     *            property for url
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     * @param cableColumnStyleEnum
     *            column style
     */
    private CableArticleColumnUI(CableArticleColumn parent,
            String value, String url, String tooltip,
            String styleClass, String style, String filterMode, String filterStyle,
            CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.url = url;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
        if (CableArticleColumnUIConstants.STATUS_VALUE.equals(value)) {
            this.filterDropdown = true;
        }
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /** @return String url */
    public String getUrl() {
        return url;
    }

    /**
     * Searches for CableArticleColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return CableArticleColumnUI enum
     */
    public static CableArticleColumnUI convertColumnLabel(String columnLabel) {
        for (CableArticleColumnUI value : CableArticleColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (CableArticleColumnUI value : CableArticleColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public boolean isFilterDropdown() {
        return this.filterDropdown;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }

    public boolean isSortable(){
        return StringUtils.isNotEmpty(this.value);
    }

}
