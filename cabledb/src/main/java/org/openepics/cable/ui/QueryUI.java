/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.List;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.services.DateUtil;

/**
 * <code>QueryUI</code> is a presentation of {@link Query} used in UI.
 */
public class QueryUI implements Serializable {

    private static final long serialVersionUID = -5599461109679532439L;

    private Query query;

    /**
     * Constructs Query UI object for given query instance.
     *
     * @param query query instance
     */
    public QueryUI(Query query) {
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    public Long getId() {
        return query.getId();
    }

    public String getCreated() {
        return DateUtil.format(query.getCreated());
    }

    public String getDescription() {
        return query.getDescription();
    }

    public void setDescription(String description) {
        query.setDescription(description);
    }

    public String getOwner() {
        return query.getOwner();
    }

    public EntityType getEntityType() {
        return query.getEntityType();
    }

    public void setEntityType(EntityType entityType) {
        query.setEntityType(entityType);
    }

    public List<QueryCondition> getQueryConditions() {
        return query.getConditions();
    }

    public void setQueryConditions(List<QueryCondition> conditions) {
        query.setConditions(conditions);
    }
}
