/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.CableArticle;
import org.openepics.cable.services.CableArticleService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.dl.CableArticleImportExportService;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.Utility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the backing requests bean for cable-articles.xhtml.
 *
 * @author Lars Johansson
 */
@ManagedBean
@ViewScoped
public class CableArticleRequestManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3468667652862118975L;

    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final Logger LOGGER = Logger.getLogger(CableArticleRequestManager.class.getName());
    private static final List<CableArticleUI> EMPTY_LIST = new ArrayList<>();

    private static final String[] STANDARD_PARTS = new String[] {CableArticle.Y, CableArticle.N};
    private List<CableArticleColumnUI> columns;
    private List<String> columnTemplate = CableArticleColumnUI.getAllColumns();

    @Inject
    private transient CableArticleService cableArticleService;
    @Inject
    private transient CableArticleImportExportService cableArticleImportExportService;

    @Inject
    private SessionService sessionService;

    private List<CableArticleUI> cableArticles;
    private List<CableArticleUI> filteredCableArticles;
    private List<CableArticleUI> selectedCableArticles = EMPTY_LIST;
    private List<CableArticleUI> deletedCableArticles;

    private String globalFilter;

    private CableArticleUI selectedCableArticle;
    private boolean isAddPopupOpened;

    // for overlay panels
    private String displayDescription;
    private String displayService;
    private String displayComments;

    private boolean isCableArticleRequested;
    private String requestedCableArticleName;

    private byte[] fileToBeImported;
    private LoaderResult<CableArticle> importResult;
    private String importFileName;
    private int cableArticlesToExportSize = 0;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;
    private String longOverlayURL;

    private CableArticle oldCableArticle;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public CableArticleRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns + history column
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = CableArticleColumnUI.values().length + 1;
        columnVisibility = ManagerUtil.setAllColumnVisible(numberOfColumns);
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display. */
    @PostConstruct
    public void init() {
        try {
            isAddPopupOpened = false;
            clearImportState();
            selectedCableArticles.clear();
            selectedCableArticle = null;
            requestedCableArticleName =
                    ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                    .getParameter("cableArticleName");
            createDynamicColumns();
            refreshCableArticles();

            // prepare datatable
            //     cookies
            //         rows per page
            //         column visibility
            //     row number (in all rows) for requested entry (if any)
            Cookie[] cookies = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                            initPaginationPageSize(cookie.getValue());
                            break;
                        case CookieUtility.CD_CABLEARTICLE_COLUMN_VISIBILITY:
                            initColumnVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }
            rowNumber = rowNumber(requestedCableArticleName);
        } catch (Exception e) {
            throw new UIException("Cable article display initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        ManagerUtil.initColumnVisibility(visibility, numberOfColumns, columnVisibility);
    }

    /**
     * Find out row number, if applicable, for selected entry in list of all entries.
     *
     * @param cableArticleName
     * @return
     */
    private int rowNumber(String cableArticleName) {
        // note
        //     currently not consider filter

        CableArticleUI cableArticleToSelect = null;
        this.isCableArticleRequested = false;
        if (requestedCableArticleName != null) {
            isCableArticleRequested = true;
            cableArticleToSelect =
                    getCableArticleFromCableArticleName(requestedCableArticleName);
        }

        selectedCableArticles.clear();
        if (cableArticleToSelect != null) {
            selectedCableArticle = cableArticleToSelect;
            selectedCableArticles.add(selectedCableArticle);

            int elementPosition = 0;
            for (CableArticleUI cableArticle : cableArticles) {
                if (cableArticle.getName().equals(requestedCableArticleName)) {
                    return elementPosition;
                }
                elementPosition++;
            }
        }

        return 0;
    }

    private CableArticleUI getCableArticleFromCableArticleName(
            final String cableArticleName) {

        if (cableArticleName == null || cableArticleName.isEmpty()) {
            return null;
        }

        CableArticleUI cableArticleToSelect = null;
        for (CableArticleUI cableArticle : cableArticles) {
            if (cableArticle.getName().equals(cableArticleName)) {
                cableArticleToSelect = cableArticle;
            }
        }
        return cableArticleToSelect;
    }

    /** Refreshes cable articles list. */
    private void refreshCableArticles() {
        cableArticles = buildCableArticleUIs();
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /**
     * Returns the cable articles to be exported,
     * which are the currently filtered and selected cable articles,
     * or all filtered cables without deleted ones if none selected.
     *
     * @return the cable articles to be exported
     */
    public List<CableArticleUI> getCableArticlesToExport() {

        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        final List<CableArticleUI> cableArticlesToExport = new ArrayList<>();
        for (CableArticleUI cableArticle : getCableArticles()) {
            if (isIncludedByFilter(cableArticle) && cableArticle.isActive())
                cableArticlesToExport.add(cableArticle);
        }
        LOGGER.fine("Returning cable articles to export: " + cableArticlesToExport.size());
        return cableArticlesToExport;
    }

    private boolean isIncludedByFilter(CableArticleUI cableArticle) {
        return !filteredCableArticlesExist() || getFilteredCableArticles().contains(cableArticle);
    }

    private boolean filteredCableArticlesExist() {
        return getFilteredCableArticles() != null && !getFilteredCableArticles().isEmpty();
    }

    /** @return the result of a test or true import */
    public LoaderResult<CableArticle> getImportResult() {
        return importResult;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear import state.");
        fileToBeImported = null;
        importFileName = null;
        importResult = null;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void cableArticleFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked cable article file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            importFileName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the cable article import from the file that was last uploaded.
     */
    public void cableArticleImportTest() {
        LOGGER.fine("Invoked cable article import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableArticleImportExportService.importCableArticles(inputStream, true);
                LOGGER.fine("Import test result: " + importResult.toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the cable article import from the file that was last uploaded.
     */
    public void cableArticleImport() {
        LOGGER.fine("Invoked cable article import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableArticleImportExportService.importCableArticles(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshCableArticles();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /** @return the cable article sheet with cable articles that were just imported */
    public StreamedContent getCableArticleSheetWithImportedCableArticles() {

        // submit back only non-deleted cable articles
        final List<CableArticle> createdOrUpdatedCableArticles = new ArrayList<>();
        for (CableArticle cableArticle : importResult.getAffected()) {
            if (cableArticle.isActive())
                createdOrUpdatedCableArticles.add(cableArticle);
        }
        return new DefaultStreamedContent(
                cableArticleImportExportService.exportCableArticles(createdOrUpdatedCableArticles),
                Utility.XLSX_CONTENT_TYPE,
                importFileName);
    }

    /** @return the cable article sheet with the cable articles from cableArticlesToExport list */
    public StreamedContent getCableArticleSheetWithCableArticlesToExport() {

        final List<CableArticle> cableArticlesToExport = new ArrayList<>();
        for (CableArticleUI cableArticleUI : getCableArticlesToExport()) {
            cableArticlesToExport.add(cableArticleUI.getCableArticle());
        }
        return new DefaultStreamedContent(
                cableArticleImportExportService.exportCableArticles(cableArticlesToExport),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_cable_articles.xlsx");
    }

    /** @return the cable article import template */
    public StreamedContent getImportTemplate() {
        LOGGER.fine("Get cable articles template");
        return new DefaultStreamedContent(
                cableArticleImportExportService.exportCableArticles(new ArrayList<>()),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_cable_articles.xlsx");
    }

    /** @return current cable article list. */
    private List<CableArticleUI> buildCableArticleUIs() {

        final List<CableArticleUI> cableArticleUIs = new ArrayList<CableArticleUI>();
        List<CableArticle> cableArticles = cableArticleService.getCableArticles();

        selectedCableArticle = null;
        selectedCableArticles = EMPTY_LIST;

        for (CableArticle cableArticle : cableArticles) {
            final CableArticleUI cableArticleUI = new CableArticleUI(cableArticle);
            cableArticleUIs.add(cableArticleUI);
        }
        return cableArticleUIs;
    }

    /** @return true if edit cable article button is enabled, otherwise false. */
    public boolean isEditButtonEnabled() {
        return sessionService.isLoggedIn()
                && selectedCableArticle != null
                && selectedCableArticle.isActive()
                && sessionService.canAdminister();
    }

    /** @return true if delete cable article button is enabled, otherwise false. */
    public boolean isDeleteButtonEnabled() {
        if (selectedCableArticles == null || selectedCableArticles.isEmpty()) {
            return false;
        }
        for (CableArticleUI cableArticleToDelete : selectedCableArticles) {
            if (!cableArticleToDelete.isActive()) {
                return false;
            }
        }
        return sessionService.canAdminister();
    }

    public String getRequestedCableArticleName() {
        return requestedCableArticleName;
    }

    public void setRequestedCableArticleName(String requestedCableArticleName) {
        this.requestedCableArticleName = requestedCableArticleName;
    }

    public boolean isCableArticleRequested() {
        return isCableArticleRequested;
    }

    /** @return the cable articles to be displayed */
    public List<CableArticleUI> getCableArticles() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return cableArticles;
    }

    /** @return the filtered cable articles to be displayed */
    public List<CableArticleUI> getFilteredCableArticles() {
        return filteredCableArticles;
    }

    /**
     * @param filteredCableArticles
     *            the filtered cable articles to set
     */
    public void setFilteredCableArticles(List<CableArticleUI> filteredCableArticles) {
        this.filteredCableArticles = filteredCableArticles;
        LOGGER.fine("Setting filtered cable articles: "
                + (filteredCableArticles != null ? filteredCableArticles.size() : "no")
                + " cable articles.");
    }

    /** @return the global text filter */
    public String getGlobalFilter() {
        return globalFilter;
    }

    /**
     * @param globalFilter
     *            the global text filter to set
     */
    public void setGlobalFilter(String globalFilter) {
        this.globalFilter = globalFilter;
        LOGGER.fine("Setting global filter: " + this.globalFilter);
    }

    /** Refreshes cable articles list based on filters. */
    public void filterCableArticles() {
        LOGGER.fine("Invoked filter cable articles.");
        refreshCableArticles();
    }

    /** @return the currently selected cable articles, cannot return null */
    public List<CableArticleUI> getSelectedCableArticles() {
        return selectedCableArticles;
    }

    /**
     * @param selectedCableArticles
     *            the cable articles to select
     */
    public void setSelectedCableArticles(List<CableArticleUI> selectedCableArticles) {
        this.selectedCableArticles =
                selectedCableArticles != null ? selectedCableArticles : EMPTY_LIST;
        LOGGER.fine("Setting selected cable articles: " + this.selectedCableArticles.size());
    }

    /** Clears the current cable article selection. */
    public void clearSelectedCableArticles() {
        LOGGER.fine("Invoked clear cable article  selection.");
        setSelectedCableArticles(null);
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                + CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE
                + "', " + getRows()
                + ", {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);

            String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                    + CookieUtility.CD_INSTALLATION_PACKAGE_COLUMN_VISIBILITY
                    + "', '" + columnVisibility2String()
                    + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
            PrimeFaces.current().executeScript(statement);
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index for column in UI, counting from left
     * @return true if column is marked as visible
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Returns column visibility for given column.
     *
     * @param column given column
     * @return column visibility
     */
    public boolean isColumnVisible(CableArticleColumnUI column) {
        int colIndex = 0;
        for(int i=0; i<numberOfColumns; i++) {
            if(CableArticleColumnUI.values()[i].equals(column)) {
                colIndex = i;
                break;
            }
        }
        return columnVisibility.get(colIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected cable articles and row selection.
     */
    public void unselectAllRows() {
        clearSelectedCableArticles();

        onRowSelect();
    }

    /**
     * Event triggered when row is selected in table in UI.
     */
    public void onRowSelect() {
        if (selectedCableArticles != null && !selectedCableArticles.isEmpty()) {
            if (selectedCableArticles.size() == 1) {
                selectedCableArticle = selectedCableArticles.get(0);
            } else {
                selectedCableArticle = null;
            }
        } else {
            selectedCableArticle = null;
        }
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    /**
     * Prepare for Add cable article dialog.
     */
    public void prepareAddPopup() {
        selectedCableArticle = new CableArticleUI();
        isAddPopupOpened = true;
    }

    /**
     * Reset values. May be used at e.g. close of delete dialog.
     */
    public void resetValues() {
        clearSelectedCableArticles();
        deletedCableArticles = null;
    }

    /**
     * Event triggered when cable article is created.
     */
    public void onCableArticleAdd() {
        final String name = selectedCableArticle.getName();
        formatCableArticle(selectedCableArticle);
        cableArticleService.createCableArticle(selectedCableArticle.getCableArticle(), sessionService.getLoggedInName());
        filterCableArticles();
        UiUtility.showInfoMessage("Cable article '" + name + "' added.");
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * Prepare for Edit cable article dialog.
     */
    public void prepareEditPopup() {
        Preconditions.checkNotNull(selectedCableArticle);
        oldCableArticle = selectedCableArticle.getCableArticle();
        cableArticleService.detachCableArticle(oldCableArticle);
        selectedCableArticle =
                new CableArticleUI(
                        cableArticleService.getCableArticleById(selectedCableArticle.getId()));
        isAddPopupOpened = false;
    }

    /**
     * Event triggered when cable article is updated.
     */
    public void onCableArticleEdit() {
        Preconditions.checkNotNull(selectedCableArticle);
        formatCableArticle(selectedCableArticle);
        final CableArticle newCableArticle = selectedCableArticle.getCableArticle();
        final boolean updated = cableArticleService.updateCableArticle(newCableArticle, oldCableArticle, sessionService.getLoggedInName());
        filterCableArticles();
        final String name = newCableArticle.getName();
        UiUtility.showInfoMessage("Cable article '" + name + (updated ? "' updated." : "' not updated."));
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * @return the list of deleted cable articles.
     */
    public List<CableArticleUI> getDeletedCableArticles() {
        return deletedCableArticles;
    }

    /**
     * The method builds a list of cable articles that are already deleted.
     * If the list is not empty, it is displayed to the user and the user is prevented from deleting them.
     */
    public void checkCableArticlesForDeletion() {
        Preconditions.checkNotNull(selectedCableArticles);
        Preconditions.checkState(!selectedCableArticles.isEmpty());

        deletedCableArticles = Lists.newArrayList();
        for (final CableArticleUI cableArticleToDelete : selectedCableArticles) {
            if (!cableArticleToDelete.isActive()) {
                deletedCableArticles.add(cableArticleToDelete);
            }
        }
    }

    /**
     * Event triggered when cable article is deleted.
     */
    public void onCableArticleDelete() {
        Preconditions.checkNotNull(deletedCableArticles);
        Preconditions.checkState(deletedCableArticles.isEmpty());
        Preconditions.checkNotNull(selectedCableArticles);
        Preconditions.checkState(!selectedCableArticles.isEmpty());
        int deletedCableArticlesCounter = 0;
        for (final CableArticleUI cableArticleToDelete : selectedCableArticles) {
            if (cableArticleService.deleteCableArticle(cableArticleToDelete.getCableArticle(), sessionService.getLoggedInName())) {
                deletedCableArticlesCounter++;
            }
        }
        clearSelectedCableArticles();
        filteredCableArticles = null;
        deletedCableArticles = null;
        filterCableArticles();
        UiUtility.showInfoMessage("Deleted " + deletedCableArticlesCounter + (deletedCableArticlesCounter == 1 ? " cable article." : " cable articles."));
    }

    /**
     * Validate cable article manufacturer.
     * Throws ValidatorException if invalid.
     *
     * @param ctx faces context
     * @param component ui component
     * @param value cable article manufacturer
     */
    public void isCableArticleManufacturerValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article manufacturer with invalid value.", null));
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article manufacturer with invalid value.", null));
        }
    }

    /**
     * Validate cable article external id.
     * Throws ValidatorException if invalid.
     *
     * @param ctx faces context
     * @param component ui component
     * @param value cable article external id
     */
    public void isCableArticleExternalIdValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article external id with invalid value.", null));
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article external id with invalid value.", null));
        }
    }

    /**
     * Validate cable article epr number.
     * Throws ValidatorException if invalid.
     *
     * @param ctx faces context
     * @param component ui component
     * @param value cable article epr number
     */
    public void isCableArticleErpNumberValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article erp number with invalid value.", null));
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article erp number with invalid value.", null));
        }
    }

    /**
     * Validate cable article short name . external id (name).
     * Throws ValidatorException if invalid.
     *
     * @param ctx faces context
     * @param component ui component
     * @param value cable article short name . external id (name)
     */
    public void isCableArticleShortNameExternalIdValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article name with invalid value.", null));
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            throw new ValidatorException(
                    new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, "Cable article name with invalid value.", null));
        }
    }

    /**
     * Validates if entered value is Float number.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not Double number
     */
    public void isFloatEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isFloatEntered(value);
    }

    /** @return the selected cable article */
    public CableArticleUI getSelectedCableArticle() {
        return selectedCableArticle;
    }

    /**
     * Return true if the current user can edit cable article, else false.
     * Note check if username in cable freeze bypass usernames.
     *
     * @return true if the current user can edit cable article, else false
     */
    public boolean getEditCableArticle() {
        return sessionService.canAdminister()
                && Utility.isUsernameInCableFreezeBypassUsernames(sessionService.getLoggedInName());
    }

    /** @return description string for display. */
    public String getDisplayDescription() {
        return displayDescription;
    }

    /**
     * Sets description string.
     *
     * @param displayDescription
     *            string
     */
    public void setDisplayDescription(String displayDescription) {
        this.displayDescription = displayDescription;
    }

    /** Formats description string. */
    public void handleDisplayDescription() {
        displayDescription = formatString(displayDescription);
    }

    /** @return service string for display. */
    public String getDisplayService() {
        return displayService;
    }

    /**
     * Sets service string.
     *
     * @param displayService
     *            service string
     */
    public void setDisplayService(String displayService) {
        this.displayService = displayService;
    }

    /** Formats service string. */
    public void handleDisplayService() {
        displayService = formatString(displayService);
    }

    /** @return comments string for display. */
    public String getDisplayComments() {
        return displayComments;
    }

    /**
     * Sets comments string.
     *
     * @param displayComments
     *            comments string.
     */
    public void setDisplayComments(String displayComments) {
        this.displayComments = displayComments;
    }

    /** Formats comments string. */
    public void handleDisplayComments() {
        displayComments = formatString(displayComments);
    }

    /**
     * Breaks string if it is too long.
     *
     * @param string
     *            string
     *
     * @return new string.
     */
    private String formatString(String string) {
        StringBuilder sb = new StringBuilder(string);
        int i = 0;
        while ((i = sb.indexOf(" ", i + 37)) != -1) {
            sb.replace(i, i + 1, "\n");
        }
        return sb.toString();
    }

    /** Resets column template to default. */
    private void resetColumnTemplate() {
        columnTemplate = CableArticleColumnUI.getAllColumns();
    }

    /** Resets displayView to default. */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in cable article data table
     *
     * @return columns
     */
    public List<CableArticleColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<CableArticleColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();
            CableArticleColumnUI column = CableArticleColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(":cableArticleTableForm:cableArticleTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /** @return number of column in current display view */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    /**
     * Formats the given cable article by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedCableArticle
     *            cable article to format
     */
    private void formatCableArticle(CableArticleUI selectedCableArticle) {
        selectedCableArticle.setDescription(
                Utility.formatWhitespace(selectedCableArticle.getDescription()));
        //  no add/edit of revision, thus no format of revision
    }

    /**
     * Return counter for number of cable articles to be exported.
     *
     * @return number of cable articles to be exported
     */
    public int getCableArticlesToExportSize() {
        return cableArticlesToExportSize;
    }

    /**
     * Update counter for number of cable articles to be exported.
     */
    public void updateCableArticlesToExportSize() {
        this.cableArticlesToExportSize = getCableArticlesToExport().size();
    }

    /**
     * Return true if the current user can import cable articles, else false.
     * Note check if username in cable freeze bypass usernames.
     *
     * @return true if the current user can import cable articles, else false
     */
    public boolean canImportCableArticles() {
        return cableArticleImportExportService.canImportCableArticles()
                && Utility.isUsernameInCableFreezeBypassUsernames(sessionService.getLoggedInName());
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Sets the URL for the long general popup.
     *
     * @param longOverlayURL
     *            the url to where the popup is pointing.
     */
    public void setLongOverlayURL(String longOverlayURL) {
        this.longOverlayURL = longOverlayURL;
    }

    /** @return URL for the long general popup. */
    public String getLongOverlayURL() {
        return this.longOverlayURL;
    }

    /** Clears the newly created selected cable article so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedCableArticle = null;
        }
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Provide suggestions for cable article given query to filter for.
     *
     * @param query query to filter for
     * @return suggestions for cable article given query to filter for.
     */
    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn()) {
            return Collections.emptyList();
        }

        final List<String> selectItems = new ArrayList<>();
        FacesContext context = FacesContext.getCurrentInstance();
        CableArticleColumnUI column =
                (CableArticleColumnUI) UIComponent.getCurrentComponent(context).getAttributes().get("column");

        if (CableArticleColumnUI.STATUS.equals(column)) {
            selectItems.add(CableArticleUI.STATUS_OBSOLETE);
            selectItems.add(CableArticleUI.STATUS_VALID);
        }
        return selectItems;
    }

    /**
     * Return tooltip for cable article link.
     *
     * @param label label
     * @param column column
     * @param value (part of) tooltip
     * @return tooltip for cable article link
     */
    public String tooltipForLink(String label, String column, String value) {
        switch (column) {
            case CableArticleColumnUIConstants.URL_CHESS_PART_VALUE:
                return "Click to open navigation dialog to " + value;
            default:
                return null;
        }
    }

    /**
     * Return tooltip for cable article column.
     *
     * @param column cable article column
     * @return tooltip for cable article column
     */
    public String tooltipForCableArticle(CableArticleColumnUI column) {
        String result;
        switch (column) {
            case DESCRIPTION:
                result = "Open Description dialog";
                break;
            default:
                result = "Open "+ column.getValue() + " dialog";
        }
        return result;
    }

    /**
     * @return available standard parts.
     */
    public String[] getStandardParts() {
        return STANDARD_PARTS;
    }

}
