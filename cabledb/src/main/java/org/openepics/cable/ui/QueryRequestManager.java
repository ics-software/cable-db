/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.services.QueryService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.dl.CableColumn;
import org.openepics.cable.services.dl.CableTypeColumn;

/**
 * This is the backing requests bean for query.xhtml.
 */
@ManagedBean
@ViewScoped
public class QueryRequestManager implements Serializable {

    private static final long serialVersionUID = 4780839104285445681L;

    private static final String EMPTY_VALUE = "<Please define>";
    private static final List<QueryUI> EMPTY_LIST = new ArrayList<>();
    private static final Logger LOGGER = Logger.getLogger(QueryRequestManager.class.getName());

    @Inject
    private SessionService sessionService;
    @Inject
    private transient QueryService queryService;

    private List<QueryUI> queries;
    private List<QueryUI> filteredQueries;
    private List<QueryUI> mostRecentQueries;
    private List<QueryCondition> queryConditions;

    private List<QueryCondition> selectedQueryConditions;
    private List<String> validationMessages;
    private QueryUI selectedQuery;
    private String description;
    private boolean isQuerySelected;
    private boolean isQueryConditionSelected;
    private boolean isCableQuery;

    /**
     * Initializes the bean for initial view display.
     */
    @PostConstruct
    public void init() {
        try {
            String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
            isCableQuery = "/cables.xhtml".equals(viewId);
            refreshQueries(null);
            refreshMostRecentQueries();
        } catch (Exception e) {
            throw new UIException("Query manager display initialization failed: " + e.getMessage(), e);
        }
    }

    /**
     * Event triggered when query is selected.
     */
    public void onQuerySelect() {
        if (selectedQuery != null) {
            selectQuery(selectedQuery);
        } else {
            unselectQuery();
        }
    }

    /**
     * Event triggered when query condition is selected.
     */
    public void onQueryConditionSelect() {
        if (selectedQuery != null && selectedQueryConditions != null && !selectedQueryConditions.isEmpty()) {
            isQueryConditionSelected = true;
        } else {
            selectedQueryConditions = null;
            isQueryConditionSelected = false;
        }
    }

    /**
     * @return list of queries.
     */
    public List<QueryUI> getQueries() {
        if (!sessionService.isLoggedIn()) {
            return EMPTY_LIST;
        }
        if (queries == EMPTY_LIST) {
            refreshQueries(null);
        }
        return queries;
    }

    /**
     * @return list of most recent queries (last three queries).
     */
    public List<QueryUI> getMostRecentQueries() {
        if (!sessionService.isLoggedIn()) {
            return EMPTY_LIST;
        }
        if (mostRecentQueries == EMPTY_LIST) {
            refreshMostRecentQueries();
        }
        return mostRecentQueries;
    }

    /** @return most recent executed query id. */
    public long getFirstMostRecentQueryId() {
        if (!mostRecentQueries.isEmpty()) {
            return mostRecentQueries.get(0).getId();
        }
        return -1;
    }

    /** @return second most recent executed query id. */
    public long getSecondMostRecentQueryId() {
        if (mostRecentQueries.size() >= 2) {
            return mostRecentQueries.get(1).getId();
        }
        return -1;
    }

    /** @return third most recent executed query id. */
    public long getThirdMostRecentQueryId() {
        if (mostRecentQueries.size() >= 3) {
            return mostRecentQueries.get(2).getId();
        }
        return -1;
    }

    /**
     * @return query conditions.
     */
    public List<QueryCondition> getQueryConditions() {
        return queryConditions;
    }

    /**
     * Set query conditions.
     *
     * @param queryConditions
     *            query conditions
     */
    public void setQueryConditions(List<QueryCondition> queryConditions) {
        this.queryConditions = queryConditions;
    }

    /**
     * @return filtered queries.
     */
    public List<QueryUI> getFilteredQueries() {
        return filteredQueries;
    }

    /**
     * Set filtered queries.
     *
     * @param filteredQueries
     *            filtered queries
     */
    public void setFilteredQueries(List<QueryUI> filteredQueries) {
        this.filteredQueries = filteredQueries;
    }

    /**
     * @return selected query.
     */
    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    /**
     * Sets selected query.
     *
     * @param selectedQuery
     *            selected query
     */
    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    /**
     * @return selected query conditions.
     */
    public List<QueryCondition> getSelectedQueryConditions() {
        return selectedQueryConditions;
    }

    /**
     * Sets selected query conditions.
     *
     * @param selectedQueryConditions
     *            selected query conditions
     */
    public void setSelectedQueryConditions(List<QueryCondition> selectedQueryConditions) {
        this.selectedQueryConditions = selectedQueryConditions;
    }

    /** @return query description. */
    public String getDescription() {
        return description;
    }

    /**
     * Sets query description.
     *
     * @param description
     *            query description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return true if query is selected, otherwise false.
     */
    public boolean getQuerySelected() {
        return isQuerySelected;
    }

    /**
     * Sets true if query is selected, otherwise false.
     *
     * @param isQuerySelected
     *            true if query is selected, otherwise false
     */
    public void setQuerySelected(boolean isQuerySelected) {
        this.isQuerySelected = isQuerySelected;
    }

    /**
     * @return true if query condition is selected, otherwise false.
     */
    public boolean getQueryConditionSelected() {
        return isQueryConditionSelected;
    }

    /**
     * Sets true if query condition is selected, otherwise false.
     *
     * @param isQueryConditionSelected
     *            true if query condition is selected, otherwise false.
     */
    public void setQueryConditionSelected(boolean isQueryConditionSelected) {
        this.isQueryConditionSelected = isQueryConditionSelected;
    }

    /**
     * @return list of all possible parenthesis open values.
     */
    public List<QueryParenthesis> getParenthesisOpenValues() {
        final List<QueryParenthesis> parenthesisValues = new ArrayList<>();
        for (QueryParenthesis parenthesis : QueryParenthesis.values()) {
            if (parenthesis == QueryParenthesis.CLOSE) {
                continue;
            }
            parenthesisValues.add(parenthesis);
        }
        return parenthesisValues;
    }

    /**
     * @return list of all possible parenthesis close values.
     */
    public List<QueryParenthesis> getParenthesisCloseValues() {
        final List<QueryParenthesis> parenthesisValues = new ArrayList<>();
        for (QueryParenthesis parenthesis : QueryParenthesis.values()) {
            if (parenthesis == QueryParenthesis.OPEN) {
                continue;
            }
            parenthesisValues.add(parenthesis);
        }
        return parenthesisValues;
    }

    /**
     * @return list of all possible cable field values.
     */
    public List<String> getFieldValues() {
        final List<String> fieldValues = new ArrayList<>();
        if (isCableQuery) {
            for (CableColumn field : CableColumn.values()) {
                fieldValues.add(field.getColumnLabel());
            }
        } else {
            for (CableTypeColumn field : CableTypeColumn.values()) {
                if (CableTypeColumn.MANUFACTURERS.equals(field)) {
                    continue;
                }
                fieldValues.add(field.getColumnLabel());
            }
        }
        return fieldValues;
    }

    /**
     * @return list of all possible comparison operator values.
     */
    public List<QueryComparisonOperator> getComparisonOperatorValues() {
        return Arrays.asList(QueryComparisonOperator.values());
    }

    /**
     * @return list of all possible boolean operator values.
     */
    public List<QueryBooleanOperator> getBooleanOperatorValues() {
        return Arrays.asList(QueryBooleanOperator.values());
    }

    /** @return validation messages. */
    public List<String> getValidationMessages() {
        return validationMessages;
    }

    /**
     * Creates new empty query with default name and empty query condition list and saves it into database.
     */
    public void addQuery() {
        if (!sessionService.isLoggedIn()) {
            return;
        }
        EntityType entityType = isCableQuery ? EntityType.CABLE : EntityType.CABLE_TYPE;
        List<QueryCondition> defaultQueryConditions = new ArrayList<QueryCondition>();
        QueryCondition queryCondition = new QueryCondition();
        queryCondition.setBooleanOperator(QueryBooleanOperator.NONE);
        queryCondition.setComparisonOperator(QueryComparisonOperator.CONTAINS);
        queryCondition.setField(
                isCableQuery ? CableColumn.CABLE_TYPE.getColumnLabel() : CableTypeColumn.NAME.getColumnLabel());
        queryCondition.setParenthesisClose(QueryParenthesis.NONE);
        queryCondition.setParenthesisOpen(QueryParenthesis.NONE);
        queryCondition.setPosition(0);
        queryCondition.setValue("");
        defaultQueryConditions.add(queryCondition);
        Query query = queryService.saveQuery(description, entityType, sessionService.getLoggedInName(),
                defaultQueryConditions);
        queryCondition.setQuery(query);
        refreshQueries(query.getId());
        refreshMostRecentQueries();
    }

    /**
     * Removes selected query from database.
     */
    public void removeSelectedQuery() {
        if (selectedQuery != null) {
            queryService.deleteQuery(selectedQuery.getQuery());
            refreshQueries(null);
            refreshMostRecentQueries();
            unselectQuery();
        }
    }

    /**
     * Edits selected query.
     */
    public void editSelectedQuery() {
        if (selectedQuery != null) {
            selectedQuery.setDescription(description);
            selectedQuery.setEntityType(isCableQuery ? EntityType.CABLE : EntityType.CABLE_TYPE);
            Query query = queryService.updateQuery(selectedQuery.getQuery());
            refreshQueries(query.getId());
            refreshMostRecentQueries();
        }
    }

    /**
     * Duplicate selected query.
     */
    public void duplicateSelectedQuery() {
        if (selectedQuery != null && sessionService.isLoggedIn()) {
            List<QueryCondition> duplicatedQueryConditions = new ArrayList<QueryCondition>();
            for (QueryCondition queryCondition : selectedQuery.getQueryConditions()) {
                QueryCondition queryConditionDuplicate = new QueryCondition();
                queryConditionDuplicate.setBooleanOperator(queryCondition.getBooleanOperator());
                queryConditionDuplicate.setComparisonOperator(queryCondition.getComparisonOperator());
                queryConditionDuplicate.setField(queryCondition.getField());
                queryConditionDuplicate.setParenthesisClose(queryCondition.getParenthesisClose());
                queryConditionDuplicate.setParenthesisOpen(queryCondition.getParenthesisOpen());
                queryConditionDuplicate.setPosition(queryCondition.getPosition());
                queryConditionDuplicate.setValue(queryCondition.getValue());
                duplicatedQueryConditions.add(queryConditionDuplicate);
            }
            EntityType entityType = selectedQuery.getEntityType();
            Query query = queryService.saveQuery(description, entityType, sessionService.getLoggedInName(),
                    duplicatedQueryConditions);
            refreshQueries(query.getId());
            refreshMostRecentQueries();
        }
    }

    /**
     * Adds new empty query condition with default values.
     */
    public void addEmptyQueryCondition() {
        if (selectedQuery != null) {
            Query query = selectedQuery.getQuery();
            int position = queryConditions.isEmpty() ? 0 : queryConditions.size();
            QueryCondition queryCondition = new QueryCondition(query, QueryParenthesis.NONE, getFieldValues().get(0),
                    QueryComparisonOperator.EQUAL, EMPTY_VALUE, QueryParenthesis.NONE, QueryBooleanOperator.NONE,
                    position);
            if (selectedQueryConditions != null && selectedQueryConditions.size() == 1) {
                int selectedPosition = selectedQueryConditions.get(0).getPosition();
                queryConditions.add(selectedPosition, queryCondition);
            } else {
                queryConditions.add(queryCondition);
            }
            selectedQueryConditions = new ArrayList<>();
            selectedQueryConditions.add(queryCondition);
            isQueryConditionSelected = true;
            reorderQueryConditions();
        }
    }

    /**
     * Removes selected query condition.
     */
    public void removeSelectedQueryCondition() {
        if (selectedQuery != null && selectedQueryConditions != null) {
            for (QueryCondition queryCondition : selectedQueryConditions) {
                queryConditions.remove(queryCondition);
            }
            reorderQueryConditions();
            selectedQueryConditions = null;
            isQueryConditionSelected = false;
        }
    }

    /**
     * Save query and query conditions.
     */
    public void saveQuery() {
        if (selectedQuery != null) {
            validationMessages = new ArrayList<>();
            boolean isValid = validateQueryConditions();
            if (isValid) {
                selectedQuery.setQueryConditions(queryConditions);
                Query query = selectedQuery.getQuery();
                Query updatedQuery = queryService.updateQuery(query);
                refreshQueries(updatedQuery.getId());
                refreshMostRecentQueries();
            }
        }
    }

    /**
     * Prepare for Add query dialog.
     */
    public void prepareAddPopup() {
        description = "";
    }

    /**
     * Prepare for Edit duplicate query dialog.
     */
    public void prepareEditDuplicatePopup() {
        if (selectedQuery != null) {
            description = selectedQuery.getDescription();
        }
    }

    /**
     * Event triggered when dialog is opened.
     */
    public void onDialogOpen() {
        refreshQueries(null);
        refreshMostRecentQueries();
    }

    /**
     * Even triggered when query is executed.
     */
    public void onQueryExecute() {
        refreshMostRecentQueries();
    }

    /**
     * Check if query description is unique
     *
     * @param ctx faces context
     * @param component ui component
     * @param value query description
     */
    public void isQueryDescriptionUnique(FacesContext ctx, UIComponent component, Object value) {
        if (!sessionService.isLoggedIn()) {
            return;
        }
        EntityType entityType = isCableQuery ? EntityType.CABLE : EntityType.CABLE_TYPE;
        if (!queryService.isQueryDescriptionUnique(String.valueOf(value), sessionService.getLoggedInName(),
                entityType)) {
            throw new ValidatorException(new FacesMessage("Query description should be unique."));
        }
    }

    public boolean isQueryConditionUpButtonEnabled() {
        return selectedQuery != null && selectedQueryConditions != null && selectedQueryConditions.size() == 1
                && selectedQueryConditions.get(0).getPosition() != 0;
    }

    public boolean isQueryConditionDownButtonEnabled() {
        if (selectedQuery == null) {
            return false;
        }
        int n = selectedQuery.getQueryConditions().size() - 1;
        return selectedQueryConditions != null && selectedQueryConditions.size() == 1
                && selectedQueryConditions.get(0).getPosition() != n;
    }

    private void reorderQueryConditions() {
        for (int i = 0; i < queryConditions.size(); i++) {
            queryConditions.get(i).setPosition(i);
        }
    }

    /**
     * Move query condition up in list.
     */
    public void moveQueryConditionUp() {
        if (selectedQueryConditions == null || selectedQueryConditions.size() != 1) {
            return;
        }
        int position = selectedQueryConditions.get(0).getPosition();
        if (position != 0 || position < queryConditions.size()) {
            QueryCondition tmp = queryConditions.get(position - 1);
            queryConditions.set(position - 1, queryConditions.get(position));
            queryConditions.set(position, tmp);
            reorderQueryConditions();
        }
    }

    /**
     * Move query condition down in list.
     */
    public void moveQueryConditionDown() {
        if (selectedQueryConditions == null || selectedQueryConditions.size() != 1) {
            return;
        }
        int position = selectedQueryConditions.get(0).getPosition();
        if (position != queryConditions.size() - 1 && position >= 0) {
            QueryCondition tmp = queryConditions.get(position + 1);
            queryConditions.set(position + 1, queryConditions.get(position));
            queryConditions.set(position, tmp);
            reorderQueryConditions();
        }
    }

    /**
     * Validates query conditions.
     *
     * @return true if query conditions are valid, otherwise false.
     */
    private boolean validateQueryConditions() {
        LOGGER.log(Level.INFO, "Validating Query with description: " + selectedQuery.getDescription());
        if (queryConditions == null || queryConditions.isEmpty()) {
            validationMessages.add("Query conditions can't be empty.");
            return false;
        }

        //  stop at first invalid item
        boolean isQueryValid = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < queryConditions.size(); i++) {
            QueryCondition queryCondition = queryConditions.get(i);
            LOGGER.log(Level.INFO, "QueryCondition in position " + queryCondition.getPosition() + ": "
                    + queryCondition.toString());
            String field = queryCondition.getField();
            if (StringUtils.isEmpty(field)) {
                validationMessages.add("Field must be selected.");
                isQueryValid = false;
                break;
            }
            String comparisonOperator = queryCondition.getComparisonOperator().toString();
            if (StringUtils.isEmpty(comparisonOperator)) {
                validationMessages.add("Comparison operator must be selected.");
                isQueryValid = false;
                break;
            }
            QueryComparisonOperator queryComparisonOperator = queryCondition.getComparisonOperator(comparisonOperator);
            if (isCableQuery) {
                CableColumn column = CableColumn.convertColumnLabel(field);
                if (column.isStringComparisonOperator() && !queryComparisonOperator.isStringComparisonOperator()
                        && queryComparisonOperator != QueryComparisonOperator.EQUAL) {
                    validationMessages
                            .add(queryCondition.getField() + " should be compared with string comparison operator.");
                    isQueryValid = false;
                    break;
                } else if (!column.isStringComparisonOperator()
                        && queryComparisonOperator.isStringComparisonOperator()) {
                    validationMessages
                            .add(queryCondition.getField() + " should be compared with numeric comparison operator.");
                    isQueryValid = false;
                    break;
                }
            } else {
                CableTypeColumn column = CableTypeColumn.convertColumnLabel(field);
                if (column.isStringComparisonOperator() && !queryComparisonOperator.isStringComparisonOperator()
                        && queryComparisonOperator != QueryComparisonOperator.EQUAL) {
                    validationMessages
                            .add(queryCondition.getField() + " should be compared with string comparison operator.");
                    isQueryValid = false;
                    break;
                } else if (!column.isStringComparisonOperator()
                        && queryComparisonOperator.isStringComparisonOperator()) {
                    validationMessages
                            .add(queryCondition.getField() + " should be compared with numeric comparison operator.");
                    isQueryValid = false;
                    break;
                }
            }
            if (StringUtils.isEmpty(queryCondition.getValue()) || queryCondition.getValue().equals(EMPTY_VALUE)) {
                validationMessages.add("Value must be entered.");
                isQueryValid = false;
                break;
            }
            String booleanOperator = queryCondition.getBooleanOperator().toString();
            if (i + 1 != queryConditions.size() && StringUtils.isEmpty(booleanOperator)) {
                validationMessages.add("Boolean operator is on the wrong place.");
                isQueryValid = false;
                break;
            }
            String parenthesisOpen = queryCondition.getParenthesisOpen().toString();
            if (QueryParenthesis.OPEN.getParenthesis().equals(parenthesisOpen)) {
                sb.append(QueryParenthesis.OPEN.getParenthesis());
            }
            String parenthesisClose = queryCondition.getParenthesisClose().toString();
            if (QueryParenthesis.CLOSE.getParenthesis().equals(parenthesisClose)) {
                sb.append(QueryParenthesis.CLOSE.getParenthesis());
            }
        }

        if (!validateParenthesis(sb.toString())) {
            validationMessages.add("Parenthesis are not correctly nested.");
            isQueryValid = false;
        }
        if (isQueryValid) {
            validationMessages.add("Query conditions are saved.");
        }
        return isQueryValid;
    }

    /**
     * Validates parenthesis.
     *
     * @param parenthesisString
     *            string with parenthesis
     *
     * @return true if parenthesis are correctly nested otherwise false
     */
    private boolean validateParenthesis(String parenthesisString) {
        if (parenthesisString.isEmpty()) {
            return true;
        }
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < parenthesisString.length(); i++) {
            char current = parenthesisString.charAt(i);
            if (current == '(') {
                stack.push(current);
            }
            if (current == ')') {
                if (stack.isEmpty()) {
                    return false;
                }
                char last = stack.peek();
                if (current == ')' && last == '(') {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    /**
     * Refreshes list of queries.
     *
     * @param queryId
     *            id for selection
     */
    private void refreshQueries(Long queryId) {
        if (sessionService.isLoggedIn()) {
            queries = buildQueryUIs(queryId, sessionService.getLoggedInName());
        } else {
            queries = EMPTY_LIST;
        }
    }

    /**
     * Refreshes most recent queries.
     */
    private void refreshMostRecentQueries() {
        if (sessionService.isLoggedIn()) {
            mostRecentQueries = buildMostRecentQueryUIs(sessionService.getLoggedInName());
        } else {
            mostRecentQueries = EMPTY_LIST;
        }
    }

    /**
     * @param queryId
     *            optional query
     * @param owner
     *            owner of the queries - logged in user
     *
     * @return list of queries UIs which user is logged in user.
     */
    private List<QueryUI> buildQueryUIs(Long queryId, String owner) {
        List<QueryUI> queryUIs = new ArrayList<>();
        for (Query query : queryService.getQueries(owner)) {
            QueryUI queryUI = new QueryUI(query);
            if (queryId != null) {
                if (query.getId().longValue() == queryId.longValue()) {
                    selectQuery(queryUI);
                }
            } else {
                unselectQuery();
            }

            if ((isCableQuery && query.getEntityType() == EntityType.CABLE)
                    || (!isCableQuery && query.getEntityType() == EntityType.CABLE_TYPE)) {
                queryUIs.add(queryUI);
            }
        }

        return queryUIs;
    }

    private void selectQuery(QueryUI query) {
        selectedQuery = query;
        description = selectedQuery.getDescription();
        queryConditions = selectedQuery.getQueryConditions();
        isQuerySelected = true;
        selectedQueryConditions = null;
        isQueryConditionSelected = false;
        Collections.sort(queryConditions);
    }

    private void unselectQuery() {
        selectedQuery = null;
        isQuerySelected = false;
        queryConditions = new ArrayList<>();
        description = "";
        selectedQueryConditions = null;
        isQueryConditionSelected = false;
    }

    /**
     * @param owner
     *            owner of the most recent queries - logged in user
     *
     * @return list of most recent queries UIs which user is logged in user.
     */
    private List<QueryUI> buildMostRecentQueryUIs(String owner) {
        return queryService.getLastThreeQueries(owner, isCableQuery ? EntityType.CABLE : EntityType.CABLE_TYPE).stream()
                .map(QueryUI::new).collect(Collectors.toList());
    }

}
