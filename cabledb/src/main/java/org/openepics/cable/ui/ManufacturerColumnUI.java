/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.openepics.cable.services.dl.ManufacturerColumn;

/**
 * Enum wrapper for ManufacturerColumn. Contains information for displaying Manufacturer columns.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum ManufacturerColumnUI {
    NAME(ManufacturerColumn.NAME, ManufacturerColumnUIConstants.NAME_VALUE, ManufacturerColumnUIConstants.NAME_TOOLTIP,
            ManufacturerColumnUIConstants.NAME_STYLECLASS, ManufacturerColumnUIConstants.NAME_STYLE,
            ManufacturerColumnUIConstants.NAME_FILTERMODE, ManufacturerColumnUIConstants.NAME_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    ADDRESS(ManufacturerColumn.ADDRESS, ManufacturerColumnUIConstants.ADDRESS_VALUE,
            ManufacturerColumnUIConstants.ADDRESS_TOOLTIP, ManufacturerColumnUIConstants.ADDRESS_STYLECLASS,
            ManufacturerColumnUIConstants.ADDRESS_STYLE, ManufacturerColumnUIConstants.ADDRESS_FILTERMODE,
            ManufacturerColumnUIConstants.ADDRESS_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    PHONE(ManufacturerColumn.PHONE, ManufacturerColumnUIConstants.PHONE_VALUE,
            ManufacturerColumnUIConstants.PHONE_TOOLTIP, ManufacturerColumnUIConstants.PHONE_STYLECLASS,
            ManufacturerColumnUIConstants.PHONE_STYLE, ManufacturerColumnUIConstants.PHONE_FILTERMODE,
            ManufacturerColumnUIConstants.PHONE_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    EMAIL(ManufacturerColumn.EMAIL, ManufacturerColumnUIConstants.EMAIL_VALUE,
            ManufacturerColumnUIConstants.EMAIL_TOOLTIP, ManufacturerColumnUIConstants.EMAIL_STYLECLASS,
            ManufacturerColumnUIConstants.EMAIL_STYLE, ManufacturerColumnUIConstants.EMAIL_FILTERMODE,
            ManufacturerColumnUIConstants.EMAIL_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    COUNTRY(ManufacturerColumn.COUNTRY, ManufacturerColumnUIConstants.COUNTRY_VALUE,
            ManufacturerColumnUIConstants.COUNTRY_TOOLTIP, ManufacturerColumnUIConstants.COUNTRY_STYLECLASS,
            ManufacturerColumnUIConstants.COUNTRY_STYLE, ManufacturerColumnUIConstants.COUNTRY_FILTERMODE,
            ManufacturerColumnUIConstants.COUNTRY_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    STATUS(ManufacturerColumn.STATUS, ManufacturerColumnUIConstants.STATUS_VALUE,
            ManufacturerColumnUIConstants.STATUS_TOOLTIP, ManufacturerColumnUIConstants.STATUS_STYLECLASS,
            ManufacturerColumnUIConstants.STATUS_STYLE, ManufacturerColumnUIConstants.STATUS_FILTERMODE,
            ManufacturerColumnUIConstants.STATUS_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private ManufacturerColumn parent;
    private String tooltip;
    private String value;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private String filterOptions;
    private boolean filterDropdown;
    private CableColumnStyle columnStyle;

    /**
     * ManufacturerColumnUI constructor
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     */
    private ManufacturerColumnUI(ManufacturerColumn parent, String value, String tooltip, String styleClass,
            String style, String filterMode, String filterStyle,
            CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
        if (ManufacturerColumnUIConstants.STATUS_VALUE.equals(value)) {
            this.filterDropdown = true;
        }
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /**
     * Searches for ManufacturerColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return ManufacturerColumnUI enum
     */
    public static ManufacturerColumnUI convertColumnLabel(String columnLabel) {
        for (ManufacturerColumnUI value : ManufacturerColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (ManufacturerColumnUI value : ManufacturerColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public String getFilterOptions() {
        return filterOptions;
    }

    public boolean isFilterDropdown() {
        return this.filterDropdown;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }
}
