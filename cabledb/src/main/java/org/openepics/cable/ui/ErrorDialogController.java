/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.ui;

import com.google.common.collect.ImmutableMap;
import com.google.common.net.HttpHeaders;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.CableProperties;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.util.UiUtility;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Controller bean class for error dialogs.
 *
 * @author Zoltan Runyo <zoltan.runyo@ess.eu>
 */
@Named
@ViewScoped
public class ErrorDialogController implements Serializable {

    private static final String SUBJECT = "Bug report";

    private String details;
    private String stackTrace;
    private String comment;
    private boolean unexpectedError;
    private CableProperties properties = CableProperties.getInstance();

    @Inject
    private MailService mailService;
    @Inject
    private SessionService sessionService;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isUnexpectedError() {
        return unexpectedError;
    }

    /**
     * Return if feedback UI is to be shown.
     *
     * @return if feedback UI is to be shown
     */
    public boolean showFeedbackUI() {
        return StringUtils.isNotEmpty(getSupportEmail()) && unexpectedError;
    }

    public void setUnexpectedError(boolean unexpectedError) {
        this.unexpectedError = unexpectedError;
    }

    /**
     * Report behavior to support email and user.
     */
    public void reportBehavior() {
        String supportEmail = getSupportEmail();
        String attachment = stackTrace;
        Map<String, String> requestHeaders = getRequestHeaders();
        String header = "Application: " + getMessagesProperty("title") + "\n" +
                "Version: " + getMessagesProperty("titleVersion") + "\n" +
                "System: " + requestHeaders.get(HttpHeaders.HOST) + "\n" +
                "User agent: " + requestHeaders.get(HttpHeaders.USER_AGENT) + "\n\n";
        String body = header + "User comment: " + (StringUtils.isNotEmpty(comment) ?  comment : "-");
        InputStream attachmentStream = IOUtils.toInputStream(attachment);
        try {
            mailService.sendMailToAddresses(Collections.singletonList(supportEmail), sessionService.getLoggedInName(),
                    SUBJECT, body, Collections.singletonList(attachmentStream),
                    Collections.singletonList("stack_trace.txt"),true, false);
            UiUtility.showMessage(FacesMessage.SEVERITY_INFO, "Email sent to support team.", "");
            dismiss();
        } catch (Exception e) {
            UiUtility.showMessage(FacesMessage.SEVERITY_ERROR, "Failed to send email to support team!",
                    "Please try again!");
        }

    }

    public String getSupportEmail() {
        return properties.getSupportEmail();
    }

    /**
     * Dismiss error message.
     */
    public void dismiss() {
        UiUtility.dismissErrorMessage();
        details = null;
        comment = null;
    }

    private String getMessagesProperty(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        Application app = context.getApplication();
        ResourceBundle messages = app.getResourceBundle(context, "msgs");
        return messages.getString(key);
    }

    private Map<String, String> getRequestHeaders() {
        HttpServletRequest request =
                (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return ImmutableMap.of(HttpHeaders.USER_AGENT, request.getHeader(HttpHeaders.USER_AGENT),
                HttpHeaders.HOST, request.getHeader(HttpHeaders.HOST));
    }
}
