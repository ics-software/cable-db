/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 */
package org.openepics.cable.ui;

import com.google.common.io.ByteStreams;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.*;
import org.openepics.cable.services.*;
import org.openepics.cable.services.dl.ConnectorImportService;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.util.ManufacturerUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.openepics.cable.services.dl.ConnectorColumn;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.Utility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.stream.Collectors;

/**
 * This is the backing requests bean for connectors.xhtml.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class ConnectorRequestManager implements Serializable {

    private static final long serialVersionUID = 8161254904185694693L;
    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final Logger LOGGER = Logger.getLogger(ConnectorRequestManager.class.getName());
    private static final List<ConnectorUI> EMPTY_LIST = new ArrayList<>();

    private List<ConnectorColumnUI> columns;
    private List<String> columnTemplate = ConnectorColumnUI.getAllColumns();

    @Inject
    private transient ConnectorService connectorService;

    @Inject
    private transient ConnectorImportService connectorImportService;

    @Inject
    private SessionService sessionService;

    @Inject
    private transient ArtifactRequestManager artifactRequestManager;
    @Inject
    private QueryService queryService;
    @Inject
    private ManufacturerService manufacturerService;

    private List<ConnectorUI> connectors;
    private List<ConnectorUI> filteredConnectors;
    private List<ConnectorUI> deletedConnectors;
    private List<ConnectorUI> selectedConnectors = EMPTY_LIST;

    private ConnectorUI selectedConnector;
    private QueryUI selectedQuery;
    private String requestedConnectorName;
    private boolean isAddPopupOpened;
    private boolean isConnectorRequested;

    // for overlays
    private String longTextOverlayHeader;
    private String longTextOverlayContent;
    private ConnectorUI longManufacturersOverlayConnector;

    private Connector oldConnector;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    private List<Manufacturer> manufacturers;
    private ConnectorManufacturer selectedManufacturer;
    private List<Manufacturer> availableManufacturers;
    private String longOverlayURL;

    private byte[] fileToBeImported;
    private LoaderResult<Connector> importResult;
    private String importFileName;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public ConnectorRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns + history column
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = ConnectorColumnUI.values().length + 1;
        columnVisibility = ManagerUtil.setAllColumnVisible(numberOfColumns);
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display, and invokes service availability check at first invocation. */
    @PostConstruct
    public void init() {
        try {
            isAddPopupOpened = false;
            connectors = new ArrayList<>();
            selectedConnectors.clear();
            selectedConnector = null;
            requestedConnectorName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getParameter("connectorName");
            createDynamicColumns();
            refreshConnectors();

            // prepare datatable
            //     cookies
            //         rows per page
            //         column visibility
            //     row number (in all rows) for requested entry (if any)
            Cookie[] cookies = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                            initPaginationPageSize(cookie.getValue());
                            break;
                        case CookieUtility.CD_CONNECTOR_COLUMN_VISIBILITY:
                            initColumnVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }
            rowNumber = rowNumber(requestedConnectorName);

            clearImportState();
        } catch (Exception e) {
            throw new UIException("Connector display initialization failed: " + e.getMessage(), e);
        }
    }

    public String getLongOverlayURL() {
        return longOverlayURL;
    }

    public void setLongOverlayURL(String longOverlayURL) {
        this.longOverlayURL = longOverlayURL;
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        ManagerUtil.initColumnVisibility(visibility, numberOfColumns, columnVisibility);
    }

    /**
     * Find out row number, if applicable, for selected entry in list of all entries.
     *
     * @param connectorName
     * @return
     */
    private int rowNumber(String connectorName) {
        // note
        //     currently not consider filter

        ConnectorUI connectorToSelect = null;
        this.isConnectorRequested = false;
        if (requestedConnectorName != null) {
            isConnectorRequested = true;
            connectorToSelect = getConnectorFromConnectorName(requestedConnectorName);
        }

        selectedConnectors.clear();
        if (connectorToSelect != null) {
            selectedConnector = connectorToSelect;
            selectedConnectors.add(selectedConnector);

            int elementPosition = 0;
            for (ConnectorUI connector : connectors) {
                if (connector.getName().equals(requestedConnectorName)) {
                    return elementPosition;
                }
                elementPosition++;
            }
        }

        return 0;
    }

    private ConnectorUI getConnectorFromConnectorName(final String connectorName) {
        if (connectorName == null || connectorName.isEmpty()) {
            return null;
        }

        ConnectorUI connectorToSelect = null;
        for (ConnectorUI connector : connectors) {
            if (connector.getName().equals(connectorName)) {
                connectorToSelect = connector;
            }
        }
        return connectorToSelect;
    }

    private void refreshConnectors() {
        connectors = buildConnectorUIs();
    }

    private List<ConnectorUI> buildConnectorUIs() {

        final List<ConnectorUI> connectorUIs = new ArrayList<ConnectorUI>();
        List<Connector> connectors = null;
        if (selectedQuery != null) {
            connectors = new ArrayList<>(
                    connectorService.getFilteredConnectors(getSqlQuery(), selectedQuery.getQuery()));
        } else {
            connectors = connectorService.getConnectors();
        }
        selectedConnectors = EMPTY_LIST;
        selectedConnector = null;

        for (Connector connector : connectors) {
            connectorUIs.add(new ConnectorUI(connector));
        }
        return connectorUIs;
    }

    public String getRequestedConnectorName() {
        return requestedConnectorName;
    }

    public void setRequestedConnectorName(String requestedConnectorName) {
        this.requestedConnectorName = requestedConnectorName;
    }

    /** @return the connectors to be displayed */
    public List<ConnectorUI> getConnectors() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return connectors;
    }

    /** @return the filtered connectors to be displayed */
    public List<ConnectorUI> getFilteredConnectors() {
        return filteredConnectors;
    }

    /**
     * @param filteredConnectors
     *            the filtered connectors to set
     */
    public void setFilteredConnectors(List<ConnectorUI> filteredConnectors) {
        this.filteredConnectors = filteredConnectors;
        LOGGER.fine(
                "Setting filtered connectors: " + (filteredConnectors != null ? filteredConnectors.size() : "none"));
    }

    /** @return the currently selected connectors, cannot return null */
    public List<ConnectorUI> getSelectedConnectors() {
        return selectedConnectors;
    }

    /**
     * @param selectedConnectors
     *            the connectors to select
     */
    public void setSelectedConnectors(List<ConnectorUI> selectedConnectors) {
        this.selectedConnectors = selectedConnectors != null ? selectedConnectors : EMPTY_LIST;
        LOGGER.fine("Setting selected connectors: " + this.selectedConnectors.size());
    }

    /** Clears the current connector selection. */
    public void clearSelectedConnectors() {
        LOGGER.fine("Invoked clear connector selection.");
        setSelectedConnectors(null);
    }

    /** @return the Connector sheet with the cables from connectorsToExport list */
    public StreamedContent getConnectorsToExport(){

        LOGGER.fine("Get connectors to export");
        final List<Connector> connectorsToExport = getConnectorsToExportList();
        LOGGER.fine("Connectors to export count: " + connectorsToExport.size());
        return new DefaultStreamedContent(connectorImportService.exportConnectors(connectorsToExport),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_connectors.xlsx");
    }

    public List<Connector> getConnectorsToExportList() {
        if (!sessionService.isLoggedIn())
            return new ArrayList<>();

        final List<Connector> result = new ArrayList<>();

        for (ConnectorUI connector : getConnectors()) {
            if (isIncludedByFilter(connector) && connector.isActive())
                result.add(connector.getConnector());
        }

        return result;
    }

    private boolean isIncludedByFilter(ConnectorUI connector) {
        return !filteredConnectorsExist() || getFilteredConnectors().contains(connector);
    }

    private boolean filteredConnectorsExist() {
        return getFilteredConnectors() != null && !getFilteredConnectors().isEmpty();
    }

    /** @return the date format string to use to display dates */
    public String getDateFormatString() {
        return DateUtil.DATE_FORMAT_STRING;
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                + CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE
                + "', " + getRows()
                + ", {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);

            String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                    + CookieUtility.CD_CONNECTOR_COLUMN_VISIBILITY
                    + "', '" + columnVisibility2String()
                    + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
            PrimeFaces.current().executeScript(statement);
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index for column in UI, counting from left
     * @return true if column is marked as visible
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected connectors and row selection.
     */
    public void unselectAllRows() {
        clearSelectedConnectors();

        onRowSelect();
    }

    /**
     * Event triggered when row is selected in table in UI.
     */
    public void onRowSelect() {
        if (selectedConnectors != null && !selectedConnectors.isEmpty()) {
            if (selectedConnectors.size() == 1) {
                selectedConnector = selectedConnectors.get(0);
            } else {
                selectedConnector = null;
            }
        } else {
            selectedConnector = null;
        }
    }

    /** @return <code>true</code> if a single connector is selected, <code>false</code> otherwise */
    public boolean isSingleConnectorSelected() {
        return (selectedConnectors != null) && (selectedConnectors.size() == 1 && selectedConnectors.get(0).isActive());
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean getIsAddPopupOpened() {
        return isAddPopupOpened;
    }

    /**
     * Prepare for Add connector dialog.
     */
    public void prepareAddPopup() {
        if (sessionService.isLoggedIn()) {
            selectedConnector = new ConnectorUI();
            isAddPopupOpened = true;
        }
    }

    /**
     * Event triggered when connector is created.
     */
    public void onConnectorAdd() {
        for (ConnectorManufacturer manufacturer : selectedConnector.getManufacturers()) {
            updateArtifact(manufacturer.getDatasheet());
        }
        formatConnector(selectedConnector);
        updateArtifact(selectedConnector.getDatasheet());
        connectorService.createConnector(
                selectedConnector.getName(),
                selectedConnector.getAssemblyInstructions(),
                selectedConnector.getDescription(),
                selectedConnector.getType(),
                selectedConnector.getDatasheet(),
                selectedConnector.getManufacturers(),
                selectedConnector.getLinkToDatasheet(),
                sessionService.getLoggedInName());

        refreshConnectors();
        UiUtility.showInfoMessage("Connector added.");
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * Prepare for Edit connector dialog.
     */
    public void prepareEditPopup() {
        Preconditions.checkState(isSingleConnectorSelected());
        Preconditions.checkNotNull(selectedConnector);
        oldConnector = selectedConnector.getConnector();
        selectedConnector = new ConnectorUI(connectorService.getConnector(selectedConnector.getName()));
        isAddPopupOpened = false;
    }

    /**
     * Event triggered when connector is updated.
     */
    public void onConnectorEdit() {
        Preconditions.checkState(isSingleConnectorSelected());
        Preconditions.checkNotNull(selectedConnector);
        final String connectorName = oldConnector.getName();
        for (ConnectorManufacturer manufacturer : selectedConnector.getManufacturers()) {
            updateArtifact(manufacturer.getDatasheet());
        }
        formatConnector(selectedConnector);
        updateArtifact(selectedConnector.getDatasheet());
        final Connector newConnector = selectedConnector.getConnector();
        final boolean updated = connectorService.updateConnector(newConnector, oldConnector, sessionService.getLoggedInName());
        refreshConnectors();
        UiUtility.showInfoMessage("Connector '" + connectorName + (updated ? "' updated." : "' not updated."));
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * @return the list of deleted connectors.
     */
    public List<ConnectorUI> getDeletedConnectors() {
        return deletedConnectors;
    }

    /**
     * Reset values. May be used at e.g. close of delete dialog.
     */
    public void resetValues() {
        deletedConnectors = null;
    }

    /**
     * The method builds a list of connectors that are already deleted. If the list is not empty, it is displayed to the
     * user and the user is prevented from deleting them.
     */
    public void checkConnectorsForDeletion() {
        Preconditions.checkNotNull(selectedConnectors);
        Preconditions.checkState(!selectedConnectors.isEmpty());

        deletedConnectors = Lists.newArrayList();
        for (final ConnectorUI connectorToDelete : selectedConnectors) {
            if (!connectorToDelete.isActive()) {
                deletedConnectors.add(connectorToDelete);
            }
        }
    }

    /**
     * Event triggered when connector is deleted.
     */
    public void onConnectorDelete() {
        Preconditions.checkNotNull(deletedConnectors);
        Preconditions.checkState(deletedConnectors.isEmpty());
        Preconditions.checkNotNull(selectedConnectors);
        Preconditions.checkState(!selectedConnectors.isEmpty());
        int deletedConnectorsCounter = 0;
        for (final ConnectorUI connectorToDelete : selectedConnectors) {
            if (connectorService.deleteConnector(connectorToDelete.getConnector(), sessionService.getLoggedInName())) {
                deletedConnectorsCounter++;
            }
        }
        clearSelectedConnectors();
        filteredConnectors = null;
        deletedConnectors = null;
        refreshConnectors();
        UiUtility.showInfoMessage("Deleted " + deletedConnectorsCounter + (deletedConnectorsCounter == 1 ? " connector." : " connectors."));
    }

    /**
     * Import and upload datasheet for selected connector.
     *
     * @param event file upload event
     */
    public void handleDatasheetUpload(FileUploadEvent event) {
        GenericArtifact artifact = selectedConnector.getDatasheet();
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        selectedConnector.setDatasheet(artifact);
    }

    /** @return the selected connector. */
    public ConnectorUI getSelectedConnector() {
        return selectedConnector;
    }

    /** @return the selected connector's manufacturers. */
    public List<ConnectorManufacturer> getSelectedManufacturers() {
        if(selectedConnector == null) {
            return null;
        }
        return selectedConnector.getManufacturers();
    }

    /** @return true if the current user can edit connectors, else false */
    public boolean getEditConnector() {
        return sessionService.canAdminister();
    }

    public boolean isEditButtonEnabled() {
        if (selectedConnector == null || !selectedConnector.isActive()) {
            return false;
        }
        return sessionService.canAdminister();
    }

    public boolean isDeleteButtonEnabled() {
        if (selectedConnectors == null || selectedConnectors.isEmpty()) {
            return false;
        }
        for (ConnectorUI connectorToDelete : selectedConnectors) {
            if (!connectorToDelete.isActive()) {
                return false;
            }
        }
        return sessionService.canAdminister();
    }

    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    /**
     * Execute selected query.
     *
     * @param e action event
     */
    public void executeQuery(ActionEvent e) {
        refreshConnectors();
    }

    /**
     * Execute query with given id.
     *
     * @param id query id
     */
    public void excuteQueryId(long id) {
        if (id != -1) {
            selectedQuery = new QueryUI(queryService.getQueryById(id));
            refreshConnectors();
        }
    }

    /**
     * Reset query and refresh connectors.
     */
    public void resetQuery() {
        selectedQuery = null;
        refreshConnectors();
    }

    public String getNumberOfFilteredItems() {
        return String.valueOf(connectors.size());
    }

    public boolean isConnectorRequested() {
        return isConnectorRequested;
    }

    private String getSqlQuery() {
        StringBuilder querySB = new StringBuilder(600);
        List<QueryCondition> queryConditions = selectedQuery.getQueryConditions();
        if (queryConditions != null && !queryConditions.isEmpty()) {
            querySB.append("SELECT c FROM Connector c");
            querySB.append(' ').append("WHERE").append(' ');

            Collections.sort(queryConditions);

            for (QueryCondition condition : queryConditions) {

                ConnectorColumn field = ConnectorColumn.convertColumnLabel(condition.getField());

                // check if field is not null
                if (field == null)
                    continue;

                if (condition.getParenthesisOpen() == QueryParenthesis.OPEN) {
                    querySB.append(QueryParenthesis.OPEN.getParenthesis());
                }

                querySB.append("c.");

                querySB.append(field.getFieldName());

                querySB.append(' ');

                String value = condition.getValue();

                QueryComparisonOperator operator = condition.getComparisonOperator();
                if (operator.isStringComparisonOperator()) {
                    if (operator == QueryComparisonOperator.STARTS_WITH) {
                        querySB.append("LIKE").append(' ').append("'").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.CONTAINS) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.ENDS_WITH) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("'");
                    }
                } else if (field.isStringComparisonOperator() && operator == QueryComparisonOperator.EQUAL) {
                    querySB.append("LIKE").append(' ').append("'").append(value).append("'");
                } else {
                    querySB.append(operator.getOperator()).append(' ').append("'").append(value).append("'");
                }

                if (condition.getParenthesisClose() == QueryParenthesis.CLOSE) {
                    querySB.append(QueryParenthesis.CLOSE.getParenthesis()).append(' ');
                } else {
                    querySB.append(' ');
                }

                if (condition.getBooleanOperator() != QueryBooleanOperator.NONE) {
                    querySB.append(condition.getBooleanOperator().getOperator()).append(' ');
                }
            }
        }

        LOGGER.fine("Query built: " + querySB.toString());

        return querySB.toString();
    }

    /** Resets column template to default */
    private void resetColumnTemplate() {
        columnTemplate = ConnectorColumnUI.getAllColumns();
    }

    /**
     * Sets column template.
     *
     * @param columnTemplate
     *            of column names to which we should set template
     */
    private void setColumnTemplate(List<String> columnTemplate) {
        this.columnTemplate = columnTemplate;
    }

    /** Resets displayView to default */
    public void resetDisplayView() {
        resetColumnTemplate();
        createDynamicColumns();
    }

    /**
     * Returns current columns to show in connector data table
     *
     * @return columns
     */
    public List<ConnectorColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<ConnectorColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();

            ConnectorColumnUI column = ConnectorColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(":connectorTableForm:connectorTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /** @return number of column in current display view. */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }


    /**
     * Checks if there is any other connector apart from selected one, that already has given name.
     *
     * @param ctx ctx
     * @param component component
     * @param value value
     * @throws ValidatorException if connector name is not unique
     */
    public void isConnectorNameValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        boolean adding = selectedConnector.getId() == null;
        if (!adding) {
            return;
        }
        Connector connector = connectorService.getConnector(stringValue);
        if (connector != null) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Connector with this name already exists.", null));
        }
    }

    /**
     * Provide suggestions for connector given query to filter for.
     *
     * @param query query to filter for
     * @return suggestions for connector given query to filter for.
     */
    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn()) {
            return Collections.emptyList();
        }

        final List<String> selectItems = new ArrayList<>();
        FacesContext context = FacesContext.getCurrentInstance();
        ConnectorColumnUI column = (ConnectorColumnUI) UIComponent.getCurrentComponent(context).getAttributes()
                .get("column");

        if (ConnectorColumnUI.STATUS.equals(column)) {
            selectItems.add(ConnectorUI.STATUS_OBSOLETE);
            selectItems.add(ConnectorUI.STATUS_VALID);
        }
        return selectItems;
    }

    /**
     * Formats the given connector by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedConnector
     *            connector to format
     */
    private void formatConnector(ConnectorUI selectedConnector) {
        selectedConnector.setDescription(Utility.formatWhitespace(selectedConnector.getDescription()));
        selectedConnector.setName(Utility.formatWhitespace(selectedConnector.getName()));
        selectedConnector.setType(Utility.formatWhitespace(selectedConnector.getType()));
    }

    /**
     * Autocomplete method for manufacturers
     *
     * @param query
     *            the query to filter manufacturers
     * @return all available manufacturers from manufacturer service.
     */
    public List<Manufacturer> completeAvailableManufacturers(String query) {
        if (manufacturers == null) {
            manufacturers = manufacturerService.getManufacturers();
        }

        availableManufacturers =
                ManufacturerUtil.completeAvailableManufacturers(
                        query,
                        manufacturers,
                        selectedConnector.getManufacturers().stream()
                            .map(cm -> cm.getManufacturer())
                            .collect(Collectors.toList()));
        return availableManufacturers;
    }

    /** @return true if current user is allowed to change cable type manufacturers */
    public boolean getChangeManufacturersPermission() {
        return sessionService.canAdminister();
    }

    /** Add column to currently selected displayView */
    public void addManufacturer() {
        List<Manufacturer> manufacturers = completeAvailableManufacturers("");
        if (manufacturers.isEmpty()) {
            UiUtility.showErrorMessage("Please define manufacturers under the manufacturer page first.");
            return;
        }
        ConnectorManufacturer manufacturer = new ConnectorManufacturer(selectedConnector.getConnector(),
                manufacturers.get(0), null, selectedConnector.getManufacturers().size());
        selectedConnector.getManufacturers().add(manufacturer);
    }

    /**
     * Remove currently selected displayViewColumn
     */
    public void removeManufacturer() {
        if (selectedManufacturer != null) {
            selectedConnector.getManufacturers().remove(selectedManufacturer);
        }
        if (selectedConnector.getManufacturers().isEmpty()) {
            selectedManufacturer = null;
        }
    }

    /**
     * Remove datasheet for selected manufacturer.
     */
    public void removeManufacturerDatasheet() {
        if (selectedManufacturer != null) {
            selectedManufacturer.setDatasheet(null);
        }
    }

    /**
     * Remove datasheet for selected connector
     */
    public void removeDatasheet() {
        if (selectedConnector != null) {
            selectedConnector.setDatasheet(null);
        }
    }

    /** Swap position of current manufacturer and the one before it. */
    public void moveManufacturerUp() {
        if (selectedConnector.getManufacturers() == null || selectedConnector.getManufacturers().size() <= 1) {
            return;
        }
        int position = selectedManufacturer.getPosition();
        if (position > 0) {
            ConnectorManufacturer movedManufacturer = selectedConnector.getManufacturers().get(position - 1);
            selectedConnector.getManufacturers().set(position, movedManufacturer);
            selectedConnector.getManufacturers().set(position - 1, selectedManufacturer);
            selectedConnector.getManufacturers().get(position).setPosition(position);
            selectedConnector.getManufacturers().get(position - 1).setPosition(position - 1);
        }

    }

    /** Swap position of current manufacturer and the one before it. */
    public void moveManufacturerDown() {
        if (selectedConnector.getManufacturers() == null || selectedConnector.getManufacturers().size() <= 1) {
            return;
        }
        int position = selectedManufacturer.getPosition();
        if (position < selectedConnector.getManufacturers().size() - 1) {
            ConnectorManufacturer movedManufacturer = selectedConnector.getManufacturers().get(position + 1);
            selectedConnector.getManufacturers().set(position, movedManufacturer);
            selectedConnector.getManufacturers().set(position + 1, selectedManufacturer);
            selectedConnector.getManufacturers().get(position).setPosition(position);
            selectedConnector.getManufacturers().get(position + 1).setPosition(position + 1);
        }
    }

    /**
     * Import and upload datasheet for selected manufacturer.
     *
     * @param event file upload event
     */
    public void handleManufacturerDatasheetUpload(FileUploadEvent event) {
        GenericArtifact artifact = selectedManufacturer.getDatasheet();
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        selectedManufacturer.setDatasheet(artifact);
    }

    /**
     * Sets overlay header text.
     *
     * @param longTextOverlayHeader
     *            the overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /**
     * Gets overlay header text.
     *
     * @return the overlay header text.
     */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets overlay content text.
     *
     * @param longTextOverlayContent
     *            the overlay content text.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Gets overlay content text.
     *
     * @return the overlay content text.
     */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Sets the cableType for the long manufacturers popup.
     *
     * @param longManufacturersOverlayConnector
     *            connector.
     */
    public void setLongManufacturersOverlayConnector(ConnectorUI longManufacturersOverlayConnector) {
        this.longManufacturersOverlayConnector = longManufacturersOverlayConnector;
    }

    /** @return list of manufacturers for the long manufacturers popup. */
    public List<ConnectorManufacturer> getLongManufacturersListOverlay() {
        return (longManufacturersOverlayConnector != null) ? longManufacturersOverlayConnector.getManufacturers()
                : null;
    }

    /** Clears the newly created selected connector so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedConnector = null;
        }
    }

    private void updateArtifact(GenericArtifact artifact) {

        if (artifact != null && artifact.getContent() != null) {
            artifact.setContent(artifact.getContent());
        }
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    public ConnectorManufacturer getSelectedManufacturer() {
        return selectedManufacturer;
    }

    /**
     * Return if selected manufacturer has an associated datasheet.
     *
     * @return true if if selected manufacturer has an associated datasheet
     */
    public boolean selectedManufacturerHasDatasheet() {
        return selectedManufacturer != null && selectedManufacturer.getDatasheet() != null;
    }

    /**
     * Return if selected connector has an associated datasheet.
     *
     * @return true if selected connector has an associated datasheet.
     */
    public boolean hasDatasheet() {
        return selectedConnector != null && selectedConnector.getDatasheet() != null;
    }

    public void setSelectedManufacturer(ConnectorManufacturer selectedManufacturer) {
        this.selectedManufacturer = selectedManufacturer;
    }

    public List<Manufacturer> getAvailableManufacturers() {
        return availableManufacturers;
    }

    public void setAvailableManufacturers(List<Manufacturer> availableManufacturers) {
        this.availableManufacturers = availableManufacturers;
    }

    /**
     * Return tooltip for connector.
     *
     * @param column connector column ui
     * @param value (part of) tooltip
     * @return tooltip for connector
     */
    public String tooltipForConnector(ConnectorColumnUI column, String value) {
        String result;
        switch (column) {
            case DESCRIPTION:
                result = "Open Description dialog";
                break;
            case TYPE:
                result = "Open Type dialog";
                break;
            case MANUFACTURERS:
                result = "Open Manufacturers dialog";
                break;
            case ASSEMBLY_INSTRUCTIONS:
            case LINK_TO_DATASHEET:
                result = "Click to open navigation dialog to " + value;
                break;
            default:
                result = "Open "+ column.getValue() + " dialog";
        }
        return result;
    }

    /**
     * Return url for column.
     *
     * @param column connector column ui
     * @param connectorUi connector ui
     * @return url for column
     */
    public String urlForColumn(ConnectorColumnUI column, ConnectorUI connectorUi) {
        if(ConnectorColumnUI.ASSEMBLY_INSTRUCTIONS.equals(column)) {
            return connectorUi.getAssemblyInstructions();
        }
        if(ConnectorColumnUI.LINK_TO_DATASHEET.equals(column)) {
            return connectorUi.getLinkToDatasheet();
        }

        return null;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear connector-import state.");
        fileToBeImported = null;
        importFileName = null;
        importResult = null;
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /** @return the connector import template */
    public StreamedContent getImportTemplate() {
        LOGGER.fine("Get connectors template");
        return new DefaultStreamedContent(connectorImportService.exportConnectors(new ArrayList<>()),
                Utility.XLSX_CONTENT_TYPE,
                "cdb_connectors.xlsx");
    }

    /** @return true if the current user can import connectors, else false */
    public Boolean canImportConnectors() {
        return connectorImportService.canImportConnectors();
    }

    public byte[] getFileToBeImported() {
        return fileToBeImported != null
                ? Arrays.copyOf(fileToBeImported, fileToBeImported.length)
                : null;
    }

    public void setFileToBeImported(byte[] fileToBeImported) {
        this.fileToBeImported = fileToBeImported != null
                ? Arrays.copyOf(fileToBeImported, fileToBeImported.length)
                : null;
    }

    public LoaderResult<Connector> getImportResult() {
        return importResult;
    }

    public void setImportResult(LoaderResult<Connector> importResult) {
        this.importResult = importResult;
    }

    public String getImportFileName() {
        return importFileName;
    }

    public void setImportFileName(String importFileName) {
        this.importFileName = importFileName;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void connectorFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked connector file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            importFileName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the connector import from the file that was last uploaded.
     */
    public void connectorImportTest() {
        LOGGER.fine("Invoked connector import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = connectorImportService.importConnectors(inputStream, true);
                LOGGER.fine("Connector-import test result: " + importResult.toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the connector import from the file that was last uploaded.
     */
    public void connectorImport() {
        LOGGER.fine("Invoked connector import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = connectorImportService.importConnectors(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshConnectors();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }
}
