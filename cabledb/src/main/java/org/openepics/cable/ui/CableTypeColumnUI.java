/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.openepics.cable.services.dl.CableTypeColumn;

/**
 * Enum wrapper for CableTypeColumn. Contains information for displaying cable type column , wrapping only columns that
 * are available to be displayed in cable data table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableTypeColumnUI {
    NAME(CableTypeColumn.NAME, CableTypeColumnUIConstants.NAME_VALUE, null, CableTypeColumnUIConstants.NAME_TOOLTIP,
            CableTypeColumnUIConstants.NAME_STYLECLASS, CableTypeColumnUIConstants.NAME_STYLE,
            CableTypeColumnUIConstants.NAME_FILTERMODE, CableTypeColumnUIConstants.NAME_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    DESCRIPTION(CableTypeColumn.DESCRIPTION, CableTypeColumnUIConstants.DESCRIPTION_VALUE, null,
            CableTypeColumnUIConstants.DESCRIPTION_TOOLTIP, CableTypeColumnUIConstants.DESCRIPTION_STYLECLASS,
            CableTypeColumnUIConstants.DESCRIPTION_STYLE, CableTypeColumnUIConstants.DESCRIPTION_FILTERMODE,
            CableTypeColumnUIConstants.DESCRIPTION_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    SERVICE(CableTypeColumn.SERVICE, CableTypeColumnUIConstants.SERVICE_VALUE, null,
            CableTypeColumnUIConstants.SERVICE_TOOLTIP, CableTypeColumnUIConstants.SERVICE_STYLECLASS,
            CableTypeColumnUIConstants.SERVICE_STYLE, CableTypeColumnUIConstants.SERVICE_FILTERMODE,
            CableTypeColumnUIConstants.SERVICE_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    DIAMETER(CableTypeColumn.DIAMETER, CableTypeColumnUIConstants.DIAMETER_VALUE, null,
            CableTypeColumnUIConstants.DIAMETER_TOOLTIP, CableTypeColumnUIConstants.DIAMETER_STYLECLASS,
            CableTypeColumnUIConstants.DIAMETER_STYLE, CableTypeColumnUIConstants.DIAMETER_FILTERMODE,
            CableTypeColumnUIConstants.DIAMETER_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    WEIGHT(CableTypeColumn.WEIGHT, CableTypeColumnUIConstants.WEIGHT_VALUE, null,
            CableTypeColumnUIConstants.WEIGHT_TOOLTIP, CableTypeColumnUIConstants.WEIGHT_STYLECLASS,
            CableTypeColumnUIConstants.WEIGHT_STYLE, CableTypeColumnUIConstants.WEIGHT_FILTERMODE,
            CableTypeColumnUIConstants.WEIGHT_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INSULATION(CableTypeColumn.INSULATION, CableTypeColumnUIConstants.INSULATION_VALUE, null,
            CableTypeColumnUIConstants.INSULATION_TOOLTIP, CableTypeColumnUIConstants.INSULATION_STYLECLASS,
            CableTypeColumnUIConstants.INSULATION_STYLE, CableTypeColumnUIConstants.INSULATION_FILTERMODE,
            CableTypeColumnUIConstants.INSULATION_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    JACKET(CableTypeColumn.JACKET, CableTypeColumnUIConstants.JACKET_VALUE, null,
            CableTypeColumnUIConstants.JACKET_TOOLTIP, CableTypeColumnUIConstants.JACKET_STYLECLASS,
            CableTypeColumnUIConstants.JACKET_STYLE, CableTypeColumnUIConstants.JACKET_FILTERMODE,
            CableTypeColumnUIConstants.JACKET_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    VOLTAGE_RATING(CableTypeColumn.VOLTAGE_RATING, CableTypeColumnUIConstants.VOLTAGE_RATING_VALUE, null,
            CableTypeColumnUIConstants.VOLTAGE_RATING_TOOLTIP, CableTypeColumnUIConstants.VOLTAGE_RATING_STYLECLASS,
            CableTypeColumnUIConstants.VOLTAGE_RATING_STYLE, CableTypeColumnUIConstants.VOLTAGE_RATING_FILTERMODE,
            CableTypeColumnUIConstants.VOLTAGE_RATING_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    FLAMABILITY(CableTypeColumn.FLAMABILITY, CableTypeColumnUIConstants.FLAMMABILITY_VALUE, null,
            CableTypeColumnUIConstants.FLAMMABILITY_TOOLTIP, CableTypeColumnUIConstants.FLAMMABILITY_STYLECLASS,
            CableTypeColumnUIConstants.FLAMMABILITY_STYLE, CableTypeColumnUIConstants.FLAMMABILITY_FILTERMODE,
            CableTypeColumnUIConstants.FLAMMABILITY_FILTERSTSYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    INSTALLATION_TYPE(CableTypeColumn.INSTALLATION_TYPE, CableTypeColumnUIConstants.INSTALLATION_TYPE_VALUE, null,
            CableTypeColumnUIConstants.INSTALLATION_TYPE_TOOLTIP,
            CableTypeColumnUIConstants.INSTALLATION_TYPE_STYLECLASS, CableTypeColumnUIConstants.INSTALLATION_TYPE_STYLE,
            CableTypeColumnUIConstants.INSTALLATION_TYPE_FILTERMODE,
            CableTypeColumnUIConstants.INSTALLATION_TYPE_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    RADIATION_RESISTANCE(CableTypeColumn.RADIATION_RESISTANCE, CableTypeColumnUIConstants.RADIATION_RESISTANCE_VALUE,
            null, CableTypeColumnUIConstants.RADIATION_RESISTANCE_TOOLTIP,
            CableTypeColumnUIConstants.RADIATION_RESISTANCE_STYLECLASS,
            CableTypeColumnUIConstants.RADIATION_RESISTANCE_STYLE,
            CableTypeColumnUIConstants.RADIATION_RESISTANCE_FILTERMODE,
            CableTypeColumnUIConstants.RADIATION_RESISTANCE_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    MANUFACTURERS(CableTypeColumn.MANUFACTURERS, CableTypeColumnUIConstants.MANUFACTURERS_VALUE,
            CableTypeColumnUIConstants.MANUFACTURERS_URL, CableTypeColumnUIConstants.MANUFACTURERS_TOOLTIP,
            CableTypeColumnUIConstants.MANUFACTURERS_STYLECLASS, CableTypeColumnUIConstants.MANUFACTURERS_STYLE,
            CableTypeColumnUIConstants.MANUFACTURERS_FILTERMODE, CableTypeColumnUIConstants.MANUFACTURERS_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGLINK),
    STATUS(CableTypeColumn.STATUS, CableTypeColumnUIConstants.STATUS_VALUE, null,
            CableTypeColumnUIConstants.STATUS_TOOLTIP, CableTypeColumnUIConstants.STATUS_STYLECLASS,
            CableTypeColumnUIConstants.STATUS_STYLE, CableTypeColumnUIConstants.STATUS_FILTERMODE,
            CableTypeColumnUIConstants.STATUS_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    COMMENTS(CableTypeColumn.COMMENTS, CableTypeColumnUIConstants.COMMENTS_VALUE, null,
            CableTypeColumnUIConstants.COMMENTS_TOOLTIP, CableTypeColumnUIConstants.COMMENTS_STYLECLASS,
            CableTypeColumnUIConstants.COMMENTS_STYLE, CableTypeColumnUIConstants.COMMENTS_FILTERMODE,
            CableTypeColumnUIConstants.COMMENTS_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    REVISION(CableTypeColumn.REVISION, CableTypeColumnUIConstants.REVISION_VALUE, null,
            CableTypeColumnUIConstants.REVISION_TOOLTIP, CableTypeColumnUIConstants.REVISION_STYLECLASS,
            CableTypeColumnUIConstants.REVISION_STYLE, CableTypeColumnUIConstants.REVISION_FILTERMODE,
            CableTypeColumnUIConstants.REVISION_FILTERSTYLE, CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private CableTypeColumn parent;
    private String tooltip;
    private String value;
    private String url;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private boolean filterDropdown;
    private CableColumnStyle columnStyle;

    /**
     * CableTypeColumnUI constructor Decides what labels in column to display according to property.
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     */
    private CableTypeColumnUI(CableTypeColumn parent, String value, String url, String tooltip, String styleClass,
            String style, String filterMode, String filterStyle,
            CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.url = url;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
        switch (value) {
        case CableTypeColumnUIConstants.INSTALLATION_TYPE_VALUE:
        case CableTypeColumnUIConstants.STATUS_VALUE:
            this.filterDropdown = true;
            break;
        default:
            break;
        }
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /** @return String capitalized property */
    public String getDisplayPropertyHandle() {
        return "handleDisplay" + Character.toUpperCase(value.charAt(0)) + value.substring(1);
    }

    /** @return String capitalized property */
    public String getDisplayProperty() {
        return "display" + Character.toUpperCase(value.charAt(0)) + value.substring(1);
    }

    /**
     * Searches for CableTypeColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return CableTypeColumnUI enum
     */
    public static CableTypeColumnUI convertColumnLabel(String columnLabel) {
        for (CableTypeColumnUI value : CableTypeColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (CableTypeColumnUI value : CableTypeColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public boolean isFilterDropdown() {
        return this.filterDropdown;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }

    public String getUrl() {
        return url;
    }
}
