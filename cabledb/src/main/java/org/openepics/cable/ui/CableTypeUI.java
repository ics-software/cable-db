/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.Serializable;
import java.util.List;

import org.openepics.cable.model.CableType;
import org.openepics.cable.model.CableTypeManufacturer;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.model.Manufacturer;

/**
 * <code>CableTypeUI</code> is a presentation of {@link CableType} used in UI.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableTypeUI implements Serializable {

    private static final long serialVersionUID = -1584235674250722373L;
    private static final String EMPTY_STRING = "";
    private static final String MANUFACTURER_BASE_URL = "manufacturers.xhtml?manufacturerName=";

    /** String representing valid cable type status. */
    public static final String STATUS_VALID = "VALID";
    /** String representing obsolete cable type status. */
    public static final String STATUS_OBSOLETE = "OBSOLETE";

    private final CableType cableType;

    /**
     * Constructs Cable type UI object for empty cable type instance.
     */
    public CableTypeUI() {
        this(new CableType());
    }

    /**
     * Constructs Cable type UI object for given cable type instance.
     *
     * @param cableType cable type instance
     */
    public CableTypeUI(CableType cableType) {
        this.cableType = cableType;
    }

    /** @return cableType.get the cable type instance this wraps */
    public CableType getCableType() {
        return cableType;
    }

    /** @return cableType.get the cable type database id. */
    public Long getId() {
        return cableType.getId();
    }

    /**
     * Set name.
     *
     * @param name
     *            name
     */
    public void setName(String name) {
        cableType.setName(name);
    }

    /** @return cableType.get the name/code */
    public String getName() {
        return cableType.getName();
    }

    /**
     * Set description.
     *
     * @param description
     *            description
     */
    public void setDescription(String description) {
        cableType.setDescription(description);
    }

    /** @return cableType.get the description */
    public String getDescription() {
        return cableType.getDescription();
    }

    /**
     * Set service/function description.
     *
     * @param service
     *            service/function description
     */
    public void setService(String service) {
        cableType.setService(service);
    }

    /** @return cableType.get the service/function description */
    public String getService() {
        return cableType.getService();
    }

    /**
     * Set voltage rating description.
     *
     * @param voltage
     *            rating description
     */
    public void setVoltage(Integer voltage) {
        cableType.setVoltage(voltage);
    }

    /** @return cableType.get the voltage rating description */
    public Integer getVoltage() {
        return cableType.getVoltage();
    }

    /**
     * Set insulation material.
     *
     * @param insulation
     *            material
     */
    public void setInsulation(String insulation) {
        cableType.setInsulation(insulation);
    }

    /** @return cableType.get the insulation material */
    public String getInsulation() {
        return cableType.getInsulation();
    }

    /**
     * Set type of jacket.
     *
     * @param jacket
     *            type of jacket
     */
    public void setJacket(String jacket) {
        cableType.setJacket(jacket);
    }

    /** @return cableType.get the type of jacket */
    public String getJacket() {
        return cableType.getJacket();
    }

    /**
     * Set flammability classification.
     *
     * @param flammability
     *            flammability classification
     */
    public void setFlammability(String flammability) {
        cableType.setFlammability(flammability);
    }

    /** @return cableType.get the flammability classification */
    public String getFlammability() {
        return cableType.getFlammability();
    }

    /** @return cableType.get the allowed installation type */
    public InstallationType getInstallationType() {
        return cableType.getInstallationType();
    }

    /**
     * Set the allowed installation type.
     *
     * @param installationType
     *            allowed installation type
     */
    public void setInstallationTypeString(String installationType) {
        cableType.setInstallationType(InstallationType.valueOf(installationType));
    }

    /** @return cableType.get the allowed installation type */
    public String getInstallationTypeString() {
        return cableType.getInstallationType() != null ? getInstallationType().toString() : EMPTY_STRING;
    }

    /**
     * Set total ionizing dose in mrad.
     *
     * @param tid
     *            total ionizing dose in mrad
     */
    public void setTid(Float tid) {
        cableType.setTid(tid);
    }

    /** @return cableType.get the total ionizing dose in mrad */
    public Float getTid() {
        return cableType.getTid();
    }

    /**
     * Set weight in kg/meter.
     *
     * @param weight
     *            weight in kg/meter
     */
    public void setWeight(Float weight) {
        cableType.setWeight(weight);
    }

    /** @return cableType.get the weight in kg/meter */
    public Float getWeight() {
        return cableType.getWeight();
    }

    /**
     * Set the outer diameter in mm.
     *
     * @param diameter
     *            the outer diameter in mm
     */
    public void setDiameter(Float diameter) {
        cableType.setDiameter(diameter);
    }

    /** @return cableType.get the outer diameter in mm */
    public Float getDiameter() {
        return cableType.getDiameter();
    }

    /**
     * Set comments.
     *
     * @param comments
     *            comments
     */
    public void setComments(String comments) {
        cableType.setComments(comments);
    }

    /** @return cableType.get the comments */
    public String getComments() {
        return cableType.getComments();
    }

    public String getRevision() {
        return cableType.getRevision();
    }

    public void setRevision(String revision) {
        cableType.setRevision(revision);
    }


    /** @return cableType.get true if this cable type is in active use, false if it was obsoleted */
    public boolean isActive() {
        return cableType.isActive();
    }

    /** @return cableType.get {@link #STATUS_VALID} if cable type is in active use, {@link #STATUS_OBSOLETE} else */
    public String getValid() {
        return cableType.isActive() ? STATUS_VALID : STATUS_OBSOLETE;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(280);
        sb.append("Cable Type: ");
        sb.append(getName());
        return sb.toString();
    }

    /**
     * Return url for given manufacturer.
     *
     * @param manufacturer manufacturer
     * @return url for given manufacturer
     */
    public String getManufacturerUrl(Manufacturer manufacturer) {
        if (manufacturer != null) {
            return MANUFACTURER_BASE_URL + manufacturer.getName();
        } else {
            return MANUFACTURER_BASE_URL;
        }
    }

    /**
     * @see org.openepics.cable.model.CableType#getManufacturers()
     * @return returns all manufacturers
     */
    public List<CableTypeManufacturer> getManufacturers() {
        return cableType.getManufacturers();
    }

    /** @return cableType.get a concated string of manufacturer names */
    public String getManufacturerNames() {
        return cableType.getManufacturersAsString(false);
    }

    /** @return cableType.get a concated string of manufacturer names and datasheets */
    public String getManufacturerAndDatasheetNames() {
        return cableType.getManufacturersAsString(true);
    }
}
