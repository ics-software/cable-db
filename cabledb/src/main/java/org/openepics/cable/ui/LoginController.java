/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.openepics.cable.services.CableService;
import org.openepics.cable.services.SessionService;

import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.fbs.FbsService;
import se.esss.ics.rbac.loginmodules.service.Message;

/**
 * A UI controller bean for the login / logout button and form.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class LoginController implements Serializable {

    private static final long serialVersionUID = 5984791196886929176L;

    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());

    private static final String UNEXPECTED_ERROR = "Unexpected error";

    @Inject
    private SessionService sessionService;
    @Inject
    private CableService cableService;
    @Inject
    private FbsService fbsService;

    private String inputUsername;
    private String inputPassword;

    private boolean loginRequested;

    private void clearPassword() {
        inputPassword = null;
    }

    /** Initializes the bean for initial view display. */
    @PostConstruct
    public void init() {
        loginRequested = !sessionService.isLoggedIn();
    }

    /** Requests login. */
    public void requestLogin() {
        LOGGER.fine("Invoked request login.");
        loginRequested = true;
    }

    /** Performs login. */
    public void login() {
        LOGGER.fine("Invoked login.");
        try {
            final Message result = sessionService.login(inputUsername, inputPassword);
            if (result.isSuccessful()) {
                loginRequested = false;
                UiUtility.showMessage(
                        FacesMessage.SEVERITY_INFO, "You are logged in", "Welcome " + inputUsername + "!");
            } else {
                UiUtility.showMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "Please try again.");
            }
        } catch (Exception e) {
            UiUtility.showMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "Please try again.");
            LOGGER.log(Level.INFO, "Login failed for " + inputUsername);
            LOGGER.log(Level.FINE, "Login failed for " + inputUsername, e);
        } finally {
            clearPassword();
        }
    }

    /** Performs logout. */
    public void logout() {
        LOGGER.fine("Invoked logout.");
        try {
            final Message result = sessionService.logout();
            UiUtility.showMessage(FacesMessage.SEVERITY_INFO, "Logout", "You have been logged out.");
        } catch (Exception e) {
            UiUtility.showMessage(FacesMessage.SEVERITY_ERROR, "Logout", "Logout has failed.");
            LOGGER.log(Level.FINE, "Strangely, logout has failed.", e);
        } finally {
            clearPassword();
        }
        loginRequested = true;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(
                    FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() +
                            FacesContext.getCurrentInstance().getViewRoot().getViewId());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, UNEXPECTED_ERROR, e);
        }
        return;
    }

    /** Cancels login in progress. */
    public void cancel() {
        LOGGER.fine("Invoked cancel.");
        clearPassword();
        loginRequested = false;
    }

    /** @return the inputUsername */
    public String getInputUsername() {
        return inputUsername;
    }

    /**
     * @param inputUsername
     *            the inputUsername to set
     */
    public void setInputUsername(String inputUsername) {
        this.inputUsername = inputUsername;
    }

    /** @return the inputPassword */
    public String getInputPassword() {
        return inputPassword;
    }

    /**
     * @param inputPassword
     *            the inputPassword to set
     */
    public void setInputPassword(String inputPassword) {
        this.inputPassword = inputPassword;
    }

    /** @return the true if the login is currently requested, else false */
    public boolean isLoginRequested() {
        return loginRequested;
    }

    /** @return the true, if the login will be redirected to cable page */
    public boolean isCablesPage() {
        return "/cables.xhtml".equals(FacesContext.getCurrentInstance().getViewRoot().getViewId());
    }
}
