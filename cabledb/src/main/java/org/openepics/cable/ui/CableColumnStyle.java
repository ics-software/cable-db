/*
 * Copyright (c) 2017 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * Cable column style class.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class CableColumnStyle {

    /**
     * Enums representing possible styles.
     */
    public enum CableColumnStyleEnum {
        /** Field is in plain text. */
        PLAIN_TEXT,
        /** Long text in field is cropped and ellipsis are added. */
        LONGTEXT,
        /** Field contains installation packages. */
        INSTALLATION_PACKAGE,
        /** Field has an additional dialog. */
        DIALOG,
        /** Field is a short link. */
        SHORTLINK,
        /** Field is a long link. */
        LONGLINK,
        /** Field is a download link. */
        DOWNLOAD,
        /** Field is a url. */
        URL,
        /**  Field is icon*/
        ICON,
    }

    private CableColumnStyleEnum styleEnum;

    /**
     * Instantiates a new Cable column style.
     *
     * @param styleEnum
     *            the style.
     */
    public CableColumnStyle(CableColumnStyleEnum styleEnum) {
        this.styleEnum = styleEnum;
    }

    /** @return true if the style is PLAIN_TEXT. */
    public boolean isPlainText() {
        return this.styleEnum == CableColumnStyleEnum.PLAIN_TEXT;
    }

    /** @return true if the style is LONGTEXT. */
    public boolean isLongText() {
        return this.styleEnum == CableColumnStyleEnum.LONGTEXT;
    }

    /** @return true if the style is INSTALLATION_PACKAGE. */
    public boolean isInstallationPackage() {
        return this.styleEnum == CableColumnStyleEnum.INSTALLATION_PACKAGE;
    }

    /** @return true if the style is DIALOG. */
    public boolean isDialog() {
        return this.styleEnum == CableColumnStyleEnum.DIALOG;
    }

    /** @return true if the style is SHORTLINK. */
    public boolean isShortLink() {
        return this.styleEnum == CableColumnStyleEnum.SHORTLINK;
    }

    /** @return true if the style is LONGLINK. */
    public boolean isLongLink() {
        return this.styleEnum == CableColumnStyleEnum.LONGLINK;
    }

    /** @return true if the style is DOWNLOAD. */
    public boolean isDownload() {
        return this.styleEnum == CableColumnStyleEnum.DOWNLOAD;
    }

    /** @return true if the style is URL. */
    public boolean isUrl() {
        return this.styleEnum == CableColumnStyleEnum.URL;
    }

    /** @return true if the style is ICON. */
    public boolean isIcon() { return this.styleEnum == CableColumnStyleEnum.ICON; }
}
