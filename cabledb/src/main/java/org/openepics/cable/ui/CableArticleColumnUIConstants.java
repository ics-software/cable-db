/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * CableArticle UI constants.
 *
 * @author Lars Johansson
 */
public class CableArticleColumnUIConstants {

    public static final String MANUFACTURER_VALUE = "manufacturer";
    public static final String MANUFACTURER_TOOLTIP = "The cable article manufacturer";
    public static final String MANUFACTURER_STYLECLASS = "data_table_header fixed_width108";
    public static final String MANUFACTURER_STYLE = "width:10%;";
    public static final String MANUFACTURER_FILTERMODE = "contains";
    public static final String MANUFACTURER_FILTERSTYLE = "width: 100%;";

    public static final String EXTERNAL_ID_VALUE = "externalId";
    public static final String EXTERNAL_ID_TOOLTIP = "The cable article external id";
    public static final String EXTERNAL_ID_STYLECLASS = "data_table_header fixed_width108";
    public static final String EXTERNAL_ID_STYLE = "width:10%;";
    public static final String EXTERNAL_ID_FILTERMODE = "contains";
    public static final String EXTERNAL_ID_FILTERSTYLE = "width: 100%;";

    public static final String ERP_NUMBER_VALUE = "erpNumber";
    public static final String ERP_NUMBER_TOOLTIP = "The cable article erp number";
    public static final String ERP_NUMBER_STYLECLASS = "data_table_header fixed_width108";
    public static final String ERP_NUMBER_STYLE = "width:10%;";
    public static final String ERP_NUMBER_FILTERMODE = "contains";
    public static final String ERP_NUMBER_FILTERSTYLE = "width: 100%;";

    public static final String ISO_CLASS_VALUE = "isoClass";
    public static final String ISO_CLASS_TOOLTIP = "The cable article iso class";
    public static final String ISO_CLASS_STYLECLASS = "data_table_header fixed_width108";
    public static final String ISO_CLASS_STYLE = "width:10%;";
    public static final String ISO_CLASS_FILTERMODE = "contains";
    public static final String ISO_CLASS_FILTERSTYLE = "width: 100%;";

    public static final String DESCRIPTION_VALUE = "description";
    public static final String DESCRIPTION_TOOLTIP = "The cable article description";
    public static final String DESCRIPTION_STYLECLASS = "data_table_header fixed_width108";
    public static final String DESCRIPTION_STYLE = "width:10%;";
    public static final String DESCRIPTION_FILTERMODE = "contains";
    public static final String DESCRIPTION_FILTERSTYLE = "width: 100%;";

    public static final String LONG_DESCRIPTION_VALUE = "longDescription";
    public static final String LONG_DESCRIPTION_TOOLTIP = "The cable article long description";
    public static final String LONG_DESCRIPTION_STYLECLASS = "data_table_header fixed_width108";
    public static final String LONG_DESCRIPTION_STYLE = "width:10%;";
    public static final String LONG_DESCRIPTION_FILTERMODE = "contains";
    public static final String LONG_DESCRIPTION_FILTERSTYLE = "width: 100%;";

    public static final String MODEL_TYPE_VALUE = "modelType";
    public static final String MODEL_TYPE_TOOLTIP = "The cable article model type";
    public static final String MODEL_TYPE_STYLECLASS = "data_table_header fixed_width108";
    public static final String MODEL_TYPE_STYLE = "width:10%;";
    public static final String MODEL_TYPE_FILTERMODE = "contains";
    public static final String MODEL_TYPE_FILTERSTYLE = "width: 100%;";

    public static final String BENDING_RADIUS_VALUE = "bendingRadius";
    public static final String BENDING_RADIUS_TOOLTIP = "The cable article bending radius";
    public static final String BENDING_RADIUS_STYLECLASS = "data_table_header fixed_width108";
    public static final String BENDING_RADIUS_STYLE = "width:10%;";
    public static final String BENDING_RADIUS_FILTERMODE = "contains";
    public static final String BENDING_RADIUS_FILTERSTYLE = "width: 100%;";

    public static final String OUTER_DIAMETER_VALUE = "outerDiameter";
    public static final String OUTER_DIAMETER_TOOLTIP = "The cable article outer diameter";
    public static final String OUTER_DIAMETER_STYLECLASS = "data_table_header fixed_width108";
    public static final String OUTER_DIAMETER_STYLE = "width:10%;";
    public static final String OUTER_DIAMETER_FILTERMODE = "contains";
    public static final String OUTER_DIAMETER_FILTERSTYLE = "width: 100%;";

    public static final String RATED_VOLTAGE_VALUE = "ratedVoltage";
    public static final String RATED_VOLTAGE_TOOLTIP = "The cable article rated voltage";
    public static final String RATED_VOLTAGE_STYLECLASS = "data_table_header fixed_width108";
    public static final String RATED_VOLTAGE_STYLE = "width:10%;";
    public static final String RATED_VOLTAGE_FILTERMODE = "contains";
    public static final String RATED_VOLTAGE_FILTERSTYLE = "width: 100%;";

    public static final String WEIGHT_PER_LENGTH_VALUE = "weightPerLength";
    public static final String WEIGHT_PER_LENGTH_TOOLTIP = "The cable article weight per length";
    public static final String WEIGHT_PER_LENGTH_STYLECLASS = "data_table_header fixed_width108";
    public static final String WEIGHT_PER_LENGTH_STYLE = "width:10%;";
    public static final String WEIGHT_PER_LENGTH_FILTERMODE = "contains";
    public static final String WEIGHT_PER_LENGTH_FILTERSTYLE = "width: 100%;";

    public static final String SHORT_NAME_VALUE = "shortName";
    public static final String SHORT_NAME_TOOLTIP = "The cable article short name";
    public static final String SHORT_NAME_STYLECLASS = "data_table_header fixed_width108";
    public static final String SHORT_NAME_STYLE = "width:10%;";
    public static final String SHORT_NAME_FILTERMODE = "contains";
    public static final String SHORT_NAME_FILTERSTYLE = "width: 100%;";

    public static final String SHORT_NAME_EXTERNAL_ID_VALUE = "shortNameExternalId";
    public static final String SHORT_NAME_EXTERNAL_ID_TOOLTIP = "The cable article short name external id";
    public static final String SHORT_NAME_EXTERNAL_ID_STYLECLASS = "data_table_header fixed_width108";
    public static final String SHORT_NAME_EXTERNAL_ID_STYLE = "width:10%;";
    public static final String SHORT_NAME_EXTERNAL_ID_FILTERMODE = "contains";
    public static final String SHORT_NAME_EXTERNAL_ID_FILTERSTYLE = "width: 100%;";

    public static final String URL_CHESS_PART_VALUE = "urlChessPart";
    public static final String URL_CHESS_PART_TOOLTIP = "The cable article url chess part";
    public static final String URL_CHESS_PART_STYLECLASS = "data_table_header fixed_width108";
    public static final String URL_CHESS_PART_STYLE = "width:10%;";
    public static final String URL_CHESS_PART_FILTERMODE = "contains";
    public static final String URL_CHESS_PART_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "valid";
    public static final String STATUS_TOOLTIP = "The cable article status";
    public static final String STATUS_STYLECLASS = "data_table_header fixed_width108";
    public static final String STATUS_STYLE = "width:10%;";
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = "width: 100%;";

    /**
     * This class is not to be instantiated.
     */
    private CableArticleColumnUIConstants() {
        throw new IllegalStateException("Utility class");
    }

}
