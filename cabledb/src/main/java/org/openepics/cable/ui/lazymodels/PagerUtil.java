/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.ui.lazymodels;

import org.openepics.cable.model.Query;
import org.openepics.cable.services.LazyService;
import org.primefaces.model.SortOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class for paging of database.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 */
public class PagerUtil {

    private static final Logger LOGGER = Logger.getLogger(PagerUtil.class.getCanonicalName());

    /**
     * This class is not to be instantiated.
     */
    private PagerUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Queries the DB for entries in paged way.
     *
     * @param lazyService the concrete lazy service which implements lazy loading
     * @param offset offset of result before index of the first result to return
     * @param totalSize the maximum size of the query that can be expected
     * @param pageSize the maximum page size
     * @param sortField the field by which to sort
     * @param sortOrder ascending/descending
     * @param filters filters to use
     * @param customQuery query to add
     *
     * @return null if query was empty, or list of History entries
     */
    public static <E> List<E> findLazyPaged(
            LazyService<E> lazyService, int offset, int totalSize, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, Object> filters, Query customQuery) {

        List<E> result = null;

        if(totalSize < pageSize) {
            pageSize = totalSize;
        }

        for(int i=0; i * pageSize < totalSize; i += pageSize) {
            /*
            Gets the paged log entries to temporary list
            Will use less amount of memory if there won't be many Hibernate
            objects in memory at once
            */
            StringBuilder sb = new StringBuilder();
            sb.append("----> Getting entries for page, findLazy(")
            .append("first: ").append(offset + i)
            .append(", pageSize: ").append(pageSize)
            .append(", sortField: ").append(sortField)
            .append(", sortOrder: ").append(sortOrder.name())
            .append(", filters: ").append(filters.entrySet())
            .append(", customQuery: ").append(customQuery != null ? customQuery.getConditions() : "-")
            .append(")");
            LOGGER.log(Level.FINEST, sb.toString());
            List<E> pageResult = lazyService.findLazy(offset + i, pageSize, sortField, sortOrder, filters, customQuery);

            //Only cycle the DB when there are still records to be queried
            if ((pageResult != null) && (!pageResult.isEmpty())) {
                //Init response only if there are records to be returned
                if(result == null) {
                    result = new ArrayList<>();
                }

                result.addAll(pageResult);
            } else {
                break;
            }
        }

        return result;
    }
}
