/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.services.dl.InstallationPackageColumn;

/**
 * Enum wrapper for InstallationPackageColumn. Contains information for displaying cable column,
 * wrapping only columns that are available to be displayed in cable data table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum InstallationPackageColumnUI {
    NAME(InstallationPackageColumn.NAME,
            InstallationPackageColumnUIConstants.NAME_VALUE,
            null,
            InstallationPackageColumnUIConstants.NAME_TOOLTIP,
            InstallationPackageColumnUIConstants.NAME_STYLECLASS,
            InstallationPackageColumnUIConstants.NAME_STYLE,
            InstallationPackageColumnUIConstants.NAME_FILTERMODE,
            InstallationPackageColumnUIConstants.NAME_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    DESCRIPTION(InstallationPackageColumn.DESCRIPTION,
            InstallationPackageColumnUIConstants.DESCRIPTION_VALUE,
            null,
            InstallationPackageColumnUIConstants.DESCRIPTION_TOOLTIP,
            InstallationPackageColumnUIConstants.DESCRIPTION_STYLECLASS,
            InstallationPackageColumnUIConstants.DESCRIPTION_STYLE,
            InstallationPackageColumnUIConstants.DESCRIPTION_FILTERMODE,
            InstallationPackageColumnUIConstants.DESCRIPTION_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    LOCATION(InstallationPackageColumn.LOCATION,
            InstallationPackageColumnUIConstants.LOCATION_VALUE,
            null,
            InstallationPackageColumnUIConstants.LOCATION_TOOLTIP,
            InstallationPackageColumnUIConstants.LOCATION_STYLECLASS,
            InstallationPackageColumnUIConstants.LOCATION_STYLE,
            InstallationPackageColumnUIConstants.LOCATION_FILTERMODE,
            InstallationPackageColumnUIConstants.LOCATION_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    CABLE_COORDINATOR(InstallationPackageColumn.CABLE_COORDINATOR,
            InstallationPackageColumnUIConstants.CABLE_COORDINATOR_VALUE,
            null,
            InstallationPackageColumnUIConstants.CABLE_COORDINATOR_TOOLTIP,
            InstallationPackageColumnUIConstants.CABLE_COORDINATOR_STYLECLASS,
            InstallationPackageColumnUIConstants.CABLE_COORDINATOR_STYLE,
            InstallationPackageColumnUIConstants.CABLE_COORDINATOR_FILTERMODE,
            InstallationPackageColumnUIConstants.CABLE_COORDINATOR_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    ROUTING_DOCUMENTATION(InstallationPackageColumn.ROUTING_DOCUMENTATION,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_VALUE,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_URL,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_TOOLTIP,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_STYLECLASS,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_STYLE,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_FILTERMODE,
            InstallationPackageColumnUIConstants.ROUTING_DOCUMENTATION_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    INSTALLER_CABLE(InstallationPackageColumn.INSTALLER_CABLE,
            InstallationPackageColumnUIConstants.INSTALLER_CABLE_VALUE,
            null,
            InstallationPackageColumnUIConstants.INSTALLER_CABLE_TOOLTIP,
            InstallationPackageColumnUIConstants.INSTALLER_CABLE_STYLECLASS,
            InstallationPackageColumnUIConstants.INSTALLER_CABLE_STYLE,
            InstallationPackageColumnUIConstants.INSTALLER_CABLE_FILTERMODE,
            InstallationPackageColumnUIConstants.INSTALLER_CABLE_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INSTALLER_CONNECTOR_A(InstallationPackageColumn.INSTALLER_CONNECTOR_A,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_A_VALUE,
            null,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_A_TOOLTIP,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_A_STYLECLASS,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_A_STYLE,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_A_FILTERMODE,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_A_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INSTALLER_CONNECTOR_B(InstallationPackageColumn.INSTALLER_CONNECTOR_B,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_B_VALUE,
            null,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_B_TOOLTIP,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_B_STYLECLASS,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_B_STYLE,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_B_FILTERMODE,
            InstallationPackageColumnUIConstants.INSTALLER_CONNECTOR_B_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INSTALLATION_PACKAGE_LEADER(InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER,
            InstallationPackageColumnUIConstants.INSTALLATION_PACKAGE_LEADER_VALUE,
            null,
            InstallationPackageColumnUIConstants.INSTALLATION_PACKAGE_LEADER_TOOLTIP,
            InstallationPackageColumnUIConstants.INSTALLATION_PACKAGE_LEADER_STYLECLASS,
            InstallationPackageColumnUIConstants.INSTALLATION_PACKAGE_LEADER_STYLE,
            InstallationPackageColumnUIConstants.INSTALLATION_PACKAGE_LEADER_FILTERMODE,
            InstallationPackageColumnUIConstants.INSTALLATION_PACKAGE_LEADER_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    NUMBER_OF_CABLES(InstallationPackageColumn.NUMBER_OF_CABLES,
            InstallationPackageColumnUIConstants.NUMBER_OF_CABLES_VALUE,
            null,
            InstallationPackageColumnUIConstants.NUMBER_OF_CABLES_TOOLTIP,
            InstallationPackageColumnUIConstants.NUMBER_OF_CABLES_STYLECLASS,
            InstallationPackageColumnUIConstants.NUMBER_OF_CABLES_STYLE,
            InstallationPackageColumnUIConstants.NUMBER_OF_CABLES_FILTERMODE,
            InstallationPackageColumnUIConstants.NUMBER_OF_CABLES_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    STATUS(InstallationPackageColumn.STATUS,
            InstallationPackageColumnUIConstants.STATUS_VALUE,
            null,
            InstallationPackageColumnUIConstants.STATUS_TOOLTIP,
            InstallationPackageColumnUIConstants.STATUS_STYLECLASS,
            InstallationPackageColumnUIConstants.STATUS_STYLE,
            InstallationPackageColumnUIConstants.STATUS_FILTERMODE,
            InstallationPackageColumnUIConstants.STATUS_FILTERSTYLE,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);

    private InstallationPackageColumn parent;
    private String tooltip;
    private String value;
    private String url;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private boolean filterDropdown;
    private CableColumnStyle columnStyle;

    /**
     * InstallationPackageColumnUI constructor decides what labels in column to display according to property.
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param url
     *            property for url
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     * @param cableColumnStyleEnum
     *            column style
     */
    private InstallationPackageColumnUI(InstallationPackageColumn parent,
            String value, String url, String tooltip,
            String styleClass, String style, String filterMode, String filterStyle,
            CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.url = url;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);
        if (InstallationPackageColumnUIConstants.STATUS_VALUE.equals(value)) {
            this.filterDropdown = true;
        }
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /** @return String url */
    public String getUrl() {
        return url;
    }

    /**
     * Searches for InstallationPackageColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return InstallationPackageColumnUI enum
     */
    public static InstallationPackageColumnUI convertColumnLabel(String columnLabel) {
        for (InstallationPackageColumnUI value : InstallationPackageColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (InstallationPackageColumnUI value : InstallationPackageColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public boolean isFilterDropdown() {
        return this.filterDropdown;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }

    public boolean isSortable(){
        return StringUtils.isNotEmpty(this.value);
    }

}
