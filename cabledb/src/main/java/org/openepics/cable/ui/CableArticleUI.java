/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

import java.io.Serializable;

import org.openepics.cable.model.CableArticle;
import org.openepics.cable.services.DateUtil;

/**
 * <code>CableArticleUI</code> is a presentation of {@link CableArticle} used in UI.
 *
 * @author Lars Johansson
 */
public class CableArticleUI implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 582993918384993704L;

    /** String representing valid cable article status. */
    public static final String STATUS_VALID = "VALID";
    /** String representing obsolete cable article status. */
    public static final String STATUS_OBSOLETE = "OBSOLETE";

    private final CableArticle cableArticle;

    /**
     * Constructs InstallationPackage UI object for empty installation package instance.
     */
    public CableArticleUI() {
        this.cableArticle = new CableArticle();
    }

    /**
     * Constructs CableArticle UI object for given cable article instance.
     *
     * @param cableArticle cable article instance
     */
    public CableArticleUI(CableArticle cableArticle) {
        this.cableArticle = cableArticle;
    }

    /** @return the cable article instance this wraps */
    public CableArticle getCableArticle() {
        return cableArticle;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(30);
        sb.append("Cable Article: ");
        sb.append(cableArticle.getName());
        return sb.toString();
    }

    public Long getId() {
        return cableArticle.getId();
    }
    public String getName() {
        return cableArticle.getName();
    }

    public String getManufacturer() {
        return cableArticle.getManufacturer();
    }
    public void setManufacturer(String manufacturer) {
        cableArticle.setManufacturer(manufacturer);
    }
    public String getExternalId() {
        return cableArticle.getExternalId();
    }
    public void setExternalId(String externalId) {
        cableArticle.setExternalId(externalId);
    }
    public String getErpNumber() {
        return cableArticle.getErpNumber();
    }
    public void setErpNumber(String erpNumber) {
        cableArticle.setErpNumber(erpNumber);
    }
    public String getIsoClass() {
        return cableArticle.getIsoClass();
    }
    public void setIsoClass(String isoClass) {
        cableArticle.setIsoClass(isoClass);
    }
    public String getDescription() {
        return cableArticle.getDescription();
    }
    public void setDescription(String description) {
        cableArticle.setDescription(description);
    }
    public String getLongDescription() {
        return cableArticle.getLongDescription();
    }
    public void setLongDescription(String longDescription) {
        cableArticle.setLongDescription(longDescription);
    }
    public String getModelType() {
        return cableArticle.getModelType();
    }
    public void setModelType(String modelType) {
        cableArticle.setModelType(modelType);
    }
    public Float getBendingRadius() {
        return cableArticle.getBendingRadius();
    }
    public void setBendingRadius(Float bendingRadius) {
        cableArticle.setBendingRadius(bendingRadius);
    }
    public Float getOuterDiameter() {
        return cableArticle.getOuterDiameter();
    }
    public void setOuterDiameter(Float outerDiameter) {
        cableArticle.setOuterDiameter(outerDiameter);
    }
    public Float getRatedVoltage() {
        return cableArticle.getRatedVoltage();
    }
    public void setRatedVoltage(Float ratedVoltage) {
        cableArticle.setRatedVoltage(ratedVoltage);
    }
    public Float getWeightPerLength() {
        return cableArticle.getWeightPerLength();
    }
    public void setWeightPerLength(Float weightPerLength) {
        cableArticle.setWeightPerLength(weightPerLength);
    }
    public String getShortName() {
        return cableArticle.getShortName();
    }
    public void setShortName(String shortName) {
        cableArticle.setShortName(shortName);
    }
    public String getShortNameExternalId() {
        return cableArticle.getShortNameExternalId();
    }
    public void setShortNameExternalId(String shortNameExternalId) {
        cableArticle.setShortNameExternalId(shortNameExternalId);
    }
    public String getUrlChessPart() {
        return cableArticle.getUrlChessPart();
    }
    public void setUrlChessPart(String urlChessPart) {
        cableArticle.setUrlChessPart(urlChessPart);
    }
    public boolean isActive() {
        return cableArticle.isActive();
    }
    public void setActive(boolean active) {
        cableArticle.setActive(active);
    }
    public String getCreated() {
        return DateUtil.format(cableArticle.getCreated());
    }
    public String getModified() {
        return DateUtil.format(cableArticle.getModified());
    }

    /**
     * @return {@link #STATUS_VALID} if cable article is in active use, {@link #STATUS_OBSOLETE} else
     */
    public String getValid() {
        return cableArticle.isActive() ? STATUS_VALID : STATUS_OBSOLETE;
    }

    /**
     * Gets condition of the CableArticleUI property.
     *
     * @param columnLabel
     *            the label of the property
     *
     * @return condition of the property
     */
    public String getCableArticlePropertyCondition() {
        if (isActive()) {
            return CableUIPropertyCondition.VALID.getCablePropertyCondition();
        }
        return CableUIPropertyCondition.INVALID.getCablePropertyCondition();
    }


    @Override
    public int hashCode() {
        return cableArticle.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return cableArticle.equals(((CableArticleUI) obj).getCableArticle());
    }

}
