/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import org.openepics.cable.util.ManufacturerUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.CableProperties;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.CableTypeManufacturer;
import org.openepics.cable.model.InstallationType;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.model.util.StringUtil;
import org.openepics.cable.services.CableTypeActiveFilter;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.ManufacturerService;
import org.openepics.cable.services.QueryService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;
import org.openepics.cable.services.dl.CableTypeColumn;
import org.openepics.cable.services.dl.CableTypeImportExportService;
import org.openepics.cable.services.dl.LoaderResult;
import org.openepics.cable.util.CookieUtility;
import org.openepics.cable.util.EncodingUtility;
import org.openepics.cable.util.UiUtility;
import org.openepics.cable.util.Utility;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import com.google.common.base.Preconditions;
import com.google.common.io.ByteStreams;
import java.io.FileNotFoundException;
import java.util.stream.Collectors;
import org.openepics.cable.model.GenericArtifact;

/**
 * This is the backing requests bean for cable-types.xhtml.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@ManagedBean
@ViewScoped
public class CableTypeRequestManager implements Serializable {

    private static final long serialVersionUID = 9208792434306606451L;

    private static final String CDB_CABLE_TYPES_XLSX = "cdb_cable_types.xlsx";
    // initial number of entities per page
    private static final int NUMBER_OF_ENTITIES_PER_PAGE = 30;
    private static final String EMPTY_STRING = "";
    private static final Logger LOGGER = Logger.getLogger(CableTypeRequestManager.class.getName());
    private static final List<CableTypeUI> EMPTY_LIST = new ArrayList<>();

    private List<CableTypeColumnUI> columns;
    private List<String> columnTemplate = CableTypeColumnUI.getAllColumns();

    @Inject
    private transient CableTypeService cableTypeService;
    @Inject
    private transient CableTypeImportExportService cableTypeImportExportService;

    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;
    @Inject
    private SessionService sessionService;
    @Inject
    private MailService mailService;
    @Inject
    private transient ArtifactRequestManager artifactRequestManager;

    @Inject
    private QueryService queryService;
    @Inject
    private ManufacturerService manufacturerService;


    private List<CableTypeUI> cableTypes;
    private List<CableTypeUI> filteredCableTypes;
    private List<CableTypeUI> selectedCableTypes = EMPTY_LIST;
    private List<CableTypeUI> deletedCableTypes;
    private List<CableTypeUI> restoredCableTypes;

    private String globalFilter;

    private byte[] fileToBeImported;
    private LoaderResult<CableType> importResult;

    private CableTypeUI selectedCableType;
    private QueryUI selectedQuery;
    private boolean isAddPopupOpened;

    // for overlay panels
    private String longTextOverlayHeader;
    private String longTextOverlayContent;
    private CableTypeUI longManufacturersOverlayCableType;

    // for notification when user request new cable type
    private int numberOfConductors;
    private float crossSectionOfConductors;
    private String voltageRating;
    private String jacket;
    private String insulation;
    private String additionalInformation;
    private String fileToBeSendName;
    private InputStream fileToBeSend;
    private boolean isCableTypeRequested;
    private String requestedCableTypeName;

    private List<Manufacturer> manufacturers;

    private CableTypeManufacturer selectedManufacturer;

    private List<Manufacturer> availableManufacturers;

    private CableType oldCableType;

    // datatable
    //     number of columns
    //     column visibility
    //     number of rows/entries per page in pagination component
    //     row number, if applicable, for requested entry in list of all entries
    private int numberOfColumns;
    private List<Boolean> columnVisibility;
    private int rows;
    private int rowNumber;

    /**
     * Constructs the controller bean and handles one time setup.
     */
    public CableTypeRequestManager() {
        // initialize datatable
        //     number of columns
        //         enum columns + history column
        //     column visibility
        //         all columns initialized as visible, may be updated
        //     rows per page
        numberOfColumns = CableTypeColumnUI.values().length + 1;
        columnVisibility = ManagerUtil.setAllColumnVisible(numberOfColumns);
        rows = NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /** Initializes the bean for initial view display. */
    @PostConstruct
    public void init() {
        try {
            isAddPopupOpened = false;

            clearImportState();
            selectedCableTypes.clear();
            selectedCableType = null;
            requestedCableTypeName = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getParameter("cableTypeName");
            createDynamicColumns();
            refreshCableTypes();

            // prepare datatable
            //     cookies
            //         rows per page
            //         column visibility
            //     row number (in all rows) for requested entry (if any)
            Cookie[] cookies = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest()).getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    switch (cookie.getName()) {
                        case CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE:
                            initPaginationPageSize(cookie.getValue());
                            break;
                        case CookieUtility.CD_CABLETYPE_COLUMN_VISIBILITY:
                            initColumnVisibility(cookie.getValue());
                            break;
                        default:
                            break;
                    }
                }
            }
            rowNumber = rowNumber(requestedCableTypeName);
        } catch (Exception e) {
            throw new UIException("Cable type display initialization failed: " + e.getMessage(), e);
        }
    }

    /** Refreshes cable types list. */
    private void refreshCableTypes() {
        cableTypes = buildCableTypeUIs();
    }

    /** @return current cable type list. */
    private List<CableTypeUI> buildCableTypeUIs() {

        final List<CableTypeUI> cableTypeUIs = new ArrayList<>();
        List<CableType> types = null;
        if (selectedQuery != null) {
            types = new ArrayList<>(
                    cableTypeService.getFilteredCableTypes(getSqlQuery(), selectedQuery.getQuery()));
        } else {
            types = cableTypeService.getAllCableTypes();
        }

        selectedCableType = null;
        selectedCableTypes = EMPTY_LIST;

        for (CableType cableType : types) {
            final CableTypeUI cableTypeUI = new CableTypeUI(cableType);
            cableTypeUIs.add(cableTypeUI);
        }
        return cableTypeUIs;
    }

    /**
     * Init pagination page size from given value.
     *
     * @param pageSize page size to be interpreted
     */
    private void initPaginationPageSize(String pageSize) {
        // keep track of page size in variable
        if (!StringUtils.isEmpty(pageSize)) {
            int value = Integer.parseInt(pageSize);
            setRows(value);
        }
    }

    /**
     * Init column visibility from given value.
     *
     * @param visibility column visibility to be interpreted
     */
    private void initColumnVisibility(String visibility) {
        // keep track of column visibility in list
        ManagerUtil.initColumnVisibility(visibility, numberOfColumns, columnVisibility);
    }

    /**
     * Find out row number, if applicable, for selected entry in list of all entries.
     *
     * @param cableTypeName
     * @return
     */
    private int rowNumber(String cableTypeName) {
        // note
        //     currently not consider filter

        CableTypeUI cableTypeToSelect = null;
        this.isCableTypeRequested = false;
        if (requestedCableTypeName != null) {
            isCableTypeRequested = true;
            cableTypeToSelect = getCableTypeFromCableTypeName(requestedCableTypeName);
        }

        selectedCableTypes.clear();
        if (cableTypeToSelect != null) {
            selectedCableType = cableTypeToSelect;
            selectedCableTypes.add(selectedCableType);

            int elementPosition = 0;
            for (CableTypeUI cableType : cableTypes) {
                if (cableType.getName().equals(requestedCableTypeName)) {
                    return elementPosition;
                }
                elementPosition++;
            }
        }

        return 0;
    }

    private CableTypeUI getCableTypeFromCableTypeName(final String cableTypeName) {
        if (cableTypeName == null || cableTypeName.isEmpty()) {
            return null;
        }

        CableTypeUI cableTypeToSelect = null;
        for (CableTypeUI cableType : cableTypes) {
            if (cableType.getName().equals(cableTypeName)) {
                cableTypeToSelect = cableType;
            }
        }

        return cableTypeToSelect;
    }

    /** @return the selected cable type's manufacturers. */
    public List<CableTypeManufacturer> getSelectedManufacturers() {
        if(selectedCableType == null) {
            return null;
        }
        return selectedCableType.getManufacturers();
    }

    /**
     * Return true if the current user can import approved cable types, else false.
     * Note check if username in cable freeze bypass usernames.
     *
     * @return true if the current user can import approved cable types, else false
     */
    public boolean canImportCableTypes() {
        return cableTypeImportExportService.canImportCableTypes()
                && Utility.isUsernameInCableFreezeBypassUsernames(sessionService.getLoggedInName());
    }

    /**
     * @return <code>true</code> if the uploaded file that hasn't been imported yet exists, <code>false</code> otherwise
     */
    public boolean getFileToBeImportedExists() {
        return fileToBeImported != null;
    }

    /**
     * Returns the cable types to be exported, which are the currently filtered and selected cable types, or all
     * filtered cables if none selected.
     *
     * @return the cables to be exported
     */
    public List<CableTypeUI> getCableTypesToExport() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        final List<CableTypeUI> cableTypesToExport = new ArrayList<>();
        for (CableTypeUI cableType : getCableTypes()) {
            if (cableType.isActive() && isIncludedByFilter(cableType)) {
                cableTypesToExport.add(cableType);
            }
        }
        LOGGER.fine("Returning cable types to export: " + cableTypesToExport.size());
        return cableTypesToExport;
    }

    /** @return true if edit cable type button is enabled, otherwise false. */
    public boolean isEditButtonEnabled() {
        return sessionService.isLoggedIn() && selectedCableType != null && selectedCableType.isActive()
                && sessionService.canAdminister();
    }

    /** @return true if Delete cable type button is enabled, otherwise false. */
    public boolean isDeleteButtonDisabled() {
        return isStatusChangeButtonDisabled(true);
    }

    /** @return true if Restore cable type button is enabled, otherwise false. */
    public boolean isRestoreButtonDisabled() {
        return isStatusChangeButtonDisabled(false);
    }

    private boolean isStatusChangeButtonDisabled(final boolean toObsoleteStatus) {
        if (selectedCableTypes == null || selectedCableTypes.isEmpty()) {
            return true;
        }

        return !sessionService.canAdminister() || selectedCableTypes.stream().
                anyMatch(cableTypeToDelete -> toObsoleteStatus != cableTypeToDelete.isActive());
    }

    private boolean isIncludedByFilter(CableTypeUI cableType) {
        return !filteredCableTypesExist() || getFilteredCableTypes().contains(cableType);
    }

    private boolean filteredCableTypesExist() {
        return getFilteredCableTypes() != null && !getFilteredCableTypes().isEmpty();
    }

    /** @return the cableTypes to be displayed */
    public List<CableTypeUI> getCableTypes() {
        if (!sessionService.isLoggedIn())
            return EMPTY_LIST;

        return cableTypes;
    }

    /** @return the list of all installation types with ALL option */
    public List<SelectItem> getInstallationTypes() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = getInstallationTypesWithoutAll();
        selectItems.add(0, new SelectItem("", "All"));
        return selectItems;
    }

    /** @return the list of all installation types without ALL option */
    public List<SelectItem> getInstallationTypesWithoutAll() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (InstallationType installationType : InstallationType.values()) {
            selectItems.add(new SelectItem(installationType.toString(), installationType.getDisplayName()));
        }
        return selectItems;
    }

    /** @return the list of all obsolete values. */
    public List<SelectItem> getObsoleteValues() {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", "All"));
        selectItems.add(new SelectItem("false", CableTypeUI.STATUS_OBSOLETE));
        selectItems.add(new SelectItem("true", CableTypeUI.STATUS_VALID));
        return selectItems;
    }

    /** @return the list of all voltage ratings */
    public List<SelectItem> getVoltages() {
        return getSelectItems(cableTypeService.getVoltageValues(CableTypeActiveFilter.ALL));
    }

    /** @return the list of all insulation types */
    public List<SelectItem> getInsulations() {
        return getSelectItems(cableTypeService.getInsulationValues(CableTypeActiveFilter.ALL));
    }

    /** @return the list of all jacket types */
    public List<SelectItem> getJackets() {
        return getSelectItems(cableTypeService.getJacketValues(CableTypeActiveFilter.ALL));
    }

    /** @return the list of all flammability classifications */
    public List<SelectItem> getFlammabilities() {
        return getSelectItems(cableTypeService.getFlammabilityValues(CableTypeActiveFilter.ALL));
    }

    private List<SelectItem> getSelectItems(Iterable<String> values) {
        if (!sessionService.isLoggedIn())
            return Collections.emptyList();

        final List<SelectItem> selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("", "All"));
        for (String insulation : values) {
            selectItems.add(new SelectItem(insulation));
        }
        return selectItems;
    }

    /** @return the filtered cableTypes to be displayed */
    public List<CableTypeUI> getFilteredCableTypes() {
        return filteredCableTypes;
    }

    /**
     * @param filteredCableTypes
     *            the filtered cableTypes to set
     */
    public void setFilteredCableTypes(List<CableTypeUI> filteredCableTypes) {
        this.filteredCableTypes = filteredCableTypes;
        LOGGER.fine("Setting filtered cableTypes: " + (filteredCableTypes != null ? filteredCableTypes.size() : "no")
                + " cableTypes.");
    }

    /** @return the global text filter */
    public String getGlobalFilter() {
        return globalFilter;
    }

    /**
     * @param globalFilter
     *            the global text filter to set
     */
    public void setGlobalFilter(String globalFilter) {
        this.globalFilter = globalFilter;
        LOGGER.fine("Setting global filter: " + this.globalFilter);
    }

    /** @return the list of all cable type active filter values */
    public List<CableTypeActiveFilter> getCableTypeActiveFilterValues() {
        return Arrays.asList(CableTypeActiveFilter.values());
    }

    /** Refreshes cable type list based on filters. */
    public void filterCableTypes() {
        LOGGER.fine("Invoked filter cable types.");
        refreshCableTypes();
    }

    /** @return the result of a test or true import */
    public LoaderResult<CableType> getImportResult() {
        return importResult;
    }

    /** Clears the import state. */
    public void clearImportState() {
        LOGGER.fine("Invoked clear import state.");
        fileToBeImported = null;
        importResult = null;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void cableTypeFileUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked cableType file upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try (InputStream inputStream = uploadedFile.getInputstream()) {
                fileToBeImported = ByteStreams.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs a test of the cableType import from the file that was last uploaded.
     */
    public void cableTypeImportTest() {
        LOGGER.fine("Invoked cableType import test.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableTypeImportExportService.importCableTypes(inputStream, true);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /**
     * Performs the cableType import from the file that was last uploaded.
     */
    public void cableTypeImport() {
        LOGGER.fine("Invoked cableType import.");

        try {
            try (InputStream inputStream = new ByteArrayInputStream(fileToBeImported)) {
                importResult = cableTypeImportExportService.importCableTypes(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (!importResult.isError()) {
                refreshCableTypes();
                fileToBeImported = null;
            }

        } catch (RuntimeException e) {
            clearImportState();
            throw e;
        }
    }

    /** @return the cable type sheet with all cable types */
    public StreamedContent getCableTypeSheet() {
        return new DefaultStreamedContent(
                cableTypeImportExportService.exportCableTypes(cableTypeService.getAllCableTypes()),
                Utility.XLSX_CONTENT_TYPE,
                CDB_CABLE_TYPES_XLSX);
    }

    /** @return the cable type sheet with the cable types from cableTypesToExport list */
    public StreamedContent getCableTypeSheetWithCableTypesToExport() {

        final List<CableType> cableTypesToExport = new ArrayList<>();
        for (CableTypeUI cableTypeUI : getCableTypesToExport()) {
            cableTypesToExport.add(cableTypeUI.getCableType());
        }
        return new DefaultStreamedContent(cableTypeImportExportService.exportCableTypes(cableTypesToExport),
                Utility.XLSX_CONTENT_TYPE,
                CDB_CABLE_TYPES_XLSX);
    }

    /** @return the cable type import template */
    public StreamedContent getImportTemplate() {
        LOGGER.fine("Get cable types template");
        return new DefaultStreamedContent(cableTypeImportExportService.exportCableTypes(new ArrayList<>()),
                Utility.XLSX_CONTENT_TYPE,
                CDB_CABLE_TYPES_XLSX);
    }

    /** @return the currently selected cable types, cannot return null */
    public List<CableTypeUI> getSelectedCableTypes() {
        return selectedCableTypes;
    }

    /**
     * @param selectedCableTypes
     *            the cable types to select
     */
    public void setSelectedCableTypes(List<CableTypeUI> selectedCableTypes) {
        this.selectedCableTypes = selectedCableTypes != null ? selectedCableTypes : EMPTY_LIST;
        LOGGER.fine("Setting selected cable types: " + this.selectedCableTypes.size());
    }

    /** Clears the current cable type selection. */
    public void clearSelectedCableTypes() {
        LOGGER.fine("Invoked clear cable type selection.");
        setSelectedCableTypes(null);
    }

    /**
     * Event triggered when paging for data table.
     */
    public void onPaginate() {
        unselectAllRows();
    }

    /**
     * Event triggered when paging for data table is complete. Allows keeping track of pagination page size.
     */
    public void onPaginatePageSize() {
        // set pagination page size cookie
        String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                + CookieUtility.CD_CABLE_PAGINATION_PAGE_SIZE
                + "', " + getRows()
                + ", {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
        PrimeFaces.current().executeScript(statement);
    }

    /**
     * Event triggered when toggling column visibility. Allows keeping track of column visibility.
     *
     * @param event toggle event
     */
    public void onToggle(ToggleEvent event) {
        // keep track of column visibility
        // set column visibility cookie
        //     serialize column visibility
        //     set cookie

        if (((Integer) event.getData()) < numberOfColumns) {
            columnVisibility.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);

            String statement = "var a=PrimeFaces.cookiesEnabled(); if(a){PrimeFaces.setCookie('"
                    + CookieUtility.CD_CABLETYPE_COLUMN_VISIBILITY
                    + "', '" + columnVisibility2String()
                    + "', {expires:" + CookieUtility.PERSISTENCE_DAYS + "});}";
            PrimeFaces.current().executeScript(statement);
        }
    }

    /**
     * Serializes column visibility into string consisting of delimiter-separated string values,
     * each <code>true</code> or <code>false</code>.
     *
     * @return
     */
    private String columnVisibility2String() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<numberOfColumns; i++) {
            sb.append(columnVisibility.get(i));
            if (i < (numberOfColumns-1))
                sb.append(CookieUtility.DELIMITER_ENTRIES);
        }
        return sb.toString();
    }

    /**
     * Returns column visibility for column with given index.
     *
     * @param columnIndex column index for column in UI, counting from left
     * @return true if column is marked as visible
     */
    public boolean isColumnVisible(int columnIndex) {
        return columnVisibility.get(columnIndex);
    }

    /**
     * Unselect all rows and proceed accordingly, i.e. clear selected cable types and row selection.
     */
    public void unselectAllRows() {
        clearSelectedCableTypes();

        onRowSelect();
    }

    /**
     * Event triggered when row is selected in table in UI.
     */
    public void onRowSelect() {
        if (selectedCableTypes != null && !selectedCableTypes.isEmpty()) {
            if (selectedCableTypes.size() == 1) {
                selectedCableType = selectedCableTypes.get(0);
            } else {
                selectedCableType = null;
            }
        } else {
            selectedCableType = null;
        }
    }

    /**
     * @return true if add popup is opened otherwise false.
     */
    public boolean isAddPopupOpened() {
        return isAddPopupOpened;
    }

    /**
     * Prepare for Add cable type dialog.
     */
    public void prepareAddPopup() {
        selectedCableType = new CableTypeUI();
        selectedManufacturer = null;
        isAddPopupOpened = true;
    }

    /**
     * Reset values. May be used at e.g. close of delete dialog.
     */
    public void resetValues() {
        deletedCableTypes = null;
        restoredCableTypes = null;
    }

    /**
     * Event triggered when cable type is created.
     */
    public void onCableTypeAdd() {
        final String name = selectedCableType.getName();
        for (CableTypeManufacturer manufacturer : selectedCableType.getManufacturers()) {
            updateArtifact(manufacturer.getDatasheet());
        }
        formatCableType(selectedCableType);
        cableTypeService.createCableType(name, selectedCableType.getInstallationType(),
                selectedCableType.getDescription(), selectedCableType.getService(), selectedCableType.getVoltage(),
                selectedCableType.getInsulation(), selectedCableType.getJacket(), selectedCableType.getFlammability(),
                selectedCableType.getTid(), selectedCableType.getWeight(), selectedCableType.getDiameter(),
                selectedCableType.getManufacturers(), selectedCableType.getComments(), selectedCableType.getRevision(),
                sessionService.getLoggedInName());
        filterCableTypes();
        UiUtility.showInfoMessage("Cable type '" + name + "' added.");
        UiUtility.updateComponent("cableDBGrowl");
    }

    /**
     * Prepare for Edit cable type dialog.
     */
    public void prepareEditPopup() {
        Preconditions.checkNotNull(selectedCableType);
        // We create a duplicate of the selected cable type from database to prevent wrong data display on close, and to
        // preserve changes for history logging.
        oldCableType = selectedCableType.getCableType();
        cableTypeService.detachCableType(oldCableType);
        selectedCableType =
                new CableTypeUI(cableTypeService.getCableTypeByName(selectedCableType.getCableType().getName()));
        selectedManufacturer = null;
        isAddPopupOpened = false;
    }

    /**
     * Event triggered when cable type is updated.
     */
    public void onCableTypeEdit() {
        Preconditions.checkNotNull(selectedCableType);
        final String name = selectedCableType.getName();
        for (CableTypeManufacturer manufacturer : selectedCableType.getManufacturers()) {
            updateArtifact(manufacturer.getDatasheet());
        }
        formatCableType(selectedCableType);
        final CableType newCableType = selectedCableType.getCableType();
        final boolean updated = cableTypeService.updateCableType(newCableType, oldCableType, sessionService.getLoggedInName());
        filterCableTypes();
        UiUtility.showInfoMessage("Cable type '" + name + (updated ? "' updated." : "' not updated."));
        UiUtility.updateComponent("cableDBGrowl");
    }

    private void updateArtifact(GenericArtifact artifact) {
        if (artifact != null && artifact.getContent() != null) {
            artifact.setContent(artifact.getContent());
        }
    }

    /**
     * @return the list of deleted cable types.
     */
    public List<CableTypeUI> getDeletedCableTypes() {
        return deletedCableTypes;
    }

    /**
     * @return the list of restored cable types.
     */
    public List<CableTypeUI> getRestoredCableTypes() {
        return restoredCableTypes;
    }

    /**
     * The method builds a list of cable types that are already deleted. If the list is not empty, it is displayed to
     * the user and the user is prevented from deleting them.
     */
    public void checkCableTypesForDeletion() {
        Preconditions.checkNotNull(selectedCableTypes);
        Preconditions.checkState(!selectedCableTypes.isEmpty());

        deletedCableTypes = selectedCableTypes.stream().filter(cableType -> !cableType.isActive()).
                collect(Collectors.toList());
    }

    /**
     * The method builds a list of cable types that are already restored. If the list is not empty, it is displayed to
     * the user and the user is prevented from restoring them.
     */
    public void checkCableTypesForRestore() {
        Preconditions.checkNotNull(selectedCableTypes);
        Preconditions.checkState(!selectedCableTypes.isEmpty());

        restoredCableTypes = selectedCableTypes.stream().filter(CableTypeUI::isActive).collect(Collectors.toList());
    }

    /**
     * Event triggered when cable type is deleted.
     */
    public void onCableTypeDelete() {
        changeCableTypeStatus(deletedCableTypes, false, "Deleted");
        deletedCableTypes = null;
    }

    /**
     * Event triggered when cable type is restored.
     */
    public void onCableTypeRestore() {
        changeCableTypeStatus(restoredCableTypes, true, "Restored");
        restoredCableTypes = null;
    }

    private void changeCableTypeStatus(List<CableTypeUI> notChangeableCableTypes, final boolean active, final String operation) {
        Preconditions.checkNotNull(notChangeableCableTypes);
        Preconditions.checkState(notChangeableCableTypes.isEmpty());
        Preconditions.checkNotNull(selectedCableTypes);
        Preconditions.checkState(!selectedCableTypes.isEmpty());
        int changedCableTypesCounter = 0;
        for (final CableTypeUI cableTypeToRestore : selectedCableTypes) {
            if (cableTypeService.changeCableTypeStatus(cableTypeToRestore.getCableType(), active, sessionService.getLoggedInName())) {
                changedCableTypesCounter++;
            }
        }
        clearSelectedCableTypes();
        filteredCableTypes = null;
        filterCableTypes();
        UiUtility.showInfoMessage(operation + " " + changedCableTypesCounter + (changedCableTypesCounter == 1 ? " cable type." : " cable types."));
    }

    /**
     * Validate installation package name. Throws ValidatorException if cable type with given name already exists.
     *
     * @param ctx faces context
     * @param component ui component
     * @param value cable type name
     */
    public void isCableTypeNameValid(FacesContext ctx, UIComponent component, Object value) {
        if (value == null) {
            return;
        }
        String stringValue = value.toString();
        if (stringValue.isEmpty()) {
            return;
        }
        boolean invalid;
        boolean adding = selectedCableType.getId() == null;
        CableType cableType = cableTypeService.getCableTypeByName(stringValue);
        if (adding) {
            // Use case: Add new cable type
            invalid = cableType != null;
        } else {
            // Use case: Edit cable type
            invalid = cableType != null && !cableType.getId().equals(selectedCableType.getId());
        }
        if (invalid) {
            throw new ValidatorException(
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cable type with this name already exists.",
                            null));
        }
    }

    /**
     * Validates if entered value is Integer number.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not Integer number
     */
    public void isIntegerEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isIntegerEntered(value);
    }

    /**
     * Validates if entered value is Float number.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not Double number
     */
    public void isFloatEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isFloatEntered(value);
    }

    /**
     * Validates if entered value is URL.
     *
     * @param ctx faces context
     * @param component component
     * @param value entered value
     *
     * @throws ValidatorException if entered value is not URL
     */
    public void isURLEntered(FacesContext ctx, UIComponent component, Object value) {
        Utility.isURLEntered(value);
    }

    /** @return the selected cable type */
    public CableTypeUI getSelectedCableType() {
        return selectedCableType;
    }

    /**
     * Return true if the current user can edit cable types, else false.
     * Note check if username in cable freeze bypass usernames.
     *
     * @return true if the current user can edit cable types, else false
     */
    public boolean getEditCableType() {
        return sessionService.canAdminister()
                && Utility.isUsernameInCableFreezeBypassUsernames(sessionService.getLoggedInName());
    }

    /** @return number of conductors. */
    public int getNumberOfConductors() {
        return numberOfConductors;
    }

    /**
     * Set number of conductors.
     *
     * @param numberOfConductors
     *            number of conductors
     */
    public void setNumberOfConductors(int numberOfConductors) {
        this.numberOfConductors = numberOfConductors;
    }

    /** @return cross section of conductors. */
    public float getCrossSectionOfConductors() {
        return crossSectionOfConductors;
    }

    /**
     * Set cross section of conductors.
     *
     * @param crossSectionOfConductors
     *            cross section of conductors
     */
    public void setCrossSectionOfConductors(float crossSectionOfConductors) {
        this.crossSectionOfConductors = crossSectionOfConductors;
    }

    /** @return voltage rating. */
    public String getVoltageRating() {
        return voltageRating;
    }

    /**
     * Set voltage rating.
     *
     * @param voltageRating
     *            voltage rating
     */
    public void setVoltageRating(String voltageRating) {
        this.voltageRating = voltageRating;
    }

    /** @return jacket. */
    public String getJacket() {
        return jacket;
    }

    /**
     * Set jacket.
     *
     * @param jacket
     *            jacket
     */
    public void setJacket(String jacket) {
        this.jacket = jacket;
    }

    /** @return insulation. */
    public String getInsulation() {
        return insulation;
    }

    /**
     * Set insulation.
     *
     * @param insulation
     *            insulation
     */
    public void setInsulation(String insulation) {
        this.insulation = insulation;
    }

    /** @return additional informations. */
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Set additional informations.
     *
     * @param additionalInformation
     *            additional information
     */
    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    /**
     * Uploads and stores the file.
     *
     * @param event
     *            the event containing the file
     */
    public void fileToBeSendUpload(FileUploadEvent event) {
        LOGGER.fine("Invoked file to be send upload.");

        try {
            final UploadedFile uploadedFile = event.getFile();
            try {
                fileToBeSend = uploadedFile.getInputstream();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            fileToBeSendName = uploadedFile.getFileName();
        } catch (RuntimeException e) {
            resetFileToBeSendParameters();
            throw e;
        }
    }

    /** @return the name of the uploaded file */
    public String getFileToBeSendName() {
        return fileToBeSendName != null ? fileToBeSendName : "";
    }

    /**
     * Send cable type request mail.
     */
    public void sendCableTypeRequest() {
        final Set<String> notifiedUsers = new HashSet<>();
        for (final String userName : userDirectoryServiceFacade.getAllAdministratorUsernames()) {
            notifiedUsers.add(userName);
        }
        if (fileToBeSend != null) {
            mailService.sendMail(notifiedUsers, sessionService.getLoggedInName(), "New cable type request",
                    generateContent(), Arrays.asList(fileToBeSend), Arrays.asList(fileToBeSendName), true, true);
        } else {
            mailService.sendMail(notifiedUsers, sessionService.getLoggedInName(), "New cable type request",
                    generateContent(), null, null, false, true);
        }
        resetFileToBeSendParameters();
    }

    /**
     * @return generated request mail content.
     */
    public String generateContent() {
        StringBuilder sb = new StringBuilder(600);
        sb.append("New Cable Type Request");
        sb.append(" (by ");
        sb.append(userDirectoryServiceFacade.getUserFullNameAndEmail(sessionService.getLoggedInName()));
        sb.append(").").append('\n').append('\n');
        sb.append("Number of Conductors: ").append(numberOfConductors).append('\n');
        sb.append("Cross-Section of Conductors (mm2): ").append(crossSectionOfConductors).append('\n');
        sb.append("Voltage Rating (V): ").append(voltageRating).append('\n');
        sb.append("Jacket: ").append(jacket).append('\n');
        sb.append("Insulation: ").append(insulation).append('\n');
        sb.append("Additional Information: ").append(additionalInformation);
        return sb.toString();
    }

    /**
     * Reset all parameters used in request dialog.
     */
    public void resetFileToBeSendParameters() {
        numberOfConductors = 0;
        crossSectionOfConductors = 0f;
        voltageRating = EMPTY_STRING;
        jacket = EMPTY_STRING;
        insulation = EMPTY_STRING;
        additionalInformation = EMPTY_STRING;
        fileToBeSend = null;
        fileToBeSendName = null;
    }

    /** @return text for overlay header. */
    public String getLongTextOverlayHeader() {
        return longTextOverlayHeader;
    }

    /**
     * Sets the overlay header text.
     *
     * @param longTextOverlayHeader
     *            overlay header text.
     */
    public void setLongTextOverlayHeader(String longTextOverlayHeader) {
        this.longTextOverlayHeader = longTextOverlayHeader;
    }

    /** @return (formatted) text for overlay content. */
    public String getLongTextOverlayContent() {
        return longTextOverlayContent;
    }

    /**
     * Sets and formats text for overlay content.
     *
     * @param longTextOverlayContent
     *            overlay content to format.
     */
    public void setLongTextOverlayContent(String longTextOverlayContent) {
        this.longTextOverlayContent = Utility.formatOverlayContentText(longTextOverlayContent);
    }

    /**
     * Sets the cableType for the long manufacturers popup.
     *
     * @param longManufacturersOverlayCableType
     *            cable type.
     */
    public void setLongManufacturersOverlayCableType(CableTypeUI longManufacturersOverlayCableType) {
        this.longManufacturersOverlayCableType = longManufacturersOverlayCableType;
    }

    /** @return list of manufacturers for the long manufacturers popup. */
    public List<CableTypeManufacturer> getLongManufacturersListOverlay() {
        return (longManufacturersOverlayCableType != null) ? longManufacturersOverlayCableType.getManufacturers()
                : null;
    }

    public QueryUI getSelectedQuery() {
        return selectedQuery;
    }

    public void setSelectedQuery(QueryUI selectedQuery) {
        this.selectedQuery = selectedQuery;
    }

    /**
     * Execute selected query.
     *
     * @param e action event
     */
    public void executeQuery(ActionEvent e) {
        queryService.updateQueryExecutionDate(selectedQuery.getQuery());
        refreshCableTypes();
    }

    /**
     * Execute query with given id
     *
     * @param id query id
     */
    public void excuteQueryId(long id) {
        if (id != -1) {
            selectedQuery = new QueryUI(queryService.getQueryById(id));
            queryService.updateQueryExecutionDate(selectedQuery.getQuery());
            refreshCableTypes();
        }
    }

    /**
     * Reset query and refresh cable types.
     */
    public void resetQuery() {
        selectedQuery = null;
        refreshCableTypes();
    }

    public String getNumberOfFilteredItems() {
        return String.valueOf(cableTypes.size());
    }

    private String getSqlQuery() {
        StringBuilder querySB = new StringBuilder(600);
        List<QueryCondition> queryConditions = selectedQuery.getQueryConditions();
        if (queryConditions != null && !queryConditions.isEmpty()) {
            querySB.append("SELECT ct FROM CableType ct");
            querySB.append(' ').append("WHERE").append(' ');

            Collections.sort(queryConditions);

            for (QueryCondition condition : queryConditions) {

                CableTypeColumn field = CableTypeColumn.convertColumnLabel(condition.getField());

                // check if field is not null
                if (field == null)
                    continue;

                if (condition.getParenthesisOpen() == QueryParenthesis.OPEN) {
                    querySB.append(QueryParenthesis.OPEN.getParenthesis());
                }

                querySB.append("ct.").append(field.getFieldName()).append(' ');

                String value = condition.getValue();
                if (field == CableTypeColumn.STATUS) {
                    value = "Yes".equalsIgnoreCase(value) ? "false" : "true";
                } else if (field == CableTypeColumn.INSTALLATION_TYPE) {
                    InstallationType installationType = InstallationType.convertToInstallationType(value);
                    if (installationType != null) {
                        value = installationType.toString();
                    }
                }

                QueryComparisonOperator operator = condition.getComparisonOperator();
                if (operator.isStringComparisonOperator()) {
                    if (operator == QueryComparisonOperator.STARTS_WITH) {
                        querySB.append("LIKE").append(' ').append("'").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.CONTAINS) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("%'");
                    } else if (operator == QueryComparisonOperator.ENDS_WITH) {
                        querySB.append("LIKE").append(' ').append("'%").append(value).append("'");
                    }
                } else if (field.isStringComparisonOperator() && operator == QueryComparisonOperator.EQUAL) {
                    querySB.append("LIKE").append(' ').append("'").append(value).append("'");
                } else {
                    querySB.append(operator.getOperator()).append(' ').append("'").append(value).append("'");
                }

                if (condition.getParenthesisClose() == QueryParenthesis.CLOSE) {
                    querySB.append(QueryParenthesis.CLOSE.getParenthesis()).append(' ');
                } else {
                    querySB.append(' ');
                }

                if (condition.getBooleanOperator() != QueryBooleanOperator.NONE) {
                    querySB.append(condition.getBooleanOperator().getOperator()).append(' ');
                }
            }
        }
        return querySB.toString();
    }

    /** Resets column template to default. */
    private void resetColumnTemplate() {
        columnTemplate = CableTypeColumnUI.getAllColumns();
    }

    /**
     * Sets column template.
     *
     * @param columnTemplate
     *            of column names to which we should set template
     */
    private void setColumnTemplate(List<String> columnTemplate) {
        this.columnTemplate = columnTemplate;
    }

    /**
     * Returns current columns to show in cable data table
     *
     * @return columns
     */
    public List<CableTypeColumnUI> getColumns() {
        return columns;
    }

    /** Builds dynamic columns */
    private void createDynamicColumns() {
        columns = new ArrayList<CableTypeColumnUI>();
        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();
            CableTypeColumnUI column = CableTypeColumnUI.convertColumnLabel(key);
            if (column != null) {
                columns.add(column);
            }
        }
    }

    /** Updates columns according to column template and loads data in them. */
    public void updateColumns() {
        UIComponent table = FacesContext.getCurrentInstance().getViewRoot().findComponent(":cableTableForm:cableTable");
        table.setValueExpression("sortBy", null);
        createDynamicColumns();
    }

    /** @return number of column in current display view */
    public int getNumberOfColumns() {
        return columnTemplate.size() + 1;
    }

    /**
     * Provide suggestions for cable type given query to filter for.
     *
     * @param query query to filter for
     * @return suggestions for cable type given query to filter for.
     */
    public List<String> completeFilter(String query) {
        if (!sessionService.isLoggedIn()) {
            return Collections.emptyList();
        }

        final List<String> selectItems = new ArrayList<>();

        FacesContext context = FacesContext.getCurrentInstance();
        CableTypeColumnUI column = (CableTypeColumnUI) UIComponent.getCurrentComponent(context).getAttributes()
                .get("column");
        switch (column) {
        case INSTALLATION_TYPE:
            for (InstallationType type : InstallationType.values()) {
                selectItems.add(type.getDisplayName());
            }
            break;
        case STATUS:
            selectItems.add(CableTypeUI.STATUS_OBSOLETE);
            selectItems.add(CableTypeUI.STATUS_VALID);
            break;
        default:
            break;
        }
        return selectItems;
    }

    /**
     * Autocomplete method for manufacturers.
     *
     * @param query
     *            the query to filter manufacturers
     * @return all available manufacturers from manufacturer service
     */
    public List<Manufacturer> completeAvailableManufacturers(String query) {
        if (manufacturers == null) {
            manufacturers = manufacturerService.getManufacturers();
        }

        availableManufacturers =
                ManufacturerUtil.completeAvailableManufacturers(
                        query,
                        manufacturers,
                        selectedCableType.getManufacturers().stream()
                            .map(cm -> cm.getManufacturer())
                            .collect(Collectors.toList()));
        return availableManufacturers;

    }

    /**
     * Formats the given cable type by trimming and collapsing all whitespaces in its string fields.
     *
     * @param selectedCableType
     *            cable type to format
     */
    private void formatCableType(CableTypeUI selectedCableType) {
        selectedCableType.setComments(Utility.formatWhitespace(selectedCableType.getComments()));
        selectedCableType.setDescription(Utility.formatWhitespace(selectedCableType.getDescription()));
        selectedCableType.setFlammability(Utility.formatWhitespace(selectedCableType.getFlammability()));
        selectedCableType.setInstallationTypeString(
                Utility.formatWhitespace(selectedCableType.getInstallationTypeString()));
        selectedCableType.setInsulation(Utility.formatWhitespace(selectedCableType.getInsulation()));
        selectedCableType.setJacket(Utility.formatWhitespace(selectedCableType.getJacket()));
        selectedCableType.setName(Utility.formatWhitespace(selectedCableType.getName()));
        selectedCableType.setService(Utility.formatWhitespace(selectedCableType.getService()));
        //  no add/edit of revision, thus no format of revision
    }

    public String getRequestedCableTypeName() {
        return requestedCableTypeName;
    }

    /**
     * @return the cableTypeManufacturer
     */
    public CableTypeManufacturer getSelectedManufacturer() {
        return selectedManufacturer;
    }

    /**
     * Return if selected manufacturer has an associated datasheet.
     *
     * @return true if if selected manufacturer has an associated datasheet
     */
    public boolean selectedManufacturerHasDatasheet() {
        return selectedManufacturer != null && selectedManufacturer.getDatasheet() != null;
    }

    /**
     * @param cableTypeManufacturer
     *            the cableTypeManufacturer to set
     */
    public void setSelectedManufacturer(CableTypeManufacturer cableTypeManufacturer) {
        this.selectedManufacturer = cableTypeManufacturer;
    }

    /** @return true if current user is allowed to change cable type manufacturers */
    public boolean getChangeManufacturersPermission() {
        return sessionService.canAdminister();
    }

    /** Add column to currently selected displayView */
    public void addManufacturer() {
        List<Manufacturer> manufacturers = completeAvailableManufacturers("");
        if (manufacturers.isEmpty()) {
            UiUtility.showErrorMessage("Please define manufacturers under the manufacturer page first.");
            return;
        }
        CableTypeManufacturer manufacturer = new CableTypeManufacturer(selectedCableType.getCableType(),
                manufacturers.get(0), null, selectedCableType.getManufacturers().size());
        selectedCableType.getManufacturers().add(manufacturer);
    }

    /** Swap position of current manufacturer and the one before it. */
    public void moveManufacturerUp() {
        if (selectedCableType.getManufacturers() == null || selectedCableType.getManufacturers().size() <= 1) {
            return;
        }
        int position = selectedManufacturer.getPosition();
        if (position > 0) {
            CableTypeManufacturer movedManufacturer = selectedCableType.getManufacturers().get(position - 1);
            selectedCableType.getManufacturers().set(position, movedManufacturer);
            selectedCableType.getManufacturers().set(position - 1, selectedManufacturer);
            selectedCableType.getManufacturers().get(position).setPosition(position);
            selectedCableType.getManufacturers().get(position - 1).setPosition(position - 1);
        }

    }

    /** Swap position of current manufacturer and the one before it. */
    public void moveManufacturerDown() {
        if (selectedCableType.getManufacturers() == null || selectedCableType.getManufacturers().size() <= 1) {
            return;
        }
        int position = selectedManufacturer.getPosition();
        if (position < selectedCableType.getManufacturers().size() - 1) {
            CableTypeManufacturer movedManufacturer = selectedCableType.getManufacturers().get(position + 1);
            selectedCableType.getManufacturers().set(position, movedManufacturer);
            selectedCableType.getManufacturers().set(position + 1, selectedManufacturer);
            selectedCableType.getManufacturers().get(position).setPosition(position);
            selectedCableType.getManufacturers().get(position + 1).setPosition(position + 1);
        }
    }

    /** Remove currently selected displayViewColumn */
    public void removeManufacturer() {
        if (selectedManufacturer != null) {
            selectedCableType.getManufacturers().remove(selectedManufacturer);
        }
        if (selectedCableType.getManufacturers().isEmpty()) {
            selectedManufacturer = null;
        }
    }

    /**
     * Remove datasheet for selected manufacturer.
     */
    public void removeManufacturerDatasheet() {
        if (selectedManufacturer != null) {
            selectedManufacturer.setDatasheet(null);
        }
    }

    public List<Manufacturer> getAvailableManufacturers() {
        return availableManufacturers;
    }

    public void setAvailableManufacturers(List<Manufacturer> availableManufacturers) {
        this.availableManufacturers = availableManufacturers;
    }

    /** Clears the newly created selected cable type so editing is disabled */
    public void clearSelection() {
        if (isAddPopupOpened) {
            selectedCableType = null;
        }
    }

    /**
     * Import and upload datasheet for selected manufacturer.
     *
     * @param event file upload event
     */
    public void handleManufacturerDatasheetUpload(FileUploadEvent event) {
        GenericArtifact artifact = selectedManufacturer.getDatasheet();
        artifact = artifactRequestManager.handleImportFileUpload(event, artifact);
        selectedManufacturer.setDatasheet(artifact);
    }

    public static int getNumberOfEntitiesPerPage() {
        return NUMBER_OF_ENTITIES_PER_PAGE;
    }

    /**
     * Finds artifact file that was uploaded on the file system and returns it to be downloaded
     *
     * @param artifact  file to be downloaded
     * @return Content of the artifact
     * @throws FileNotFoundException
     *             Thrown if file was not found on file system
     */
    public DefaultStreamedContent downloadFile(GenericArtifact artifact) throws FileNotFoundException {
        // guess mime type based on the original file name, not on the name of the blob (UUID).
        final String contentType =
                FacesContext.getCurrentInstance().getExternalContext().getMimeType(artifact.getName());
        return new DefaultStreamedContent(
                new ByteArrayInputStream(artifact.getContent()), contentType, artifact.getName());
    }

    /**
     * Encode a value and return the encoded value.
     *
     * @param value the value to encode
     * @return the encoded value
     *
     * @see EncodingUtility#ENCODING_SCHEME
     * @see EncodingUtility#encode(String)
     * @see EncodingUtility#decode(String)
     */
    public String encodeValue(String value) {
        return EncodingUtility.encode(value);
    }

    /**
     * Returns row number, if applicable, for first entry in page that contains selected entry (in list of all entries).
     *
     * Note. Consider number of entries per page for pagination.
     *
     * @return row number, if applicable, for first entry in page that contains selected entry
     */
    public int getRowNumber() {
        return getRows() * (rowNumber / getRows());
    }

    /**
     * Returns (current) number of rows/entries per page in pagination component.
     *
     * @return number of rows per page
     */
    public int getRows() {
        return rows;
    }

    /**
     * Sets (current) number of rows/entries per page in pagination component.
     *
     * @param rows number of rows per page
     */
    public void setRows(int rows) {
        this.rows = rows;
    }


    /**
     * Return tooltip for cable type column.
     *
     * @param column cable type column
     * @return tooltip for cable type column
     */
    public String tooltipForCableTypes(CableTypeColumnUI column){
        String result;
        switch (column) {
            case DESCRIPTION:
                result = "Open Description dialog";
                break;
            case MANUFACTURERS:
                result = "Open Manufacturers dialog";
                break;
            case SERVICE:
                result = "Open Service/Function dialog";
                break;
            case INSULATION:
                result = "Open Insulation dialog";
                break;
            case JACKET:
                result = "Open Jacket dialog";
                break;
            case FLAMABILITY:
                result = "Open Flammable class dialog";
                break;
            case COMMENTS:
                result = "Open Comment dialog";
                break;
            default:
                result = "Open "+ column.getValue() + " dialog";
        }
        return result;
    }

}
