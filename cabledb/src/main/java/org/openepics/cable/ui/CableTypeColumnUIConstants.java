/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * Cable type UI constants.
 */
public final class CableTypeColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "Cable type name";
    public static final String NAME_STYLECLASS = "data_table_header fixed_width256";
    public static final String NAME_STYLE = null;
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 100%;";

    public static final String DESCRIPTION_VALUE = "description";
    public static final String DESCRIPTION_TOOLTIP = "Cable type description";
    public static final String DESCRIPTION_STYLECLASS = "data_table_header fixed_width286";
    public static final String DESCRIPTION_STYLE = null;
    public static final String DESCRIPTION_FILTERMODE = "contains";
    public static final String DESCRIPTION_FILTERSTYLE = "width: 100%;";

    public static final String SERVICE_VALUE = "service";
    public static final String SERVICE_TOOLTIP = "Cable type service/function";
    public static final String SERVICE_STYLECLASS = "data_table_header fixed_width192";
    public static final String SERVICE_STYLE = null;
    public static final String SERVICE_FILTERMODE = "contains";
    public static final String SERVICE_FILTERSTYLE = "width: 100%;";

    public static final String DIAMETER_VALUE = "diameter";
    public static final String DIAMETER_TOOLTIP = "Outer diameter of the cable type (mm)";
    public static final String DIAMETER_STYLECLASS = "data_table_header fixed_width140";
    public static final String DIAMETER_STYLE = null;
    public static final String DIAMETER_FILTERMODE = "contains";
    public static final String DIAMETER_FILTERSTYLE = "width: 100%;";

    public static final String WEIGHT_VALUE = "weight";
    public static final String WEIGHT_TOOLTIP = "Weight of the cable type (kg per meter)";
    public static final String WEIGHT_STYLECLASS = "data_table_header fixed_width128";
    public static final String WEIGHT_STYLE = null;
    public static final String WEIGHT_FILTERMODE = "contains";
    public static final String WEIGHT_FILTERSTYLE = "width: 100%;";

    public static final String INSULATION_VALUE = "insulation";
    public static final String INSULATION_TOOLTIP = "Cable type insulation material";
    public static final String INSULATION_STYLECLASS = "data_table_header fixed_width128";
    public static final String INSULATION_STYLE = null;
    public static final String INSULATION_FILTERMODE = "contains";
    public static final String INSULATION_FILTERSTYLE = "width: 100%;";

    public static final String JACKET_VALUE = "jacket";
    public static final String JACKET_TOOLTIP = "Cable type jacket";
    public static final String JACKET_STYLECLASS = "data_table_header fixed_width128";
    public static final String JACKET_STYLE = null;
    public static final String JACKET_FILTERMODE = "contains";
    public static final String JACKET_FILTERSTYLE = null;

    public static final String VOLTAGE_RATING_VALUE = "voltage";
    public static final String VOLTAGE_RATING_TOOLTIP = "Cable type voltage rating";
    public static final String VOLTAGE_RATING_STYLECLASS = "data_table_header fixed_width140";
    public static final String VOLTAGE_RATING_STYLE = null;
    public static final String VOLTAGE_RATING_FILTERMODE = "contains";
    public static final String VOLTAGE_RATING_FILTERSTYLE = null;

    public static final String FLAMMABILITY_VALUE = "flammability";
    public static final String FLAMMABILITY_TOOLTIP = "Cable type flammability classification";
    public static final String FLAMMABILITY_STYLECLASS = "data_table_header fixed_width140";
    public static final String FLAMMABILITY_STYLE = null;
    public static final String FLAMMABILITY_FILTERMODE = "contains";
    public static final String FLAMMABILITY_FILTERSTSYLE = null;

    public static final String INSTALLATION_TYPE_VALUE = "installationType";
    public static final String INSTALLATION_TYPE_TOOLTIP = "The installation types allowed"
            + " for this cable type for the purpose of cable installation package";
    public static final String INSTALLATION_TYPE_STYLECLASS = "data_table_header fixed_width140";
    public static final String INSTALLATION_TYPE_STYLE = null;
    public static final String INSTALLATION_TYPE_FILTERMODE = "contains";
    public static final String INSTALLATION_TYPE_FILTERSTYLE = null;

    public static final String RADIATION_RESISTANCE_VALUE = "tid";
    public static final String RADIATION_RESISTANCE_TOOLTIP = "Total ionizing dose for the cable type (kGy)";
    public static final String RADIATION_RESISTANCE_STYLECLASS = "data_table_header fixed_width208";
    public static final String RADIATION_RESISTANCE_STYLE = null;
    public static final String RADIATION_RESISTANCE_FILTERMODE = "contains";
    public static final String RADIATION_RESISTANCE_FILTERSTYLE = "width: 100%;";

    public static final String MODE_VALUE = "model";
    public static final String MODE_TOOLTIP = "An URL to manufacturer and model document";
    public static final String MODE_STYLECLASS = "data_table_header fixed_width108";
    public static final String MODE_STYLE = null;
    public static final String MODE_FILTERMODE = "contains";
    public static final String MODE_FILTERSTYLE = "width: 100%;";

    public static final String MANUFACTURERS_VALUE = "manufacturerAndDatasheetNames";
    public static final String MANUFACTURERS_URL = "manufacturerUrl";
    public static final String MANUFACTURERS_TOOLTIP = "The manufacturer of the cable type";
    public static final String MANUFACTURERS_STYLECLASS = "data_table_header fixed_width208";
    public static final String MANUFACTURERS_STYLE = null;
    public static final String MANUFACTURERS_FILTERMODE = "contains";
    public static final String MANUFACTURERS_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "valid";
    public static final String STATUS_TOOLTIP = "Cable type status";
    public static final String STATUS_STYLECLASS = "data_table_header fixed_width92";
    public static final String STATUS_STYLE = null;
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = null;

    public static final String COMMENTS_VALUE = "comments";
    public static final String COMMENTS_TOOLTIP = "Comment for the cable type";
    public static final String COMMENTS_STYLECLASS = "data_table_header fixed_width286";
    public static final String COMMENTS_STYLE = null;
    public static final String COMMENTS_FILTERMODE = "contains";
    public static final String COMMENTS_FILTERSTYLE = "width: 100%;";

    public static final String REVISION_VALUE = "revision";
    public static final String REVISION_TOOLTIP = "Revision for the cable type";
    public static final String REVISION_STYLECLASS = "data_table_header fixed_width92";
    public static final String REVISION_STYLE = null;
    public static final String REVISION_FILTERMODE = "contains";
    public static final String REVISION_FILTERSTYLE = "width: 100%;";

    /**
     * This class is not to be instantiated.
     */
    private CableTypeColumnUIConstants() {
        throw new IllegalStateException("Utility class");
    }

}
