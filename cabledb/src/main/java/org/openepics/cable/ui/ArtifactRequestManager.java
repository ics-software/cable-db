/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facility for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 *
 */
package org.openepics.cable.ui;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;

import com.google.common.io.ByteStreams;
import java.util.Date;
import javax.inject.Inject;
import org.openepics.cable.model.GenericArtifact;
import org.openepics.cable.services.SessionService;

/**
 * This is the backing requests bean for artifacts.
 *
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@ManagedBean
@ViewScoped
public class ArtifactRequestManager implements Serializable {

    private static final long serialVersionUID = 8161254904185694693L;

    @Inject
    private SessionService sessionService;

    /** Initializes the bean for initial view display, and invokes service availability check at first invocation. */
    @PostConstruct
    public void init() {}

    /**
     * Handles upload event by storing file into the given artifact
     *
     * @param event
     *            uploading event containing file
     * @param artifact
     *            to store the file in
     * @return the artifact with stored file
     */
    public GenericArtifact handleImportFileUpload(FileUploadEvent event, GenericArtifact artifact) {
        try (InputStream inputStream = event.getFile().getInputstream()) {
            byte[] importData = ByteStreams.toByteArray(inputStream);
            String newName = FilenameUtils.getName(event.getFile().getFileName());
            if (artifact == null) {
                artifact = new GenericArtifact();
            }
            artifact.setModifiedAt(new Date());
            artifact.setModifiedBy(sessionService.getLoggedInName());
            artifact.setName(newName);
            artifact.setContent(importData);
            return artifact;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
