/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.services.dl.CableColumn;

/**
 * Enum wrapper for CableColumn. Contains information for displaying cable column , wrapping only columns that are
 * available to be displayed in cable data table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableColumnUI {

    SYSTEM(CableColumn.SYSTEM,
            CableColumnUIConstants.SYSTEM_VALUE,
            null,
            CableColumnUIConstants.SYSTEM_TOOLTIP,
            CableColumnUIConstants.SYSTEM_STYLECLASS,
            CableColumnUIConstants.SYSTEM_STYLE,
            CableColumnUIConstants.SYSTEM_FILTERMODE,
            CableColumnUIConstants.SYSTEM_FILTERSTYLE,
            true,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    SUBSYSTEM(CableColumn.SUBSYSTEM,
            CableColumnUIConstants.SUBSYSTEM_VALUE,
            null,
            CableColumnUIConstants.SUBSYSTEM_TOOLTIP,
            CableColumnUIConstants.SUBSYSTEM_STYLECLASS,
            CableColumnUIConstants.SUBSYSTEM_STYLE,
            CableColumnUIConstants.SUBSYSTEM_FILTERMODE,
            CableColumnUIConstants.SUBSYSTEM_FILTERSTYLE,
            true,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    CLASS(CableColumn.CLASS,
            CableColumnUIConstants.CLASS_VALUE,
            null,
            CableColumnUIConstants.CLASS_TOOLTIP,
            CableColumnUIConstants.CLASS_STYLECLASS,
            CableColumnUIConstants.CLASS_STYLE,
            CableColumnUIConstants.CLASS_FILTERMODE,
            CableColumnUIConstants.CLASS_FILTERSTYLE,
            true,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INFORMATION(CableColumn.INFORMATION,
            CableColumnUIConstants.INFORMATION_VALUE,
            null,
            CableColumnUIConstants.INFORMATION_TOOLTIP,
            CableColumnUIConstants.INFORMATION_STYLECLASS,
            CableColumnUIConstants.INFORMATION_STYLE,
            CableColumnUIConstants.INFORMATION_FILTERMODE,
            CableColumnUIConstants.INFORMATION_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.ICON),
    LOCK_STATE(CableColumn.LOCK_STATE,
            CableColumnUIConstants.LOCK_STATE_VALUE,
            null,
            CableColumnUIConstants.LOCK_STATE_TOOLTIP,
            CableColumnUIConstants.LOCK_STATE_STYLECLASS,
            CableColumnUIConstants.LOCK_STATE_STYLE,
            CableColumnUIConstants.LOCK_STATE_FILTERMODE,
            CableColumnUIConstants.LOCK_STATE_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.ICON),
    NAME(CableColumn.NAME,
            CableColumnUIConstants.NAME_VALUE,
            null,
            CableColumnUIConstants.NAME_TOOLTIP,
            CableColumnUIConstants.NAME_STYLECLASS,
            CableColumnUIConstants.NAME_STYLE,
            CableColumnUIConstants.NAME_FILTERMODE,
            CableColumnUIConstants.NAME_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    FBS_TAG(CableColumn.FBS_TAG,
            CableColumnUIConstants.FBS_TAG_VALUE,
            null,
            CableColumnUIConstants.FBS_TAG_TOOLTIP,
            CableColumnUIConstants.FBS_TAG_STYLECLASS,
            CableColumnUIConstants.FBS_TAG_STYLE,
            CableColumnUIConstants.FBS_TAG_FILTERMODE,
            CableColumnUIConstants.FBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    MODIFIED(CableColumn.MODIFIED,
            CableColumnUIConstants.MODIFIED_VALUE,
            null,
            CableColumnUIConstants.MODIFIED_TOOLTIP,
            CableColumnUIConstants.MODIFIED_STYLECLASS,
            CableColumnUIConstants.MODIFIED_STYLE,
            CableColumnUIConstants.MODIFIED_FILTERMODE,
            CableColumnUIConstants.MODIFIED_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    CABLE_ARTICLE(CableColumn.CABLE_ARTICLE,
            CableColumnUIConstants.CABLEARTICLE_VALUE,
            CableColumnUIConstants.CABLEARTICLE_URL,
            CableColumnUIConstants.CABLEARTICLE_TOOLTIP,
            CableColumnUIConstants.CABLEARTICLE_STYLECLASS,
            CableColumnUIConstants.CABLEARTICLE_STYLE,
            CableColumnUIConstants.CABLEARTICLE_FILTERMODE,
            CableColumnUIConstants.CABLEARTICLE_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.URL),
    CABLE_TYPE(CableColumn.CABLE_TYPE,
            CableColumnUIConstants.CABLETYPE_VALUE,
            CableColumnUIConstants.CABLETYPE_URL,
            CableColumnUIConstants.CABLETYPE_TOOLTIP,
            CableColumnUIConstants.CABLETYPE_STYLECLASS,
            CableColumnUIConstants.CABLETYPE_STYLE,
            CableColumnUIConstants.CABLETYPE_FILTERMODE,
            CableColumnUIConstants.CABLETYPE_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.URL),
    CONTAINER(CableColumn.CONTAINER,
            CableColumnUIConstants.CONTAINER_VALUE,
            CableColumnUIConstants.CONTAINER_URL,
            CableColumnUIConstants.CONTAINER_TOOLTIP,
            CableColumnUIConstants.CONTAINER_STYLECLASS,
            CableColumnUIConstants.CONTAINER_STYLE,
            CableColumnUIConstants.CONTAINER_FILTERMODE,
            CableColumnUIConstants.CONTAINER_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    ELECTRICAL_DOCUMENTATION(CableColumn.ELECTRICAL_DOCUMENTATION,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_VALUE,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_URL,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_TOOLTIP,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_STYLECLASS,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_STYLE,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_FILTERMODE,
            CableColumnUIConstants.ELECTRICAL_DOCUMENTATION_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    FROM_ESS_NAME(CableColumn.FROM_ESS_NAME,
            CableColumnUIConstants.FROM_ESS_NAME_VALUE,
            CableColumnUIConstants.FROM_ESS_NAME_URL,
            CableColumnUIConstants.FROM_ESS_NAME_TOOLTIP,
            CableColumnUIConstants.FROM_ESS_NAME_STYLECLASS,
            CableColumnUIConstants.FROM_ESS_NAME_STYLE,
            CableColumnUIConstants.FROM_ESS_NAME_FILTERMODE,
            CableColumnUIConstants.FROM_ESS_NAME_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.DIALOG),
    FROM_FBS_TAG(CableColumn.FROM_FBS_TAG,
            CableColumnUIConstants.FROM_FBS_TAG_VALUE,
            null,
            CableColumnUIConstants.FROM_FBS_TAG_TOOLTIP,
            CableColumnUIConstants.FROM_FBS_TAG_STYLECLASS,
            CableColumnUIConstants.FROM_FBS_TAG_STYLE,
            CableColumnUIConstants.FROM_FBS_TAG_FILTERMODE,
            CableColumnUIConstants.FROM_FBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    FROM_LBS_TAG(CableColumn.FROM_LBS_TAG,
            CableColumnUIConstants.FROM_LBS_TAG_VALUE,
            null,
            CableColumnUIConstants.FROM_LBS_TAG_TOOLTIP,
            CableColumnUIConstants.FROM_LBS_TAG_STYLECLASS,
            CableColumnUIConstants.FROM_LBS_TAG_STYLE,
            CableColumnUIConstants.FROM_LBS_TAG_FILTERMODE,
            CableColumnUIConstants.FROM_LBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    FROM_ENCLOSURE(CableColumn.FROM_ENCLOSURE_ESS_NAME,
            CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_VALUE,
            null,
            CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_TOOLTIP,
            CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_STYLECLASS,
            CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_STYLE,
            CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_FILTERMODE,
            CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    FROM_ENCLOSURE_FBS_TAG(CableColumn.FROM_ENCLOSURE_FBS_TAG,
            CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_VALUE,
            null,
            CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_TOOLTIP,
            CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_STYLECLASS,
            CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_STYLE,
            CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_FILTERMODE,
            CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    FROM_CONNECTOR(CableColumn.FROM_CONNECTOR,
            CableColumnUIConstants.FROM_CONNECTOR_VALUE,
            CableColumnUIConstants.FROM_CONNECTOR_URL,
            CableColumnUIConstants.FROM_CONNECTOR_TOOLTIP,
            CableColumnUIConstants.FROM_CONNECTOR_STYLECLASS,
            CableColumnUIConstants.FROM_CONNECTOR_STYLE,
            CableColumnUIConstants.FROM_CONNECTOR_FILTERMODE,
            CableColumnUIConstants.FROM_CONNECTOR_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.URL),
    FROM_USER_LABEL(CableColumn.FROM_USER_LABEL,
            CableColumnUIConstants.FROM_LABEL_VALUE,
            null,
            CableColumnUIConstants.FROM_LABEL_TOOLTIP,
            CableColumnUIConstants.FROM_LABEL_STYLECLASS,
            CableColumnUIConstants.FROM_LABEL_STYLE,
            CableColumnUIConstants.FROM_LABEL_FILTERMODE,
            CableColumnUIConstants.FROM_LABEL_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    TO_ESS_NAME(CableColumn.TO_ESS_NAME,
            CableColumnUIConstants.TO_ESS_NAME_VALUE,
            CableColumnUIConstants.TO_ESS_NAME_URL,
            CableColumnUIConstants.TO_ESS_NAME_TOOLTIP,
            CableColumnUIConstants.TO_ESS_NAME_STYLECLASS,
            CableColumnUIConstants.TO_ESS_NAME_STYLE,
            CableColumnUIConstants.TO_ESS_NAME_FILTERMODE,
            CableColumnUIConstants.TO_ESS_NAME_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.DIALOG),
    TO_FBS_TAG(CableColumn.TO_FBS_TAG,
            CableColumnUIConstants.TO_FBS_TAG_VALUE,
            null,
            CableColumnUIConstants.TO_FBS_TAG_TOOLTIP,
            CableColumnUIConstants.TO_FBS_TAG_STYLECLASS,
            CableColumnUIConstants.TO_FBS_TAG_STYLE,
            CableColumnUIConstants.TO_FBS_TAG_FILTERMODE,
            CableColumnUIConstants.TO_FBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    TO_LBS_TAG(CableColumn.TO_LBS_TAG,
            CableColumnUIConstants.TO_LBS_TAG_VALUE,
            null,
            CableColumnUIConstants.TO_LBS_TAG_TOOLTIP,
            CableColumnUIConstants.TO_LBS_TAG_STYLECLASS,
            CableColumnUIConstants.TO_LBS_TAG_STYLE,
            CableColumnUIConstants.TO_LBS_TAG_FILTERMODE,
            CableColumnUIConstants.TO_LBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    TO_ENCLOSURE(CableColumn.TO_ENCLOSURE_ESS_NAME,
            CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_VALUE,
            null,
            CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_TOOLTIP,
            CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_STYLECLASS,
            CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_STYLE,
            CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_FILTERMODE,
            CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    TO_ENCLOSURE_FBS_TAG(CableColumn.TO_ENCLOSURE_FBS_TAG,
            CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_VALUE,
            null,
            CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_TOOLTIP,
            CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_STYLECLASS,
            CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_STYLE,
            CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_FILTERMODE,
            CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    TO_CONNECTOR(CableColumn.TO_CONNECTOR,
            CableColumnUIConstants.TO_CONNECTOR_VALUE,
            CableColumnUIConstants.TO_CONNECTOR_URL,
            CableColumnUIConstants.TO_CONNECTOR_TOOLTIP,
            CableColumnUIConstants.TO_CONNECTOR_STYLECLASS,
            CableColumnUIConstants.TO_CONNECTOR_STYLE,
            CableColumnUIConstants.TO_CONNECTOR_FILTERMODE,
            CableColumnUIConstants.TO_CONNECTOR_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.URL),
    TO_USER_LABEL(CableColumn.TO_USER_LABEL,
            CableColumnUIConstants.TO_LABEL_VALUE,
            null,
            CableColumnUIConstants.TO_LABEL_TOOLTIP,
            CableColumnUIConstants.TO_LABEL_STYLECLASS,
            CableColumnUIConstants.TO_LABEL_STYLE,
            CableColumnUIConstants.TO_LABEL_FILTERMODE,
            CableColumnUIConstants.TO_LABEL_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    INSTALLATION_PACKAGE(CableColumn.INSTALLATION_PACKAGE,
            CableColumnUIConstants.INSTALLATION_PACKAGE_VALUE,
            CableColumnUIConstants.INSTALLATION_PACKAGE_URL,
            CableColumnUIConstants.INSTALLATION_PACKAGE_TOOLTIP,
            CableColumnUIConstants.INSTALLATION_PACKAGE_STYLECLASS,
            CableColumnUIConstants.INSTALLATION_PACKAGE_STYLE,
            CableColumnUIConstants.INSTALLATION_PACKAGE_FILTERMODE,
            CableColumnUIConstants.INSTALLATION_PACKAGE_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.URL),
    OWNERS(CableColumn.OWNERS,
            CableColumnUIConstants.OWNERS_VALUE,
            null,
            CableColumnUIConstants.OWNERS_TOOLTIP,
            CableColumnUIConstants.OWNERS_STYLECLASS,
            CableColumnUIConstants.OWNERS_STYLE,
            CableColumnUIConstants.OWNERS_FILTERMODE,
            CableColumnUIConstants.OWNERS_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    STATUS(CableColumn.STATUS,
            CableColumnUIConstants.STATUS_VALUE,
            null, CableColumnUIConstants.STATUS_TOOLTIP,
            CableColumnUIConstants.STATUS_STYLECLASS,
            CableColumnUIConstants.STATUS_STYLE,
            CableColumnUIConstants.STATUS_FILTERMODE,
            CableColumnUIConstants.STATUS_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    INSTALLATION_DATE(CableColumn.INSTALLATION_DATE,
            CableColumnUIConstants.INSTALLATION_BY_VALUE,
            null,
            CableColumnUIConstants.INSTALLATION_BY_TOOLTIP,
            CableColumnUIConstants.INSTALLATION_BY_STYLECLASS,
            CableColumnUIConstants.INSTALLATION_BY_STYLE,
            CableColumnUIConstants.INSTALLATION_BY_FILTERMODE,
            CableColumnUIConstants.INSTALLATION_BY_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    COMMENTS(CableColumn.COMMENTS,
            CableColumnUIConstants.COMMENTS_VALUE,
            CableColumnUIConstants.COMMENTS_URL,
            CableColumnUIConstants.COMMENTS_TOOLTIP,
            CableColumnUIConstants.COMMENTS_STYLECLASS,
            CableColumnUIConstants.COMMENTS_STYLE,
            CableColumnUIConstants.COMMENTS_FILTERMODE,
            CableColumnUIConstants.COMMENTS_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.LONGTEXT),
    REVISION(CableColumn.REVISION,
            CableColumnUIConstants.REVISION_VALUE,
            null,
            CableColumnUIConstants.REVISION_TOOLTIP,
            CableColumnUIConstants.REVISION_STYLECLASS,
            CableColumnUIConstants.REVISION_STYLE,
            CableColumnUIConstants.REVISION_FILTERMODE,
            CableColumnUIConstants.REVISION_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT),
    CABLE_HISTORY(CableColumn.CABLE_HISTORY,
            CableColumnUIConstants.CABLE_HISTORY_VALUE,
            null,
            CableColumnUIConstants.CABLE_HISTORY_TOOLTIP,
            CableColumnUIConstants.CABLE_HISTORY_STYLECLASS,
            CableColumnUIConstants.CABLE_HISTORY_STYLE,
            CableColumnUIConstants.CABLE_HISTORY_FILTERMODE,
            CableColumnUIConstants.CABLE_HISTORY_FILTERSTYLE,
            false,
            CableColumnStyle.CableColumnStyleEnum.PLAIN_TEXT);


    private CableColumn parent;
    private String tooltip;
    private String value;
    private String url;
    private String styleClass;
    private String style;
    private String filterMode;
    private String filterStyle;
    private boolean renderText;
    private boolean renderCommand;
    private boolean renderDownload;
    private boolean filterDropdown;
    private String endpointValue = null;
    private String endpoint = null;
    private CableColumnStyle columnStyle;

    /**
     * CableColumnUI constructor Decides what labels in column to display according to property.
     *
     * @param parent
     *            parent for getting column label
     * @param value
     *            property for getting data to display
     * @param tooltip
     *            tooltip to show on mouse hover
     * @param styleClass
     *            styleClass for setting column width. Example: "fixed_width140"
     * @param style
     *            style for displaying column. Examples: "text-align:center", null, text-align:left"
     * @param filterMode
     *            filterMode. Examples: "exact", "contains"
     * @param filterStyle
     *            filterStyle to set filter width. Example: "width: 92px;"
     * @param filterCookie
     *            filterCookie to enable or disable cookie for column filter
     */
    private CableColumnUI(CableColumn parent, String value, String url, String tooltip,
            String styleClass, String style,
            String filterMode, String filterStyle, boolean filterCookie,
            CableColumnStyle.CableColumnStyleEnum cableColumnStyleEnum) {
        this.parent = parent;
        this.tooltip = tooltip;
        this.value = value;
        this.url = url;
        this.styleClass = styleClass;
        this.style = style;
        this.filterMode = filterMode;
        this.filterStyle = filterStyle;
        this.renderText = false;
        this.renderCommand = false;
        this.renderDownload = false;
        this.filterDropdown = false;
        this.columnStyle = new CableColumnStyle(cableColumnStyleEnum);

        switch (value) {
        case CableColumnUIConstants.FROM_ESS_NAME_VALUE:
        case CableColumnUIConstants.TO_ESS_NAME_VALUE:
            this.endpointValue = "device";
            break;
        case CableColumnUIConstants.FROM_FBS_TAG_VALUE:
        case CableColumnUIConstants.TO_FBS_TAG_VALUE:
            this.endpointValue = "deviceFbsTag";
            break;
        case CableColumnUIConstants.FROM_LBS_TAG_VALUE:
        case CableColumnUIConstants.TO_LBS_TAG_VALUE:
            this.endpointValue = "building";
            break;
        case CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_VALUE:
        case CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_VALUE:
            this.endpointValue = "rack";
            break;
        case CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_VALUE:
        case CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_VALUE:
            this.endpointValue = "rackFbsTag";
            break;
        case CableColumnUIConstants.FROM_CONNECTOR_VALUE:
        case CableColumnUIConstants.TO_CONNECTOR_VALUE:
            this.endpointValue = "connector";
            break;
        case CableColumnUIConstants.FROM_LABEL_VALUE:
        case CableColumnUIConstants.TO_LABEL_VALUE:
            this.endpointValue = "label";
            break;
        case CableColumnUIConstants.STATUS_VALUE:
            this.filterDropdown = true;
            break;
        default:
        }

        switch (value) {
        case CableColumnUIConstants.FROM_ESS_NAME_VALUE:
        case CableColumnUIConstants.FROM_FBS_TAG_VALUE:
        case CableColumnUIConstants.FROM_LBS_TAG_VALUE:
        case CableColumnUIConstants.FROM_ENCLOSURE_ESS_NAME_VALUE:
        case CableColumnUIConstants.FROM_ENCLOSURE_FBS_TAG_VALUE:
        case CableColumnUIConstants.FROM_CONNECTOR_VALUE:
        case CableColumnUIConstants.FROM_LABEL_VALUE:
            endpoint = "endpointA";
            break;
        case CableColumnUIConstants.TO_ESS_NAME_VALUE:
        case CableColumnUIConstants.TO_FBS_TAG_VALUE:
        case CableColumnUIConstants.TO_LBS_TAG_VALUE:
        case CableColumnUIConstants.TO_ENCLOSURE_ESS_NAME_VALUE:
        case CableColumnUIConstants.TO_ENCLOSURE_FBS_TAG_VALUE:
        case CableColumnUIConstants.TO_CONNECTOR_VALUE:
        case CableColumnUIConstants.TO_LABEL_VALUE:
            endpoint = "endpointB";
            break;
        default:
        }
    }

    public String getFieldName() {
        return parent.getFieldName();
    }

    /** @return String styleClass */
    public String getStyleClass() {
        return styleClass;
    }

    /** @return String style */
    public String getStyle() {
        return style;
    }

    /** @return String filterMode */
    public String getFilterMode() {
        return filterMode;
    }

    /** @return String filterStyle */
    public String getFilterStyle() {
        return filterStyle;
    }

    /** @return String columnLabel */
    public String getColumnLabel() {
        return parent.getColumnLabel();
    }

    @Override
    public String toString() {
        return parent.toString();
    }

    /** @return String tooltip */
    public String getTooltip() {
        return tooltip;
    }

    /** @return String property */
    public String getValue() {
        return value;
    }

    /** @return boolean renderText */
    public boolean getRenderText() {
        return renderText;
    }

    /** @return boolean renderCommand */
    public boolean getRenderCommand() {
        return renderCommand;
    }

    /** @return boolean renderDownload */
    public boolean getRenderDownload() {
        return renderDownload;
    }

    /**
     * Searches for CableColumnUI enum with column label
     *
     * @param columnLabel
     *            for which we want to find the enum
     * @return CableColumnUI enum
     */
    public static CableColumnUI convertColumnLabel(String columnLabel) {
        for (CableColumnUI value : CableColumnUI.values()) {
            if (value.getColumnLabel().equals(columnLabel)) {
                return value;
            }
        }
        return null;
    }

    /**
     * Returns columnLabels from all enums.
     *
     * @return list of all columnLabels.
     */
    public static List<String> getAllColumns() {
        List<String> valid = new ArrayList<String>();
        for (CableColumnUI value : CableColumnUI.values()) {
            valid.add(value.getColumnLabel());
        }
        return valid;
    }

    public boolean isFilterDropdown() {
        return filterDropdown;
    }

    public String getEndpointValue() {
        return endpointValue;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public CableColumnStyle getColumnStyle() {
        return columnStyle;
    }

    public String getUrl() {
        return url;
    }

    /***
     * Determines if a column is sortable. Only 'Information', and 'Locked' pictogram columns are not sortable
     * @return true, if column is sortable, or false, if column is not sortable
     * ( false is only for 'information', and 'Locked' pictogram column)
     */

    public boolean isSortable(){
        return !(StringUtils.isNotEmpty(this.value)
                    && (CableColumnUI.INFORMATION.getValue().equalsIgnoreCase(this.value)) ||
                (CableColumnUI.LOCK_STATE.getValue().equalsIgnoreCase(this.value)));
    }
}
