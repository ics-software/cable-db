/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * Manufacturer UI constants.
 */
public final class ManufacturerColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The name of the first manufacturer";
    public static final String NAME_STYLECLASS = "data_table_header";
    public static final String NAME_STYLE = "width:10%; min-width:120px";
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 100%;";

    public static final String ADDRESS_VALUE = "address";
    public static final String ADDRESS_TOOLTIP = "The address of the manufacturer";
    public static final String ADDRESS_STYLECLASS = "data_table_header";
    public static final String ADDRESS_STYLE = "width:20%; min-width:160px";
    public static final String ADDRESS_FILTERMODE = "contains";
    public static final String ADDRESS_FILTERSTYLE = "width: 100%;";

    public static final String PHONE_VALUE = "phoneNumber";
    public static final String PHONE_TOOLTIP = "The contact phone number for the manufacturer";
    public static final String PHONE_STYLECLASS = "data_table_header";
    public static final String PHONE_STYLE = "width:15%; min-width:120px";
    public static final String PHONE_FILTERMODE = "contains";
    public static final String PHONE_FILTERSTYLE = "width: 100%;";

    public static final String EMAIL_VALUE = "email";
    public static final String EMAIL_TOOLTIP = "The contact email address for the manufacturer";
    public static final String EMAIL_STYLECLASS = "data_table_header";
    public static final String EMAIL_STYLE = "width:20%; min-width:160px";
    public static final String EMAIL_FILTERMODE = "contains";
    public static final String EMAIL_FILTERSTYLE = "width: 100%;";

    public static final String COUNTRY_VALUE = "country";
    public static final String COUNTRY_TOOLTIP = "Country of the manufacturer";
    public static final String COUNTRY_STYLECLASS = "data_table_header";
    public static final String COUNTRY_STYLE = "width:10%; min-width:80px";
    public static final String COUNTRY_FILTERMODE = "contains";
    public static final String COUNTRY_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "status";
    public static final String STATUS_TOOLTIP = "The status of the manufacturer";
    public static final String STATUS_STYLECLASS = "data_table_header";
    public static final String STATUS_STYLE = "width:10%; min-width:64px";
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUES = "statusValues";

    /**
     * This class is not to be instantiated.
     */
    private ManufacturerColumnUIConstants() {
        throw new IllegalStateException("Utility class");
    }

}
