/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.ui;

/**
 * Cable UI constants.
 */
public final class CableColumnUIConstants {

    public static final String NAME_VALUE = "name";
    public static final String NAME_TOOLTIP = "The cable name";
    public static final String NAME_STYLECLASS = "data_table_header fixed_width108";
    public static final String NAME_STYLE = null;
    public static final String NAME_FILTERMODE = "contains";
    public static final String NAME_FILTERSTYLE = "width: 100%;";

    public static final String FBS_TAG_VALUE = "fbsTag";
    public static final String FBS_TAG_TOOLTIP = "The cable FBS tag";
    public static final String FBS_TAG_STYLECLASS = "data_table_header fixed_width208";
    public static final String FBS_TAG_STYLE = null;
    public static final String FBS_TAG_FILTERMODE = "contains";
    public static final String FBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String MODIFIED_VALUE = "modified";
    public static final String MODIFIED_TOOLTIP = "Date when cable was modified";
    public static final String MODIFIED_STYLECLASS = "data_table_header fixed_width160";
    public static final String MODIFIED_STYLE = null;
    public static final String MODIFIED_FILTERMODE = "contains";
    public static final String MODIFIED_FILTERSTYLE = "width: 100%;";

    public static final String CABLEARTICLE_VALUE = "cableArticle";
    public static final String CABLEARTICLE_URL = "cableArticleUrl";
    public static final String CABLEARTICLE_TOOLTIP = "The article of the cable";
    public static final String CABLEARTICLE_STYLECLASS = "data_table_header fixed_width128";
    public static final String CABLEARTICLE_STYLE = null;
    public static final String CABLEARTICLE_FILTERMODE = "contains";
    public static final String CABLEARTICLE_FILTERSTYLE = "width: 100%;";

    public static final String CABLETYPE_VALUE = "cableType";
    public static final String CABLETYPE_URL = "cableTypeUrl";
    public static final String CABLETYPE_TOOLTIP = "The type of the cable";
    public static final String CABLETYPE_STYLECLASS = "data_table_header fixed_width128 grayedOut";
    public static final String CABLETYPE_STYLE = "";
    public static final String CABLETYPE_FILTERMODE = "contains";
    public static final String CABLETYPE_FILTERSTYLE = "width: 100%;";

    public static final String CONTAINER_VALUE = "container";
    public static final String CONTAINER_URL = "containerUrl";
    public static final String CONTAINER_TOOLTIP = "The container cable number";
    public static final String CONTAINER_STYLECLASS = "data_table_header fixed_width108";
    public static final String CONTAINER_STYLE = null;
    public static final String CONTAINER_FILTERMODE = "contains";
    public static final String CONTAINER_FILTERSTYLE = "width: 100%;";

    public static final String ELECTRICAL_DOCUMENTATION_VALUE = "electricalDocumentation";
    public static final String ELECTRICAL_DOCUMENTATION_URL = "eDocUrl";
    public static final String ELECTRICAL_DOCUMENTATION_TOOLTIP = "Electrical documentation";
    public static final String ELECTRICAL_DOCUMENTATION_STYLECLASS = "data_table_header fixed_width208";
    public static final String ELECTRICAL_DOCUMENTATION_STYLE = null;
    public static final String ELECTRICAL_DOCUMENTATION_FILTERMODE = "contains";
    public static final String ELECTRICAL_DOCUMENTATION_FILTERSTYLE = "width: 100%;";

    public static final String FROM_ESS_NAME_VALUE = "endpointDeviceNameA";
    public static final String FROM_ESS_NAME_URL = "endpointDeviceAUrl";
    public static final String FROM_ESS_NAME_TOOLTIP = "The ESS name of the origin endpoint";
    public static final String FROM_ESS_NAME_STYLECLASS = "data_table_header fixed_width208";
    public static final String FROM_ESS_NAME_STYLE = null;
    public static final String FROM_ESS_NAME_FILTERMODE = "contains";
    public static final String FROM_ESS_NAME_FILTERSTYLE = "width: 100%;";

    public static final String FROM_FBS_TAG_VALUE = "endpointDeviceNameAFbsTag";
    public static final String FROM_FBS_TAG_TOOLTIP = "The FBS tag of the origin endpoint";
    public static final String FROM_FBS_TAG_STYLECLASS = "data_table_header fixed_width208";
    public static final String FROM_FBS_TAG_STYLE = null;
    public static final String FROM_FBS_TAG_FILTERMODE = "contains";
    public static final String FROM_FBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String FROM_LBS_TAG_VALUE = "endpointBuildingA";
    public static final String FROM_LBS_TAG_TOOLTIP = "The LBS tag of the origin endpoint";
    public static final String FROM_LBS_TAG_STYLECLASS = "data_table_header fixed_width140";
    public static final String FROM_LBS_TAG_STYLE = null;
    public static final String FROM_LBS_TAG_FILTERMODE = "contains";
    public static final String FROM_LBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String FROM_ENCLOSURE_ESS_NAME_VALUE = "endpointRackA";
    public static final String FROM_ENCLOSURE_ESS_NAME_TOOLTIP = "The enclosure ESS name of the origin endpoint";
    public static final String FROM_ENCLOSURE_ESS_NAME_STYLECLASS = "data_table_header fixed_width150";
    public static final String FROM_ENCLOSURE_ESS_NAME_STYLE = null;
    public static final String FROM_ENCLOSURE_ESS_NAME_FILTERMODE = "contains";
    public static final String FROM_ENCLOSURE_ESS_NAME_FILTERSTYLE = "width: 100%;";

    public static final String FROM_ENCLOSURE_FBS_TAG_VALUE = "endpointRackAFbsTag";
    public static final String FROM_ENCLOSURE_FBS_TAG_TOOLTIP = "The enclosure FBS tag of the origin endpoint";
    public static final String FROM_ENCLOSURE_FBS_TAG_STYLECLASS = "data_table_header fixed_width208";
    public static final String FROM_ENCLOSURE_FBS_TAG_STYLE = null;
    public static final String FROM_ENCLOSURE_FBS_TAG_FILTERMODE = "contains";
    public static final String FROM_ENCLOSURE_FBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String FROM_CONNECTOR_VALUE = "endpointConnectorA";
    public static final String FROM_CONNECTOR_URL = "endpointConnectorAUrl";
    public static final String FROM_CONNECTOR_TOOLTIP = "The connector of the origin endpoint";
    public static final String FROM_CONNECTOR_STYLECLASS = "data_table_header fixed_width140";
    public static final String FROM_CONNECTOR_STYLE = null;
    public static final String FROM_CONNECTOR_FILTERMODE = "contains";
    public static final String FROM_CONNECTOR_FILTERSTYLE = "width: 100%;";

    public static final String FROM_LABEL_VALUE = "endpointLabelA";
    public static final String FROM_LABEL_TOOLTIP = "The user label of the origin endpoint";
    public static final String FROM_LABEL_STYLECLASS = "data_table_header fixed_width450";
    public static final String FROM_LABEL_STYLE = null;
    public static final String FROM_LABEL_FILTERMODE = "contains";
    public static final String FROM_LABEL_FILTERSTYLE = "width: 100%;";

    public static final String TO_ESS_NAME_VALUE = "endpointDeviceNameB";
    public static final String TO_ESS_NAME_URL = "endpointDeviceBUrl";
    public static final String TO_ESS_NAME_TOOLTIP = "The ESS name of the destination endpoint";
    public static final String TO_ESS_NAME_STYLECLASS = "data_table_header fixed_width208";
    public static final String TO_ESS_NAME_STYLE = null;
    public static final String TO_ESS_NAME_FILTERMODE = "contains";
    public static final String TO_ESS_NAME_FILTERSTYLE = "width: 100%;";

    public static final String TO_FBS_TAG_VALUE = "endpointDeviceNameBFbsTag";
    public static final String TO_FBS_TAG_TOOLTIP = "The FBS tag of the destination endpoint";
    public static final String TO_FBS_TAG_STYLECLASS = "data_table_header fixed_width208";
    public static final String TO_FBS_TAG_STYLE = null;
    public static final String TO_FBS_TAG_FILTERMODE = "contains";
    public static final String TO_FBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String TO_LBS_TAG_VALUE = "endpointBuildingB";
    public static final String TO_LBS_TAG_TOOLTIP = "The LBS tag of the destination endpoint";
    public static final String TO_LBS_TAG_STYLECLASS = "data_table_header fixed_width140";
    public static final String TO_LBS_TAG_STYLE = null;
    public static final String TO_LBS_TAG_FILTERMODE = "contains";
    public static final String TO_LBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String TO_ENCLOSURE_ESS_NAME_VALUE = "endpointRackB";
    public static final String TO_ENCLOSURE_ESS_NAME_TOOLTIP = "The enclosure ESS name of the destination endpoint";
    public static final String TO_ENCLOSURE_ESS_NAME_STYLECLASS = "data_table_header fixed_width150";
    public static final String TO_ENCLOSURE_ESS_NAME_STYLE = null;
    public static final String TO_ENCLOSURE_ESS_NAME_FILTERMODE = "contains";
    public static final String TO_ENCLOSURE_ESS_NAME_FILTERSTYLE = "width: 100%;";

    public static final String TO_ENCLOSURE_FBS_TAG_VALUE = "endpointRackBFbsTag";
    public static final String TO_ENCLOSURE_FBS_TAG_TOOLTIP = "The enclosure FBS tag of the destination endpoint";
    public static final String TO_ENCLOSURE_FBS_TAG_STYLECLASS = "data_table_header fixed_width208";
    public static final String TO_ENCLOSURE_FBS_TAG_STYLE = null;
    public static final String TO_ENCLOSURE_FBS_TAG_FILTERMODE = "contains";
    public static final String TO_ENCLOSURE_FBS_TAG_FILTERSTYLE = "width: 100%;";

    public static final String TO_CONNECTOR_VALUE = "endpointConnectorB";
    public static final String TO_CONNECTOR_URL = "endpointConnectorBUrl";
    public static final String TO_CONNECTOR_TOOLTIP = "The connector of the destination endpoint";
    public static final String TO_CONNECTOR_STYLECLASS = "data_table_header fixed_width140";
    public static final String TO_CONNECTOR_STYLE = null;
    public static final String TO_CONNECTOR_FILTERMODE = "contains";
    public static final String TO_CONNECTOR_FILTERSTYLE = "width: 100%;";

    public static final String TO_LABEL_VALUE = "endpointLabelB";
    public static final String TO_LABEL_TOOLTIP = "The user label of the destination endpoint";
    public static final String TO_LABEL_STYLECLASS = "data_table_header fixed_width450";
    public static final String TO_LABEL_STYLE = null;
    public static final String TO_LABEL_FILTERMODE = "contains";
    public static final String TO_LABEL_FILTERSTYLE = "width: 100%;";

    public static final String SYSTEM_VALUE = "system";
    public static final String SYSTEM_TOOLTIP = "System";
    public static final String SYSTEM_STYLECLASS = "data_table_header fixed_width48";
    public static final String SYSTEM_STYLE = "text-align:center";
    public static final String SYSTEM_FILTERMODE = "exact";
    public static final String SYSTEM_FILTERSTYLE = "width: 100%;";

    public static final String SUBSYSTEM_VALUE = "subsystem";
    public static final String SUBSYSTEM_TOOLTIP = "Sub-system";
    public static final String SUBSYSTEM_STYLECLASS = "data_table_header fixed_width48";
    public static final String SUBSYSTEM_STYLE = "text-align:center";
    public static final String SUBSYSTEM_FILTERMODE = "exact";
    public static final String SUBSYSTEM_FILTERSTYLE = "width: 100%;";

    public static final String CLASS_VALUE = "cableClass";
    public static final String CLASS_TOOLTIP = "Cable class";
    public static final String CLASS_STYLECLASS = "data_table_header fixed_width48";
    public static final String CLASS_STYLE = "text-align:center";
    public static final String CLASS_FILTERMODE = "exact";
    public static final String CLASS_FILTERSTYLE = "width: 100%;";

    public static final String INFORMATION_VALUE = "information";
    public static final String INFORMATION_TOOLTIP = "Cable error information";
    public static final String INFORMATION_STYLECLASS = "data_table_header fixed_width48";
    public static final String INFORMATION_STYLE = "text-align:center";
    public static final String INFORMATION_FILTERMODE = "exact";
    public static final String INFORMATION_FILTERSTYLE = "width: 100%;";

    public static final String LOCK_STATE_VALUE = "lockedForEditing";
    public static final String LOCK_STATE_TOOLTIP = "Cable Editing Lock State";
    public static final String LOCK_STATE_STYLECLASS = "data_table_header fixed_width48";
    public static final String LOCK_STATE_STYLE = "text-align:center";
    public static final String LOCK_STATE_FILTERMODE = "exact";
    public static final String LOCK_STATE_FILTERSTYLE = "width: 100%;";

    public static final String INSTALLATION_PACKAGE_VALUE = "installationPackage";
    public static final String INSTALLATION_PACKAGE_URL = "installationPackageUrl";
    public static final String INSTALLATION_PACKAGE_TOOLTIP = "The installation package value of cable";
    public static final String INSTALLATION_PACKAGE_STYLECLASS = "data_table_header fixed_width80";
    public static final String INSTALLATION_PACKAGE_STYLE = null;
    public static final String INSTALLATION_PACKAGE_FILTERMODE = "contains";
    public static final String INSTALLATION_PACKAGE_FILTERSTYLE = "width: 100%;";

    public static final String OWNERS_VALUE = "ownersString";
    public static final String OWNERS_TOOLTIP = "The users that are responsible for this cable,"
            + " usually the same person that is performing the upload";
    public static final String OWNERS_STYLECLASS = "data_table_header fixed_width140";
    public static final String OWNERS_STYLE = null;
    public static final String OWNERS_FILTERMODE = "exact";
    public static final String OWNERS_FILTERSTYLE = "width: 100%;";

    public static final String STATUS_VALUE = "status";
    public static final String STATUS_TOOLTIP = "Cable status";
    public static final String STATUS_STYLECLASS = "data_table_header fixed_width128";
    public static final String STATUS_STYLE = null;
    public static final String STATUS_FILTERMODE = "contains";
    public static final String STATUS_FILTERSTYLE = null;

    public static final String INSTALLATION_BY_VALUE = "installationBy";
    public static final String INSTALLATION_BY_TOOLTIP =
            "The date by which the installation of this cable is performed";
    public static final String INSTALLATION_BY_STYLECLASS = "data_table_header fixed_width160";
    public static final String INSTALLATION_BY_STYLE = null;
    public static final String INSTALLATION_BY_FILTERMODE = "contains";
    public static final String INSTALLATION_BY_FILTERSTYLE = "width: 100%;";

    public static final String COMMENTS_VALUE = "comments";
    public static final String COMMENTS_URL = "commentsUrl";
    public static final String COMMENTS_TOOLTIP = "Comment for the cable";
    public static final String COMMENTS_STYLECLASS = "data_table_header fixed_width286";
    public static final String COMMENTS_STYLE = null;
    public static final String COMMENTS_FILTERMODE = "contains";
    public static final String COMMENTS_FILTERSTYLE = "width: 100%;";

    public static final String REVISION_VALUE = "revision";
    public static final String REVISION_TOOLTIP = "The revision of the cable";
    public static final String REVISION_STYLECLASS = "data_table_header fixed_width92";
    public static final String REVISION_STYLE = null;
    public static final String REVISION_FILTERMODE = "contains";
    public static final String REVISION_FILTERSTYLE = "width: 100%;";

    public static final String CABLE_HISTORY_VALUE = "history";
    public static final String CABLE_HISTORY_TOOLTIP = "Cable history";
    public static final String CABLE_HISTORY_STYLECLASS = "fixed_width64";
    public static final String CABLE_HISTORY_STYLE = "width:64px;";
    public static final String CABLE_HISTORY_FILTERMODE = null;
    public static final String CABLE_HISTORY_FILTERSTYLE = null;

    public static final String EMPTY_FILTER_DROPDOWN_VALUE = "<empty>";
    public static final String NON_DELETED_DROPDOWN_VALUE = "NON-DELETED";

    /**
     * This class is not to be instantiated.
     */
    private CableColumnUIConstants() {
        throw new IllegalStateException("Utility class");
    }

}
