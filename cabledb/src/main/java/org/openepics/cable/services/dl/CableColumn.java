/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import org.apache.commons.lang3.StringUtils;

/**
 * This represents the column names that are present in the header of the spreadsheet cable template.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum CableColumn implements FieldIntrospection {

    SYSTEM("SYSTEM", "system", "Sy",
            true, false, false),
    SUBSYSTEM("SUBSYSTEM", "subsystem", "Su",
            true, false, false),
    CLASS("CLASS", "cableClass", "Cl",
            true, false, true),
    INFORMATION("INFORMATION", "information", "I",
            false, false, false),
    LOCK_STATE("LOCK STATE", "lockState", "Locked",
            false, false, false),
    NAME("CABLE NAME", "name", "Name",
            true, false, false),
    FBS_TAG("CABLE FBS TAG", "fbsTag", "FBS Tag",
            true, false, true),
    MODIFIED("MODIFIED", "modified", "Modified",
            false, false, false),
    CABLE_ARTICLE("CABLE ARTICLE", "cableArticle", "Article",
            true, false, true),
    CABLE_TYPE("CABLE TYPE", "cableType", "Type",
            true, false, true),
    CONTAINER("CONTAINER (bundle)", "container", "Container (bundle)",
            true, false, true),
    ELECTRICAL_DOCUMENTATION("ELECTRICAL DOCUMENTATION", "electricalDocumentation", "Electrical documentation",
            true, false, true),
    FROM_ESS_NAME("FROM (ENDPOINT):ESS NAME", "device", "From ESS Name",
            true, false, true),
    FROM_FBS_TAG("FROM (ENDPOINT):FBS TAG", "deviceFbsTag", "From FBS Tag",
            true, false, true),
    FROM_LBS_TAG("FROM (ENDPOINT):LBS TAG", "building", "From LBS Tag",
            true, false, true),
    FROM_ENCLOSURE_ESS_NAME("FROM (ENDPOINT):ENCLOSURE ESS NAME", "rack", "From Enclosure ESS Name",
            true, false, true),
    FROM_ENCLOSURE_FBS_TAG("FROM (ENDPOINT):ENCLOSURE FBS TAG", "rackFbsTag", "From Enclosure FBS Tag",
            true, false, true),
    FROM_CONNECTOR("FROM (ENDPOINT):CONNECTOR", "connector", "From Connector",
            true, false, true),
    FROM_USER_LABEL("FROM (ENDPOINT):USER LABEL", "label", "From User Label",
            true, false, true),
    TO_ESS_NAME("TO (ENDPOINT):ESS NAME", "device", "To ESS Name",
            true, false, true),
    TO_FBS_TAG("TO (ENDPOINT):FBS TAG", "deviceFbsTag", "To FBS Tag",
            true, false, true),
    TO_LBS_TAG("TO (ENDPOINT):LBS TAG", "building", "To LBS Tag",
            true, false, true),
    TO_ENCLOSURE_ESS_NAME("TO (ENDPOINT):ENCLOSURE ESS NAME", "rack", "To Enclosure ESS Name",
            true, false, true),
    TO_ENCLOSURE_FBS_TAG("TO (ENDPOINT):ENCLOSURE FBS TAG", "rackFbsTag", "To Enclosure FBS Tag",
            true, false, true),
    TO_CONNECTOR("TO (ENDPOINT):CONNECTOR", "connector", "To Connector",
            true, false, true),
    TO_USER_LABEL("TO (ENDPOINT):USER LABEL", "label", "To User Label",
            true, false, true),
    INSTALLATION_PACKAGE("INSTALLATION PACKAGE", "installationPackage", "Installation Package",
            true, false, true),
    OWNERS("OWNERS", "ownersString", "Owners",
            true, false, true),
    STATUS("STATUS", "status", "Status",
            true, false, true),
    INSTALLATION_DATE("INSTALLATION DATE", "installationBy", "Installation Date",
            true, false, false),
    COMMENTS("COMMENTS", "comments", "Comments",
            true, false, true),
    REVISION("REVISION", "revision", "Revision",
            true, false, true),
    CABLE_HISTORY("HISTORY", "History", "History",
            false, false, false);

    /*
     * isExcelColumn
     *      presence as column/field in Excel sheet
     * isExcelVolatileColumn
     *      presence as column/field in Excel sheet but volatile, i.e. presence not guaranteed
     *      if not considered column in Excel, then value does not matter
     */

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isExcelVolatileColumn;
    private final boolean isStringComparisonOperator;

    private CableColumn(String stringValue, String fieldName, String columnLabel,
            boolean isExcelColumn, boolean isExcelVolatileColumn, boolean isStringComparisonOperator) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isExcelVolatileColumn = isExcelVolatileColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isExcelVolatileColumn() {
        return isExcelVolatileColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Return cable column given column label.
     *
     * @param columnLabel column label
     * @return cable column
     */
    public static CableColumn convertColumnLabel(String columnLabel) {
        if (MODIFIED.getColumnLabel().equals(columnLabel)) {
            return MODIFIED;
        } else if (CABLE_ARTICLE.getColumnLabel().equals(columnLabel)) {
            return CABLE_ARTICLE;
        } else if (CABLE_TYPE.getColumnLabel().equals(columnLabel)) {
            return CABLE_TYPE;
        } else if (CONTAINER.getColumnLabel().equals(columnLabel)) {
            return CONTAINER;
        } else if (FROM_ESS_NAME.getColumnLabel().equals(columnLabel)) {
            return FROM_ESS_NAME;
        } else if (FROM_FBS_TAG.getColumnLabel().equals(columnLabel)) {
            return FROM_FBS_TAG;
        } else if (FROM_LBS_TAG.getColumnLabel().equals(columnLabel)) {
            return FROM_LBS_TAG;
        } else if (FROM_ENCLOSURE_ESS_NAME.getColumnLabel().equals(columnLabel)) {
            return FROM_ENCLOSURE_ESS_NAME;
        } else if (FROM_ENCLOSURE_FBS_TAG.getColumnLabel().equals(columnLabel)) {
            return FROM_ENCLOSURE_FBS_TAG;
        } else if (FROM_CONNECTOR.getColumnLabel().equals(columnLabel)) {
            return FROM_CONNECTOR;
        } else if (FROM_USER_LABEL.getColumnLabel().equals(columnLabel)) {
            return FROM_USER_LABEL;
        } else if (TO_ESS_NAME.getColumnLabel().equals(columnLabel)) {
            return TO_ESS_NAME;
        } else if (TO_FBS_TAG.getColumnLabel().equals(columnLabel)) {
            return TO_FBS_TAG;
        } else if (TO_LBS_TAG.getColumnLabel().equals(columnLabel)) {
            return TO_LBS_TAG;
        } else if (TO_ENCLOSURE_ESS_NAME.getColumnLabel().equals(columnLabel)) {
            return TO_ENCLOSURE_ESS_NAME;
        } else if (TO_ENCLOSURE_FBS_TAG.getColumnLabel().equals(columnLabel)) {
            return TO_ENCLOSURE_FBS_TAG;
        } else if (TO_CONNECTOR.getColumnLabel().equals(columnLabel)) {
            return TO_CONNECTOR;
        } else if (TO_USER_LABEL.getColumnLabel().equals(columnLabel)) {
            return TO_USER_LABEL;
        } else if (SYSTEM.getColumnLabel().equals(columnLabel)) {
            return SYSTEM;
        } else if (SUBSYSTEM.getColumnLabel().equals(columnLabel)) {
            return SUBSYSTEM;
        } else if (CLASS.getColumnLabel().equals(columnLabel)) {
            return CLASS;
        } else if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (FBS_TAG.getColumnLabel().equals(columnLabel)) {
            return FBS_TAG;
        } else if (INSTALLATION_PACKAGE.getColumnLabel().equals(columnLabel)) {
            return INSTALLATION_PACKAGE;
        } else if (OWNERS.getColumnLabel().equals(columnLabel)) {
            return OWNERS;
        } else if (STATUS.getColumnLabel().equals(columnLabel)) {
            return STATUS;
        } else if (INSTALLATION_DATE.getColumnLabel().equals(columnLabel)) {
            return INSTALLATION_DATE;
        } else if (COMMENTS.getColumnLabel().equals(columnLabel)) {
            return COMMENTS;
        } else if (REVISION.getColumnLabel().equals(columnLabel)) {
            return REVISION;
        } else if(ELECTRICAL_DOCUMENTATION.getColumnLabel().equals(columnLabel)) {
            return ELECTRICAL_DOCUMENTATION;
        }
        return null;
    }

    public boolean isSortable() {
        return !(StringUtils.isNotEmpty(columnLabel) && INFORMATION.fieldName.equalsIgnoreCase(columnLabel));
    }
}
