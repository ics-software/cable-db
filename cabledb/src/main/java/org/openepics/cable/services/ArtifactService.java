/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.GenericArtifact;


/**
 * <code>ArtifactService</code> is the service layer that handles individual artifact operations.
 *
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Stateless
public class ArtifactService {
    @PersistenceContext
    private EntityManager em;

    /**
     * Update content of artifact.
     *
     * @param artifact
     *            to update
     */
    public void updateArtifact(GenericArtifact artifact) {
        if (artifact != null && artifact.getContent() != null) {
            artifact.setContent(artifact.getContent());
        }
    }

    /**
     * Deletes artifact from the blob store and database
     *
     * @param artifact
     *            artifact to delete
     */
    public void deleteArtifact(GenericArtifact artifact) {
        artifact = getArtifactById(artifact.getId());
        em.remove(artifact);
    }

    /**
     * Returns an artifact with the specified id.
     *
     * @param id the artifact id
     * @return the artifact, or null if the artifact with such id cannot be found
     */
    private GenericArtifact getArtifactById(Long id) {
        return em.find(GenericArtifact.class, id);
    }
}
