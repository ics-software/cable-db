/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.CableArticle;
import org.openepics.cable.services.CableArticleService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.util.Utility;

/**
 * <code>CableArticleLoader</code> is {@link DataLoader} that creates and persists cable articles
 * from {@link DSRecord} data.
 *
 * @author Lars Johansson
 */
public class CableArticleLoader extends DataLoader<CableArticle> {

    private static final Logger LOGGER = Logger.getLogger(CableArticleLoader.class.getName());

    private static final String INVALID = "Invalid ";

    @Inject
    private CableArticleService cableArticleService;
    @Inject
    private SessionService sessionService;

    // used for measuring time performance
    private long time;
    private long totalTime;

    // used for session information
    private String loggedInName;

    @Override
    public LoaderResult<CableArticle> load(InputStream stream, boolean test) {
        LOGGER.warning("Starting load");
        resetTime();

        // retrieve information before import
        //     session information
        // print import statistics and time

        loggedInName = sessionService.getLoggedInName();

        LoaderResult<CableArticle> result = super.load(stream, test);

        totalTime += getPassedTime();
        if (test) {
            LOGGER.info("# cable articles (data):         " + dataRows);
            LOGGER.info("# cable articles (create test):  " + createRowsTest);
            LOGGER.info("# cable articles (update test):  " + updateRowsTest);
            LOGGER.info("# cable articles (delete test):  " + deleteRowsTest);
            LOGGER.info("# cable articles (skipped test): " + result.getImportFileStatistic().getSkippedRowsTest());
        } else {
            LOGGER.info("# cable articles (data):    " + dataRows);
            LOGGER.info("# cable articles (create):  " + createRows);
            LOGGER.info("# cable articles (update):  " + updateRows);
            LOGGER.info("# cable articles (delete):  " + deleteRows);
            LOGGER.info("# cable articles (skipped): " + result.getImportFileStatistic().getSkippedRows());
        }
        LOGGER.info("Import time: " + totalTime + " ms");

        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {
        checkAllColumnsPresent(record);

        if (report.isError()) {
            stopLoad = true;
            return;
        }

        switch (record.getCommand()) {
        case CREATE:
            createCableArticle(record);
            break;
        case UPDATE:
            updateCableArticle(record);
            break;
        case DELETE:
            deleteCableArticle(record);
            break;
        default:
            // do nothing
            break;
        }
    }

    /** Processes the record for cable article creation. */
    private void createCableArticle(DSRecord record) {
        checkCableArticleAttributeValidity(record);

        if (report.isError() && !test)
            return;

        final String manufacturer = getRecordValue(record, CableArticleColumn.MANUFACTURER, false, false);
        final String externalId = getRecordValue(record, CableArticleColumn.EXTERNAL_ID, false, false);
        final String erpNumber = getRecordValue(record, CableArticleColumn.ERP_NUMBER, false, false);
        final String isoClass = getRecordValue(record, CableArticleColumn.ISO_CLASS);
        final String description = getRecordValue(record, CableArticleColumn.DESCRIPTION, true, true);
        final String longDescription = getRecordValue(record, CableArticleColumn.LONG_DESCRIPTION, true, true);
        final String modelType = getRecordValue(record, CableArticleColumn.MODEL_TYPE, false, false);
        final Float bendingRadius = Utility.toFloat(getRecordValue(record, CableArticleColumn.BENDING_RADIUS));
        final Float outerDiameter = Utility.toFloat(getRecordValue(record, CableArticleColumn.OUTER_DIAMETER));
        final Float ratedVoltage = Utility.toFloat(getRecordValue(record, CableArticleColumn.RATED_VOLTAGE));
        final Float weightPerLength = Utility.toFloat(getRecordValue(record, CableArticleColumn.WEIGHT_PER_LENGTH));
        final String shortName = getRecordValue(record, CableArticleColumn.SHORT_NAME, false, false);
        final String shortNameExternalId = getRecordValue(record, CableArticleColumn.SHORT_NAME_EXTERNAL_ID, false, false);
        final String urlChessPart = getRecordValue(record, CableArticleColumn.URL_CHESS_PART, false, false);

        if (cableArticleService.getCableArticleByShortNameExternalId(shortNameExternalId) != null) {
            report.addMessage(getErrorMessage(
                    getCableArticleNameInfo(shortNameExternalId) + " already exists in the database.",
                    record,
                    CableArticleColumn.MANUFACTURER,
                    manufacturer));
            if (test) {
                createRowsTest++;
            }
            return;
        }

        if (report.isError() && !test)
            return;

        if (test) {
            createRowsTest++;
        } else {
            CableArticle cableArticle =
                    cableArticleService.createCableArticle(
                            manufacturer, externalId, erpNumber, isoClass,
                            description, longDescription, modelType,
                            bendingRadius, outerDiameter, ratedVoltage, weightPerLength,
                            shortName, shortNameExternalId, urlChessPart,
                            true, new Date(), new Date(),
                            loggedInName);

            createRows++;
            report.addAffected(cableArticle);
        }

        report.addMessage(new ValidationMessage("Adding new cable article.", false, record.getRowLabel(), null));
    }

    /** Processes the record for cable article update. */
    private void updateCableArticle(DSRecord record) {
        checkCableArticleAttributeValidity(record);// validation

        if (report.isError() && !test)
            return;

        if (!checkCableArticleName(record)) {
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final String manufacturer = getRecordValue(record, CableArticleColumn.MANUFACTURER, false, false);
        final String externalId = getRecordValue(record, CableArticleColumn.EXTERNAL_ID, false, false);
        final String erpNumber = getRecordValue(record, CableArticleColumn.ERP_NUMBER, false, false);
        final String isoClass = getRecordValue(record, CableArticleColumn.ISO_CLASS);
        final String description = getRecordValue(record, CableArticleColumn.DESCRIPTION, true, true);
        final String longDescription = getRecordValue(record, CableArticleColumn.LONG_DESCRIPTION, true, true);
        final String modelType = getRecordValue(record, CableArticleColumn.MODEL_TYPE, false, false);
        final Float bendingRadius = Utility.toFloat(getRecordValue(record, CableArticleColumn.BENDING_RADIUS));
        final Float outerDiameter = Utility.toFloat(getRecordValue(record, CableArticleColumn.OUTER_DIAMETER));
        final Float ratedVoltage = Utility.toFloat(getRecordValue(record, CableArticleColumn.RATED_VOLTAGE));
        final Float weightPerLength = Utility.toFloat(getRecordValue(record, CableArticleColumn.WEIGHT_PER_LENGTH));
        final String shortName = getRecordValue(record, CableArticleColumn.SHORT_NAME, false, false);
        final String shortNameExternalId = getRecordValue(record, CableArticleColumn.SHORT_NAME_EXTERNAL_ID, false, false);
        final String urlChessPart = getRecordValue(record, CableArticleColumn.URL_CHESS_PART, false, false);

        final CableArticle oldCableArticle = cableArticleService.getCableArticleByShortNameExternalId(shortNameExternalId);
        cableArticleService.detachCableArticle(oldCableArticle);
        final CableArticle cableArticle = cableArticleService.getCableArticleByShortNameExternalId(shortNameExternalId);

        if (!cableArticle.isActive()) {
            report.addMessage(new ValidationMessage(
                    getCableArticleNameInfo(shortNameExternalId)
                            + " cannot be updated as it is already deleted.",
                    true,
                    record.getRowLabel(),
                    null,
                    manufacturer));
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        if (report.isError() && !test)
            return;

        cableArticle.setManufacturer(manufacturer);
        cableArticle.setExternalId(externalId);
        cableArticle.setErpNumber(erpNumber);
        cableArticle.setIsoClass(isoClass);
        cableArticle.setDescription(description);
        cableArticle.setLongDescription(longDescription);
        cableArticle.setModelType(modelType);
        cableArticle.setBendingRadius(bendingRadius);
        cableArticle.setOuterDiameter(outerDiameter);
        cableArticle.setRatedVoltage(ratedVoltage);
        cableArticle.setWeightPerLength(weightPerLength);
        cableArticle.setShortName(shortName);
        cableArticle.setShortNameExternalId(shortNameExternalId);
        cableArticle.setUrlChessPart(urlChessPart);

        if (test) {
            updateRowsTest++;
        } else {
            if (cableArticleService.updateCableArticle(cableArticle, oldCableArticle, loggedInName)) {
                updateRows++;
            }
            report.addAffected(cableArticle);
        }
        report.addMessage(new ValidationMessage(
                "Updating " + getCableArticleNameInfo(shortNameExternalId), false, record.getRowLabel(), null));
    }

    /** Processes the record for cable article deletion. */
    private void deleteCableArticle(DSRecord record) {
        if (!checkCableArticleName(record)) {
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        final String shortNameExternalId = getRecordValue(record, CableArticleColumn.SHORT_NAME_EXTERNAL_ID, false, false);

        final CableArticle cableArticle = cableArticleService.getCableArticleByShortNameExternalId(shortNameExternalId);

        if (!cableArticle.isActive()) {
            report.addMessage(new ValidationMessage(
                    getCableArticleNameInfo(shortNameExternalId) + " is already deleted.",
                    true,
                    record.getRowLabel(),
                    shortNameExternalId));
        }

        if (report.isError() && !test)
            return;

        if (test) {
            deleteRowsTest++;
        } else {
            if (cableArticleService.deleteCableArticle(cableArticle, loggedInName)) {
                deleteRows++;
            }
            report.addAffected(cableArticle);
        }
        report.addMessage(new ValidationMessage(
                "Deleting " + getCableArticleNameInfo(shortNameExternalId) + ".",
                false,
                record.getRowLabel(),
                null));
    }

    /**
     * Return value for column in record.
     * Assumes empty value is to be replaced with null value.
     *
     * @param record record
     * @param column column
     * @return value for column in record
     */
    private String getRecordValue(DSRecord record, CableArticleColumn column) {
        return getRecordValue(record, column, true, false);
    }

    /**
     * Return value for column in record.
     *
     * @param record record
     * @param column column
     * @param nullIfEmpty if empty value is to be replaced with null value
     * @param trimFrom true if leading and trailing whitespaces are to be trimmed, false if all whitespaces are to be trimmed.
     *                 Only relevant when nullIfEmpty is true.
     * @return value for column in record
     */
    private String getRecordValue(DSRecord record, CableArticleColumn column, boolean nullIfEmpty, boolean trimFrom) {
        return nullIfEmpty
                ? trimFrom
                        ? Utility.formatRecordValueTrimFrom(record.getField(column).toString())
                        : Utility.formatRecordValue(record.getField(column).toString())
                : Utility.formatWhitespace(record.getField(column).toString());
    }

    /** Check that the record has all expected columns. */
    private void checkAllColumnsPresent(DSRecord record) {
        for (final CableArticleColumn column : CableArticleColumn.values()) {
            if (!column.isExcelColumn()) {
                continue;
            }
            if (column.isExcelVolatileColumn()) {
                // if volatile column/field, presence in Excel not guaranteed
                //     @see InstallationPackageColumn#REVISION
                continue;
            }

            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by importing an old template or importing a wrong file.", true));
            }
        }
    }

    /** Check that the installation package attributes are valid. */
    private void checkCableArticleAttributeValidity(DSRecord record) {
        final String manufacturer = getRecordValue(record, CableArticleColumn.MANUFACTURER, false, false);
        final String externalId = getRecordValue(record, CableArticleColumn.EXTERNAL_ID, false, false);
        final String erpNumber = getRecordValue(record, CableArticleColumn.ERP_NUMBER, false, false);
        final String isoClass = getRecordValue(record, CableArticleColumn.ISO_CLASS);
        final String description = getRecordValue(record, CableArticleColumn.DESCRIPTION, true, true);
        final String longDescription = getRecordValue(record, CableArticleColumn.LONG_DESCRIPTION, true, true);
        final String modelType = getRecordValue(record, CableArticleColumn.MODEL_TYPE, false, false);
        final Float bendingRadius = Utility.toFloat(getRecordValue(record, CableArticleColumn.BENDING_RADIUS));
        final Float outerDiameter = Utility.toFloat(getRecordValue(record, CableArticleColumn.OUTER_DIAMETER));
        final Float ratedVoltage = Utility.toFloat(getRecordValue(record, CableArticleColumn.RATED_VOLTAGE));
        final Float weightPerLength = Utility.toFloat(getRecordValue(record, CableArticleColumn.WEIGHT_PER_LENGTH));
        final String shortName = getRecordValue(record, CableArticleColumn.SHORT_NAME, false, false);
        final String shortNameExternalId = getRecordValue(record, CableArticleColumn.SHORT_NAME_EXTERNAL_ID, false, false);
        final String urlChessPart = getRecordValue(record, CableArticleColumn.URL_CHESS_PART, false, false);

        try {
            if (StringUtils.isBlank(manufacturer)) {
                report.addMessage(
                        getErrorMessage(
                                "MANUFACTURER is not specified.", record, CableArticleColumn.MANUFACTURER));
            }
            validateStringSize(manufacturer, CableArticleColumn.MANUFACTURER);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.MANUFACTURER, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.MANUFACTURER,
                            manufacturer));
        }

        try {
            if (StringUtils.isBlank(externalId)) {
                report.addMessage(
                        getErrorMessage(
                                "EXTERNAL ID is not specified.", record, CableArticleColumn.EXTERNAL_ID));
            }
            validateStringSize(externalId, CableArticleColumn.EXTERNAL_ID);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.EXTERNAL_ID, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.EXTERNAL_ID,
                            externalId));
        }

        try {
            if (StringUtils.isBlank(erpNumber)) {
                report.addMessage(
                        getErrorMessage(
                                "ERP NUMBER is not specified.", record, CableArticleColumn.ERP_NUMBER));
            }
            validateStringSize(manufacturer, CableArticleColumn.ERP_NUMBER);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.ERP_NUMBER, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.ERP_NUMBER,
                            erpNumber));
        }

        try {
            validateStringSize(isoClass, CableArticleColumn.ISO_CLASS);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.ISO_CLASS, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.ISO_CLASS,
                            isoClass));
        }

        // DESCRIPTION, LONG_DESCRIPTION
        //     size 65545
        // BENDING_RADIUS, OUTER_DIAMETER, RATED_VOLTAGE, WEIGHT_PER_LENGTH
        //     float

        try {
            validateStringSize(modelType, CableArticleColumn.MODEL_TYPE);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.MODEL_TYPE, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.MODEL_TYPE,
                            modelType));
        }

        try {
            if (StringUtils.isBlank(shortName)) {
                report.addMessage(
                        getErrorMessage(
                                "SHORT NAME is not specified.", record, CableArticleColumn.SHORT_NAME));
            }
            validateStringSize(externalId, CableArticleColumn.SHORT_NAME);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.SHORT_NAME, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.SHORT_NAME,
                            shortName));
        }

        try {
            if (StringUtils.isBlank(shortNameExternalId)) {
                report.addMessage(
                        getErrorMessage(
                                "SHORT NAME EXTERNAL ID is not specified.", record, CableArticleColumn.SHORT_NAME_EXTERNAL_ID));
            }
            validateStringSize(externalId, CableArticleColumn.SHORT_NAME_EXTERNAL_ID);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.SHORT_NAME_EXTERNAL_ID, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.SHORT_NAME_EXTERNAL_ID,
                            shortNameExternalId));
        }

        try {
            validateStringSize(urlChessPart, CableArticleColumn.URL_CHESS_PART);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableArticleColumn.URL_CHESS_PART, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            CableArticleColumn.URL_CHESS_PART,
                            urlChessPart));
        }
    }

    /**
     * Checks the validity of cable article name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkCableArticleName(DSRecord record) {
        final String shortNameExternalId = getRecordValue(record, CableArticleColumn.SHORT_NAME_EXTERNAL_ID, false, false);

        final CableArticle cableArticle = cableArticleService.getCableArticleByShortNameExternalId(shortNameExternalId);
        if (cableArticle == null) {
            report.addMessage(getErrorMessage(
                    "Cable article ("+shortNameExternalId+") does not exist in the database.",
                    record,
                    CableArticleColumn.MANUFACTURER,
                    shortNameExternalId));
            return false;
        }
        return true;
    }

    private String getCableArticleNameInfo(String shortNameExternalId) {
        return "CableArticle with shortNameExternalId " + shortNameExternalId;
    }

    private void validateStringSize(final String value, final CableArticleColumn column) {
        super.validateStringSize(value, column, CableArticle.class);
    }

}
