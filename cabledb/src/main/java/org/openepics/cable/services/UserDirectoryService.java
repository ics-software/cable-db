/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Set;

/**
 * This is a service that provides directory data on users.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public interface UserDirectoryService {


    /** Tests that the directory service is reachable, and throws an exception if it isn't. */
    public void validate();

    /**
     * Returns a set of all user names currently registered in the directory.
     *
     * @return the set
     */
    public Set<String> getAllUsernames();

    /**
     * Returns a set of names of all users that have user permission of the cable database.
     *
     * @return the set
     */
    public Set<String> getAllUserUsernames();

    /**
     * Returns a set of names of all users that have administer permission of the cable database.
     *
     * @return the set
     */
    public Set<String> getAllAdministratorUsernames();

    /**
     * Retrieves the email address for the given user.
     *
     * @param username
     *            the user name
     * @return the user email
     */
    public String getEmail(String username);

    /**
     * Retrieves fur name for the given user.
     *
     * @param username
     *            the user name
     * @return the full name of the user
     */
    public String getUserFullName(String username);

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username
     *            the user name
     * @return the user full name and email
     */
    public String getUserFullNameAndEmail(String username);
}
