/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import org.openepics.cable.model.Connector;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * This is the service layer that handles connector import and export operations.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
@Stateless
public class ConnectorImportService {

    private static final Logger LOGGER = Logger.getLogger(InstallationPackageImportExportService.class.getName());

    private static final String NO_IMPORT_PERMISSION = "You do not have permission to import connectors.";
    private static final String NO_EXPORT_PERMISSION = "You do not have permission to export connectors.";

    @Resource
    private EJBContext context;

    @Inject
    private ConnectorLoader connectorLoader;

    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;

    @Inject
    private MailService mailService;

    /**
     * Import connectors from input stream.
     *
     * @param inputStream input stream representing Excel sheet content
     * @return result for import of connectors
     */
    public LoaderResult<Connector> importConnectors(InputStream inputStream) {
        return importConnectors(inputStream, false);
    }

    /**
     * Import connectors from input stream. May be test import in which case, data is examined but not imported.
     *
     * @param inputStream input stream representing Excel sheet content
     * @param test if test import
     * @return result for import of connectors
     */
    public LoaderResult<Connector> importConnectors(InputStream inputStream, boolean test) {
        // We read inputStream into byte as we need to use it twice.
        byte[] bytes;
        try {
            bytes = ByteStreams.toByteArray(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ByteArrayInputStream byteInputstream = new ByteArrayInputStream(bytes);

        if (!canImportConnectors()) {
            throw new IllegalStateException(NO_IMPORT_PERMISSION);
        }

        LoaderResult<Connector> report = connectorLoader.load(byteInputstream, test);

        if (!test && !report.isError()) {
            // Send mail
            final Set<String> notifiedUsers = new HashSet<>();

            notifiedUsers.addAll(userDirectoryServiceFacade.getAllAdministratorUsernames());

            if (!Lists.newArrayList(report.getAffected()).isEmpty()) {
                String content = "Connectors have been imported in the Cable Database" + " (by "
                        + userDirectoryServiceFacade.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")";
                byteInputstream.reset();
                mailService.sendMail(notifiedUsers, sessionService.getLoggedInName(),
                        "Connectors imported notification", content,
                        Arrays.asList(byteInputstream), Arrays.asList("cdb_connectors_imported.xlsx"), true, true);
            }
        } else {
            context.setRollbackOnly();
        }

        LOGGER.fine("Returning result: " + report);

        return report;
    }

    /**
     * Exports connectors cables to an Excel spreadsheet.
     *
     * @param connectors
     *            the connectors to export
     * @return the input stream representing the Excel file
     *
     * @throws IllegalStateException
     *             if current user does not have permission for this action
     */
    public InputStream exportConnectors(Iterable<Connector> connectors) {

        if (!sessionService.isLoggedIn()) {
            throw new IllegalStateException(NO_EXPORT_PERMISSION);
        }

        final ConnectorSaver connectorSaver = new ConnectorSaver(connectors);
        return connectorSaver.save();
    }

    /** @return true if the current user can import connectors, else false */
    public boolean canImportConnectors() {
        return sessionService.canAdminister();
    }
}
