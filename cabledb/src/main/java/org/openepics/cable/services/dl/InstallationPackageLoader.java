/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.InstallationPackageService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.InstallationPackageNumbering;
import org.openepics.cable.util.Utility;

/**
 * <code>InstallationPackageLoader</code> is {@link DataLoader} that creates and persists installation packages
 * from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class InstallationPackageLoader extends DataLoader<InstallationPackage> {

    private static final Logger LOGGER = Logger.getLogger(InstallationPackageLoader.class.getName());

    private static final String INVALID = "Invalid ";
//    public static final String VALID = "VALID";
//    public static final String OBSOLETE = "OBSOLETE";

    @Inject
    private InstallationPackageService installationPackageService;
    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;

    // used for measuring time performance
    private long time;
    private long totalTime;

    // used for session information
    private String loggedInName;

    private Set<String> validUsernames;

    @Override
    public LoaderResult<InstallationPackage> load(InputStream stream, boolean test) {
        LOGGER.warning("Starting load");
        resetTime();

        // retrieve information before import
        //     usernames from ldap
        //     session information
        // clear
        // print import statistics and time

        validUsernames = userDirectoryServiceFacade.getAllUsernames();

        loggedInName = sessionService.getLoggedInName();

        LoaderResult<InstallationPackage> result = super.load(stream, test);

        validUsernames = null;

        totalTime += getPassedTime();
        if (test) {
            LOGGER.info("# installation packages (data):         " + dataRows);
            LOGGER.info("# installation packages (create test):  " + createRowsTest);
            LOGGER.info("# installation packages (update test):  " + updateRowsTest);
            LOGGER.info("# installation packages (delete test):  " + deleteRowsTest);
            LOGGER.info("# installation packages (skipped test): " + result.getImportFileStatistic().getSkippedRowsTest());
        } else {
            LOGGER.info("# installation packages (data):    " + dataRows);
            LOGGER.info("# installation packages (create):  " + createRows);
            LOGGER.info("# installation packages (update):  " + updateRows);
            LOGGER.info("# installation packages (delete):  " + deleteRows);
            LOGGER.info("# installation packages (skipped): " + result.getImportFileStatistic().getSkippedRows());
        }
        LOGGER.info("Import time: " + totalTime + " ms");

        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {
        checkAllColumnsPresent(record);

        if (report.isError()) {
            stopLoad = true;
            return;
        }

        switch (record.getCommand()) {
        case CREATE:
            createInstallationPackage(record);
            break;
        case UPDATE:
            updateInstallationPackage(record);
            break;
        case DELETE:
            deleteInstallationPackage(record);
            break;
        default:
            // do nothing
            break;
        }
    }

    /** Processes the record for installation package creation. */
    private void createInstallationPackage(DSRecord record) {
        checkInstallationPackageAttributeValidity(record);

        if (report.isError() && !test)
            return;

        final String name = getRecordValue(record, InstallationPackageColumn.NAME);

        if (installationPackageService.getInstallationPackageByName(name) != null) {
            report.addMessage(getErrorMessage(
                    getInstallationPackageNameInfo(name) + " already exists in the database.",
                    record,
                    InstallationPackageColumn.NAME,
                    name));
            if (test) {
                createRowsTest++;
            }
            return;
        }

        final String description = getRecordValue(record, InstallationPackageColumn.DESCRIPTION, true, true);
        final String location = getRecordValue(record, InstallationPackageColumn.LOCATION);
        final String cableCoordinator = getRecordValue(record, InstallationPackageColumn.CABLE_COORDINATOR);
        final String routingDocumentation = getRecordValue(record, InstallationPackageColumn.ROUTING_DOCUMENTATION);
        final String installerCable = getRecordValue(record, InstallationPackageColumn.INSTALLER_CABLE);
        final String installerConnectorA = getRecordValue(record, InstallationPackageColumn.INSTALLER_CONNECTOR_A);
        final String installerConnectorB = getRecordValue(record, InstallationPackageColumn.INSTALLER_CONNECTOR_B);
        final String installationPackageLeader = getRecordValue(record, InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER);

        if (report.isError() && !test)
            return;

        // revision is optional column/field
        //     administrator only
        //     no previous value to consider

        if (test) {
            createRowsTest++;
        } else {
            InstallationPackage installationPackage = new InstallationPackage(
                    name, description, location, cableCoordinator, routingDocumentation,
                    installerCable, installerConnectorA, installerConnectorB, installationPackageLeader,
                    true);
            installationPackage.setLocation(location);
            installationPackage =
                    installationPackageService.createInstallationPackage(
                            installationPackage, loggedInName);
            createRows++;
            report.addAffected(installationPackage);
        }

        report.addMessage(new ValidationMessage("Adding new installation package.", false, record.getRowLabel(), null));
    }

    /** Processes the record for installation package update. */
    private void updateInstallationPackage(DSRecord record) {
        checkInstallationPackageAttributeValidity(record);// validation

        if (report.isError() && !test)
            return;

        if (!checkInstallationPackageName(record)) {
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final String name = getRecordValue(record, InstallationPackageColumn.NAME);

        final InstallationPackage oldInstallationPackage = installationPackageService.getInstallationPackageByName(name);
        installationPackageService.detachInstallationPackage(oldInstallationPackage);
        final InstallationPackage installationPackage = installationPackageService.getInstallationPackageByName(name);

        if (!installationPackage.isActive()) {
            report.addMessage(new ValidationMessage(
                    getInstallationPackageNameInfo(name)
                            + " cannot be updated as it is already deleted.",
                    true,
                    record.getRowLabel(),
                    null,
                    name));
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final String description = getRecordValue(record, InstallationPackageColumn.DESCRIPTION, true, true);
        final String location = getRecordValue(record, InstallationPackageColumn.LOCATION);
        final String cableCoordinator = getRecordValue(record, InstallationPackageColumn.CABLE_COORDINATOR);
        final String routingDocumentation = getRecordValue(record, InstallationPackageColumn.ROUTING_DOCUMENTATION);
        final String installerCable = getRecordValue(record, InstallationPackageColumn.INSTALLER_CABLE);
        final String installerConnectorA = getRecordValue(record, InstallationPackageColumn.INSTALLER_CONNECTOR_A);
        final String installerConnectorB = getRecordValue(record, InstallationPackageColumn.INSTALLER_CONNECTOR_B);
        final String installationPackageLeader = getRecordValue(record, InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER);

        if (report.isError() && !test)
            return;

        installationPackage.setName(name);
        installationPackage.setDescription(description);
        installationPackage.setCableCoordinator(cableCoordinator);
        installationPackage.setLocation(location);
        installationPackage.setRoutingDocumentation(routingDocumentation);
        installationPackage.setInstallerCable(installerCable);
        installationPackage.setInstallerConnectorA(installerConnectorA);
        installationPackage.setInstallerConnectorB(installerConnectorB);
        installationPackage.setInstallationPackageLeader(installationPackageLeader);

        // revision is optional column/field
        //     administrator only
        //     previous value to consider

        if (test) {
            updateRowsTest++;
        } else {
            if (installationPackageService.updateInstallationPackage(
                    installationPackage, oldInstallationPackage, loggedInName)) {
                updateRows++;
            }
            report.addAffected(installationPackage);
        }
        report.addMessage(new ValidationMessage(
                "Updating installation package '" + name + "'.", false, record.getRowLabel(), null));
    }

    /** Processes the record for installation package deletion. */
    private void deleteInstallationPackage(DSRecord record) {
        if (!checkInstallationPackageName(record)) {
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        final String name = getRecordValue(record, InstallationPackageColumn.NAME);
        final InstallationPackage installationPackage = installationPackageService.getInstallationPackageByName(name);

        if (!installationPackage.isActive()) {
            report.addMessage(new ValidationMessage(
                    getInstallationPackageNameInfo(name) + " is already deleted.",
                    true,
                    record.getRowLabel(),
                    name));
        }

        if (report.isError() && !test)
            return;

        if (test) {
            deleteRowsTest++;
        } else {
            if (installationPackageService.deleteInstallationPackage(installationPackage, loggedInName)) {
                deleteRows++;
            }
            report.addAffected(installationPackage);
        }
        report.addMessage(new ValidationMessage(
                "Deleting installation package with number " + name + ".",
                false,
                record.getRowLabel(),
                null));
    }

    /**
     * Return value for column in record.
     * Assumes empty value is to be replaced with null value.
     *
     * @param record record
     * @param column column
     * @return value for column in record
     */
    private String getRecordValue(DSRecord record, InstallationPackageColumn column) {
        return getRecordValue(record, column, true, false);
    }

    /**
     * Return value for column in record.
     *
     * @param record record
     * @param column column
     * @param nullIfEmpty if empty value is to be replaced with null value
     * @param trimFrom true if leading and trailing whitespaces are to be trimmed, false if all whitespaces are to be trimmed.
     *                 Only relevant when nullIfEmpty is true.
     * @return value for column in record
     */
    private String getRecordValue(DSRecord record, InstallationPackageColumn column, boolean nullIfEmpty, boolean trimFrom) {
        return nullIfEmpty
                ? trimFrom
                        ? Utility.formatRecordValueTrimFrom(record.getField(column).toString())
                        : Utility.formatRecordValue(record.getField(column).toString())
                : Utility.formatWhitespace(record.getField(column).toString());
    }

    /** Check that the record has all expected columns. */
    private void checkAllColumnsPresent(DSRecord record) {
        for (final InstallationPackageColumn column : InstallationPackageColumn.values()) {
            if (!column.isExcelColumn()) {
                continue;
            }
            if (column.isExcelVolatileColumn()) {
                // if volatile column/field, presence in Excel not guaranteed
                //     @see InstallationPackageColumn#REVISION
                continue;
            }

            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by importing an old template or importing a wrong file.", true));
            }
        }
    }

    /** Check that the installation package attributes are valid. */
    private void checkInstallationPackageAttributeValidity(DSRecord record) {
        final String name = getRecordValue(record, InstallationPackageColumn.NAME);
        try {
            if (StringUtils.isBlank(name)) {
                report.addMessage(getErrorMessage("NAME is not specified.", record, InstallationPackageColumn.NAME));
            }
            validateStringSize(name, InstallationPackageColumn.NAME);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.NAME, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.NAME,
                            name));
        }

        final String description = getRecordValue(record, InstallationPackageColumn.DESCRIPTION, true, true);
        try {
            validateStringSize(description, InstallationPackageColumn.DESCRIPTION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.DESCRIPTION, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.DESCRIPTION,
                            description));
        }

        final String location = getRecordValue(record, InstallationPackageColumn.LOCATION);
        try {
            validateStringSize(location, InstallationPackageColumn.LOCATION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.LOCATION, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.LOCATION,
                            location));
        }

        final String cableCoordinator = getRecordValue(record, InstallationPackageColumn.CABLE_COORDINATOR);
        try {
            validateStringSize(cableCoordinator, InstallationPackageColumn.CABLE_COORDINATOR);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.CABLE_COORDINATOR, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.CABLE_COORDINATOR,
                            cableCoordinator));
        }
        List<String> usernames = StringUtils.isNotEmpty(cableCoordinator)
                ? Utility.splitStringIntoList(cableCoordinator)
                : null;
        if (usernames != null) {
            if (!validUsernames(usernames)) {
                report.addMessage(
                        getErrorMessage(
                                "Cable Coordinator is not present in the user directory.",
                                record,
                                InstallationPackageColumn.CABLE_COORDINATOR,
                                cableCoordinator));
            }
        }

        final String routingDocumentation = getRecordValue(record, InstallationPackageColumn.ROUTING_DOCUMENTATION);
        try {
            validateStringSize(routingDocumentation, InstallationPackageColumn.ROUTING_DOCUMENTATION);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.ROUTING_DOCUMENTATION, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.ROUTING_DOCUMENTATION,
                            routingDocumentation));
        }

        final String installerCable = getRecordValue(record, InstallationPackageColumn.INSTALLER_CABLE);
        try {
            validateStringSize(installerCable, InstallationPackageColumn.INSTALLER_CABLE);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLER_CABLE, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.INSTALLER_CABLE,
                            installerCable));
        }
        if (!InstallationPackageNumbering.isValid(installerCable)) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLER_CABLE);
            report.addMessage(
                    getErrorMessage(
                            INVALID + InstallationPackageColumn.INSTALLER_CABLE,
                            record,
                            InstallationPackageColumn.INSTALLER_CABLE,
                            installerCable));
        }

        final String installerConnectorA = getRecordValue(record, InstallationPackageColumn.INSTALLER_CONNECTOR_A);
        try {
            validateStringSize(installerConnectorA, InstallationPackageColumn.INSTALLER_CONNECTOR_A);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLER_CONNECTOR_A, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.INSTALLER_CONNECTOR_A,
                            installerConnectorA));
        }
        if (!InstallationPackageNumbering.isValid(installerConnectorA)) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLER_CONNECTOR_A);
            report.addMessage(
                    getErrorMessage(
                            INVALID + InstallationPackageColumn.INSTALLER_CONNECTOR_A,
                            record,
                            InstallationPackageColumn.INSTALLER_CONNECTOR_A,
                            installerConnectorA));
        }

        final String installerConnectorB = getRecordValue(record, InstallationPackageColumn.INSTALLER_CONNECTOR_B);
        try {
            validateStringSize(installerConnectorB, InstallationPackageColumn.INSTALLER_CONNECTOR_B);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLER_CONNECTOR_B, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.INSTALLER_CONNECTOR_B,
                            installerConnectorB));
        }
        if (!InstallationPackageNumbering.isValid(installerConnectorB)) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLER_CONNECTOR_B);
            report.addMessage(
                    getErrorMessage(
                            INVALID + InstallationPackageColumn.INSTALLER_CONNECTOR_B,
                            record,
                            InstallationPackageColumn.INSTALLER_CONNECTOR_B,
                            installerConnectorB));
        }

        final String installationPackageLeader = getRecordValue(record, InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER);
        try {
            validateStringSize(installationPackageLeader, InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER, e);
            report.addMessage(
                    getErrorMessage(
                            e.getMessage(),
                            record,
                            InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER,
                            installationPackageLeader));
        }
        usernames = StringUtils.isNotEmpty(installationPackageLeader)
                ? Utility.splitStringIntoList(installationPackageLeader)
                : null;
        if (usernames != null) {
            if (!validUsernames(usernames)) {
                report.addMessage(
                        getErrorMessage(
                                "Installation Package Leader is not present in the user directory.",
                                record,
                                InstallationPackageColumn.INSTALLATION_PACKAGE_LEADER,
                                installationPackageLeader));
            }
        }
    }

    /**
     * Checks the validity of installation package name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkInstallationPackageName(DSRecord record) {
        final String name = getRecordValue(record, InstallationPackageColumn.NAME);

        final InstallationPackage installationPackage = installationPackageService.getInstallationPackageByName(name);
        if (installationPackage == null) {
            report.addMessage(getErrorMessage(
                    getInstallationPackageNameInfo(name) + " does not exist in the database.",
                    record,
                    InstallationPackageColumn.NAME,
                    name));
            return false;
        }
        return true;
    }

    private String getInstallationPackageNameInfo(String installationPackageName) {
        return "InstallationPackage with name " + installationPackageName;
    }

    private void validateStringSize(final String value, final InstallationPackageColumn column) {
        super.validateStringSize(value, column, InstallationPackage.class);
    }

    protected Set<String> getCableClassesAsSet(String cableClasses) {
        return new HashSet<>(Arrays.asList(cableClasses.split("\\s*,\\s*")));
    }

    protected boolean areCableClassesValid(String cableClasses) {
        Set<String> classes = getCableClassesAsSet(cableClasses);
        for (String klass : classes) {
            if (!CableNumbering.isValidCableClass(klass))
                return false;
        }
        return true;
    }

    /**
     * Check validity of usernames
     *
     * @param usernames usernames to check
     * @return true if all usernames are correct else false
     */
    private boolean validUsernames(List<String> usernames) {
        for (String user : usernames) {
            if (!validUsernames.contains(user)) {
                return false;
            }
        }
        return true;
    }

}
