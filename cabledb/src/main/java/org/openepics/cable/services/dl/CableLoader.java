/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableArticle;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.CableType;
import org.openepics.cable.model.Connector;
import org.openepics.cable.model.Endpoint;
import org.openepics.cable.model.InstallationPackage;
import org.openepics.cable.services.CableArticleService;
import org.openepics.cable.services.CableService;
import org.openepics.cable.services.CableTypeService;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.Names;
import org.openepics.cable.services.InstallationPackageService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;
import org.openepics.cable.ui.CableRequestManager;
import org.openepics.cable.util.CableNumbering;
import org.openepics.cable.util.StatusTransitionException;
import org.openepics.cable.util.StatusTransitionUtil;
import org.openepics.cable.util.Utility;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsService;
import org.openepics.cable.util.fbs.FbsUtil;

/**
 * <code>CableLoader</code> is {@link DataLoader} that creates and persists cables from {@link DSRecord} data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author <a href="mailto:marcel.salmic@cosylab.com">Marcel Salmic</a>
 */
@Stateless
public class CableLoader extends DataLoader<Cable> {

    private static final Logger LOGGER = Logger.getLogger(CableLoader.class.getName());

    private static enum ValidationChoice {CREATE, UPDATE};

    private static final String CHESS_AT_LEAST_ONE_OF_TWO_VALUES = "At least one of the two values is to be specified.";
    private static final String CHESS_MAPPING                    = "CHESS mapping Tag/ESS Name/Cable Name";
    private static final String INVALID                          = "Invalid ";
    private static final String IS_NOT_FOUND_IN                  = " is not found in ";
    private static final String IS_OVER_MAXIMUM_ALLOWED_LENGTH   = " is over maximum allowed length ";

    private static final String FROM_ENDPOINT      = "FROM ENDPOINT";
    private static final String TO_ENDPOINT        = "TO ENDPOINT";
    private static final String ESS_NAME           = "ESS NAME";
    private static final String FBS_TAG            = "FBS TAG";
    private static final String LBS_TAG            = "LBS TAG";
    private static final String ENCLOSURE_ESS_NAME = "ENCLOSURE ESS NAME";
    private static final String ENCLOSURE_FBS_TAG  = "ENCLOSURE FBS TAG";
    private static final String CONNECTOR          = "CONNECTOR";
    private static final String LABEL              = "LABEL";

    @Inject
    private CableService cableService;
    @Inject
    private CableArticleService cableArticleService;
    @Inject
    private CableTypeService cableTypeService;
    @Inject
    private ConnectorService connectorService;
    @Inject
    private InstallationPackageService installationPackageService;
    @Inject
    private Names names;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;
    @Inject
    private FbsService fbsService;
    @Inject
    private CableRequestManager cableRequestManager;

    @Inject
    private SessionService sessionService;

    private Set<String> validEndpointNames;
    private Set<String> validUsernames;
    private final Map<String, CableArticle> cableArticles = new HashMap<>();
    private final Map<String, CableType> cableTypes = new HashMap<>();
    private final Map<String, Connector> connectors = new HashMap<>();
    private final Map<String, InstallationPackage> installationPackages = new HashMap<>();
    private final Map<String, Cable> originalCables = new HashMap<>();
    private final Map<String, Cable> cables = new HashMap<>();

    private List<Cable> cablesToCreate;
    private List<Cable> cablesToDelete;
    private List<Cable> cablesToUpdate;
    private List<Cable> oldCablesToUpdate;

    // maps to hold tag mappings after using ITIP client for retrieval of FBS XML
    Map<String, FbsElement> mappingsCableNameToFbsElement = Collections.emptyMap();
    Map<String, FbsElement> mappingsEssNameToFbsElement   = Collections.emptyMap();
    Map<String, FbsElement> mappingsIdToFbsElement        = Collections.emptyMap();
    Map<String, FbsElement> mappingsTagToFbsElement       = Collections.emptyMap();

    // used for measuring time performance
    private long time;
    private long totalTime;

    // used for session information
    private boolean canAdminister;
    private boolean canManageOwnedCables;
    private String loggedInName;

    // distinguish between endpoints
    private enum EndpointType {
        TYPE_FROM_ENDPOINT, TYPE_TO_ENDPOINT
    }

    /**
     * Default constructor.
     */
    public CableLoader() {
        super();
    }

    @Override
    public LoaderResult<Cable> load(InputStream stream, boolean test) {
        LOGGER.warning("Starting load");
        resetTime();

        // retrieve information before import
        //     fbs tag mappings
        //     names from naming
        //     usernames from ldap
        //     cable types, connectors,  installation packages
        //     session information
        // clear
        // print import statistics and time

        final List<FbsElement> fbsElements = fbsService.getAllFbsElements();
        if (!fbsElements.isEmpty()) {
            mappingsCableNameToFbsElement = FbsUtil.readMappingsCableNameToFbsElement(fbsElements);
            mappingsEssNameToFbsElement   = FbsUtil.readMappingsEssNameToFbsElement(fbsElements);
            mappingsIdToFbsElement        = FbsUtil.readMappingsIdToFbsElement(fbsElements);
            mappingsTagToFbsElement       = FbsUtil.readMappingsTagToFbsElement(fbsElements);

            LOGGER.fine("# FBS mappings:            " + fbsElements.size());
            LOGGER.fine("# FBS mappings cable name: " + mappingsCableNameToFbsElement.size());
            LOGGER.fine("# FBS mappings ESS name:   " + mappingsEssNameToFbsElement.size());
            LOGGER.fine("# FBS mappings id:         " + mappingsIdToFbsElement.size());
            LOGGER.fine("# FBS mappings tag:        " + mappingsTagToFbsElement.size());
        }

        validEndpointNames = names.getActiveNames();
        validUsernames = userDirectoryServiceFacade.getAllUsernames();

        cableArticles.clear();
        for (CableArticle cableArticle : cableArticleService.getCableArticles()) {
            cableArticles.put(cableArticle.getName(), cableArticle);
        }
        cableTypes.clear();
        for (CableType cableType : cableTypeService.getAllCableTypes()) {
            cableTypes.put(cableType.getName(), cableType);
        }
        connectors.clear();
        for (Connector connector : connectorService.getConnectors()) {
            connectors.put(connector.getName(), connector);
        }
        installationPackages.clear();
        for (InstallationPackage installationPackage : installationPackageService.getActiveInstallationPackages()) {
            installationPackages.put(installationPackage.getName(), installationPackage);
        }

        canAdminister = sessionService.canAdminister();
        canManageOwnedCables = sessionService.canManageOwnedCables();
        loggedInName = sessionService.getLoggedInName();

        LoaderResult<Cable> result = super.load(stream, test);

        // clear mappings
        mappingsCableNameToFbsElement.clear();
        mappingsEssNameToFbsElement.clear();
        mappingsIdToFbsElement.clear();
        mappingsTagToFbsElement.clear();
        mappingsCableNameToFbsElement = null;
        mappingsEssNameToFbsElement = null;
        mappingsIdToFbsElement = null;
        mappingsTagToFbsElement = null;

        // possibly other commit
        validEndpointNames = null;
        validUsernames = null;

        totalTime += getPassedTime();
        if (test) {
            LOGGER.info("# cables (data):         " + dataRows);
            LOGGER.info("# cables (create test):  " + createRowsTest);
            LOGGER.info("# cables (update test):  " + updateRowsTest);
            LOGGER.info("# cables (delete test):  " + deleteRowsTest);
            LOGGER.info("# cables (skipped test): " + result.getImportFileStatistic().getSkippedRowsTest());
        } else {
            LOGGER.info("# cables (data):    " + dataRows);
            LOGGER.info("# cables (create):  " + createRows);
            LOGGER.info("# cables (update):  " + updateRows);
            LOGGER.info("# cables (delete):  " + deleteRows);
            LOGGER.info("# cables (skipped): " + result.getImportFileStatistic().getSkippedRows());
        }
        LOGGER.info("Import time: " + totalTime + " ms");

        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {
        try {
            switch (record.getCommand()) {
            case CREATE:
                createCable(record);
                break;
            case UPDATE:
                updateCable(record);
                break;
            case DELETE:
                deleteCable(record);
                break;
            default:
                // do nothing
                break;
            }
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, e.getMessage(), e);
            report.addMessage(new ValidationMessage(e.getMessage(), true));
            stopLoad = true;
        }
    }

    @Override
    public void updateRecordList(Iterable<DSRecord> records) {
        List<String> cableNames = new ArrayList<>();
        try {
            for (DSRecord record : records) {

                if (DSCommand.UPDATE.equals(record.getCommand()) || DSCommand.DELETE.equals(record.getCommand())) {
                    String name = Utility.formatWhitespace(getMandatoryField(record, CableColumn.NAME));
                    cableNames.add(name);
                }

            }
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, e.getMessage(), e);
            report.addMessage(new ValidationMessage(e.getMessage(), true));
            stopLoad = true;
            return;
        }

        List<Cable> oldCableList = cableService.getCablesByName(cableNames, true, true);
        originalCables.clear();
        for (Cable cable : oldCableList) {
            originalCables.put(cable.getName(), cable);
        }
        List<Cable> cableList = cableService.getCablesByName(cableNames, false, true);
        cables.clear();
        for (Cable cable : cableList) {
            cables.put(cable.getName(), cable);
        }

        cablesToCreate = new ArrayList<>();
        cablesToUpdate = new ArrayList<>();
        oldCablesToUpdate = new ArrayList<>();
        cablesToDelete = new ArrayList<>();

        totalTime +=

                getPassedTime();

        super.updateRecordList(records);

        if (!test) {
            createRows = cablesToCreate.size();
            List<Cable> newCables = cableService.createCables(cablesToCreate, loggedInName, false);
            updateRows = cableService.updateCables(cablesToUpdate, oldCablesToUpdate, loggedInName, false);
            deleteRows = cableService.deleteCables(cablesToDelete, loggedInName);

            for (Cable cable : newCables) {
                report.addAffected(cable);
            }
        }
    }

    /**
     * Processes the record for cable creation.
     *
     * @see CableLoader#createCheckValidityEndpoint(DSRecord, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     */
    private void createCable(DSRecord record) {
        final List<String> owners = Utility.splitStringIntoList(
                Utility.formatWhitespace(getMandatoryField(record, CableColumn.OWNERS)));
        if (permissionDeniedToModify(owners)) {
            report.addMessage(
                    getErrorMessage(
                            "You do not have permission to add cables you don't own" + " "
                                    + getUserAndOwnersInfo(owners) + ".",
                            record, CableColumn.OWNERS));
        }

        if (report.isError() && !test)
            return;

        // 2nd parameter for cable with values to compare with
        //     null since no cable to compare with
        checkCableAttributeValidityForCreate(record, null);

        if (report.isError() && !test)
            return;

        final String system = CableNumbering.getSystemNumber(Utility.formatRecordValue(getMandatoryField(record, CableColumn.SYSTEM)));
        final String subsystem = CableNumbering.getSubsystemNumber(Utility.formatRecordValue(getMandatoryField(record, CableColumn.SUBSYSTEM)));
        final String cableClass = CableNumbering.getCableClassLetter(Utility.formatRecordValue(getMandatoryField(record, CableColumn.CLASS)));

        final String fbsTagValue = Utility.formatRecordValue(getOptionalField(record, CableColumn.FBS_TAG, null));

        String cableArticleString = Utility.formatRecordValue(getOptionalField(record, CableColumn.CABLE_ARTICLE, null));
        CableArticle cableArticle = null;
        if (cableArticleString != null && !cableArticleString.isEmpty()) {
            cableArticle = cableArticles.get(cableArticleString);
        }

        final String cableTypeString = Utility.formatRecordValue(getOptionalField(record, CableColumn.CABLE_TYPE, null));
        CableType cableType = null;
        if (cableTypeString != null && !cableTypeString.isEmpty()) {
            cableType = cableTypes.get(Utility.formatWhitespace(cableTypeString));
        }

        final String container = Utility.formatRecordValue(getOptionalField(record, CableColumn.CONTAINER, null));
        String electronicDocumentation = StringUtils.trimToNull(Utility.formatRecordValue(getOptionalField(record, CableColumn.ELECTRICAL_DOCUMENTATION, null)));

        // update mappings
        //     fields
        //         from ess name           - from fbs tag           - chess id             (endpoint)
        //         from enclosure ess name - from enclosure fbs tag - enclosure chess id   (endpoint)
        //         to ess name             - to fbs tag             - chess id             (endpoint)
        //         to enclosure ess name   - to enclosure fbs tag   - enclosure chess id   (endpoint)
        // check validity of endpoint values, create endpoints

        final Endpoint endpointA = createCheckValidityEndpoint(record, EndpointType.TYPE_FROM_ENDPOINT);
        final Endpoint endpointB = createCheckValidityEndpoint(record, EndpointType.TYPE_TO_ENDPOINT);

        String installationPackageString = Utility.formatRecordValue(getOptionalField(record, CableColumn.INSTALLATION_PACKAGE, null));
        InstallationPackage installationPackage = null;
        if (installationPackageString != null && !installationPackageString.isEmpty()) {
            installationPackage = installationPackages.get(installationPackageString);
        }

        CableStatus newStatus = CableStatus.INSERTED;
        final String status = Utility.formatRecordValue(getOptionalField(record, CableColumn.STATUS, null));
        if (!StringUtils.isBlank(status)) {
            newStatus = CableStatus.convertToCableStatus(status);
            if (newStatus != CableStatus.INSERTED && !canAdminister) {
                report.addMessage(new ValidationMessage(
                        "New cable STATUS can be set to other than INSERTED only by administrator.", true,
                        record.getRowLabel(), null));
            }
        }

        final Date installationBy = Utility.toDate(Utility.formatRecordValue(getOptionalField(record, CableColumn.INSTALLATION_DATE, null)));
        final String comments = Utility.formatRecordValueTrimFrom(getOptionalField(record, CableColumn.COMMENTS, null));

        // revision is optional column/field
        //     administrator only
        //     no previous value to consider
        final String revision =
                canAdminister ?
                        Utility.formatRecordValue(getOptionalField(record, CableColumn.REVISION, null))
                        : null;

        // possibly create cable with fbs tag
        //     suggested fbs tag to be available in mappings
        boolean createWitFbsTag = false;
        if (!StringUtils.isEmpty(fbsTagValue)) {
            // get fbs element (chess id) for fbs tag
            FbsElement fbsElement = mappingsTagToFbsElement.get(fbsTagValue);

            if (fbsElement == null) {
                report.addMessage(
                        getErrorMessage(FBS_TAG + IS_NOT_FOUND_IN + CHESS_MAPPING,
                                record, CableColumn.FBS_TAG, fbsTagValue));
            }

            if (report.isError() && !test)
                return;

            createWitFbsTag = true;
        }

        if (test) {
            createRowsTest++;
        } else {
            Cable cable = new Cable(
                    system, subsystem, cableClass, null, owners, newStatus, cableArticle, cableType, container,
                    endpointA, endpointB, new Date(), new Date(), installationPackage, installationBy, comments,
                    revision, electronicDocumentation);

            // possibly create cable with fbs tag
            if (createWitFbsTag) {
                cable.setFbsTag(fbsTagValue);
            }

            cablesToCreate.add(cable);
        }

        report.addMessage(new ValidationMessage("Adding new cable.", false, record.getRowLabel(), null));

    }

    /**
     * @return true if the current user doesn't have permission to modify cable with a all of the given owners,
     *         else false
     */
    private boolean permissionDeniedToModify(List<String> owners) {
        return !canAdminister
                && (!canManageOwnedCables || !owners.contains(loggedInName));
    }

    /**
     * Processes the record for cable update.
     *
     * @see CableLoader#createCheckValidityEndpoint(DSRecord, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     */
    private void updateCable(DSRecord record) {
        if (isCableNameInvalid(record)) {
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final String cableName = Utility.formatWhitespace(getMandatoryField(record, CableColumn.NAME));
        final Cable cable = cables.get(cableName);
        final Cable oldCable = originalCables.get(cableName);

        // 2nd parameter for cable with values to compare with
        checkCableAttributeValidityForUpdate(record, cable);

        if (cable.getStatus() == CableStatus.DELETED) {
            report.addMessage(
                    new ValidationMessage(getCableNameInfo(cableName) + " cannot be updated as it is already deleted.",
                            true, record.getRowLabel(), null, cableName));
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final List<String> owners = cable.getOwners();
        if (permissionDeniedToModify(owners)) {
            report.addMessage(
                    getErrorMessage("You do not have permission to update cables you don't own" + " "
                            + getUserAndOwnersInfo(owners) + ".", record, CableColumn.OWNERS));
        }

        //restrict owners to modify cables when the cable is locked from editing
        if((!canAdminister) && (cableRequestManager.isCableLocked(cable))) {
            report.addMessage(getErrorMessage("The editing for this cable is locked! " +
                    "Please, contact an admin if you want to modify it!", record, CableColumn.OWNERS));
        }

        if (report.isError() && !test)
            return;

        final String system = CableNumbering.getSystemNumber(Utility.formatRecordValue(getOptionalField(record, CableColumn.SYSTEM, cable.getSystem())));
        final String subsystem = CableNumbering.getSubsystemNumber(Utility.formatRecordValue(getOptionalField(record, CableColumn.SUBSYSTEM, cable.getSubsystem())));
        final String cableClass = CableNumbering.getCableClassLetter(Utility.formatRecordValue(getOptionalField(record, CableColumn.CLASS, cable.getCableClass())));

        final String fbsTagValue = Utility.formatRecordValue(getOptionalField(record, CableColumn.FBS_TAG, null));

        boolean cableNameChange = false;
        if (!cable.getSystem().equals(system)
                || !cable.getSubsystem().equals(subsystem)
                || !cable.getCableClass().equals(cableClass)) {
            cableNameChange = true;
        }
        if (cableNameChange && !canAdminister) {
            report.addMessage(
                    getErrorMessage(
                            "You do not have permission to change cable name with columns "
                            + CableColumn.SYSTEM + ", " + CableColumn.SUBSYSTEM + ", " + CableColumn.CLASS + ".",
                            record, CableColumn.NAME));
        }

        CableStatus status = checkStatusForUpdate(record, cable);

        if (report.isError() && !test)
            return;

        cable.setSystem(system);
        cable.setSubsystem(subsystem);
        cable.setCableClass(cableClass);
        cable.setStatus(status);
        cable.setInstallationBy(Utility.toDateNull(Utility.formatRecordValue(getOptionalField(record, CableColumn.INSTALLATION_DATE, Objects.toString(cable.getInstallationBy(), null)))));

        setNonDependentFields(record, cable);

        // revision is optional column/field
        //     administrator only
        //     previous value to consider
        if (canAdminister) {
            cable.setRevision(Utility.formatRecordValue(getOptionalField(record, CableColumn.REVISION, cable.getRevision())));
        }

        // update mapping for cable name
        //     fields
        //         cable name              - fbs tag                - chess id             (cable)
        //     --------------------------------------------------------------------------------
        //     not necessary to keep track of chess id for cable name but do it to have it uniform

        FbsElement fbsElement = mappingsCableNameToFbsElement.get(cable.getName());
        String chessId = fbsElement != null ? fbsElement.id  : null;
        String fbsTag  = fbsElement != null ? fbsElement.tag : null;

        if (!StringUtils.isEmpty(fbsTag)) {
            if (!StringUtils.isEmpty(fbsTagValue)) {
                if (!StringUtils.equals(fbsTag, fbsTagValue)) {
                    // if fbsTag available but not matching fbsTagValue
                    //     excel not matching chess
                    report.addMessage(
                            getErrorMessage(FBS_TAG + " is not matching " + ESS_NAME + "/" + FBS_TAG
                                    + " in " + CHESS_MAPPING, record, CableColumn.FBS_TAG, fbsTagValue));
                }
            }
            if (!StringUtils.equals(cable.getFbsTag(), fbsTag)) {
                cable.setChessId(chessId);
                cable.setFbsTag(fbsTag);
            }
        } else if (!StringUtils.isEmpty(fbsTagValue)) {
            FbsElement fbsElementTagValue = mappingsTagToFbsElement.get(fbsTagValue);
            if (fbsElementTagValue == null) {
                // if fbsTagValue available but not matching chess
                //     excel not matching chess
                report.addMessage(
                        getErrorMessage(FBS_TAG + IS_NOT_FOUND_IN + CHESS_MAPPING,
                                record, CableColumn.FBS_TAG, fbsTagValue));
            }
            if (!StringUtils.equals(cable.getFbsTag(), fbsTagValue)) {
                cable.setChessId(null);
                cable.setFbsTag(fbsTagValue);
            }
        }

        if (report.isError() && !test)
            return;

        // update mappings
        //     fields
        //         from ess name           - from fbs tag           - chess id             (endpoint)
        //         from enclosure ess name - from enclosure fbs tag - enclosure chess id   (endpoint)
        //         to ess name             - to fbs tag             - chess id             (endpoint)
        //         to enclosure ess name   - to enclosure fbs tag   - enclosure chess id   (endpoint)
        // check validity of endpoint values, update endpoints
        updateCheckValidityEndpoint(record, cable.getEndpointA(), EndpointType.TYPE_FROM_ENDPOINT);
        updateCheckValidityEndpoint(record, cable.getEndpointB(), EndpointType.TYPE_TO_ENDPOINT);

        if (test) {
            updateRowsTest++;
        } else {
            cablesToUpdate.add(cable);
            oldCablesToUpdate.add(oldCable);
            report.addAffected(cable);
        }
        if (!cableNameChange) {
            report.addMessage(
                    new ValidationMessage("Updating cable '" + cableName + "'.", false, record.getRowLabel(), null));
        } else {
            report.addMessage(new ValidationMessage(
                    "Warning: updating cable '" + cableName + "' will change its current cable name.", false,
                    record.getRowLabel(), null));
        }
    }

    /**
     * Sets the non-dependent fields for a cable if update was called.
     * These fields are not generating errors if some are missing.
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param cable the cable that has to be updated
     */
    private void setNonDependentFields(DSRecord record, Cable cable) {
        final Integer seqNumber = cable.getSeqNumber();

        final String container = Utility.formatRecordValue(getOptionalField(record, CableColumn.CONTAINER, cable.getContainer()));

        String electronicDocumentation = StringUtils.trimToNull(Utility.formatRecordValue(getOptionalField(record, CableColumn.ELECTRICAL_DOCUMENTATION, cable.getElectricalDocumentation())));

        String cableArticleString = Utility.formatRecordValue(getOptionalField(record, CableColumn.CABLE_ARTICLE, null));
        CableArticle cableArticle = null;
        if (cableArticleString != null && !cableArticleString.isEmpty()) {
            cableArticle = cableArticles.get(cableArticleString);
        }

        final String cableTypeString = Utility.formatRecordValue(getOptionalField(record, CableColumn.CABLE_TYPE, Objects.toString(cable.getCableType(), null)));
        CableType cableType = null;
        if (cableTypeString != null && !cableTypeString.isEmpty()) {
            cableType = cableTypes.get(cableTypeString);
        }

        String installationPackageString = Utility.formatRecordValue(getOptionalField(record, CableColumn.INSTALLATION_PACKAGE, Objects.toString(cable.getInstallationPackage(), null)));
        InstallationPackage installationPackage = null;
        if (installationPackageString != null && !installationPackageString.isEmpty()) {
            installationPackage = installationPackages.get(installationPackageString);
        }

        final List<String> newOwners = Utility.splitStringIntoList(Utility.formatRecordValue(getOptionalField(record, CableColumn.OWNERS, cable.getOwnersString())));

        final String comments = Utility.formatRecordValueTrimFrom(getOptionalField(record, CableColumn.COMMENTS, cable.getComments()));

        cable.setSeqNumber(seqNumber);
        cable.setContainer(container);
        cable.setElectricalDocumentation(electronicDocumentation);
        cable.setCableArticle(cableArticle);
        cable.setCableType(cableType);
        cable.setInstallationPackage(installationPackage);
        cable.setOwners(newOwners);
        cable.setComments(comments);
    }

    /**
     * If new-, and old status differs, then it checks the Cable status if it is allowed to be change the cable.
     * It returns the new status for it if the update is allowed
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param cable the cable that has to be updated
     * @return the status of the cable
     */
    private CableStatus checkStatusForUpdate(DSRecord record, Cable cable) {
        CableStatus status = CableStatus.convertToCableStatus(getOptionalField(record, CableColumn.STATUS, Objects.toString(cable.getStatus(), null)));

        if (status == null) {
            status = cable.getStatus();
            report.addMessage(new ValidationMessage("Cable status not recognized. Will remain unchanged",
                    false, record.getRowLabel(), CableColumn.STATUS.getColumnLabel()));
        }

        if (status != cable.getStatus()) {
            if (!canAdminister) {
                report.addMessage(
                        getErrorMessage("Only admin can change cable status.",
                                record, CableColumn.STATUS, cable.getStatus().getDisplayName()));
            } else {
                try {
                    StatusTransitionUtil.transitionAllowed(cable.getStatus(), status);
                } catch (StatusTransitionException e) {
                    report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableColumn.STATUS, status.getDisplayName()));
                }
            }
        }
        return status;
    }

    /** Processes the record for cable deletion. */
    private void deleteCable(DSRecord record) {
        if (isCableNameInvalid(record)) {
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        final String cableName = Utility.formatRecordValue(getMandatoryField(record, CableColumn.NAME));
        final Cable cable = originalCables.get(cableName);

        final List<String> owners = new ArrayList<>(cable.getOwners());
        if (permissionDeniedToModify(owners)) {
            report.addMessage(
                    getErrorMessage("You do not have permission to delete cables you don't own" + " "
                            + getUserAndOwnersInfo(owners) + ".", record, CableColumn.OWNERS));
        }

        if (report.isError() && !test)
            return;

        if (cable.getStatus() == CableStatus.DELETED) {
            report.addMessage(
                    new ValidationMessage(getCableNameInfo(cableName) + " is already deleted.",
                            true, record.getRowLabel(), cableName));
        }

        if (report.isError() && !test)
            return;

        if (test) {
            deleteRowsTest++;
        } else {
            cablesToDelete.add(cable);
            report.addAffected(cable);
        }
        report.addMessage(new ValidationMessage("Deleting cable with number " + cableName + ".", false,
                record.getRowLabel(), null));
    }

    /**
     * Check that the cable attributes are valid.
     *
     * @param record
     * @param cable
     *
     * @see CableLoader#createCheckValidityEndpoint(DSRecord, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     */
    private void checkCableAttributeValidityForCreate(DSRecord record, Cable cable) {
        // mandatory field
        //     column mandatory
        //     value mandatory
        //     value valid
        // ----------
        // getMandatoryField - column mandatory (not value)

        final String systemLabel = Utility.formatRecordValue(getMandatoryField(record, CableColumn.SYSTEM));
        final String subsystemLabel = Utility.formatRecordValue(getMandatoryField(record, CableColumn.SUBSYSTEM));
        final String cableClassLabel = Utility.formatRecordValue(getMandatoryField(record, CableColumn.CLASS));
        final List<String> owners = Utility.splitStringIntoList(Utility.formatRecordValue(getMandatoryField(record, CableColumn.OWNERS)));
        final String status = Utility.formatRecordValue(getMandatoryField(record, CableColumn.STATUS));

        checkCableAttributeValidity(record, true, ValidationChoice.CREATE, cable,
                systemLabel, subsystemLabel, cableClassLabel, owners, status);
    }

    /**
     * Check that the cable attributes are valid.
     *
     * @param record
     * @param cable
     *
     * @see CableLoader#createCheckValidityEndpoint(DSRecord, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     */
    private void checkCableAttributeValidityForUpdate(DSRecord record, Cable cable) {
        // optional field
        //     value valid (if available)
        // ----------
        // getOptionalField - column optional

        final String systemLabel = Utility.formatRecordValue(getOptionalField(record, CableColumn.SYSTEM, null));
        final String subsystemLabel = Utility.formatRecordValue(getOptionalField(record, CableColumn.SUBSYSTEM, null));
        final String cableClassLabel = Utility.formatRecordValue(getOptionalField(record, CableColumn.CLASS, null));
        final List<String> owners = Utility.splitStringIntoList(Utility.formatRecordValue(getOptionalField(record, CableColumn.OWNERS, null)));
        final String status = Utility.formatRecordValue(getOptionalField(record, CableColumn.STATUS, null));

        checkCableAttributeValidity(record, false, ValidationChoice.UPDATE, cable,
                systemLabel, subsystemLabel, cableClassLabel, owners, status);
    }

    /**
     * Check that the cable attributes are valid.
     * Note endpoint attributes checked elsewhere.
     *
     * @param record
     * @param mandatory
     * @param validationChoice
     * @param systemLabel
     * @param subsystemLabel
     * @param cableClassLabel
     * @param owners
     * @param status
     *
     * @see CableLoader#createCheckValidityEndpoint(DSRecord, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType)
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     */
    private void checkCableAttributeValidity(DSRecord record, boolean mandatory, ValidationChoice validationChoice, Cable cable,
            String systemLabel, String subsystemLabel, String cableClassLabel,
            List<String> owners, String status) {

        // cable article and cable type are to be checked together
        //     create
        //         admin - article or type or both, validate value(s)
        //         user  - article and not type,    validate value(s)
        //     update
        //         admin - neither or article or type or both, validate value(s)
        //         user  - neither or article,                 validate value(s), not change type and clear type if change article

        final String cableArticle = Utility.formatRecordValue(getOptionalField(record, CableColumn.CABLE_ARTICLE, null));
        final String cableType    = Utility.formatRecordValue(getOptionalField(record, CableColumn.CABLE_TYPE, null));

        boolean articleAvailable  = cableArticle != null;
        boolean articleEmpty      = StringUtils.isEmpty(cableArticle);
        boolean articleDifferent  = 	(cable == null && !articleEmpty)
                                    || 	(cable != null && (
                                                cable.getCableArticle() == null && !articleEmpty
                                            || 	cable.getCableArticle() != null &&  articleEmpty));

        boolean typeAvailable     = cableType != null;
        boolean typeEmpty         = StringUtils.isEmpty(cableType);
        boolean typeDifferent     = 	(cable == null && !typeEmpty)
                                    || 	(cable != null && (
                                                cable.getCableType() == null && !typeEmpty
                                            || 	cable.getCableType() != null &&  typeEmpty));

        boolean messageShownCableArticleNotSet = false;

        // validateCableArticle
        // validateCableType
        if (ValidationChoice.UPDATE.equals(validationChoice) && (articleAvailable || typeAvailable)) {
            if (articleEmpty && typeEmpty) {
                report.addMessage(getErrorMessage(
                        "Cable ARTICLE is not set.", record, CableColumn.CABLE_ARTICLE, cableArticle));
                messageShownCableArticleNotSet = true;
            }

            // update
            //     columns different or not
            if (sessionService.canAdminister()) {
                if (articleDifferent) {
                    validateCableArticle(record, cableArticle);
                }
                if (typeDifferent) {
                    validateCableType(record, cableType);
                }
            } else {
                if (articleDifferent) {
                    validateCableArticle(record, cableArticle);
                    cable.setCableType(null);
                }
                if (typeDifferent) {
                    report.addMessage(getErrorMessage(
                            "Cable TYPE is not valid to change.", record, CableColumn.CABLE_TYPE, cableType));
                }
            }
        } else {
            // create
            //     columns available or not
            //     columns empty or not
            if (!articleAvailable && !typeAvailable) {
                throw new IllegalArgumentException(
                        "Could not find mandatory column '" + CableColumn.CABLE_ARTICLE + "'.");
            }
            if (articleEmpty && typeEmpty) {
                report.addMessage(getErrorMessage(
                        "Cable ARTICLE is not set.", record, CableColumn.CABLE_ARTICLE, cableArticle));
                messageShownCableArticleNotSet = true;
            }

            // either column is available
            if (sessionService.canAdminister()) {
                if (!articleEmpty) {
                    validateCableArticle(record, cableArticle);
                }
                if (!typeEmpty) {
                    validateCableType(record, cableType);
                }
            } else {
                if (!articleAvailable) {
                    throw new IllegalArgumentException(
                            "Could not find mandatory column '" + CableColumn.CABLE_ARTICLE + "'.");
                }
                if (articleEmpty && !messageShownCableArticleNotSet) {
                    report.addMessage(getErrorMessage(
                            "Cable ARTICLE is not set.", record, CableColumn.CABLE_ARTICLE, cableArticle));
                } else {
                    validateCableArticle(record, cableArticle);
                }
                if (!typeEmpty) {
                    report.addMessage(getErrorMessage(
                            "Cable TYPE is not valid to set.", record, CableColumn.CABLE_TYPE, cableType));
                }
            }
        }

        final Object installationBy = Utility.formatRecordValue(getOptionalField(record, CableColumn.INSTALLATION_DATE, null));
        final String installationPackage = Utility.formatRecordValue(getOptionalField(record, CableColumn.INSTALLATION_PACKAGE, null));
        final String comments = Utility.formatRecordValueTrimFrom(getOptionalField(record, CableColumn.COMMENTS, null));

        // validation
        validateSystemLabel(record, mandatory, systemLabel);
        validateSubsystemLabel(record, mandatory, systemLabel, subsystemLabel);
        validateClassLabel(record, mandatory, cableClassLabel);
        validateOwners(record, mandatory, owners);
        validateStatus(record, mandatory, status);
        validateInstallationPackage(record, installationPackage);

        try {
            Utility.toDate(installationBy);
        } catch (ClassCastException e) {
            LOGGER.log(Level.FINEST, "Invalid INSTALLATION_BY", e);
            report.addMessage(
                    getErrorMessage("INSTALLATION BY field is not in the valid format.",
                            record,
                            CableColumn.INSTALLATION_DATE,
                            getOptionalField(record, CableColumn.INSTALLATION_DATE, null)));
        }

        try {
            validateStringSize(comments, CableColumn.COMMENTS);
        } catch (DbFieldLengthViolationException e) {
            LOGGER.log(Level.FINEST, INVALID + CableColumn.COMMENTS, e);
            report.addMessage(
                    getErrorMessage(e.getMessage(), record, CableColumn.COMMENTS, comments));
        }

        // revision is optional column/field
        //     administrator only
        validateRevision(record);

        // Check system - subsystem consistency

    }

    /**
     * Validates if user has right to change Cable Revision, and Revision string has correct format
     *
     * @param record the DSRecord of the cable (XLS row)
     */
    private void validateRevision(DSRecord record) {
        if (canAdminister) {
            final String revision = Utility.formatRecordValue(getOptionalField(record, CableColumn.REVISION, null));
            try {
                validateStringSize(revision, CableColumn.REVISION);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.REVISION, e);
                report.addMessage(
                        getErrorMessage(e.getMessage(), record, CableColumn.REVISION, revision));
            }
        }
    }

    /**
     * Validates if given installation package is valid
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param installationPackage the chosen new installation package
     */
    private void validateInstallationPackage(DSRecord record, String installationPackage) {
        if (!StringUtils.isEmpty(installationPackage)
                && installationPackages.get(installationPackage) == null) {
            report.addMessage(
                    getErrorMessage(
                            "INSTALLATION PACKAGE is not registered in the database.",
                            record,
                            CableColumn.INSTALLATION_PACKAGE,
                            installationPackage));
        }
    }

    /**
     * Validates cable status that it is not empty, and is in valid format
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param mandatory if value is mandatory
     * @param status the chosen new cable status
     */
    private void validateStatus(DSRecord record, boolean mandatory, String status) {
        if (mandatory || status != null) {
            if (!StringUtils.isBlank(status)) {
                if (CableStatus.convertToCableStatus(status) == null) {
                    report.addMessage(getErrorMessage("Cable STATUS is invalid.", record, CableColumn.STATUS, status));
                }
            } else {
                report.addMessage(getErrorMessage("Cable STATUS is not set.", record, CableColumn.STATUS));
            }
        }
    }

    /**
     * Validates cable owners
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param mandatory if value is mandatory
     * @param owners the owners list
     */
    private void validateOwners(DSRecord record, boolean mandatory, List<String> owners) {
        if (mandatory || owners != null) {
            try {
                if (Utility.isNullOrEmpty(owners)) {
                    report.addMessage(getErrorMessage("Cable OWNERS are not set.", record, CableColumn.OWNERS));
                } else {
                    if (!validOwners(owners)) {
                        report.addMessage(
                                getErrorMessage("Cable OWNERS are not present in the user directory.",
                                        record, CableColumn.OWNERS, String.join(" , ", owners)));
                    }
                }
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.OWNERS, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.OWNERS, String.join(" , ", owners)));
            }
        }
    }

    /**
     * Validates if given cable article is valid
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param cableArticle the chosen new cable article
     */
    private void validateCableArticle(DSRecord record, String cableArticle) {
        if (!StringUtils.isEmpty(cableArticle) && cableArticles.get(cableArticle) == null) {
            report.addMessage(
                    getErrorMessage(
                            "Cable ARTICLE is not registered in the database.",
                            record,
                            CableColumn.CABLE_ARTICLE,
                            cableArticle));
        }
    }

    /**
     * Validates cableType that it not empty, and is valid
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param cableType the chosen new cable type
     */
    private void validateCableType(DSRecord record, String cableType) {
        if (!StringUtils.isEmpty(cableType) && cableTypes.get(cableType) == null) {
            report.addMessage(getErrorMessage("Specified Cable TYPE does not exist.", record, CableColumn.CABLE_TYPE,
                    cableType));
        }
    }

    /**
     * Validates cable class
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param mandatory if value is mandatory
     * @param cableClassLabel the chosen new cable class
     */
    private void validateClassLabel(DSRecord record, boolean mandatory, String cableClassLabel) {
        // valid cable class
        //     cable class character or cable class label
        // --------
        // forgiving mode, allow only cable class character

        if (mandatory || cableClassLabel != null) {
            final String cableClass = CableNumbering.getCableClassLetter(cableClassLabel);
            try {
                if (!CableNumbering.isValidCableClass(cableClassLabel)) {
                    report.addMessage(
                            getErrorMessage("CABLE CLASS is not valid cable class.",
                                    record, CableColumn.CLASS, cableClassLabel));
                }
                validateStringSize(cableClass, CableColumn.CLASS);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.CLASS, e);
                report.addMessage(
                        getErrorMessage(e.getMessage(), record, CableColumn.CLASS, cableClass));
            }
        }
    }

    /**
     * Validates cable subsystem
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param mandatory if value is mandatory
     * @param systemLabel the chosen new system
     * @param subsystemLabel the chosen new subsystem (depends on chosen system)
     */
    private void validateSubsystemLabel(DSRecord record, boolean mandatory, String systemLabel, String subsystemLabel) {
        // valid combination of system and subsystem implies valid subsystem

        if (mandatory || subsystemLabel != null) {
            try {
                final String subsystem = CableNumbering.getSubsystemNumber(subsystemLabel);
                if (!CableNumbering.areValidSystemAndSubsystemLabels(systemLabel, subsystemLabel)) {
                    report.addMessage(
                            getErrorMessage("SUBSYSTEM field is not valid subsystem.",
                                    record, CableColumn.SUBSYSTEM, subsystemLabel));
                }
                validateStringSize(subsystem, CableColumn.SUBSYSTEM);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.SUBSYSTEM, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.SUBSYSTEM, subsystemLabel));
            }
        }
    }

    /**
     * Validates cable system
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param mandatory if value is mandatory
     * @param systemLabel the chosen new system
     */
    private void validateSystemLabel(DSRecord record, boolean mandatory, String systemLabel) {
        // valid system
        //     system character or system label
        // --------
        // forgiving mode, allow only system character

        if (mandatory || systemLabel != null) {
            try {
                final String system = CableNumbering.getSystemNumber(systemLabel);
                if (!CableNumbering.isValidSystem(systemLabel)) {
                    report.addMessage(
                            getErrorMessage("SYSTEM field is not valid system.",
                                    record, CableColumn.SYSTEM, systemLabel));
                }
                validateStringSize(system, CableColumn.SYSTEM);
            } catch (DbFieldLengthViolationException e) {
                LOGGER.log(Level.FINEST, INVALID + CableColumn.SYSTEM, e);
                report.addMessage(getErrorMessage(e.getMessage(), record, CableColumn.SYSTEM, systemLabel));
            }
        }
    }

    /**
     * Checks the validity of cable name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return false if the name is valid, else true
     */
    private boolean isCableNameInvalid(DSRecord record) {
        final String cableName = Utility.formatRecordValue(getOptionalField(record, CableColumn.NAME, null));
        if (!CableNumbering.isValidCableName(cableName)) {
            report.addMessage(
                    getErrorMessage("Cable NAME is not in the valid format.", record, CableColumn.NAME, cableName));
            return true;
        }
        final Cable cable = originalCables.get(cableName);
        if (cable == null) {
            report.addMessage(
                    getErrorMessage(getCableNameInfo(cableName) + " does not exist in the database.",
                            record, CableColumn.NAME, cableName));
            return true;
        }
        return false;
    }

    private String getUserAndOwnersInfo(List<String> owners) {
        return "(logged in user: " + loggedInName + ", owners: " + String.join(" , ", owners) + ")";
    }

    private String getCableNameInfo(String cableName) {
        return "Cable with name " + cableName;
    }

    private void validateStringSize(final String value, final CableColumn column) {
        super.validateStringSize(value, column, Cable.class);
    }

    private void validateEndpointStringSize(final String value, final CableColumn column) {
        super.validateStringSize(value, column, Endpoint.class);
    }

    /**
     * Check validity of owners
     *
     * @param owners
     *            usernames of the owners to check
     * @return true if all usernames of the owners are correct else false
     */
    private boolean validOwners(List<String> owners) {
        for (String user : owners) {
            if (!validUsernames.contains(user)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieves data from the mandatory field, or throws an exception if column could not be found.
     *
     * @param record
     *            the record containing data in interest
     * @param column
     *            column the column of the field we want to retrieve record to check.
     * @return value of the filed
     * @throws IllegalArgumentException
     *             if column could not be found
     */
    private String getMandatoryField(DSRecord record, CableColumn column) {
        try {
            return record.getField(column).toString();
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, "Column not found", e);
            throw new IllegalArgumentException("Could not find mandatory column '" + column + "'.", e);
        }
    }

    /**
     * Retrieves data from the optional field, or returns default value if column could not be found.
     *
     * @param record
     *            the record containing data in interest
     * @param column
     *            column the column of the field we want to retrieve record to check.
     * @param defaultValue
     *            the default value to return if column is not present
     * @return value of the filed or default value if column could not be found
     */
    private String getOptionalField(DSRecord record, CableColumn column, String defaultValue) {
        try {
            return record.getField(column).toString();
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.FINEST, "Column not found, will use default value.", e);
            return defaultValue;
        }
    }

    /**
     * Create endpoint, get and set values, update FBS tag mappings, check validity.
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param endpointType which endpoint has to be created
     * @return a newly created Endpoint
     *
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     * @see CableService#updateFbsTagMappings(String, Iterable, Map, Map, Map)
     */
    private Endpoint createCheckValidityEndpoint(DSRecord record, EndpointType endpointType) {
        // cablecolumns
        CableColumn cableColumnDevice = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_ESS_NAME
                : CableColumn.TO_ESS_NAME;
        CableColumn cableColumnDeviceFbsTag = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_FBS_TAG
                : CableColumn.TO_FBS_TAG;
        CableColumn cableColumnDeviceRack = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_ENCLOSURE_ESS_NAME
                : CableColumn.TO_ENCLOSURE_ESS_NAME;
        CableColumn cableColumnDeviceRackFbsTag = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_ENCLOSURE_FBS_TAG
                : CableColumn.TO_ENCLOSURE_FBS_TAG;
        CableColumn cableColumnBuilding = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_LBS_TAG
                : CableColumn.TO_LBS_TAG;
        CableColumn cableColumnLabel = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_USER_LABEL
                : CableColumn.TO_USER_LABEL;
        CableColumn cableColumnConnector = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_CONNECTOR
                : CableColumn.TO_CONNECTOR;

        // values
        final String deviceName = Utility.formatRecordValue(getOptionalField(record, cableColumnDevice, null));
        final String deviceFbsTag = Utility.formatRecordValue(getOptionalField(record, cableColumnDeviceFbsTag, null));
        final String deviceRack = Utility.formatRecordValue(getOptionalField(record, cableColumnDeviceRack, null));
        final String deviceRackFbsTag = Utility.formatRecordValue(getOptionalField(record, cableColumnDeviceRackFbsTag, null));
        final String deviceBuilding = Utility.formatRecordValue(getOptionalField(record, cableColumnBuilding, null));
        final String deviceUserLabel = Utility.formatRecordValue(getOptionalField(record, cableColumnLabel, null));
        final String connectorName = Utility.formatRecordValue(getOptionalField(record, cableColumnConnector, null));

        Connector connector = null;
        if (connectorName != null && !connectorName.isEmpty()) {
            connector = connectors.get(connectorName);
        }

        // create endpoint
        Endpoint endpoint = new Endpoint(deviceName, deviceBuilding, deviceRack, connector, deviceUserLabel,
                deviceFbsTag, deviceRackFbsTag, false);

        // update endpoint incl fbs tag mappings - check validity
        updateCheckValidityEndpoint(record, endpoint, endpointType, true);

        // endpoint uuid to be empty if endpoint device fbs tag is non-empty
        //     handled elsewhere

        // validity checked, no need to further check endpoint attributes
        return endpoint;
    }

    /**
     * Update endpoint, get and set values, update FBS tag mappings, check validity.
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param endpoint which endpoint has to be updated
     * @param endpointType what is the endpoint type
     * @param create data/fields comes from XLS record, or from cable. If true, it comes from the cable.
     */
    private void updateCheckValidityEndpoint(DSRecord record, Endpoint endpoint,
            EndpointType endpointType, boolean create) {
        // ----------------------------------------------------------------------------------------------------
        // update endpoint incl fbs tag mappings - check validity
        //     fields
        //         from ess name           - from fbs tag           - chess id             (endpoint)
        //         from enclosure ess name - from enclosure fbs tag - enclosure chess id   (endpoint)
        //         to ess name             - to fbs tag             - chess id             (endpoint)
        //         to enclosure ess name   - to enclosure fbs tag   - enclosure chess id   (endpoint)
        //     ------------------------------------------------------------------------------------------------
        //     essName/tag - tag/essName
        //     chess id has predence over fbs tag has precedence over ess name
        //     check validity for fields directly after get fbs tag mappings
        //
        //     alternative implementations for retrieval of tag mapping(s)
        //         performance implications
        //         retrieve one tag mapping
        //         retrieve all tag mappings
        //
        //     endpoint
        //         device, device fbs tag, device chess id
        //             and/or
        //             uuid empty if fbs tag non-empty
        //         building
        //         rack, rack fbs tag, rack chess id
        //             and/or/none
        //         label
        //         connector
        //
        // ----------------------------------------------------------------------------------------------------
        // history/diff string handled in cableservice
        // ----------------------------------------------------------------------------------------------------
        // if create
        //     values from endpoint (createCheckValidityEndpoint)
        // else
        //     values from record or endpoint
        //         ess name, fbs tag
        //             either or both from import (record)
        //         enclosure ess name, enclosure fbs tag
        //             if either in import then both from import, else from existing (endpoint)
        //         lbs tag, connector, label
        //             if not from import (record) then from existing (endpoint)
        // ----------------------------------------------------------------------------------------------------

        // cablecolumns
        CableColumn cableColumnDevice = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_ESS_NAME
                : CableColumn.TO_ESS_NAME;
        CableColumn cableColumnDeviceFbsTag = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_FBS_TAG
                : CableColumn.TO_FBS_TAG;
        CableColumn cableColumnDeviceRack = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_ENCLOSURE_ESS_NAME
                : CableColumn.TO_ENCLOSURE_ESS_NAME;
        CableColumn cableColumnDeviceRackFbsTag = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_ENCLOSURE_FBS_TAG
                : CableColumn.TO_ENCLOSURE_FBS_TAG;
        CableColumn cableColumnBuilding = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_LBS_TAG
                : CableColumn.TO_LBS_TAG;
        CableColumn cableColumnLabel = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_USER_LABEL
                : CableColumn.TO_USER_LABEL;
        CableColumn cableColumnConnector = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT)
                ? CableColumn.FROM_CONNECTOR
                : CableColumn.TO_CONNECTOR;

        // values
        //     if create - value from endpoint
        final String deviceName = create
                ? endpoint.getDevice()
                : Utility.formatRecordValue(getOptionalField(record, cableColumnDevice, endpoint.getDevice()));
        final String deviceFbsTag = create
                ? endpoint.getDeviceFbsTag()
                : Utility.formatRecordValue(getOptionalField(record, cableColumnDeviceFbsTag, endpoint.getDeviceFbsTag()));
        final String deviceRack = create
                ? endpoint.getRack()
                : Utility.formatRecordValue(getOptionalField(record, cableColumnDeviceRack, endpoint.getRack()));
        final String deviceRackFbsTag = create
                ? endpoint.getRackFbsTag()
                : Utility.formatRecordValue(getOptionalField(record, cableColumnDeviceRackFbsTag, endpoint.getRackFbsTag()));
        final String deviceBuilding = create
                ? endpoint.getBuilding()
                : Utility.formatRecordValue(getOptionalField(record, cableColumnBuilding, endpoint.getBuilding()));
        final String deviceUserLabel = create
                ? endpoint.getLabel()
                : Utility.formatRecordValue(getOptionalField(record, cableColumnLabel, endpoint.getLabel()));
        final String connectorName = create
                ? (endpoint.getConnector() == null ? null : endpoint.getConnector().getName())
                : Utility.formatRecordValue(getOptionalField(record, cableColumnConnector, Objects.toString(endpoint.getConnector(), null)));

        Connector connector = null;
        if (connectorName != null && !connectorName.isEmpty()) {
            connector = connectors.get(connectorName);
        }

        // validation
        //     validate device rack if create or if different from previous
        boolean validateDeviceRack = false;
        String deviceRackExisting = endpoint.getRack();
        String deviceRackFbsTagExisting = endpoint.getRackFbsTag();

        // update endpoint if necessary
        if (!StringUtils.equals(deviceName, endpoint.getDevice())) {
            endpoint.setDevice(deviceName);
        }
        if (!StringUtils.equals(deviceFbsTag, endpoint.getDeviceFbsTag())) {
            endpoint.setDeviceFbsTag(deviceFbsTag);
        }
        if (!StringUtils.equals(deviceRack, endpoint.getRack())) {
            validateDeviceRack = true;
            endpoint.setRack(deviceRack);
        }
        if (!StringUtils.equals(deviceRackFbsTag, endpoint.getRackFbsTag())) {
            endpoint.setRackFbsTag(deviceRackFbsTag);
        }
        if (!StringUtils.equals(deviceBuilding, endpoint.getBuilding())) {
            endpoint.setBuilding(deviceBuilding);
        }
        if (!StringUtils.equals(deviceUserLabel, endpoint.getLabel())) {
            endpoint.setLabel(deviceUserLabel);
        }
        if (connector == null || !connector.equals(endpoint.getConnector())) {
            endpoint.setConnector(connector);
        }

        // ----------------------------------------------------------------------------------------------------

        String endpointFromTo = endpointType.equals(EndpointType.TYPE_FROM_ENDPOINT) ? FROM_ENDPOINT : TO_ENDPOINT;
        String chessId = null;
        String essName = null;
        String fbsTag = null;

        // ------------------------------------------------------------
        // device, device fbs tag
        if (!StringUtils.isEmpty(deviceFbsTag)) {
            // get chess id, ess name for fbs tag
            FbsElement fbsElement = mappingsTagToFbsElement.get(deviceFbsTag);
            chessId = fbsElement != null ? fbsElement.id      : null;
            essName = fbsElement != null ? fbsElement.essName : null;

            if (fbsElement == null) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + FBS_TAG + IS_NOT_FOUND_IN + CHESS_MAPPING,
                                record, cableColumnDeviceFbsTag, deviceFbsTag));
            }

            // check validity - if non-empty deviceName, then to be same as essName
            if (!StringUtils.isEmpty(deviceName)
                    && !StringUtils.equals(deviceName, essName)) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + ESS_NAME + " is not same as in " + CHESS_MAPPING,
                                record, cableColumnDevice, deviceName));
            }

            // update endpoint device chess id, device name
            endpoint.setDeviceChessId(chessId);
            endpoint.setDevice(essName);
        } else if (!StringUtils.isEmpty(deviceName)) {
            // get chess id, fbs tag for ess name
            FbsElement fbsElement = mappingsEssNameToFbsElement.get(deviceName);
            chessId = fbsElement != null ? fbsElement.id  : null;
            fbsTag  = fbsElement != null ? fbsElement.tag : null;

            // check validity - no need

            // update endpoint device chess id, device fbs tag
            endpoint.setDeviceChessId(chessId);
            endpoint.setDeviceFbsTag(fbsTag);
        } else {
            report.addMessage(
                    getErrorMessage("Neither " + endpointFromTo + " " + ESS_NAME
                            + " nor " + endpointFromTo + " " + FBS_TAG + " is specified. "
                            + CHESS_AT_LEAST_ONE_OF_TWO_VALUES,
                            record, cableColumnDevice, deviceName));
        }
        // validation
        if (!StringUtils.isEmpty(deviceName)) {
            try {
                if (!validEndpointNames.contains(deviceName)) {
                    report.addMessage(
                            getErrorMessage(endpointFromTo + " " + ESS_NAME
                                    + " is not registered in the Naming System",
                                    record, cableColumnDevice, deviceName));
                }
                validateEndpointStringSize(deviceName, cableColumnDevice);
            } catch (DbFieldLengthViolationException e) {
                // might be somewhat different message format
                LOGGER.log(Level.FINEST, INVALID + cableColumnDevice, e);
                report.addMessage(
                        getErrorMessage(e.getMessage(), record, cableColumnDevice, deviceName));
            }
        }
        // ------------------------------------------------------------
        // building
        if (!StringUtils.isEmpty(deviceBuilding)) {
            if (!deviceBuilding.startsWith("+")) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + LBS_TAG
                                + " is not a valid LBS TAG. Value should be empty or start with + character.",
                                record, cableColumnBuilding, deviceBuilding));
            }
            if (deviceBuilding.length() > Endpoint.MAX_LABEL_SIZE) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + LBS_TAG
                                + " is over maximum allowed length " + Endpoint.MAX_LABEL_SIZE,
                                record, cableColumnBuilding, deviceBuilding));
            }
        }
        // ------------------------------------------------------------
        // rack, rack fbs tag
        if (!StringUtils.isEmpty(deviceRackFbsTag)) {
          // get chess id, ess name for fbs tag
          FbsElement fbsElement = mappingsTagToFbsElement.get(deviceRackFbsTag);
          chessId = fbsElement != null ? fbsElement.id      : null;
          essName = fbsElement != null ? fbsElement.essName : null;

            if (fbsElement == null) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + ENCLOSURE_FBS_TAG
                                + IS_NOT_FOUND_IN + CHESS_MAPPING,
                                record, cableColumnDeviceRackFbsTag, deviceRackFbsTag));
            }

            // check validity - if non-empty deviceRack, then to be same as essName
            if (!StringUtils.isEmpty(deviceRack)
                    && StringUtils.isNotEmpty(essName)
                    && !StringUtils.equals(deviceRack, essName)) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + ENCLOSURE_ESS_NAME
                                + " is not same as in " + CHESS_MAPPING,
                                record, cableColumnDeviceRack, deviceRack));
            }

            // not replace rack value
            //     not assign essName to rack since existing values may be non-ESS names

            // update endpoint rack, rack chess id
            endpoint.setRack(essName);
            endpoint.setRackChessId(chessId);
        } else if (!StringUtils.isEmpty(deviceRack)) {
          // get chess id, fbs tag for ess name
          FbsElement fbsElement = mappingsEssNameToFbsElement.get(deviceRack);
          chessId = fbsElement != null ? fbsElement.id  : null;
          fbsTag  = fbsElement != null ? fbsElement.tag : null;

            // check validity - no need

            // update endpoint rack chess id, rack fbs tag
            endpoint.setRackChessId(chessId);
            endpoint.setRackFbsTag(fbsTag);
        } else {
            validateDeviceRack = false;
            endpoint.setRack(deviceRackExisting);
            endpoint.setRackFbsTag(deviceRackFbsTagExisting);
        }
        // validation
        //     empty field or value from naming, previous values to be accepted
        if (!StringUtils.isEmpty(deviceRack)) {
            if (validateDeviceRack && !validEndpointNames.contains(deviceRack)) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + ENCLOSURE_ESS_NAME
                                + " is not registered in the Naming System",
                                record, cableColumnDeviceRack, deviceRack));
            }
            if (deviceRack.length() > Endpoint.MAX_LABEL_SIZE) {
                report.addMessage(
                        getErrorMessage(endpointFromTo + " " + ENCLOSURE_ESS_NAME
                                + IS_OVER_MAXIMUM_ALLOWED_LENGTH + Endpoint.MAX_LABEL_SIZE,
                                record, cableColumnDeviceRack, deviceRack));
            }
        }
        // ----------------------------------------------------------------------------------------------------
        // label
        if (deviceUserLabel != null && deviceUserLabel.length() > Endpoint.MAX_LABEL_SIZE) {
            report.addMessage(
                    getErrorMessage(endpointFromTo + " " + LABEL
                            + IS_OVER_MAXIMUM_ALLOWED_LENGTH + Endpoint.MAX_LABEL_SIZE,
                            record, cableColumnLabel, deviceUserLabel));
        }
        // ----------------------------------------------------------------------------------------------------
        // connector
        if (!StringUtils.isBlank(connectorName)
                && !connectors.containsKey(connectorName)) {
            report.addMessage(getErrorMessage(endpointFromTo + " " + CONNECTOR + " "
                    + " is not registered present in the cable database",
                    record, cableColumnConnector, connectorName));
        }
        // ----------------------------------------------------------------------------------------------------
    }

    /**
     * Update endpoint, get and set values, update FBS tag mappings, check validity.
     *
     * @param record the DSRecord of the cable (XLS row)
     * @param endpoint which endpoint has to be updated
     * @param endpointType what is the endpoint type
     *
     * @see CableLoader#updateCheckValidityEndpoint(DSRecord, Endpoint, EndpointType, boolean)
     * @see CableService#updateFbsTagMappings(String, Iterable, Map, Map, Map)
     */
    private void updateCheckValidityEndpoint(DSRecord record, Endpoint endpoint, EndpointType endpointType) {
        // note
        //     difference compared to createCheckValidityEndpoint

        // update endpoint incl fbs tag mappings - check validity
        updateCheckValidityEndpoint(record, endpoint, endpointType, false);

        // endpoint uuid to be empty if endpoint device fbs tag is non-empty
        if (!StringUtils.isEmpty(endpoint.getDeviceFbsTag())) {
            endpoint.setUuid(null);
        }

        // validity checked, no need to further check endpoint attributes
    }

}
