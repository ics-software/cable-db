/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.faces.bean.RequestScoped;
import javax.interceptor.Interceptor;
import javax.security.auth.login.LoginException;

import se.esss.ics.rbac.loginmodules.service.RBACSSOSessionService;

/**
 * A session bean holding the logged in user information.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@RequestScoped
@Alternative
@Priority(Interceptor.Priority.APPLICATION+10)
public class RBACSessionService extends RBACSSOSessionService implements SessionService, Serializable {

    private static final long serialVersionUID = 887338652473840125L;

    private static final Logger LOGGER = Logger.getLogger(RBACSessionService.class.getName());

    /** @return true if the user can administer database, else false; if no user logged in, false is returned */
    public boolean canAdminister() {
        LOGGER.log(Level.FINE, String.format("Checking administer permission for user %s", getLoggedInName()));
        final boolean permission = hasPermission(CableRBACDefinitions.ADMINISTER_CABLE_DB_PERMISSION);
        LOGGER.log(Level.FINE, String.format("Administer permission for user %s: %b", getLoggedInName(), permission));

        try {
            final boolean runtimePermission = hasRuntimePermission(CableRBACDefinitions.ADMINISTER_CABLE_DB_PERMISSION);
            LOGGER.log(Level.FINE, String.format("Administer runtime permission for user %s: %b", getLoggedInName(),
                    runtimePermission));
        } catch (LoginException e) {
            LOGGER.log(Level.FINE, String.format("Could not obtain administer runtime permission for user %s",
                    getLoggedInName()), e);
        }
        return permission;
    }

    /**
     * @return true if the user can manage cables that they own, else false; if no user logged in, false is returned
     */
    public boolean canManageOwnedCables() {
        LOGGER.log(Level.FINE, String.format("Checking manage owned cables permission for user %s", getLoggedInName()));
        final boolean permission = hasPermission(CableRBACDefinitions.MANAGE_OWNED_CABLES_PERMISSION);
        LOGGER.log(Level.FINE, String.format("Manage owned cables permission for user %s: %b", getLoggedInName(),
                permission));
        try {
            final boolean runtimePermission = hasRuntimePermission(CableRBACDefinitions.MANAGE_OWNED_CABLES_PERMISSION);
            LOGGER.log(Level.FINE, String.format("Manage owned cables runtime permission for user %s: %b",
                    getLoggedInName(), runtimePermission));
        } catch (LoginException e) {
            LOGGER.log(Level.FINE, String.format("Could not obtain manage owned cables runtime permission for user %s",
                    getLoggedInName()), e);
        }
        return permission;
    }
}
