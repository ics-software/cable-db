/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

/**
 * Exception used in import of data from Excel sheet.
 */
public class DbFieldLengthViolationException extends RuntimeException {

    private static final long serialVersionUID = -7367328797455776721L;

    /**
     * Constructs a new exception.
     */
    public DbFieldLengthViolationException() {
        super();
    }

    /**
     * Constructs a new exception.
     *
     * @param message exception message
     */
    public DbFieldLengthViolationException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception.
     *
     * @param cause formal cause for throwing exception
     */
    public DbFieldLengthViolationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception.
     *
     * @param message exception message
     * @param cause formal cause for throwing exception
     */
    public DbFieldLengthViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
