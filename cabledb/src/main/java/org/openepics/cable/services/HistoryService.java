/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.model.History;
import org.openepics.cable.model.Query;
import org.openepics.cable.ui.HistoryColumnUI;
import org.openepics.cable.util.UiUtility;
import org.primefaces.model.SortOrder;

import com.google.common.collect.Lists;

/**
 * <code>HistoryService</code> is the service layer that handles history operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class HistoryService {

    private static final Logger LOGGER = Logger.getLogger(HistoryService.class.getName());

    @PersistenceContext
    private EntityManager em;

    public static final String EMPTY_STRING = "";

    public static final String DIFF_CHANGE_EXPLANATION = "\n(Attribute: newValue (oldValue)):\n";

    private final LazyService<History> lazyService = new LazyService<History>() {
        /**
         * Returns only a subset of data based on sort column, sort order and
         * filtered by all the fields.
         *
         * @param first the index of the first result to return
         * @param pageSize the number of results
         * @param sortField the field by which to sort
         * @param sortOrder ascending/descending
         * @param filters filters to use
         * @param customQuery not used
         * @return The required entities.
         */
        @Override
        public List<History> findLazy(
                final int first, final int pageSize,
                final @Nullable String sortField, final @Nullable SortOrder sortOrder,
                final @Nullable Map<String, Object> filters, Query customQuery) {

            LOGGER.log(Level.FINEST, "HistoryService:findLazy offset: " + first + ", pageSize: " + pageSize);
            final CriteriaBuilder cb = em.getCriteriaBuilder();
            final CriteriaQuery<History> cq = cb.createQuery(History.class);
            final Root<History> historyRecord = cq.from(History.class);

            addSortingOrder(sortField, sortOrder, cb, cq, historyRecord);

            List<Predicate> predicates = buildPredicateList(cb, historyRecord, filters);

            cq.where(predicates.toArray(new Predicate[]{}));

            final TypedQuery<History> query = em.createQuery(cq);
            query.setFirstResult(first);
            query.setMaxResults(pageSize);
            return query.getResultList();
        }
    };

    public LazyService<History> getLazyService() {
        return lazyService;
    }


    /**
     * Retrieves and returns history with specified id.
     *
     * @param id
     *            history id
     *
     * @return history with specified id.
     */
    public History getHistoryById(Long id) {
        return em.find(History.class, id);
    }

    /**
     * Creates new history entry.
     *
     * @param operation
     *            entity operation
     * @param entityName
     *            entity name
     * @param entityType
     *            entity type
     * @param entityId
     *            entity id
     * @param entry
     *            history entry
     * @param validityEntry
     *            history entry for validity
     * @param userId
     *            username of the user making the change.
     */
    public void createHistoryEntry(EntityTypeOperation operation, String entityName, EntityType entityType,
            Long entityId, String entry, String validityEntry, String userId) {
        // avoid flooding storage with empty history entries for update
        if (operation == EntityTypeOperation.UPDATE && StringUtils.isEmpty(entry)) {
            return;
        }

        final Date timestamp = new Date();
        final History history = new History(timestamp, operation, userId, entityName, entityType, entityId,
                constructHistoryEntry(operation, entityName, entityType, entry, validityEntry));
        em.persist(history);
    }

    /**
     * Constructs a string describing a change of an entity.
     *
     * @param operation
     *            entity operation
     * @param entityName
     *            entity name
     * @param entityType
     *            entity type
     * @param entry
     *            contains description of internal changes of an entity. Used for {@link EntityTypeOperation#UPDATE} and
     *            {@link EntityTypeOperation#VALIDATE}. Ignored for {@link EntityTypeOperation#CREATE} and
     *            {@link EntityTypeOperation#DELETE}
     * @param validityEntry
     *            contains the entry for validity. If the cable is valid and if not why.
     * @return String describing a change of an entity.
     */
    private String constructHistoryEntry(EntityTypeOperation operation, String entityName, EntityType entityType,
            String entry, String validityEntry) {
        StringBuilder sb = new StringBuilder(constructEntryHeader(operation, entityName, entityType, validityEntry));
        if ((operation == EntityTypeOperation.UPDATE) || (operation == EntityTypeOperation.REUSE)) {
            if (entry.isEmpty()) {
                sb.append(": (no change)\n");
            } else {
                sb.append(" ").append(DIFF_CHANGE_EXPLANATION).append(entry);
            }
        } else if (operation == EntityTypeOperation.VALIDATE) {
            sb.append("\n").append(entry);
        }
        return sb.toString();
    }

    /**
     * Constructs a history entry header string, describing which entity was changed and what type of change it was. The
     * data is formatted: "Operation EntityType: EntityName"
     *
     * @param operation
     *            entity operation
     * @param entityName
     *            entity name
     * @param entityType
     *            entity type
     * @param validityEntry
     *            validity of the cable and elements
     * @return String describing what happened.
     */
    private String constructEntryHeader(EntityTypeOperation operation, String entityName, EntityType entityType,
            String validityEntry) {
        return operation.getDisplayName() + "d " + entityType.getDisplayName().toLowerCase() + " with name: "
                + entityName + "\n" + validityEntry + "Changes:";
    }

    /**
     * Return the number of elements in the table.
     *
     * @param filters
     *            map of filters
     * @return the number of elements in the table
     */
    public long getRowCount(final @Nullable Map<String, Object> filters) {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<Long> cqCount = cb.createQuery(Long.class);
        final Root<History> historyRecordCount = cqCount.from(History.class);

        List<Predicate> predicatesCount = buildPredicateList(cb, historyRecordCount, filters);

        cqCount.where(predicatesCount.toArray(new Predicate[] {}));

        cqCount.select(cb.count(historyRecordCount));
        return em.createQuery(cqCount).getSingleResult();
    }

    private void addSortingOrder(final String sortField, final SortOrder sortOrder, final CriteriaBuilder cb,
            final CriteriaQuery<History> cq, final Root<History> cableRecord) {
        if ((sortField != null) && (sortOrder != null) && (sortOrder != SortOrder.UNSORTED)) {
            for (HistoryColumnUI column : HistoryColumnUI.values()) {
                if (sortField.equals(column.getValue())) {
                    switch (column) {
                    case ENTITY_NAME:
                    case ENTRY:
                    case USER_ID:
                    case ENTITY_TYPE:
                    case OPERATION:
                        cq.orderBy(
                                sortOrder == SortOrder.ASCENDING ? cb.asc(cb.lower(cableRecord.get(column.getValue())))
                                        : cb.desc(cb.lower(cableRecord.get(column.getValue()))));
                        break;
                    default:
                        cq.orderBy(sortOrder == SortOrder.ASCENDING ? cb.asc(cableRecord.get(column.getValue()))
                                : cb.desc(cableRecord.get(column.getValue())));
                    }
                    break;
                }
            }
        } else {
            cq.orderBy(cb.desc(cableRecord.get(HistoryColumnUI.TIMESTAMP.getValue())));
        }
    }

    private List<Predicate> buildPredicateList(final CriteriaBuilder cb, final Root<History> historyRecord,
            final @Nullable Map<String, Object> filters) {
        final List<Predicate> predicates = Lists.newArrayList();

        for (HistoryColumnUI column : HistoryColumnUI.values()) {
            if (filters.containsKey(column.getValue())) {

                Expression<String> expression = historyRecord.get(column.getValue());
                Expression<String> lowercaseExpression = cb.lower(expression.as(String.class));
                Object filter = filters.get(column.getValue());
                String stringFilter = filter.toString();
                String likeFilter = "%" + escapeDbString(stringFilter).toLowerCase() + "%";

                Predicate comparisonExpression = cb.like(lowercaseExpression, likeFilter, '\\');
                switch (column) {
                case ENTITY_TYPE:
                    EntityType type = UiUtility.parseIntoEnum(stringFilter, EntityType.class);
                    comparisonExpression = cb.equal(expression, type);
                    break;
                case OPERATION:
                    EntityTypeOperation operation = UiUtility.parseIntoEnum(stringFilter, EntityTypeOperation.class);
                    comparisonExpression = cb.equal(expression, operation);
                    break;
                case ENTITY_ID:
                    if (filter instanceof Long) {
                        comparisonExpression = cb.equal(lowercaseExpression, stringFilter);
                    } else {
                        // default string like compare
                    }
                    break;
                default:
                    // default string like compare
                    break;
                }
                predicates.add(comparisonExpression);
            }
        }
        return predicates;
    }

    /**
     * Escapes the characters that have a special meaning in the database LIKE query, '%' and '_'.
     *
     * @param dbString
     *            the string to escape
     * @return the escaped string
     */
    protected String escapeDbString(final String dbString) {
        return dbString.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("\\[", "\\\\[").replaceAll("]",
                "\\\\]");
    }

    /**
     * Returns string with difference between two attribute values.
     *
     * @param attrName
     *            attribute name
     * @param value
     *            new attribute value
     * @param oldValue
     *            old attribute value
     *
     * @return string with difference between two attribute values.
     */
    public static String getDiffForAttributes(final String attrName, final Object value, final Object oldValue) {
        if (value != null && oldValue != null) {
            if (!value.toString().equals(oldValue.toString())) {
                return getDiffString(attrName, value.toString(), oldValue.toString());
            }
        } else if (value == null && oldValue != null) {
            return getDiffString(attrName, null, oldValue.toString());
        } else if (value != null && oldValue == null) {
            return getDiffString(attrName, value.toString(), null);
        }
        return EMPTY_STRING;
    }

    /**
     * Generates string with difference between two attribute values.
     *
     * @param attrName
     *            attribute name
     * @param value
     *            new attribute value
     * @param oldValue
     *            old attribute value
     *
     * @return string with difference between two attribute values.
     */
    private static String getDiffString(final String attrName, final String value, final String oldValue) {
        if (StringUtils.trimToNull(value) == null && StringUtils.trimToNull(oldValue) == null) {
            return EMPTY_STRING;
        }

        return attrName + ": " +
                (value == null ? "null" : value) +
                " (" +
                (oldValue == null ? "null" : oldValue) +
                ")\n";
    }

}
