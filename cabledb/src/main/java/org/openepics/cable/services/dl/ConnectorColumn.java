/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the Connector table.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public enum ConnectorColumn implements FieldIntrospection {

    NAME("CONNECTOR NAME", "name", "Name", true, true, false),
    DESCRIPTION("CONNECTOR DESCRIPTION", "description", "Description", true, true, false),
    TYPE("CONNECTOR TYPE", "connectorType", "Type", true, true, false),
    DATASHEET("DATASHEET", "datasheet", "DataSheet", false, false, false),
    MANUFACTURERS("MANUFACTURERS", "manufacturers", "Manufacturers", true, true, false),
    STATUS("STATUS", "status", "Status", false, true, false),
    ASSEMBLY_INSTRUCTIONS("ASSEMBLY INSTRUCTIONS", "assemblyInstructions", "Assembly Instructions", true, true, false),
    LINK_TO_DATASHEET("LINK TO DATASHEET", "linkToDatasheet", "Link to Datasheet", true, true, true);

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isStringComparisonOperator;
    private final boolean isExcelVolatileColumn;

    private ConnectorColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isStringComparisonOperator, boolean isExcelVolatileColumn) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
        this.isExcelVolatileColumn = isExcelVolatileColumn;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    public boolean isExcelVolatileColumn() {
        return isExcelVolatileColumn;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Return connector column given column label.
     *
     * @param columnLabel column label
     * @return connector column
     */
    public static ConnectorColumn convertColumnLabel(String columnLabel) {
        if (NAME.getColumnLabel().equals(columnLabel)) {
            return NAME;
        } else if (DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return DESCRIPTION;
        } else if (TYPE.getColumnLabel().equals(columnLabel)) {
            return TYPE;
        } else if (DATASHEET.getColumnLabel().equals(columnLabel)) {
            return DATASHEET;
        } else if (MANUFACTURERS.getColumnLabel().equals(columnLabel)) {
            return MANUFACTURERS;
        } else if (STATUS.getColumnLabel().equals(columnLabel)) {
            return STATUS;
        }
        return null;
    }
}
