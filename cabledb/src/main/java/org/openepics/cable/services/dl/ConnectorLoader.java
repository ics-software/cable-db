/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import org.openepics.cable.model.Connector;
import org.openepics.cable.model.ConnectorManufacturer;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.services.ConnectorService;
import org.openepics.cable.services.ManufacturerService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.util.Utility;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <code>ConnectorLoader</code> is {@link DataLoader} that creates and persists cable types from {@link DSRecord} data.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
@Stateless
public class ConnectorLoader extends DataLoader<Connector> {

    private static final Logger LOGGER = Logger.getLogger(ConnectorLoader.class.getName());

    private static final String INVALID = "Invalid ";

    @Inject
    private ConnectorService connectorService;
    @Inject
    private SessionService sessionService;
    @Inject
    private ManufacturerService manufacturerService;

    // used for measuring time performance
    private long time;
    private long totalTime;

    // used for session information
    private String loggedInName;

    @Override
    public LoaderResult<Connector> load(InputStream stream, boolean test) {
        LOGGER.warning("Starting load");
        resetTime();

        // retrieve information before import
        //     session information
        // print import statistics and time

        loggedInName = sessionService.getLoggedInName();

        LoaderResult<Connector> result = super.load(stream, test);

        totalTime += getPassedTime();
        if (test) {
            LOGGER.info("# connectors (data):         " + dataRows);
            LOGGER.info("# connectors (create test):  " + createRowsTest);
            LOGGER.info("# connectors (update test):  " + updateRowsTest);
            LOGGER.info("# connectors (delete test):  " + deleteRowsTest);
            LOGGER.info("# connectors (skipped test): " + result.getImportFileStatistic().getSkippedRowsTest());
        } else {
            LOGGER.info("# connectors (data):    " + dataRows);
            LOGGER.info("# connectors (create):  " + createRows);
            LOGGER.info("# connectors (update):  " + updateRows);
            LOGGER.info("# connectors (delete):  " + deleteRows);
            LOGGER.info("# connectors (skipped): " + result.getImportFileStatistic().getSkippedRows());
        }
        LOGGER.info("Import time: " + totalTime + " ms");

        return result;
    }

    private void resetTime() {
        time = System.currentTimeMillis();
        totalTime = 0;
    }

    private long getPassedTime() {
        long currentTime = System.currentTimeMillis();
        long passedTime = currentTime - time;
        time = currentTime;
        return passedTime;
    }

    @Override
    public void updateRecord(DSRecord record) {
        checkAllColumnsPresent(record);

        if (report.isError()) {
            stopLoad = true;
            return;
        }

        switch (record.getCommand()) {
            case CREATE:
                createConnector(record);
                break;
            case UPDATE:
                updateConnector(record);
                break;
            case DELETE:
                deleteConnector(record);
                break;
            default:
                // do nothing
                break;
        }
    }

    /** Processes the record for connector creation. */
    private void createConnector(DSRecord record) {
        final String owner = loggedInName;

        if (report.isError() && !test) {
            return;
        }

        final String name = getRecordValue(ConnectorColumn.NAME, record, false, null, true);

        if (connectorService.getConnector(name) != null) {
            report.addMessage(getErrorMessage(getConnectorNameInfo(name) + " already exists in the database.", record,
                    ConnectorColumn.NAME, name));
            if (test) {
                createRowsTest++;
            }
            return;
        }

        final String description = getRecordValue(ConnectorColumn.DESCRIPTION, record, true, null, true);
        final String type = getRecordValue(ConnectorColumn.TYPE, record, false, null, true);
        final String assemblyInstruction = getRecordValue(ConnectorColumn.ASSEMBLY_INSTRUCTIONS, record, false, null, true);
        final String linkToDatasheet = getRecordValue(ConnectorColumn.LINK_TO_DATASHEET, record, false, null, true);
        final List<String> manufacturerNames = Utility.splitStringIntoList(getRecordValue(ConnectorColumn.MANUFACTURERS, record, false, null, false));

        final List<ConnectorManufacturer> manufacturers = new ArrayList<>();
        if(manufacturerNames != null) {
            for (String manufacturerName : manufacturerNames) {
                Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerName);
                if (manufacturer != null) {
                    manufacturers.add(new ConnectorManufacturer(null, manufacturer, null, manufacturers.size()));
                }
            }
        }

        if (report.isError() && !test) {
            return;
        }

        if (test) {
            createRowsTest++;
        } else {
            Connector connector = connectorService.createConnector(name, assemblyInstruction, description, type,
                    null, manufacturers, linkToDatasheet, owner);
            createRows++;
            report.addAffected(connector);
        }

        report.addMessage(new ValidationMessage("Adding new connector.", false, record.getRowLabel(), null));
    }

    /** Processes the record for connector update. */
    private void updateConnector(DSRecord record) {
        if (!checkConnectorName(record)) {
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final String name = Utility.formatRecordValue(record.getField(ConnectorColumn.NAME).toString());

        final Connector oldConnector = connectorService.getConnector(name);
        connectorService.detachConnector(oldConnector);
        final Connector connector = connectorService.getConnector(name);

        if (!connector.isActive()) {
            report.addMessage(new ValidationMessage(
                    getConnectorNameInfo(name) + " cannot be updated as it is already deleted.", true,
                    record.getRowLabel(), null, name));
            if (test) {
                updateRowsTest++;
            }
            return;
        }

        final String description = getRecordValue(ConnectorColumn.DESCRIPTION, record, true, connector.getDescription(), true);
        final String type = getRecordValue(ConnectorColumn.TYPE, record, false, connector.getType(), true);
        final String assemblyInstruction = getRecordValue(ConnectorColumn.ASSEMBLY_INSTRUCTIONS, record, false, connector.getAssemblyInstructions(), true);
        final String linkToDatasheet = getRecordValue(ConnectorColumn.LINK_TO_DATASHEET, record, false, connector.getLinkToDatasheet(), true);
        String manufactList = getRecordValue(ConnectorColumn.MANUFACTURERS, record, false, connector.getManufacturersAsString(false), false);

        List<String> manufacturerNames = new ArrayList<>();

        if(manufactList != null) {
            manufacturerNames.addAll(Utility.splitStringIntoList(manufactList));
        }

        final List<ConnectorManufacturer> existingManufacturers = connector.getManufacturers();
        final List<ConnectorManufacturer> manufacturers = new ArrayList<>();
        for (String manufacturerName : manufacturerNames) {
            Manufacturer manufacturer = manufacturerService.getManufacturer(manufacturerName);
            if (manufacturer != null) {
                manufacturers.add(new ConnectorManufacturer(connector, manufacturer, null, manufacturers.size()));
            }
        }
        // Iterate through manufacturers on the cable type and preserve ones that are specified in the import file
        for (ConnectorManufacturer existingConnectorManufacturer : existingManufacturers) {
            final String existingConnectorManufacturerName = existingConnectorManufacturer.getManufacturer().getName();
            // Iterate through manufacturers specified in the import file to check if current existing manufacturer is
            // amongst them. It yes swap the new manufacturer with the existing one, preserving the imported order.
            for (int i = 0; i < manufacturers.size(); i++) {
                if (existingConnectorManufacturerName.equals(manufacturers.get(i).getManufacturer().getName())) {
                    manufacturers.set(i, existingConnectorManufacturer);
                    break;
                }
            }
        }

        if (report.isError() && !test) {
            return;
        }

        connector.setName(name);
        connector.setDescription(description);
        connector.setType(type);
        connector.setAssemblyInstructions(assemblyInstruction);
        connector.setLinkToDatasheet(linkToDatasheet);
        connector.setManufacturers(manufacturers);

        if (test) {
            updateRowsTest++;
        } else {
            if (connectorService.updateConnector(connector, oldConnector, loggedInName)) {
                updateRows++;
            }
            report.addAffected(connector);
        }
        report.addMessage(
                new ValidationMessage(
                        "Updating connector '" + name + "'.", false, record.getRowLabel(), null));
    }

    private String getRecordValue(ConnectorColumn column, DSRecord record, boolean trimFrom, String defValue, boolean validateValue) {
        try {
            String result = trimFrom
                    ? Utility.formatRecordValueTrimFrom(record.getField(column).toString())
                    : Utility.formatRecordValue(record.getField(column).toString());

            if (validateValue) {
                validateStringSize(result, column);
            }

            return result;
        } catch (IllegalArgumentException e) {
            if (column.isExcelVolatileColumn()){
                return defValue;
            }

            LOGGER.log(Level.FINEST, INVALID + column, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, column,
                    Objects.toString(record.getField(column), null)));
        } catch (Exception e) {
            LOGGER.log(Level.FINEST, INVALID + column, e);
            report.addMessage(getErrorMessage(e.getMessage(), record, column,
                    Objects.toString(record.getField(column), null)));
        }

        return null;
    }

    /** Processes the record for connector deletion. */
    private void deleteConnector(DSRecord record) {
        if (!checkConnectorName(record)) {
            if (test) {
                deleteRowsTest++;
            }
            return;
        }

        final String name = Utility.formatRecordValue(record.getField(ConnectorColumn.NAME).toString());
        final Connector connector = connectorService.getConnector(name);

        if (report.isError() && !test)
            return;

        if (!connector.isActive()) {
            report.addMessage(new ValidationMessage(getConnectorNameInfo(name) + " is already deleted.", true,
                    record.getRowLabel(), name));
        }

        if (report.isError() && !test)
            return;

        if (test) {
            deleteRowsTest++;
        } else {
            if (connectorService.deleteConnector(connector, loggedInName)) {
                deleteRows++;
            }
            report.addAffected(connector);
        }
        report.addMessage(new ValidationMessage("Deleting connector with number " + name + ".", false,
                record.getRowLabel(), null));
    }

    /** Check that the record has all expected columns. */
    private void checkAllColumnsPresent(DSRecord record) {
        for (final ConnectorColumn column : ConnectorColumn.values()) {
            if (!column.isExcelColumn()) {
                continue;
            }

            if(column.isExcelVolatileColumn()) {
                //skip not necessary columns
                continue;
            }

            try {
                record.getField(column);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.FINEST, "Column not found", e);
                report.addMessage(new ValidationMessage("Could not find column '" + column + "'."
                        + " This can be caused by importing an old template or importing a wrong file.", true));
            }
        }
    }

    /**
     * Checks the validity of connector name and adds appropriate messages to report.
     *
     * @param record
     *            the record to check
     * @return true if the name is valid, else false
     */
    private boolean checkConnectorName(DSRecord record) {
        final String name = Utility.formatRecordValue(record.getField(ConnectorColumn.NAME).toString());

        final Connector connector = connectorService.getConnector(name);
        if (connector == null) {
            report.addMessage(getErrorMessage(getConnectorNameInfo(name) + " does not exist in the database.",
                    record, ConnectorColumn.NAME, name));
            return false;
        }
        return true;
    }

    private String getConnectorNameInfo(String connectorName) {
        return "Connector with name " + connectorName;
    }

    private void validateStringSize(final String value, final ConnectorColumn column) {
        super.validateStringSize(value, column, Connector.class);
    }
}
