/*
 * Copyright (c) 2016 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import se.esss.ics.rbac.loginmodules.service.Message;

/**
 * <code>SessionService</code> is the service layer that handles session operations.
 */
public interface SessionService {

    /**
     * Returns the distinguished name of the logged in user. If no user is logged in, null is returned.
     *
     * @return the username of the logged in user
     */
    public String getLoggedInName();

    /**
     * Returns the name of the logged in user, which should be used for all display purposes. In this case this should
     * be the user's first name. If the user is not logged in, null is returned.
     *
     * @return the visible name of the logged in user
     */
    public String getVisibleName();

    /**
     * @return true if the user is logged in
     */
    public boolean isLoggedIn();

    /**
     * Logs in with the given credentials. Whether the action succeeded is stored in the returned result.
     *
     * @param username the user name to use
     * @param password the password
     * @return the result with the message and successful flag
     */
    public Message login(String username, String password);

    /**
     * Logs out the currently logged in user and returns the result as the message. If the logout was successful, the
     * message is set as successful, if it failed, the successful flag is set to false.
     *
     * @return the result
     */
    public Message logout();

    /** @return true if the user can administer database, else false; if no user logged in, false is returned */
    public boolean canAdminister();

    /**
     * @return true if the user can manage cables that they own, else false; if no user logged in, false is returned
     */
    public boolean canManageOwnedCables();
}
