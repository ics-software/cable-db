/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This contains some utility methods that deal with dates.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DateUtil {

    /** date format string that displays only date without time */
    public static final String DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";

    private static final ThreadLocal<DateFormat> LOCAL_DATE_FORMAT = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat(DATE_FORMAT_STRING);
        }
    };

    /**
     * Parses the string using the {@link #DATE_FORMAT_STRING} format.
     *
     * @param string
     *            the string to parse
     * @return the date or null if input string is null
     * @throws ParseException
     *             if string cannot be parsed
     */
    public static Date parse(String string) throws ParseException {
        return string != null ? LOCAL_DATE_FORMAT.get().parse(string) : null;
    }

    /**
     * Formats the date to a string using the {@link #DATE_FORMAT_STRING} format.
     *
     * @param date
     *            the date to format
     * @return the string or null if date is null
     */
    public static String format(Date date) {
        return date != null ? LOCAL_DATE_FORMAT.get().format(date) : null;
    }

    /**
     * Compares the given dates for equality, ignoring time portion of the dates.
     *
     * @param date1
     *            the first date
     * @param date2
     *            the second date
     *
     * @return true if dates are equal, else false.
     */
    public static boolean equalIgnoreTime(Date date1, Date date2) {
        if (date1 == null && date2 == null)
            return true;
        if (date1 == null || date2 == null)
            return false;

        return format(date1).equals(format(date2));
    }

    /**
     * Returns the given date with the time part set to 0.
     *
     * @param date
     *            the date
     * @return the updated date
     */
    public static Date clearTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private DateUtil() {}
}
