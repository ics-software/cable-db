/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This represents a result of load operation, consisting of the error status, load {@link ValidationMessage}s, list of
 * objects affected by the load, and so on.
 *
 * @param <T>
 *            Class of the objects affected by the load.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class LoaderResult<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = -6862169843189916294L;

    /**
     * Utility class for file import statistics.
     */
    public static class ImportFileStatistics {
        private final int dataRows;
        private final int createRows;
        private final int updateRows;
        private final int deleteRows;
        private final int createRowsTest;
        private final int updateRowsTest;
        private final int deleteRowsTest;

        /** Default constructor with <code>0</code> statistics */
        public ImportFileStatistics() {
            dataRows = createRows = updateRows = deleteRows = 0;
            createRowsTest = updateRowsTest = deleteRowsTest = 0;
        }

        /**
         * Create a new ImportFileStatistics with statistics.
         *
         * @param dataRows number of rows with data in file to be imported
         * @param createRows number of rows that are created
         * @param updateRows number of rows that are updated
         * @param deleteRows number of rows that are deleted
         */
        public ImportFileStatistics(int dataRows, int createRows, int updateRows, int deleteRows) {
            this.dataRows = dataRows;
            this.createRows = createRows;
            this.updateRows = updateRows;
            this.deleteRows = deleteRows;

            createRowsTest = updateRowsTest = deleteRowsTest = 0;
        }

        /**
         * Create a new ImportFileStatistics with statistics.
         *
         * @param dataRows number of rows with data in file to be imported
         * @param createRows number of rows that are created
         * @param updateRows number of rows that are updated
         * @param deleteRows number of rows that are deleted
         * @param createRowsTest number of rows that are created in test phase
         * @param updateRowsTest number of rows that are updated in test phase
         * @param deleteRowsTest number of rows that are deleted in test phase
         */
        public ImportFileStatistics(int dataRows, int createRows, int updateRows, int deleteRows,
                int createRowsTest, int updateRowsTest, int deleteRowsTest) {
            this.dataRows = dataRows;
            this.createRows = createRows;
            this.updateRows = updateRows;
            this.deleteRows = deleteRows;

            this.createRowsTest = createRowsTest;
            this.updateRowsTest = updateRowsTest;
            this.deleteRowsTest = deleteRowsTest;
        }

        /** @return the dataRows */
        public int getDataRows() {
            return dataRows;
        }

        /** @return the createRows */
        public int getCreateRows() {
            return createRows;
        }

        /** @return the updateRows */
        public int getUpdateRows() {
            return updateRows;
        }

        /** @return the deleteRows */
        public int getDeleteRows() {
            return deleteRows;
        }

        /** @return the number of lines actually containing any of the 3 commands */
        public int getImportRows() {
            return createRows + updateRows + deleteRows;
        }

        /** @return the number of lines not handled
         */
        public int getSkippedRows() {
            return getDataRows() - getImportRows();
        }

        /** @return the createRowsTest */
        public int getCreateRowsTest() {
            return createRowsTest;
        }

        /** @return the updateRowsTest */
        public int getUpdateRowsTest() {
            return updateRowsTest;
        }

        /** @return the deleteRowsTest */
        public int getDeleteRowsTest() {
            return deleteRowsTest;
        }

        /** @return the number of lines actually containing any of the 3 commands in test phase */
        public int getImportRowsTest() {
            return createRowsTest + updateRowsTest + deleteRowsTest;
        }

        /** @return the number of lines not handled in test phase
         */
        public int getSkippedRowsTest() {
            return getDataRows() - getImportRowsTest();
        }
    }

    private List<ValidationMessage> messages = new ArrayList<ValidationMessage>();
    private List<ValidationMessage> errors = new ArrayList<ValidationMessage>();
    private boolean error;
    private List<T> affected = new ArrayList<T>();
    private int headerIndex;
    private DSHeader header = new DSHeader();
    private ImportFileStatistics importFileStatistic;

    /** @return the messages of this report */
    public List<ValidationMessage> getMessages() {
        return messages;
    }

    /** @return the messages that are errors of this report */
    public List<ValidationMessage> getErrors() {
        return errors;
    }

    /** @return the report error state */
    public boolean isError() {
        return error;
    }

    /**
     * Adds the message to the report
     *
     * @param message
     *            the message to add
     */
    public void addMessage(ValidationMessage message) {
        messages.add(message);
        if (message.isError()) {
            error = true;
            errors.add(message);
        }
    }

    /**
     * Adds an object affected by the load to this report
     *
     * @param object
     *            the affected object
     */
    public void addAffected(T object) {
        affected.add(object);
    }

    /**
     * Appends the content of the given result to this one.
     *
     * @param result
     *            the result to append
     */
    public void addResult(LoaderResult<T> result) {
        for (ValidationMessage message : result.getMessages()) {
            addMessage(message);
        }
        for (T object : result.getAffected()) {
            addAffected(object);
        }

        headerIndex = Math.max(headerIndex, result.getHeaderIndex());
        header.addHeader(result.header);
    }

    /**
     * Returns objects that were affected by the load.
     *
     * @return the objects
     */
    public Iterable<T> getAffected() {
        return affected;
    }

    /**
     * Returns the header information.
     *
     * @return the last processed header
     */
    public DSHeader getHeader() {
        return header;
    }

    /**
     * @param header
     *            the header information
     */
    public void setHeader(DSHeader header) {
        this.header = header;
    }

    /**
     * Returns the index of the header.
     *
     * @return the 0-based row index, or -1 for none
     */
    public int getHeaderIndex() {
        return headerIndex;
    }

    /**
     * @param headerIndex
     *            the index of the header
     */
    public void setHeaderIndex(int headerIndex) {
        this.headerIndex = headerIndex;
    }

    /** @return import file statistic. */
    public ImportFileStatistics getImportFileStatistic() {
        return importFileStatistic;
    }

    /**
     * Set import file statistic.
     *
     * @param importFileStatistic
     *            import file statistic
     */
    public void setImportFileStatistic(ImportFileStatistics importFileStatistic) {
        this.importFileStatistic = importFileStatistic;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        for (ValidationMessage message : messages) {
            builder.append(message.toString());
            builder.append("\n");
        }

        if (isError()) {
            builder.append("There were some errors.\n");
        }
        return builder.toString();
    }
}
