/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services;

import org.apache.commons.lang3.StringUtils;
import org.openepics.cable.CableProperties;
import org.openepics.cable.dto.NotificationDTO;
import org.openepics.cable.model.Cable;
import org.openepics.cable.model.util.StringUtil;
import org.openepics.cable.services.dl.CableSaver;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <code>NotificationService</code> is the service layer that handles notification operations.
 *
 * @author Imre Toth <imre.toth@esss.se>
 * @author Lars Johansson
 */
@Singleton
@Startup
public class NotificationService {

    private static final Logger LOGGER = Logger.getLogger(NotificationService.class.getName());

    private static final String CABLE_BATCH_UPDATE_CHANGE_NOTIFICATION_XLSX =
            "Cable_batch_update_change_notification.xlsx";

    private static String appBaseUrl = CableProperties.getInstance().getApplicationBaseURL();

    @Inject
    private CableService cableService;
    @Inject
    private MailService mailService;

    @Inject
    private UserDirectoryService userDirectoryService;

    /**
     * Sends email notification to admins if unexpected changes happened in CHESS
     *
     * @param mapOwnersCablesUnexpectedChanges contains the owners of the changed cables, and the parameters
     * @return was the mail sending successful, or not
     */
    public boolean notifyUserFromChange(Map<String, List<NotificationDTO>> mapOwnersCablesUnexpectedChanges) {
        LOGGER.log(Level.INFO, "Unexpected changes in Cable, trying to send email to admins");

        if (!CableProperties.getInstance().getBatchNotificationEnabled()) {
            LOGGER.log(Level.FINE, "Batch notification email is disabled, will not send email to admins about change");

            return true;
        }

        if ((mapOwnersCablesUnexpectedChanges == null) || (mapOwnersCablesUnexpectedChanges.isEmpty())) {
            LOGGER.log(Level.FINE, "Unexpected changes are empty, will not send notification to admins");

            return false;
        }

        Set<String> administrators = userDirectoryService.getAllAdministratorUsernames();

        if ((administrators == null) || administrators.isEmpty()) {
            LOGGER.log(Level.FINE,
                    "Can not notify admins from sending changes from Naming, admin email list is empty!");
            return false;
        }

        List<String> adminMails = new ArrayList<>(administrators.size());
        List<String> ownerNames = new ArrayList<>(administrators.size());
        String email = null;
        String userFullName = null;

        for (String admin : administrators) {
            try {
                // user directory service may give null value for invalid username
                email = userDirectoryService.getEmail(admin);
                if (StringUtils.isNotEmpty(email)) {
                    adminMails.add(email);
                }
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "Error while trying to resolve admin email, " + e);
            }
        }

        // go through map - owners and cables that have been changed
        //     send email
        //         to owners about cables that have been changed
        //         cc cable coordinators (cable administrators if cable coordinators unavailable)

        for (Map.Entry<String, List<NotificationDTO>> entry : mapOwnersCablesUnexpectedChanges.entrySet()) {
            try {

                String subject = "Cable batch update change notification";

                List<String> ownerMails = new ArrayList<>();
                String[] cableOwners = entry.getKey().split(",");
                for (String owner : cableOwners) {
                    try {
                        // user directory service may give null value for invalid username
                        email = userDirectoryService.getEmail(owner.trim());
                        userFullName = userDirectoryService.getUserFullName(owner.trim());
                        if (StringUtils.isNotEmpty(email)) {
                            ownerMails.add(email);
                        }
                        if (StringUtils.isNotEmpty(userFullName)) {
                            ownerNames.add(userFullName);
                        }
                    } catch (Exception e) {
                        LOGGER.log(
                                Level.WARNING,
                                "Email not found in RBAC when trying to notify users from change: " + owner);
                    }
                }

                // further split cables that have been changed per cable coordinators
                //     find out cable coordinators for cables - for each entry in map
                //     cc in email
                //         1) cable coordinators
                //         2) cable administrators (if cable coordinators unavailable)

                Map<String, List<NotificationDTO>> mapCableCoordinatorsCablesUnexpectedChanges = new HashMap<>();
                for (NotificationDTO notificationDTO : entry.getValue()) {
                    String cableCoordinators = notificationDTO != null && notificationDTO.getCable() != null
                            && notificationDTO.getCable().getInstallationPackage() != null
                            ? notificationDTO.getCable().getInstallationPackage().getCableCoordinator()
                            : null;
                    List<NotificationDTO> cablesUnexpectedChanges =
                            mapCableCoordinatorsCablesUnexpectedChanges.get(cableCoordinators);
                    if (cablesUnexpectedChanges == null) {
                        cablesUnexpectedChanges = new ArrayList<>();
                    }
                    cablesUnexpectedChanges.add(notificationDTO);
                    mapCableCoordinatorsCablesUnexpectedChanges.put(cableCoordinators, cablesUnexpectedChanges);
                }

                // go through map - owners, cable coordinators and cables that have been changed
                //     prepare email
                //     send email

                List<String> ccMails = new ArrayList<>();
                for (Map.Entry<String, List<NotificationDTO>> entryCableCoordinators
                        : mapCableCoordinatorsCablesUnexpectedChanges.entrySet()) {
                    // possible to have cable object in ChangeDTO, however might be multiple (5) ChangeDTO for one cable
                    // prepare attachment with cables
                    //     Excel input stream
                    //         get list of of cables from cable names
                    //         get input stream to Excel from list of cables

                    ccMails.clear();
                    if (entryCableCoordinators.getKey() == null) {
                        // cc cable administrators
                        ccMails.addAll(adminMails);
                    } else {
                        // find out cable coordinators email addresses
                        for (String cableCoordinator : StringUtil.splitToList(entryCableCoordinators.getKey())) {
                            // user directory service may give null value for invalid username
                            email = userDirectoryService.getEmail(cableCoordinator.trim());
                            if (StringUtils.isNotEmpty(email)) {
                                ccMails.add(email);
                            }
                        }
                    }

                    // split per cable system within entry
                    //     first character in cable name
                    Map<String, List<NotificationDTO>> mapSystemsCablesUnexpectedChanges = new HashMap<>();
                    Set<String> cableSystems = new TreeSet<>();
                    for (NotificationDTO notificationDTO : entryCableCoordinators.getValue()) {
                        String system = notificationDTO.getCableName().substring(0, 1);
                        cableSystems.add(system);
                        remarkChange(mapSystemsCablesUnexpectedChanges, system, notificationDTO);
                    }

                    // go through unexpected changes per system per cable owners
                    for (Map.Entry<String, List<NotificationDTO>> entrySystem : mapSystemsCablesUnexpectedChanges.entrySet()) {
                        Set<String> cableNames = new TreeSet<>();
                        for (NotificationDTO notificationDTO : entrySystem.getValue()) {
                            cableNames.add(notificationDTO.getCableName());
                        }
                        List<Cable> cables = cableService.getCablesByName(cableNames, true, true);
                        // sort
                        Collections.sort(cables,                            Comparator.comparing(Cable::getName));
                        Collections.sort(entrySystem.getValue(), Comparator.comparing(NotificationDTO::getCableName));

                        // save to Excel (but not export to file/stream)
                        final CableSaver cableSaver = new CableSaver(cables, entrySystem.getValue());

                        String subject2 = "[System " + entrySystem.getKey() + "] "
                                + subject + " - <" + String.join(", ", ownerNames) + "> - " + appBaseUrl;

                        LOGGER.log(Level.INFO, "notifyUserFromChange, cableSystems.size:  " + cableSystems.size());
                        for (String cableSystem : cableSystems) {
                            LOGGER.log(Level.INFO, "notifyUserFromChange, cableSystem:        " + cableSystem);
                        }
                        LOGGER.log(Level.INFO, "notifyUserFromChange, sendEmail, subject2: " + subject2);

                        mailService.sendMail(
                                ownerMails, ccMails, subject2,
                                generateUnexpectedChangeNotificationBody(
                                        entrySystem.getValue(),
                                        true,
                                        cables.size(),
                                        entrySystem.getValue().size()),
                                Arrays.asList(cableSaver.save()),
                                Arrays.asList(CABLE_BATCH_UPDATE_CHANGE_NOTIFICATION_XLSX),
                                true);
                    }
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Error while trying to send changes-notification to admins", e);
                return false;
            }
        }
        return true;
    }

    /**
     * Remark change for system.
     *
     * @param mapSystemsCablesUnexpectedChanges map
     * @param system system
     * @param notification notification
     *
     * @see CableService#remarkChange(Map<String, List<ChangeDTO>>, String ChangeDTO)
     */
    private void remarkChange(Map<String, List<NotificationDTO>> mapSystemsCablesUnexpectedChanges, String system,
            NotificationDTO notification) {

        List<NotificationDTO> cablesUnexpectedChanges =
                mapSystemsCablesUnexpectedChanges.get(system);
        if (cablesUnexpectedChanges == null) {
            cablesUnexpectedChanges = new ArrayList<>();
        }
        cablesUnexpectedChanges.add(notification);
        mapSystemsCablesUnexpectedChanges.put(system, cablesUnexpectedChanges);
    }

    /**
     * Generates Thymeleaf template and fills data with the given parameter values
     *
     * @param unexpectedChanges modified area structure values with operation
     * @param addFooter         does the email need to have a footer, or not
     * @return the HTML template body filled with data
     */
    private String generateUnexpectedChangeNotificationBody(
            List<NotificationDTO> unexpectedChanges, boolean addFooter, int numberChangedCables, int numberChanges) {
        TemplateEngine engine = generateTemplateEngine();

        final Context ctx = new Context();
        ctx.setVariable("unexpectedChange", unexpectedChanges);
        ctx.setVariable("addfooter", addFooter);
        ctx.setVariable("baseurl", appBaseUrl.endsWith("/") ? appBaseUrl : appBaseUrl + "/");
        ctx.setVariable("numberChangedCables", numberChangedCables);
        ctx.setVariable("numberChanges", numberChanges);

        return engine.process("emailTemplate/unexpected_change.html", ctx);
    }

    /**
     * Generates the Thymeleaf template engine
     *
     * @return: generate template engine
     */
    private TemplateEngine generateTemplateEngine() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();

        templateResolver.setTemplateMode("HTML5");
        templateResolver.setCacheable(false);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(templateResolver);
        return engine;
    }
}
