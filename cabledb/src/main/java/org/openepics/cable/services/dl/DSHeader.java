/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facilitty for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */
package org.openepics.cable.services.dl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This holds header definition of loaded data. It is used internally by {@link RecordStream} to assemble records.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DSHeader implements Serializable {

    private static final long serialVersionUID = -2745217060691654996L;

    private static class Column implements Serializable {

        private static final long serialVersionUID = -8042794144469387490L;

        private int index;
        private String label;

        private Column(int index, String label) {
            super();
            this.index = index;
            this.label = label;
        }
    }

    private static final String EMPTY_STRING = "";

    private Map<String, Column> headerDef;

    /** Constructs an instance with no definition. */
    DSHeader() {
        headerDef = new HashMap<String, Column>();
    }

    /**
     * Returns the index of a column.
     *
     * @param name
     *            the column name
     * @return 0-based index
     */
    public int getColumnIndex(String name) {
        if (headerDef.containsKey(name)) {
            return headerDef.get(name).index;
        }
        return -1;
    }

    /**
     * Returns the label of a column.
     *
     * @param name
     *            the column name
     * @return the column label (A, B, ...)
     */
    public String getColumnLabel(String name) {
        if (headerDef.containsKey(name)) {
            return headerDef.get(name).label;
        }
        return EMPTY_STRING;
    }

    /**
     * Stores a column definition.
     *
     * @param name
     *            the column name
     * @param index
     *            the column index, 0 based
     * @param label
     *            the column label (A, B, ...)
     */
    public void setColumn(String name, int index, String label) {
        headerDef.put(name, new Column(index, label));
    }

    /** @return true if any definitions stored in the header, else false */
    public boolean hasDefinitions() {
        return !headerDef.isEmpty();
    }

    /** @return the defined column names */
    public Iterable<String> getColumnNames() {
        return headerDef.keySet();
    }

    /** Clears all stored definitions. */
    public void clear() {
        headerDef.clear();
    }

    /**
     * Appends the definition of the given header to this one.
     *
     * @param header
     *            the header to append
     */
    public void addHeader(DSHeader header) {
        for (String name : header.getColumnNames()) {
            setColumn(name, header.getColumnIndex(name), header.getColumnLabel(name));
        }
    }
}
