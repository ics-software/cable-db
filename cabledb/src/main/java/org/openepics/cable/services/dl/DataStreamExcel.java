/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facilitty for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */
package org.openepics.cable.services.dl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;
import org.apache.poi.common.usermodel.Hyperlink;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * This is an implementation of {@link DataStream} that retrieves the data from an Excel spreadsheet.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DataStreamExcel implements DataStream {

    private static int ROW_REF_PART_INDEX = 1;
    private static int COLUMN_REF_PART_INDEX = 2;

    private static final UrlValidator URL_VALIDATOR = new UrlValidator();

    private int nextRowNum;
    private InputStream stream;
    private Workbook workbook;
    private Sheet sheet;
    private int lastRowNumber;
    private FormulaEvaluator evaluator;
    private Row currentRow;

    private List<Integer> urlColumns = Collections.emptyList();

    /**
     * @return the urlColumns
     */
    public List<Integer> getUrlColumns() {
        return urlColumns;
    }

    /**
     * @param urlColumns
     *            the urlColumns to set
     */
    public void setUrlColumns(List<Integer> urlColumns) {
        this.urlColumns = urlColumns;
    }

    @Override
    public void open(InputStream stream) {
        this.stream = stream;

        try {
            workbook = WorkbookFactory.create(this.stream);
        } catch (InvalidFormatException | IOException e) {
            throw new RuntimeException(e);
        }
        if (workbook == null) {
            throw new RuntimeException("Invalid stream format.");
        }
        sheet = workbook.getSheetAt(0);
        if (sheet == null) {
            throw new RuntimeException("Workbook has no sheet 0.");
        }
        evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        currentRow = null;
        nextRowNum = 0;
        lastRowNumber = sheet.getLastRowNum();
    }

    @Override
    public void close() {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            stream = null;
        }
    }

    @Override
    public void nextRow() {
        if (!isEndOfStream()) {
            currentRow = sheet.getRow(nextRowNum);
        }
        ++nextRowNum;
    }

    @Override
    public Object getCell(int colNum) {
        if (currentRow == null) {
            return "";
        }
        final Cell cell = currentRow.getCell(colNum);

        return getCellValue(cell);
    }

    @Override
    public Object getCell(int rowNum, int colNum) {
        final Cell cell = sheet.getRow(rowNum).getCell(colNum);
        return getCellValue(cell);
    }

    @Override
    public boolean containsMergedCells() {
        if (currentRow == null) {
            return false;
        }
        int rowNum = currentRow.getRowNum();
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            if (range.getFirstRow() <= rowNum && rowNum <= range.getLastRow()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<CellRangeAddress> getMergedRegions() {
        List<CellRangeAddress> currentRowMergedRegions = new ArrayList<>();
        if (currentRow == null || !containsMergedCells()) {
            return currentRowMergedRegions;
        }
        int rowNum = currentRow.getRowNum();
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            if (range.getFirstRow() <= rowNum && rowNum <= range.getLastRow()) {
                currentRowMergedRegions.add(range);
            }
        }
        return currentRowMergedRegions;

    }

    private Object getCellValue(Cell cell) {

        if (cell == null)
            return "";

        // handle cells containing an URL
        final URL urlValue = getCellURL(cell);
        // if any URL, return it, else check cell value
        if (urlValue != null) {
            return urlValue;
        }

        Object value;
        switch (cell.getCellType()) {
        case Cell.CELL_TYPE_BLANK:
            value = getBlankCellValue(cell);
            break;
        case Cell.CELL_TYPE_FORMULA:
            value = getFormulaCellValue(cell);
            break;
        case Cell.CELL_TYPE_NUMERIC:
            value = getNumericCellValue(cell);
            break;
        case Cell.CELL_TYPE_STRING:
            value = getStringCellValue(cell);
            break;
        case Cell.CELL_TYPE_BOOLEAN:
            value = getBooleanCellValue(cell);
            break;
        default:
            value = cell.getStringCellValue();
            break;
        }
        return value;
    }

    private URL getCellURL(Cell cell) {
        int column = cell.getColumnIndex();
        if (urlColumns.contains(column)) {
            final Hyperlink link = cell.getHyperlink();
            if (link != null && link.getType() == Hyperlink.LINK_URL && URL_VALIDATOR.isValid(link.getAddress())) {
                try {
                    return new URL(link.getAddress());
                } catch (MalformedURLException e) {
                    return null;
                }
            }
        }
        return null;
    }

    private Object getBlankCellValue(Cell cell) {
        return "";
    }

    private Object getFormulaCellValue(Cell cell) {
        CellValue cellValue = evaluator.evaluate(cell);
        if (cellValue != null) {
            final String columnValue = cellValue.getStringValue();
            if (columnValue == null) {
                return Double.toString(cellValue.getNumberValue());
            }
            return columnValue;
        } else {
            return "";
        }
    }

    private Object getNumericCellValue(Cell cell) {
        if (HSSFDateUtil.isCellDateFormatted(cell)) {
            return cell.getDateCellValue();
        } else {
            double value = cell.getNumericCellValue();
            // if value is an integer, strip the decimal zeros
            if (value == (int) value) {
                return Integer.toString(Double.valueOf(value).intValue());
            } else {
                return Double.toString(value);
            }
        }
    }

    private Object getStringCellValue(Cell cell) {

        // handle urls that are stored as strings
        if (URL_VALIDATOR.isValid(cell.getStringCellValue().trim())) {
            try {
                return new URL(cell.getStringCellValue());
            } catch (MalformedURLException e) {
                // handle cell as if it did not contain a link
            }
        }
        return cell.getStringCellValue().trim();
    }

    private Object getBooleanCellValue(Cell cell) {
        return Boolean.toString(cell.getBooleanCellValue());
    }

    @Override
    public int getRowSize() {
        // if empty or not yet read row return 0
        if (currentRow == null)
            return 0;

        return currentRow.getLastCellNum();
    }

    @Override
    public boolean isEndOfStream() {
        return nextRowNum > lastRowNumber;
    }

    @Override
    public String getColumnLabel(int colNum) {
        // if empty row take first cell as reference
        CellReference cellReference = new CellReference(currentRow != null ? currentRow.getRowNum() : 0, colNum);
        String[] cellRefParts = cellReference.getCellRefParts();
        return cellRefParts[COLUMN_REF_PART_INDEX];
    }

    @Override
    public int getRowIndex() {
        return nextRowNum - 1;
    }

    @Override
    public String getRowLabel() {
        // if empty row take first cell as reference
        CellReference cellReference = new CellReference(currentRow != null ? currentRow.getRowNum() : 0, 0);
        String[] cellRefParts = cellReference.getCellRefParts();
        return cellRefParts[ROW_REF_PART_INDEX];
    }
}
