/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.CableType;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the service layer that handles approved cable type import and export operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableTypeImportExportService {

    private static final String NO_IMPORT_PERMISSION = "You do not have permission to import approved cable types.";
    private static final String NO_EXPORT_PERMISSION = "You do not have permission to export approved cable types.";

    @Resource
    private EJBContext context;
    @Inject
    private CableTypeLoader cableTypeLoader;
    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;
    @Inject
    private MailService mailService;

    /**
     * Imports cable types from Excel spreadsheet.
     *
     * @param inputStream the input stream representing the Excel file
     * @return the report with validation state and messages
     *
     * @throws IllegalStateException if current user does not have permission for this action
     */
    public LoaderResult<CableType> importCableTypes(InputStream inputStream) {
        return importCableTypes(inputStream, false);
    }

    /**
     * Imports cable types from Excel spreadsheet.
     *
     * @param inputStream the input stream representing the Excel file
     * @param test if true, only a test whether the import would succeed is performed, and no cable types are imported
     * @return the report with validation state and messages
     *
     * @throws IllegalStateException if current user does not have permission for this action
     */
    public LoaderResult<CableType> importCableTypes(InputStream inputStream, boolean test) {
        // We read inputStream into byte as we need to use it twice.
        byte[] bytes;
        try {
            bytes = ByteStreams.toByteArray(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ByteArrayInputStream byteInputstream = new ByteArrayInputStream(bytes);

        if (!canImportCableTypes()) {
            throw new IllegalStateException(NO_IMPORT_PERMISSION);
        }
        LoaderResult<CableType> report = cableTypeLoader.load(byteInputstream, test);
        if (!test && !report.isError()) {
            // Send mail
            final Set<String> notifiedUsers = new HashSet<>();
            for (final String userName : userDirectoryServiceFacade.getAllAdministratorUsernames()) {
                notifiedUsers.add(userName);
            }

            if (!Lists.newArrayList(report.getAffected()).isEmpty()) {
                String content = "Cable types have been imported in the Cable Database" + " (by "
                        + userDirectoryServiceFacade.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")";
                byteInputstream.reset();
                mailService.sendMail(notifiedUsers, sessionService.getLoggedInName(),
                        "Cable types imported notification", content, Arrays.asList(byteInputstream),
                        Arrays.asList("cdb_cable_types_imported.xlsx"), true, true);
            }
        } else {
            context.setRollbackOnly();
        }

        return report;
    }

    /**
     * Exports cable types to an Excel spreadsheet.
     *
     * @param cableTypes
     *            the cable types to export
     * @return the input stream representing the Excel file
     *
     * @throws IllegalStateException
     *             if current user does not have permission for this action
     */
    public InputStream exportCableTypes(Iterable<CableType> cableTypes) {
        if (!sessionService.isLoggedIn()) {
            throw new IllegalStateException(NO_EXPORT_PERMISSION);
        }

        final CableTypeSaver cableTypeSaver = new CableTypeSaver(cableTypes);
        return cableTypeSaver.save();
    }

    /** @return true if the current user can import approved cable types, else false */
    public boolean canImportCableTypes() {
        return sessionService.canAdminister();
    }

}
