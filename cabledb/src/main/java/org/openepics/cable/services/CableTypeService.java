/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.*;
import org.openepics.cable.services.dl.CableTypeColumn;

/**
 * <code>CableTypeService</code> is the service layer that handles cable type operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableTypeService {

    private static final Logger LOGGER = Logger.getLogger(CableTypeService.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;
    @Inject
    private ArtifactService artifactService;

    /**
     * Retrieve cable type by the name.
     *
     * @param name
     *            the cable type name
     * @return the cable type, or null if the cable type with such name does not exist
     */
    public CableType getCableTypeByName(String name) {
        try {
            return em.createQuery("SELECT ct FROM CableType ct WHERE ct.name = :name", CableType.class)
                    .setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.log(Level.FINEST, "Cable type with name: " + name + " not found", e);
            return null;
        }
    }

    /**
     * Return a list of filtered cable types.
     *
     * @param customQuery custom query
     * @param query query
     * @return a list of filtered cable types
     */
    public List<CableType> getFilteredCableTypes(String customQuery, Query query) {
        List<CableType> filteredCableTypes = new ArrayList<>();
        if (customQuery != null && !customQuery.isEmpty()) {
            filteredCableTypes = em.createQuery(customQuery, CableType.class).getResultList();
        }
        return filteredCableTypes;
    }

    /** @return a list of all cable types */
    public List<CableType> getAllCableTypes() {
        return em.createQuery("SELECT ct FROM CableType ct", CableType.class).getResultList();
    }

    /**
     * Creates a cable type in the database and returns it.
     *
     * @param name
     *            the name/code
     * @param installationType
     *            the allowed installation type
     * @param description
     *            the description
     * @param service
     *            the service/function description
     * @param voltage
     *            the voltage rating description
     * @param insulation
     *            the insulation material
     * @param jacket
     *            the type of jacket
     * @param flammability
     *            the flammability classification
     * @param tid
     *            the total ionizing dose in mrad
     * @param weight
     *            the weight in kg/meter
     * @param diameter
     *            the outer diameter in mm
     * @param manufacturers
     *            the manufacturers of this cable type
     * @param comments
     *            the comments
     * @param revision
     *            the revision
     * @param userId
     *            username of user creating the cableType, for history record
     *
     * @return the created cable type
     */
    public CableType createCableType(String name, InstallationType installationType, String description, String service,
            Integer voltage, String insulation, String jacket, String flammability, Float tid, Float weight,
            Float diameter, List<CableTypeManufacturer> manufacturers, String comments, String revision,
            String userId) {

        if (getCableTypeByName(name) != null) {
            throw new IllegalArgumentException("Cable type with name '" + name + "' already exists.");
        }

        final CableType cableType = new CableType(name, installationType, description, service, voltage, insulation,
                jacket, flammability, tid, weight, diameter, manufacturers, comments, revision);

        if (manufacturers != null) {
            for (CableTypeManufacturer manufacturer : manufacturers) {
                manufacturer.setCableType(cableType);
            }
        }

        em.persist(cableType);

        historyService.createHistoryEntry(EntityTypeOperation.CREATE, cableType.getName(), EntityType.CABLE_TYPE,
                cableType.getId(), "", "", userId);

        return cableType;
    }

    /**
     * Updates the attributes on the given cable type.
     *
     * @param cableType
     *            the cable type with modified attributes to save to the database
     * @param oldCableType
     *            the cable type before modification
     * @param userId
     *            username of user updating the cableType, for history record
     * @return true if that cable type was updated, false if the cable type was not updated
     */
    public boolean updateCableType(CableType cableType, CableType oldCableType, String userId) {
        // not update if content is same
        if (cableType.equalsContent(oldCableType)) {
            return false;
        }

        CableType cableTypeByName = getCableTypeByName(cableType.getName());
        if (cableTypeByName != null && !cableType.getId().equals(cableTypeByName.getId())) {
            throw new IllegalArgumentException("Cable type with name '" + cableType.getName() + "' already exists.");
        }

        if (cableType.getManufacturers() != null) {
            for( CableTypeManufacturer cm : cableType.getManufacturers()) {
                if (cm.getDatasheet() == null) {
                    GenericArtifact oldDatasheet =
                            oldCableType.getManufacturers().stream()
                            .filter(m -> m.getId().equals(cm.getId()))
                            .findFirst().orElse(new CableTypeManufacturer())
                            .getDatasheet();
                    if(oldDatasheet != null) {
                        artifactService.deleteArtifact(oldDatasheet);
                    }
                }
            }
        }
        em.merge(cableType);

        historyService.createHistoryEntry(EntityTypeOperation.UPDATE, cableType.getName(), EntityType.CABLE_TYPE,
                cableType.getId(), getChangeString(cableType, oldCableType), "", userId);
        return true;
    }

    /**
     * Changes cable type's status in the database.
     *
     * @param cableType the cable type to modify
     * @param active cable type's new status
     * @param userId username of user modifying the cableType, for history record
     * @return <code>true</code> if the cable type's status was changed, <code>false</code> if the cable type's
     * status was already set to new status, no modification needed
     */
    public boolean changeCableTypeStatus(final CableType cableType, final boolean active, final String userId) {
        em.merge(cableType);

        if (cableType.isActive() == active) {
            return false;
        }

        cableType.setActive(active);
        em.merge(cableType);

        historyService.createHistoryEntry(active ? EntityTypeOperation.RESTORE : EntityTypeOperation.DELETE,
                cableType.getName(), EntityType.CABLE_TYPE, cableType.getId(), "", "", userId);

        return true;
    }

    /**
     * Return an iteration of all distinct voltage values in the database.
     *
     * @param filter
     *            the filter to apply to the returned values
     * @return an iteration of all distinct voltage values in the database
     */
    public Iterable<String> getVoltageValues(CableTypeActiveFilter filter) {
        List<Integer> voltageValues = em
                .createQuery("SELECT DISTINCT(ct.voltage) FROM CableType ct WHERE ct.voltage IS NOT NULL"
                        + getCableTypeActiveFilterConditionClause(filter), Integer.class)
                .getResultList();
        List<String> stringVoltageValues = new ArrayList<>();
        for (Integer voltageValue : voltageValues) {
            stringVoltageValues.add(String.valueOf(voltageValue));
        }
        return stringVoltageValues;
    }

    /**
     * Return an iteration of all distinct insulation values in the database.
     *
     * @param filter
     *            the filter to apply to the returned values
     * @return an iteration of all distinct insulation values in the database
     */
    public Iterable<String> getInsulationValues(CableTypeActiveFilter filter) {
        return getDistinctValues("insulation", filter);
    }

    /**
     * Return an iteration of all distinct jacket values in the database.
     *
     * @param filter
     *            the filter to apply to the returned values
     * @return an iteration of all distinct jacket values in the database
     */
    public Iterable<String> getJacketValues(CableTypeActiveFilter filter) {
        return getDistinctValues("jacket", filter);
    }

    /**
     * Return an iteration of all distinct flammability values in the database.
     *
     * @param filter
     *            the filter to apply to the returned values
     * @return an iteration of all distinct flammability values in the database
     */
    public Iterable<String> getFlammabilityValues(CableTypeActiveFilter filter) {
        return getDistinctValues("flammability", filter);
    }

    private Iterable<String> getDistinctValues(String fieldName, CableTypeActiveFilter filter) {
        return em.createQuery("SELECT DISTINCT(ct." + fieldName + ") FROM CableType ct WHERE ct." + fieldName + " <> ''"
                + getCableTypeActiveFilterConditionClause(filter), String.class).getResultList();
    }

    private String getCableTypeActiveFilterConditionClause(CableTypeActiveFilter filter) {
        String clause = "";
        switch (filter) {
        case ALL:
            // no condition
            break;
        case VALID:
            clause = " AND ct.active = true";
            break;
        case OBSOLETE:
            clause = " AND ct.active = false";
            break;
        default:
        }
        return clause;
    }

    /**
     * Generates and returns string with all changed cable type attributes.
     *
     * @param cableType
     *            new cable type
     * @param oldCableType
     *            old cable type
     *
     * @return string with all changed cable type attributes.
     */
    private String getChangeString(CableType cableType, CableType oldCableType) {
        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.NAME.getColumnLabel(), cableType.getName(), oldCableType.getName()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.DESCRIPTION.getColumnLabel(),
                cableType.getDescription(),
                oldCableType.getDescription()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.SERVICE.getColumnLabel(), cableType.getService(), oldCableType.getService()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.VOLTAGE_RATING.getColumnLabel(), cableType.getVoltage(), oldCableType.getVoltage()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.INSULATION.getColumnLabel(), cableType.getInsulation(), oldCableType.getInsulation()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.JACKET.getColumnLabel(), cableType.getJacket(), oldCableType.getJacket()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.FLAMABILITY.getColumnLabel(),
                cableType.getFlammability(),
                oldCableType.getFlammability()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.INSTALLATION_TYPE.getColumnLabel(),
                cableType.getInstallationType().toString(),
                oldCableType.getInstallationType().toString()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.RADIATION_RESISTANCE.getColumnLabel(), cableType.getTid(), oldCableType.getTid()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.WEIGHT.getColumnLabel(), cableType.getWeight(), oldCableType.getWeight()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.DIAMETER.getColumnLabel(), cableType.getDiameter(), oldCableType.getDiameter()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.MANUFACTURERS.getColumnLabel(),
                cableType.getManufacturersAsString(true),
                oldCableType.getManufacturersAsString(true)));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.COMMENTS.getColumnLabel(), cableType.getComments(), oldCableType.getComments()));
        sb.append(HistoryService.getDiffForAttributes(
                CableTypeColumn.REVISION.getColumnLabel(), cableType.getRevision(), oldCableType.getRevision()));
        return sb.toString();
    }

    /** Performs clearing of the persistance context, thus detaching all entities. */
    public void detachAllCableTypes() {
        em.clear();
    }

    /**
     * Detaches a single cable type from entity manager
     *
     * @param cableType
     *            the cableType to detach
     */
    public void detachCableType(CableType cableType) {
        em.detach(cableType);
    }
}
