/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

/**
 * This represents the column names that are present in the header of the CableArticle table.
 *
 * @author Lars Johansson
 */
public enum CableArticleColumn implements FieldIntrospection {

    // note column label for SHORT_NAME_EXTERNAL_ID
    //     Name instead of SHORT_NAME_EXTERNAL_ID
    MANUFACTURER("MANUFACTURER", "manufacturer", "Manufacturer", true, true, false),
    EXTERNAL_ID("EXTERNAL ID", "externalId", "External ID", true, true, false),
    ERP_NUMBER("ERP NUMBER", "erpNumber", "ERP Number", true, true, false),
    ISO_CLASS("ISO CLASS", "isoClass", "ISO Class", true, true, false),
    DESCRIPTION("DESCRIPTION", "description", "Description", true, true, false),
    LONG_DESCRIPTION("LONG DESCRIPTION", "longDescription", "Long Description", true, true, false),
    MODEL_TYPE("MODEL TYPE", "modelType", "Model Type", true, true, false),
    BENDING_RADIUS("BENDING RADIUS", "bendingRadius", "Bending Radius (mm)", true, false, false),
    OUTER_DIAMETER("OUTER DIAMETER", "outerDiameter", "Outer Diameter (mm)", true, false, false),
    RATED_VOLTAGE("RATED VOLTAGE", "ratedVoltage", "Rated Voltage (V)", true, false, false),
    WEIGHT_PER_LENGTH("WEIGHT PER LENGTH", "weightPerLength", "Weight Per Length (kg/km)", true, false, false),
    SHORT_NAME("SHORT NAME", "shortName", "Short Name", true, true, false),
    SHORT_NAME_EXTERNAL_ID("SHORT NAME.EXTERNAL ID", "shortNameExternalId", "Name", true, true, false),
    URL_CHESS_PART("URL CHESS PART", "urlChessPart", "URL Chess Part", true, true, false),
    STATUS("STATUS", "status", "Status", false, false, true);

    /*
     * isExcelColumn
     *      presence as column/field in Excel sheet
     * isExcelVolatileColumn
     *      presence as column/field in Excel sheet but volatile, i.e. presence not guaranteed
     *      if not considered column in Excel, then value does not matter
     */

    private final String stringValue;
    private final String fieldName;
    private final String columnLabel;
    private final boolean isExcelColumn;
    private final boolean isStringComparisonOperator;
    private final boolean isExcelVolatileColumn;

    private CableArticleColumn(String stringValue, String fieldName, String columnLabel, boolean isExcelColumn,
            boolean isStringComparisonOperator, boolean isExcelVolatileColumn) {
        this.stringValue = stringValue;
        this.fieldName = fieldName;
        this.columnLabel = columnLabel;
        this.isExcelColumn = isExcelColumn;
        this.isStringComparisonOperator = isStringComparisonOperator;
        this.isExcelVolatileColumn = isExcelVolatileColumn;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public boolean isExcelColumn() {
        return isExcelColumn;
    }

    public boolean isStringComparisonOperator() {
        return isStringComparisonOperator;
    }

    public boolean isExcelVolatileColumn() {
        return isExcelVolatileColumn;
    }

    /**
     * Return cable article column given column label.
     *
     * @param columnLabel column label
     * @return cable article column
     */
    public static CableArticleColumn convertColumnLabel(String columnLabel) {
        if (MANUFACTURER.getColumnLabel().equals(columnLabel)) {
            return MANUFACTURER;
        } else if (EXTERNAL_ID.getColumnLabel().equals(columnLabel)) {
            return EXTERNAL_ID;
        } else if (ERP_NUMBER.getColumnLabel().equals(columnLabel)) {
            return ERP_NUMBER;
        } else if (ISO_CLASS.getColumnLabel().equals(columnLabel)) {
            return ISO_CLASS;
        } else if (DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return DESCRIPTION;
        } else if (LONG_DESCRIPTION.getColumnLabel().equals(columnLabel)) {
            return LONG_DESCRIPTION;
        } else if (MODEL_TYPE.getColumnLabel().equals(columnLabel)) {
            return MODEL_TYPE;
        } else if (BENDING_RADIUS.getColumnLabel().equals(columnLabel)) {
            return BENDING_RADIUS;
        } else if (OUTER_DIAMETER.getColumnLabel().equals(columnLabel)) {
            return OUTER_DIAMETER;
        } else if (RATED_VOLTAGE.getColumnLabel().equals(columnLabel)) {
            return RATED_VOLTAGE;
        } else if (WEIGHT_PER_LENGTH.getColumnLabel().equals(columnLabel)) {
            return WEIGHT_PER_LENGTH;
        } else if (SHORT_NAME.getColumnLabel().equals(columnLabel)) {
            return SHORT_NAME;
        } else if (SHORT_NAME_EXTERNAL_ID.getColumnLabel().equals(columnLabel)) {
            return SHORT_NAME_EXTERNAL_ID;
        } else if (URL_CHESS_PART.getColumnLabel().equals(columnLabel)) {
            return URL_CHESS_PART;
        } else if (STATUS.getColumnLabel().equals(columnLabel)) {
            return STATUS;
        }
        return null;
    }

}
