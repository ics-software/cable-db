/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.CableArticle;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the service layer that handles approved cable article import and export operations.
 *
 * @author Lars Johansson
 */
@Stateless
public class CableArticleImportExportService {

    private static final Logger LOGGER = Logger.getLogger(CableArticleImportExportService.class.getName());

    private static final String NO_IMPORT_PERMISSION = "You do not have permission to import cable articles.";
    private static final String NO_EXPORT_PERMISSION = "You do not have permission to export cable articles.";

    @Resource
    private EJBContext context;
    @Inject
    private CableArticleLoader cableArticleLoader;

    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;
    @Inject
    private MailService mailService;

    /**
     * Import cable articles from input stream.
     *
     * @param inputStream input stream representing Excel sheet content
     * @return result for import of cable articles
     */
    public LoaderResult<CableArticle> importCableArticles(InputStream inputStream) {
        return importCableArticles(inputStream, false);
    }

    /**
     * Import cable articles from input stream. May be test import in which case, data is examined but not imported.
     *
     * @param inputStream input stream representing Excel sheet content
     * @param test if test import
     * @return result for import of cable articles
     */
    public LoaderResult<CableArticle> importCableArticles(InputStream inputStream, boolean test) {
        // We read inputStream into byte as we need to use it twice.
        byte[] bytes;
        try {
            bytes = ByteStreams.toByteArray(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ByteArrayInputStream byteInputstream = new ByteArrayInputStream(bytes);

        if (!canImportCableArticles()) {
            throw new IllegalStateException(NO_IMPORT_PERMISSION);
        }

        LoaderResult<CableArticle> report = cableArticleLoader.load(byteInputstream, test);

        if (!test && !report.isError()) {
            // Send mail
            final Set<String> notifiedUsers = new HashSet<>();
            for (final String userName : userDirectoryServiceFacade.getAllAdministratorUsernames()) {
                notifiedUsers.add(userName);
            }

            if (!Lists.newArrayList(report.getAffected()).isEmpty()) {
                String content = "Cable articles have been imported in the Cable Database" + " (by "
                        + userDirectoryServiceFacade.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")";
                byteInputstream.reset();
                mailService.sendMail(notifiedUsers,
                        sessionService.getLoggedInName(),
                        "Cable articles imported notification",
                        content,
                        Arrays.asList(byteInputstream),
                        Arrays.asList("cdb_cable_articles_imported.xlsx"),
                        true,
                        true);
            }
        } else {
            context.setRollbackOnly();
        }

        LOGGER.fine("Returning result: " + report);

        return report;
    }

    /**
     * Exports cable articles to an Excel spreadsheet.
     *
     * @param cableArticles the cable articles to export
     * @return the input stream representing the Excel file
     *
     * @throws IllegalStateException if current user does not have permission for this action
     */
    public InputStream exportCableArticles(Iterable<CableArticle> cableArticles) {

        if (!sessionService.isLoggedIn()) {
            throw new IllegalStateException(NO_EXPORT_PERMISSION);
        }

        final CableArticleSaver cableArticleSaver = new CableArticleSaver(cableArticles);
        return cableArticleSaver.save();
    }

    /**
     * Returns true if the current user can import cable articles, else false.
     *
     * @return true if the current user can import cable articles, else false
     */
    public boolean canImportCableArticles() {
        return sessionService.canAdminister();
    }

}
