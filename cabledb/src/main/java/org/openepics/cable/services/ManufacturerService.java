/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.EntityTypeOperation;
import org.openepics.cable.model.Manufacturer;
import org.openepics.cable.model.ManufacturerStatus;
import org.openepics.cable.model.Query;
import org.openepics.cable.services.dl.ManufacturerColumn;

/**
 * <code>ManufacturerService</code> is the service layer that handles individual manufacturer operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class ManufacturerService {

    private static final Logger LOGGER = Logger.getLogger(ManufacturerService.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Inject
    private HistoryService historyService;

    /** @return a list of all manufacturers */
    public List<Manufacturer> getManufacturers() {
        return em.createQuery("SELECT c FROM Manufacturer c", Manufacturer.class).getResultList();
    }

    /**
     * Return a list of filtered manufacturers.
     *
     * @param customQuery
     *            custom query for filtering manufacturers
     * @param query
     *            query to update
     *
     * @return a list of filtered manufacturers
     */
    public List<Manufacturer> getFilteredManufacturers(String customQuery, Query query) {
        List<Manufacturer> filteredManufacturers = new ArrayList<Manufacturer>();
        if (customQuery != null && !customQuery.isEmpty()) {
            filteredManufacturers = em.createQuery(customQuery, Manufacturer.class).getResultList();
        }
        return filteredManufacturers;
    }

    /**
     * Returns all manufacturers for which at least one of the specified fields matches the given regular expression.
     *
     * @param fields
     *            the list of string fields to check
     * @param regExp
     *            regular expression containing only literals, wildcard (*) and single character (?)
     *
     * @return a list of matching manufacturers
     */
    public List<Manufacturer> getFilteredManufacturers(Collection<ManufacturerColumn> fields, String regExp) {
        // convert regular expression to JPA SQL LIKE format
        if (regExp != null && !regExp.isEmpty()) {
            regExp = regExp.replace('*', '%').replace('?', '_');
        } else {
            regExp = "%";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT c FROM Manufacturer c");

        boolean first = true;
        for (ManufacturerColumn field : fields) {

            if (first) {
                stringBuilder.append(" WHERE");
                first = false;
            } else {
                stringBuilder.append(" OR");
            }

            stringBuilder.append(field.getFieldName());
            stringBuilder.append(" LIKE :regExp");
        }

        String query = stringBuilder.toString();

        return em.createQuery(query, Manufacturer.class).setParameter("regExp", regExp).getResultList();
    }

    /**
     * Returns a Manufacturer with the specified code.
     *
     * @param code
     *            code of the manufacturer
     * @return the manufacturer, or null if the manufacturer with such code cannot be found
     */
    public Manufacturer getManufacturerByCode(String code) {
        try {
            return em.createQuery("SELECT c FROM Manufacturer c WHERE c.code = :code", Manufacturer.class)
                    .setParameter("code", code).getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Manufacturer with code : " + code + " not found.", e);
            return null;
        }
    }

    /**
     * Returns a Manufacturer with the specified name.
     *
     * @param name
     *            name of the manufacturer
     * @return the manufacturer, or null if the manufacturer with such name cannot be found
     */
    public Manufacturer getManufacturer(String name) {
        try {
            return em.createQuery("SELECT c FROM Manufacturer c WHERE c.name = :name", Manufacturer.class)
                    .setParameter("name", name).getSingleResult();
        } catch (IllegalArgumentException | NoResultException e) {
            LOGGER.log(Level.FINEST, "Manufacturer with name: " + name + " not found", e);
            return null;
        }
    }

    /**
     * Created manufacturer.
     *
     * @param name
     *            of the manufacturer
     * @param address
     *            the manufacturer address
     * @param phoneNumber
     *            the contact number of the manufacturer
     * @param email
     *            the contact email of the manufacturer
     * @param country
     *            the country of the manufacturer
     * @param userId
     *            username of user creating the manufacturer, for history record
     *
     * @return a new Manufacturer instance.
     */
    public Manufacturer createManufacturer(String name, String address, String phoneNumber, String email,
            String country, String userId) {

        final Date created = new Date();
        final Date modified = created;
        final Manufacturer manufacturer = new Manufacturer(name, address, phoneNumber, email, country, created,
                modified);
        em.persist(manufacturer);

        historyService.createHistoryEntry(EntityTypeOperation.CREATE, manufacturer.getName(), EntityType.MANUFACTURER,
                manufacturer.getId(), "", "", userId);

        return manufacturer;
    }

    /**
     * Updates the attributes on the given manufacturer.
     *
     * @param manufacturer
     *            the manufacturer with modified attributes to save to the database
     * @param oldManufacturer
     *            the manufacturer before modification
     * @param userId
     *            username of user updating the manufacturer, for history record
     * @return true if that manufacturer was updated, false if the manufacturer was not updated
     */
    public boolean updateManufacturer(Manufacturer manufacturer, Manufacturer oldManufacturer, String userId) {
        // not update if content is same
        if (manufacturer.equals(oldManufacturer)) {
            return false;
        }

        manufacturer.setModified(new Date());
        em.merge(manufacturer);

        historyService.createHistoryEntry(EntityTypeOperation.UPDATE, manufacturer.getName(), EntityType.MANUFACTURER,
                manufacturer.getId(), getChangeString(manufacturer, oldManufacturer), "", userId);
        return true;
    }

    /**
     * Marks the manufacturer deleted in the database.
     *
     * @param manufacturer
     *            the manufacturer to delete
     * @param userId
     *            username of user deleting the manufacturer, for history record
     *
     * @return true if the manufacturer was deleted, false if the cable was already deleted
     */
    public boolean deleteManufacturer(Manufacturer manufacturer, String userId) {
        if (manufacturer.getStatus() == ManufacturerStatus.DELETED) {
            return false;
        }

        manufacturer.setStatus(ManufacturerStatus.DELETED);
        manufacturer.setModified(new Date());
        em.merge(manufacturer);

        historyService.createHistoryEntry(EntityTypeOperation.DELETE, manufacturer.getName(), EntityType.MANUFACTURER,
                manufacturer.getId(), "", "", userId);
        return true;
    }

    /**
     * Generates and returns string with all changed manufacturer attributes.
     *
     * @param manufacturer
     *            new manufacturer
     * @param oldManufacturer
     *            old manufacturer
     *
     * @return string with all changed manufacturer attributes.
     */
    private String getChangeString(Manufacturer manufacturer, Manufacturer oldManufacturer) {
        StringBuilder sb = new StringBuilder(900);
        sb.append(HistoryService.getDiffForAttributes(
                ManufacturerColumn.NAME.getColumnLabel(), manufacturer.getName(), oldManufacturer.getName()));
        sb.append(HistoryService.getDiffForAttributes(
                ManufacturerColumn.ADDRESS.getColumnLabel(), manufacturer.getAddress(), oldManufacturer.getAddress()));
        sb.append(HistoryService.getDiffForAttributes(
                ManufacturerColumn.PHONE.getColumnLabel(),
                manufacturer.getPhoneNumber(),
                oldManufacturer.getPhoneNumber()));
        sb.append(HistoryService.getDiffForAttributes(
                ManufacturerColumn.EMAIL.getColumnLabel(), manufacturer.getEmail(), oldManufacturer.getEmail()));
        sb.append(HistoryService.getDiffForAttributes(
                ManufacturerColumn.COUNTRY.getColumnLabel(), manufacturer.getCountry(), oldManufacturer.getCountry()));
        return sb.toString();
    }
}
