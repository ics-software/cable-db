/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services.dl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.openepics.cable.model.Cable;
import org.openepics.cable.model.CableStatus;
import org.openepics.cable.model.EntityType;
import org.openepics.cable.model.Query;
import org.openepics.cable.model.QueryBooleanOperator;
import org.openepics.cable.model.QueryComparisonOperator;
import org.openepics.cable.model.QueryCondition;
import org.openepics.cable.model.QueryParenthesis;
import org.openepics.cable.services.MailService;
import org.openepics.cable.services.SessionService;
import org.openepics.cable.services.UserDirectoryServiceFacade;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

/**
 * This is the service layer that handles cable import and export operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Stateless
public class CableImportExportService {

    private static final Logger LOGGER = Logger.getLogger(CableImportExportService.class.getName());

    private static final String NO_IMPORT_PERMISSION = "You do not have permission to import cables.";
    private static final String NO_EXPORT_PERMISSION = "You do not have permission to export cables.";

    @Resource
    private EJBContext context;
    @Inject
    private CableLoader cableLoader;

    @Inject
    private SessionService sessionService;
    @Inject
    private UserDirectoryServiceFacade userDirectoryServiceFacade;

    @Inject
    private MailService mailService;

    /**
     * Imports cables from Excel spreadsheet.
     *
     * @param inputStream the input stream representing the Excel file
     * @return the report with validation state and messages
     *
     * @throws IllegalStateException if current user does not have permission for this action
     */
    public LoaderResult<Cable> importCables(InputStream inputStream) {
        return importCables(inputStream, false);
    }

    /**
     * Imports cables from Excel spreadsheet.
     *
     * @param inputStream the input stream representing the Excel file
     * @param test if true, only a test whether the import would succeed is performed, and no cables are imported
     * @return the report with validation state and messages
     *
     * @throws IllegalStateException if current user does not have permission for this action
     */
    public LoaderResult<Cable> importCables(InputStream inputStream, boolean test) {
        byte[] bytes;
        try {
            bytes = ByteStreams.toByteArray(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ByteArrayInputStream byteInputstream = new ByteArrayInputStream(bytes);

        if (!canImportCables()) {
            throw new IllegalStateException(NO_IMPORT_PERMISSION);
        }

        LoaderResult<Cable> report = cableLoader.load(byteInputstream, test);
        // Send mail
        if (!test && !report.isError()) {
            // keep track of imported systems
            Set<String> systems = new TreeSet<>();

            final List<Cable> uploadedCables = new ArrayList<>();
            for (Cable cable : report.getAffected()) {
                uploadedCables.add(cable);
                systems.add(cable.getSystem());
            }

            // prepare subject in email
            String subject = "Cables imported notification";
            String subjectSystems = "";
            if (!systems.isEmpty()) {
                Iterator<String> iter = systems.iterator();
                while (iter.hasNext()) {
                    subjectSystems += iter.next() + " ";
                }
                subject = "[System " + subjectSystems.trim() + "] " + subject;
            }

            List<InputStream> attachments = new ArrayList<InputStream>();
            List<String> filenames = new ArrayList<String>();
            attachments.add(exportCables(uploadedCables));
            filenames.add("cdb_cables_numbered.xlsx");
            attachments.add(byteInputstream);
            filenames.add("cdb_cables_imported.xlsx");

            final Set<String> notifiedUsers = new HashSet<>();
            for (final Cable cable : report.getAffected()) {
                for (final String userName : cable.getOwners()) {
                    notifiedUsers.add(userName);
                }
            }
            for (final String userName : userDirectoryServiceFacade.getAllAdministratorUsernames()) {
                notifiedUsers.add(userName);
            }

            if (!Lists.newArrayList(report.getAffected()).isEmpty()) {
                String content = "Cables have been imported in the Cable Database" + " (by "
                        + userDirectoryServiceFacade.getUserFullNameAndEmail(sessionService.getLoggedInName()) + ")";
                byteInputstream.reset();
                mailService.sendMail(notifiedUsers, sessionService.getLoggedInName(), subject,
                        content, attachments, filenames, true, true);
            }
        } else {
            context.setRollbackOnly();
        }

        LOGGER.fine("Returning result: " + report);

        return report;
    }

    /**
     * Exports cables to an Excel spreadsheet.
     *
     * @param cables
     *            the cables to export
     * @return the input stream representing the Excel file
     *
     * @throws IllegalStateException
     *             if current user does not have permission for this action
     */
    public InputStream exportCables(Iterable<Cable> cables) {

        if (!sessionService.isLoggedIn()) {
            throw new IllegalStateException(NO_EXPORT_PERMISSION);
        }

        // perform fast validation before exporting to avoid exporting obsolete data
        //     validation before export disabled to improve performance

        final CableSaver cableSaver = new CableSaver(cables);
        return cableSaver.save();
    }

    /** @return true if the current user can import cables, else false */
    public boolean canImportCables() {
        return sessionService.canAdminister() || sessionService.canManageOwnedCables();
    }

    /**
     * Constructs a new query for filtering out deleted cables and prepends it with passed query.
     *
     * @param query
     *            current query to which we want to add not deleted conditions
     * @return a new query with all conditions of the passed query and added condition for filtering out deleted cables
     */
    public Query getNotDeletedQuery(Query query) {
        List<QueryCondition> conditions;
        if (query != null && query.getConditions() != null) {
            conditions = query.getConditions();
        } else {
            conditions = new ArrayList<QueryCondition>();
        }
        QueryCondition notDeletedCondition = new QueryCondition(null, QueryParenthesis.NONE,
                CableColumn.STATUS.getColumnLabel(), QueryComparisonOperator.NOT_EQUAL,
                CableStatus.DELETED.getDisplayName(), QueryParenthesis.NONE, QueryBooleanOperator.NONE,
                conditions.size());
        conditions.add(notDeletedCondition);

        query = new Query(null, EntityType.CABLE, null, null, conditions);
        return query;
    }
}
