/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.services.dl;

import org.openepics.cable.model.Connector;

import java.io.InputStream;

/**
 * This saves {@link Connector} instances to Excel spreadsheet.
 *
 * @author Imre Toth <imre.toth@esss.se>
 */
public class ConnectorSaver extends DataSaverExcelStream<Connector> {
    /**
     * Constructs this object and stores the data objects.
     *
     * @param objects the objects to save
     */
    protected ConnectorSaver(Iterable<Connector> objects) {
        super(objects);

        saveAndValidate();
    }

    @Override
    protected InputStream getTemplate() {
        return this.getClass().getResourceAsStream("/templates/cdb_connectors.xlsx");
    }

    @Override
    protected void save(Connector connector) {
        setCommand(DSCommand.UPDATE);

        setValue(ConnectorColumn.NAME, connector.getName());
        setValue(ConnectorColumn.DESCRIPTION, connector.getDescription());
        setValue(ConnectorColumn.TYPE, connector.getType());
        setValue(ConnectorColumn.ASSEMBLY_INSTRUCTIONS, connector.getAssemblyInstructions());
        setValue(ConnectorColumn.LINK_TO_DATASHEET, connector.getLinkToDatasheet());
        setValue(ConnectorColumn.MANUFACTURERS, connector.getManufacturersAsString(false));
    }
}
