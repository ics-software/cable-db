/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facilitty for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.util.CellRangeAddress;

/**
 * This represents a record stream. It reads from {@link DataStream} and packs the data into {@link DSRecord}s.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class RecordStream {

    private static final boolean LOGGING = false;
    private static final Logger LOGGER = Logger.getLogger(RecordStream.class.getName());

    private static final String ADDED_A_COLUMN_HEADER = "Added a column header ";

    private boolean endOfStream;
    private DataStream dataStream;
    private DSHeader header = null;
    private DSRecord currentRecord = null;

    private Iterable<String> urlColumnNames;

    /**
     * Constructs a record stream.
     *
     * @param urlColumnNames
     *            The list of column names that can contain URL links, used for spreadsheet parsing optimization.
     */
    public RecordStream(Iterable<String> urlColumnNames) {
        super();
        this.urlColumnNames = urlColumnNames;
    }

    /**
     * Opens the given stream to read records from. After this call, this object owns the stream.
     *
     * @param stream
     *            the stream to read from
     */
    public void open(InputStream stream) {
        dataStream = new DataStreamExcel();
        dataStream.open(stream);
        currentRecord = new DSRecord();
        endOfStream = false;
        header = new DSHeader();
    }

    private void setHeader() {
        int colCount = dataStream.getRowSize();
        String colName;

        // clear old header, if any
        header.clear();

        // check if header is merged
        if (!dataStream.containsMergedCells()) {
            for (int c = 1; c < colCount; c++) {
                colName = dataStream.getCell(c).toString();
                if (colName != null && !colName.trim().isEmpty()) {
                    header.setColumn(colName, c, dataStream.getColumnLabel(c));
                    if (LOGGING)
                        LOGGER.log(Level.FINER, ADDED_A_COLUMN_HEADER + colName);
                }
            }
        } else {
            List<CellRangeAddress> ranges = dataStream.getMergedRegions();

            // Checking if all cells are included in merged regions and adding them otherwise
            for (int c = 1; c < colCount; c++) {
                boolean contained = false;
                for (CellRangeAddress range : ranges) {
                    if (range.isInRange(dataStream.getRowIndex(), c)) {
                        contained = true;
                        break;
                    }
                }
                if (!contained) {
                    ranges.add(new CellRangeAddress(dataStream.getRowIndex(), dataStream.getRowIndex(), c, c));
                }
            }
            // count merged rows
            int mergedRows = 0;
            for (CellRangeAddress range : ranges) {
                mergedRows = Math.max(mergedRows, range.getLastRow() - range.getFirstRow() + 1);
            }
            for (CellRangeAddress range : ranges) {
                // read header for headers with 2 merged rows and 1 column
                if (range.getFirstColumn() == range.getLastColumn() && range.getFirstRow() != range.getLastRow()) {
                    int c = range.getFirstColumn() == 0 ? 1 : range.getFirstColumn();
                    colName = dataStream.getCell(range.getFirstRow(), c).toString();
                    if (colName != null && !colName.trim().isEmpty()) {
                        header.setColumn(colName, c, dataStream.getColumnLabel(c));
                        if (LOGGING)
                            LOGGER.log(Level.FINER, ADDED_A_COLUMN_HEADER + colName);
                    }
                    // read header for headers with 2 separate rows and 1 or more columns
                } else {
                    String prefix = dataStream.getCell(range.getFirstRow(), range.getFirstColumn()).toString();
                    if (prefix == null || prefix.trim().isEmpty()) {
                        continue;
                    }
                    int firstColumn = range.getFirstColumn() == 0 ? 1 : range.getFirstColumn();
                    for (int c = firstColumn; c < range.getLastColumn() + 1; c++) {
                        StringBuilder sb = new StringBuilder(prefix).append(":");
                        sb.append(dataStream.getCell(range.getFirstRow() + mergedRows - 1, c).toString());
                        colName = sb.toString();
                        if (colName != null && !colName.trim().isEmpty()) {
                            header.setColumn(colName, c, dataStream.getColumnLabel(c));
                            if (LOGGING)
                                LOGGER.log(Level.FINER, ADDED_A_COLUMN_HEADER + colName);
                        }
                    }
                }
            }
            // move to the next row (which is not a header)
            for (int i = 1; i < mergedRows; i++) {
                dataStream.nextRow();
            }
        }
        dataStream.setUrlColumns(getUrlColumns(header));
    }

    private List<Integer> getUrlColumns(DSHeader header) {
        List<Integer> urlColumns = new ArrayList<Integer>();
        for (String columnName : urlColumnNames) {
            try {
                urlColumns.add(header.getColumnIndex(columnName));
            } catch (Exception e) {
                LOGGER.log(Level.FINEST, "Column " + columnName + " is not present in imported EXCEL template.", e);
            }
        }
        return urlColumns;
    }

    private void readRecord() {
        if (currentRecord.getCommand() == DSCommand.NONE)
            // ignore empty commands
            return;

        if (!header.hasDefinitions()) {
            if (LOGGING)
                LOGGER.log(Level.WARNING, "Header not set. Ignoring record.");
            throw new IllegalStateException("Cannot read record without header");
        }

        currentRecord.clear();
        for (String columnName : header.getColumnNames()) {
            Object value = dataStream.getCell(header.getColumnIndex(columnName));
            if (value != null) {
                currentRecord.setField(columnName, value);
                if (LOGGING)
                    LOGGER.finer(columnName + ":" + value + ":");
            }
        }
    }

    private boolean readNextRecord() {
        if (dataStream.isEndOfStream()) {
            endOfStream = true;
            currentRecord.setCommand(DSCommand.NONE);
            return false;
        }

        dataStream.nextRow();
        String command = dataStream.getCell(0).toString();

        if (LOGGING)
            LOGGER.log(Level.FINER, "Input command: " + command);
        // check if it is a control record i.e. starts with OPERATION, CREATE, UPDATE, DELETE etc
        if (command.equals(DSCommand.OPERATION.toString())) {
            currentRecord.setCommand(DSCommand.OPERATION);
            setHeader();
        } else if (command.equals(DSCommand.CREATE.toString())) {
            currentRecord.setCommand(DSCommand.CREATE);
        } else if (command.equals(DSCommand.UPDATE.toString())) {
            currentRecord.setCommand(DSCommand.UPDATE);
        } else if (command.equals(DSCommand.DELETE.toString())) {
            currentRecord.setCommand(DSCommand.DELETE);
        } else {
            currentRecord.setCommand(DSCommand.NONE);
        }

        currentRecord.setRowIndex(dataStream.getRowIndex());
        currentRecord.setRowLabel(dataStream.getRowLabel());

        readRecord();

        return true;
    }

    /**
     * Reads and returns the next record.
     *
     * @return the record or null if no next record
     */
    public DSRecord getNextRecord() {
        if (endOfStream) {
            return null;
        } else {
            currentRecord = new DSRecord();
            readNextRecord();
            return currentRecord;
        }
    }

    /** @return true if there is next record, else false */
    public boolean hasNextRecord() {
        return !endOfStream;
    }

    /** Closes the stream releasing all resources associated to it. */
    public void close() {
        if (dataStream != null) {
            dataStream.close();
        }
        currentRecord = null;
    }

    /**
     * Returns the column label from header definition currently in use.
     *
     * @param name
     *            the name of the column
     * @return the label
     */
    public String getColumnLabel(String name) {
        return header.getColumnLabel(name);
    }

    /**
     * Returns the last processed header. This method can be called after the stream closes.
     *
     * @return the header
     */
    public DSHeader getHeader() {
        return header;
    }
}
