/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.services.dl;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.cable.model.Persistable;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Abstract super class for store and save data to Excel.
 *
 * @author Zoltan Runyo <zoltan.runyo@esss.se>
 *
 * @param <T> type
 */
public abstract class DataSaverExcelStream<T> extends DataSaverExcelBase<T>  {

    private Iterable<T> objects;
    private List<? extends DataValidation> dataValidations;

    /**
     * Constructs this object and stores the data objects.
     *
     * @param objects
     *            the objects to save
     */
    protected DataSaverExcelStream(Iterable<T> objects) {
        this.objects = objects;

        try {
            workbook = new SXSSFWorkbook(new XSSFWorkbook(getTemplate()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // process the template with loader to obtain required template information
        final DataLoader<Persistable> dataLoader = new DataLoader<>();
        final LoaderResult<Persistable> result = dataLoader.load(getTemplate());
        header = result.getHeader();
        maxRowIndex = workbook.getSpreadsheetVersion().getMaxRows() - 1;
        endRowIndex = result.getHeaderIndex() + 1;
        currentRow = endRowIndex;

        sheet = workbook.getSheetAt(0);
        dataValidations = sheet.getDataValidations();

        drawing = sheet.createDrawingPatriarch();
        creationHelper = workbook.getCreationHelper();
        anchor = creationHelper.createClientAnchor();

        unlockedCellStyle = getUnlockedCellStyle();
        hyperlinkCellStyle = getHyperlinkCellStyle();
        dateCellStyle = getDateCellStyle();
        unlockedHighlightCellStyle = getUnlockedCellStyle(true);
        hyperlinkHighlightCellStyle = getHyperlinkCellStyle(true);
        dateHighlightCellStyle = getDateCellStyle(true);

        setColumnsValidValues();

        // subclasses MUST call saveAndValidate() at end of constructor
        //     allow subclasses to receive additional input parameters in constructor
        //     that may be used in implementation of abstract and other methods

    }

    protected void saveAndValidate() {
        for (T object : objects) {
            save(object);
            currentRow++;
        }

        updateValidations();
    }

    /**
     * Returns the template excel file to be used to save data to.
     *
     * @return the input stream from which to read the template
     */
    protected abstract InputStream getTemplate();

    protected void addCustomValidationModification(DataValidation dataValidation) {}

    private void updateValidations() {
        DataValidationHelper dataValidationHelper = sheet.getDataValidationHelper();
        for (DataValidation dataValidation : dataValidations) {
            String formula = dataValidation.getValidationConstraint().getFormula1();
            if (!StringUtils.isEmpty(formula)) {
                for(CellRangeAddress cellRangeAddress : dataValidation.getRegions().getCellRangeAddresses()) {
                    cellRangeAddress.setFirstRow(endRowIndex);
                    cellRangeAddress.setLastRow(maxRowIndex);
                }

                addCustomValidationModification(dataValidation);

                sheet.addValidationData(dataValidationHelper.createValidation(dataValidation.getValidationConstraint(),
                        dataValidation.getRegions()));
            }
        }
    }
}

