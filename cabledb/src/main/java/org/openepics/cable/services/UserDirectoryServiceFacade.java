/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.services;

import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * <code>UserDirectoryServiceFacade</code> is the service facade layer
 * that handles individual user directory operations.
 *
 * Note that class acts as service facade to underlying user directory service,
 * essentially relaying operations, without consideration to speed of operations.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 * @author Lars Johansson
 */
@Stateless
public class UserDirectoryServiceFacade {

    @Inject
    private transient UserDirectoryService userDirectoryService;

    /**
     * Returns a set of all user names currently registered in the directory.
     *
     * @return the set
     */
    public Set<String> getAllUsernames() {
        return userDirectoryService.getAllUsernames();
    }

    /**
     * Returns a set of names of all users that have administer permission of the cable database.
     *
     * @return the set
     */
    public Set<String> getAllAdministratorUsernames() {
        return userDirectoryService.getAllAdministratorUsernames();
    }

    /**
     * Retrieves the email address for the given user.
     *
     * @param username
     *            the user name
     * @return the user email
     */
    public String getEmail(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getEmail(username);
    }

    /**
     * Retrieves the full name of the given user.
     *
     * @param username
     *            the user name
     * @return the user full name
     */
    public String getUserFullName(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getUserFullName(username);
    }

    /**
     * Retrieves the full name and email of the user in the form Name Surname - mail@mail.com
     *
     * @param username
     *            the user name
     * @return the user full name and email
     */
    public String getUserFullNameAndEmail(String username) {
        // do not do any user data caching for simplicity
        return userDirectoryService.getUserFullNameAndEmail(username);
    }
}
