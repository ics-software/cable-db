/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This software is Copyright by the Board of Trustees of Michigan
 * State University (c) Copyright 2012.
 *
 * You may use this software under the terms of the GNU public license
 *  (GPL). The terms of this license are described at:
 *       http://www.gnu.org/licenses/gpl.txt
 *
 * Contact Information:
 *   Facilitty for Rare Isotope Beam
 *   Michigan State University
 *   East Lansing, MI 48824-1321
 *   http://frib.msu.edu
 */
package org.openepics.cable.services.dl;

import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Column;

import org.apache.mina.core.RuntimeIoException;
import org.openepics.cable.model.Persistable;
import org.openepics.cable.services.dl.LoaderResult.ImportFileStatistics;

import com.google.common.base.Preconditions;

/**
 * This is a base class that processes records from {@link RecordStream}. Any record consumer should extend this class.
 *
 * Base implementation reads through the whole stream, but does nothing with the records.
 *
 * @param <T>
 *            Class of the objects that are affected when processing records.
 *
 * @author <a href="mailto:vuppala@frib.msu.org">Vasu Vuppala</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class DataLoader<T extends Serializable> {

    private static final Logger LOGGER = Logger.getLogger(DataLoader.class.getName());

    private static final int RECORD_LIST_SIZE = 1000;

    private static final int DEFAULT_VARCHAR_LENGTH = 255;

    private RecordStream recordStream;

    private LoaderResult<T> loaderReport;
    protected boolean test;
    protected boolean stopLoad;
    protected LoaderResult<T> report;

    // for statistic
    protected int dataRows;
    protected int createRows;
    protected int updateRows;
    protected int deleteRows;
    protected int createRowsTest;
    protected int updateRowsTest;
    protected int deleteRowsTest;

    /**
     * Processes the record.
     *
     * Extend this method to process the record. This method does nothing.
     *
     * @param record
     *            the record to process
     */
    protected void updateRecord(DSRecord record) {
        // base implementation does nothing
    }

    /**
     * Processes a number of records.
     *
     * Extend this method to process a number of records at the same time. Base method calls updateRecord for each
     * individual record.
     *
     * @param records
     *            the records to process
     */
    protected void updateRecordList(Iterable<DSRecord> records) {
        for (DSRecord record : records) {
            updateRecord(record);
            if (report.isError() && !test || stopLoad) {
                return;
            }
        }
    }

    /**
     * Constructs a generic data loader.
     */
    public DataLoader() {
        this(Collections.<String> emptyList());
    }

    /**
     * Constructs a generic data loader.
     *
     * @param urlColumns
     *            The list of column names that can contain URL links, used for spreadsheet parsing optimization.
     */
    public DataLoader(Iterable<String> urlColumns) {
        super();
        recordStream = new RecordStream(urlColumns);
    }

    /**
     * Loads all the records from the stream and processes them. The stream is closed on completion.
     *
     * @param stream
     *            the stream to read data from
     *
     * @return the report for the load
     */
    public LoaderResult<T> load(InputStream stream) {
        return load(stream, false);
    }

    /**
     * Loads all the records from the stream and processes them. The stream is closed on completion.
     *
     * @param stream
     *            the stream to read data from
     * @param test
     *            if true, only a test whether the load would succeed is performed, and no data is imported to the
     *            database.
     *
     * @return the report for the load
     */
    public LoaderResult<T> load(InputStream stream, boolean test) {
        this.test = test;
        stopLoad = false;
        dataRows = createRows = updateRows = deleteRows = 0;
        createRowsTest = updateRowsTest = deleteRowsTest = 0;
        recordStream.open(stream);
        loaderReport = new LoaderResult<T>();
        List<DSRecord> records = new ArrayList<>();
        boolean loading = !stopLoad && recordStream.hasNextRecord();
        while (loading) {
            report = new LoaderResult<T>();

            DSRecord record = recordStream.getNextRecord();
            if (record == null) {
                LOGGER.log(Level.WARNING, "Record should not be null but is");
                continue;
            }
            LOGGER.fine("Processing record " + record.getCommand());
            switch (record.getCommand()) {
            // not command
            case NONE:
                break;
            // header command
            case OPERATION:
                report.setHeader(recordStream.getHeader());
                report.setHeaderIndex(record.getRowIndex());
                break;
            // update record
            case CREATE:
                dataRows++;
                records.add(record);
                break;
            // update record
            case UPDATE:
                dataRows++;
                records.add(record);
                break;
            // delete record
            case DELETE:
                dataRows++;
                records.add(record);
                break;
            default:
                LOGGER.log(Level.WARNING, "Invalid command: " + record.getCommand());
                break;
            }
            loading = !stopLoad && recordStream.hasNextRecord();

            if (records.size() >= RECORD_LIST_SIZE || !loading) {
                updateRecordList(records);
                records.clear();
            }
            loaderReport.addResult(report);
        }
        loaderReport.setImportFileStatistic(
                new ImportFileStatistics(dataRows,
                        createRows, updateRows, deleteRows,
                        createRowsTest, updateRowsTest, deleteRowsTest));
        recordStream.close();
        return loaderReport;
    }

    /**
     * Returns the column label from header definition currently in use.
     *
     * @param name
     *            the name of the column
     * @return the label
     */
    protected String getColumnLabel(String name) {
        return recordStream.getColumnLabel(name);
    }

    /**
     * @param message
     *            the message string
     * @param record
     *            the record for which the validation message is created
     * @param field
     *            the field for which the validation message is created
     * @param value
     *            of the field
     * @return returns the error validation message for a given record and field
     */
    protected ValidationMessage getErrorMessage(String message, DSRecord record, Object field, String value) {
        return new ValidationMessage(message, true, record.getRowLabel(), getColumnLabel(field.toString()), value);
    }

    /**
     * @param message
     *            the message string
     * @param record
     *            the record for which the validation message is created
     * @param field
     *            the field for which the validation message is created
     * @return returns the error validation message for a given record and field
     */
    protected ValidationMessage getErrorMessage(String message, DSRecord record, Object field) {
        return new ValidationMessage(message, true, record.getRowLabel(), getColumnLabel(field.toString()));
    }

    /**
     * Checks whether the value string length is not too long for the database definition. The method returns
     * <code>false</code> if the entity has no such field, or if the filed data type is not {@link String}.
     *
     * @param value
     *            the string to store
     * @param fieldDescriptor
     *            the name of the entity field
     * @param entity
     *            the {@link Class} of the entity
     * @throws DbFieldLengthViolationException
     *             if the length of value violates the database restrictions.
     */
    protected void validateStringSize(final String value, final FieldIntrospection fieldDescriptor,
            Class<? extends Persistable> entity) {
        try {
            Preconditions.checkNotNull(fieldDescriptor);
            if (value == null)
                return;
            final Field field = entity.getDeclaredField(fieldDescriptor.getFieldName());
            if (!field.getType().equals(String.class)) {
                throw new RuntimeIoException(fieldDescriptor + " data type is not String.");
            }
            final Column annotation = field.getAnnotation(Column.class);
            if ((annotation == null) && (value.length() > DEFAULT_VARCHAR_LENGTH)) {
                throw new DbFieldLengthViolationException(
                        fieldDescriptor + " value exceeds maximum length: " + DEFAULT_VARCHAR_LENGTH);
            } else if ((annotation != null) && (value.length() > annotation.length())) {
                throw new DbFieldLengthViolationException(
                        fieldDescriptor + " value exceeds maximum length: " + annotation.length());
            }
            // everything checks OK
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

}
