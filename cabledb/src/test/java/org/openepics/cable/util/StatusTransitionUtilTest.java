/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import org.junit.Test;
import org.openepics.cable.model.CableStatus;
import java.util.*;
import static org.junit.Assert.*;

/**
 * Test for cable status transitions
 *
 * @author Imre Toth <imre.toth@esss.se>
 **/
public class StatusTransitionUtilTest {

    /**
     * Column: FROM
     * Row: to
     * x: allowed
     * <p>
     *              | INSERTED  | APPROVED  | ROUTED    | PULLED    | COMMISSIONED  | NOT_IN_USE    | DELETED
     * INSERTED     |     x     |     x     |           |           |               |               |
     * APPROVED     |     x     |     x     |     x     |           |               |               |
     * ROUTED       |           |     x     |     x     |           |               |               |
     * PULLED       |           |           |           |     x     |       x       |       x       |
     * COMMISSIONED |           |           |     x     |     x     |       x       |               |
     * NOT_IN_USE   |           |           |           |     x     |       x       |       x       |
     * DELETED      |     x     |     x     |     x     |           |               |       x       |    x
     */

    @Test(expected = StatusTransitionException.class)
    public void nullToStatusShouldFail() {
        StatusTransitionUtil.transitionAllowed(null, CableStatus.INSERTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void statusToNullShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, null);
    }

    @Test(expected = StatusTransitionException.class)
    public void deleteTransitionShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.INSERTED);
    }

    //INSERTED

    @Test
    public void insertedToInserted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.INSERTED);
        assertTrue(result);
    }

    @Test
    public void insertedToApproved() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.APPROVED);
        assertTrue(result);
    }

    @Test(expected = StatusTransitionException.class)
    public void insertedToRoutedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.ROUTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void insertedToPulledShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.PULLED);
    }

    @Test(expected = StatusTransitionException.class)
    public void insertedToCommissionedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.COMMISSIONED);
    }

    @Test(expected = StatusTransitionException.class)
    public void insertedToNotInUseShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.NOT_IN_USE);
    }

    @Test
    public void insertedToDeleted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.INSERTED, CableStatus.DELETED);
        assertTrue(result);
    }

    //APPROVED

    @Test
    public void approvedToInserted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.INSERTED);
        assertTrue(result);
    }

    @Test
    public void approvedToApproved() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.APPROVED);
        assertTrue(result);
    }

    @Test
    public void approvedToRouted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.ROUTED);
        assertTrue(result);
    }

    @Test(expected = StatusTransitionException.class)
    public void approvedToPulledShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.PULLED);
    }

    @Test(expected = StatusTransitionException.class)
    public void approvedToCommissionedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.COMMISSIONED);
    }

    @Test(expected = StatusTransitionException.class)
    public void approvedToNotInUseShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.NOT_IN_USE);
    }

    @Test
    public void approvedToDeleted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.APPROVED, CableStatus.DELETED);
        assertTrue(result);
    }

    //ROUTED

    @Test(expected = StatusTransitionException.class)
    public void routedToInsertedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.INSERTED);
    }

    @Test
    public void routedToApproved() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.APPROVED);
        assertTrue(result);
    }

    @Test
    public void routedToRouted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.ROUTED);
        assertTrue(result);
    }

    @Test
    public void routedToPulled() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.PULLED);
        assertTrue(result);
    }

    @Test
    public void routedToCommissioned() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.COMMISSIONED);
        assertTrue(result);
    }

    @Test(expected = StatusTransitionException.class)
    public void routedToNotInUseShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.NOT_IN_USE);
    }

    @Test
    public void routedToDeleted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.ROUTED, CableStatus.DELETED);
        assertTrue(result);
    }

    //PULLED

    @Test(expected = StatusTransitionException.class)
    public void pulledToInsertedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.INSERTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void pulledToApprovedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.APPROVED);
    }

    @Test(expected = StatusTransitionException.class)
    public void pulledToRoutedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.ROUTED);
    }

    @Test
    public void pulledToPulled() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.PULLED);
        assertTrue(result);
    }

    @Test
    public void pulledToCommissioned() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.COMMISSIONED);
        assertTrue(result);
    }

    @Test
    public void pulledToNotInUse() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.NOT_IN_USE);
        assertTrue(result);
    }

    @Test(expected = StatusTransitionException.class)
    public void insertedToDeletedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.PULLED, CableStatus.DELETED);
    }

    //COMMISSIONED

    @Test(expected = StatusTransitionException.class)
    public void commissionedToInsertedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.INSERTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void commissionedToApprovedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.APPROVED);
    }

    @Test(expected = StatusTransitionException.class)
    public void commissionedToRoutedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.ROUTED);
    }

    @Test
    public void commissionedToPulled() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.PULLED);
        assertTrue(result);
    }

    @Test
    public void commissionedToCommissioned() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.COMMISSIONED);
        assertTrue(result);
    }

    @Test
    public void commissionedToNotInUse() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.NOT_IN_USE);
        assertTrue(result);
    }

    @Test(expected = StatusTransitionException.class)
    public void commissionedToDeletedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.COMMISSIONED, CableStatus.DELETED);
    }

    //NOT_IN_USE

    @Test(expected = StatusTransitionException.class)
    public void notInUseToInsertedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.INSERTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void notInUseToApprovedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.APPROVED);
    }

    @Test(expected = StatusTransitionException.class)
    public void notInUseToRoutedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.ROUTED);
    }

    @Test
    public void notInUseToPulled() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.PULLED);
        assertTrue(result);
    }

    @Test(expected = StatusTransitionException.class)
    public void notInUseToCommissionedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.COMMISSIONED);
    }

    @Test
    public void notInUseToNotInUse() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.NOT_IN_USE);
        assertTrue(result);
    }

    @Test
    public void notInUseToDeleted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.NOT_IN_USE, CableStatus.DELETED);
        assertTrue(result);
    }

    //DELETED

    @Test(expected = StatusTransitionException.class)
    public void deletedToInsertedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.INSERTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void deletedToApprovedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.APPROVED);
    }

    @Test(expected = StatusTransitionException.class)
    public void deletedToRoutedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.ROUTED);
    }

    @Test(expected = StatusTransitionException.class)
    public void deletedToPulledShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.PULLED);
    }

    @Test(expected = StatusTransitionException.class)
    public void deletedToCommissionedShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.COMMISSIONED);
    }

    @Test(expected = StatusTransitionException.class)
    public void deletedToNotInUseShouldFail() {
        StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.NOT_IN_USE);
    }

    @Test
    public void deletedToDeleted() {
        boolean result = StatusTransitionUtil.transitionAllowed(CableStatus.DELETED, CableStatus.DELETED);
        assertTrue(result);
    }

    //Possible status values test
    @Test(expected = StatusTransitionException.class)
    public void emptyStatusShouldFail() {
        StatusTransitionUtil.possibleStatuses(null);
    }

    @Test
    public void insertedStatusesShouldNotBeEmpty() {
        Collection<CableStatus> cableStatuses = StatusTransitionUtil.possibleStatuses(CableStatus.INSERTED);
        assertNotNull(cableStatuses);
        assertTrue(cableStatuses.size() > 0);
    }
    @Test
    public void deleteStatusesShouldBeEmpty() {
        Collection<CableStatus> cableStatuses = StatusTransitionUtil.possibleStatuses(CableStatus.DELETED);
        assertNull(cableStatuses);
    }
}
