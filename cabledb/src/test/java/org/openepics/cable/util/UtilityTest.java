/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Purpose to test Utility class.
 *
 * @author Lars Johansson
 */
public class UtilityTest {

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloatNull() {
        Float f = null;
        assertFalse(Utility.equalsFloat(f));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat1Param() {
        Float f = 1f;
        assertFalse(Utility.equalsFloat(f));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat2Params1() {
        Float f1 = null;
        Float f2 = null;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat2Params2() {
        Float f1 = null;
        Float f2 = 1f;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat2Params3() {
        Float f1 = 1f;
        Float f2 = null;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat2Params4() {
        Float f1 = 1f;
        Float f2 = 2f;
        assertFalse(Utility.equalsFloat(f1, f2));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat2Params5() {
        Float f1 = 1f;
        Float f2 = 1f;
        assertTrue(Utility.equalsFloat(f1, f2));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat3Params1() {
        Float f1 = 1.5f;
        Float f2 = null;
        Float f3 = 3.5f;
        assertFalse(Utility.equalsFloat(f1, f2, f3));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat3Params2() {
        Float f1 = 1.5f;
        Float f2 = 1.5f;
        Float f3 = 3.5f;
        assertFalse(Utility.equalsFloat(f1, f2, f3));
    }

    /**
     * Test for comparison of float objects.
     */
    @Test
    public void testEqualsFloat3Params3() {
        Float f1 = 3.5f;
        Float f2 = 3.5f;
        Float f3 = 3.5f;
        assertTrue(Utility.equalsFloat(f1, f2, f3));
    }

    @Test
    public void testNullIfEmpty() {
        assertEquals(null,                     Utility.nullIfEmpty(null));
        assertEquals(null,                     Utility.nullIfEmpty(""));
        assertEquals(" ",                      Utility.nullIfEmpty(" "));
        assertEquals("   ",                    Utility.nullIfEmpty("   "));
        assertEquals(" asdf  ",                Utility.nullIfEmpty(" asdf  "));
        assertEquals("asdf",                   Utility.nullIfEmpty("asdf"));
        assertEquals("asdf zxcv",              Utility.nullIfEmpty("asdf zxcv"));
        assertEquals("asdf\nzxcv",             Utility.nullIfEmpty("asdf\nzxcv"));
        assertEquals("asdf\nzxcv\n\n\nghjk  ", Utility.nullIfEmpty("asdf\nzxcv\n\n\nghjk  "));
        assertEquals(" \nasdf\nzxcv\n",        Utility.nullIfEmpty(" \nasdf\nzxcv\n"));
    }

    @Test
    public void testFormatWhitespace() {
        assertEquals(null,                     Utility.formatWhitespace(null));
        assertEquals("",                       Utility.formatWhitespace(""));
        assertEquals("",                       Utility.formatWhitespace(" "));
        assertEquals("",                       Utility.formatWhitespace("   "));
        assertEquals("asdf",                   Utility.formatWhitespace(" asdf  "));
        assertEquals("asdf",                   Utility.formatWhitespace("asdf"));
        assertEquals("asdf zxcv",              Utility.formatWhitespace("asdf zxcv"));
        assertEquals("asdf zxcv",              Utility.formatWhitespace("asdf\nzxcv"));
        assertEquals("asdf zxcv ghjk",         Utility.formatWhitespace("asdf\nzxcv\n\n\nghjk  "));
        assertEquals("asdf zxcv",              Utility.formatWhitespace(" \nasdf\nzxcv\n"));
    }

    @Test
    public void testFormatformatWhitespaceTrimFrom() {
        assertEquals(null,                     Utility.formatWhitespaceTrimFrom(null));
        assertEquals("",                       Utility.formatWhitespaceTrimFrom(""));
        assertEquals("",                       Utility.formatWhitespaceTrimFrom(" "));
        assertEquals("",                       Utility.formatWhitespaceTrimFrom("   "));
        assertEquals("asdf",                   Utility.formatWhitespaceTrimFrom(" asdf  "));
        assertEquals("asdf",                   Utility.formatWhitespaceTrimFrom("asdf"));
        assertEquals("asdf zxcv",              Utility.formatWhitespaceTrimFrom("asdf zxcv"));
        assertEquals("asdf\nzxcv",             Utility.formatWhitespaceTrimFrom("asdf\nzxcv"));
        assertEquals("asdf\nzxcv\n\n\nghjk",   Utility.formatWhitespaceTrimFrom("asdf\nzxcv\n\n\nghjk  "));
        assertEquals("asdf\nzxcv",             Utility.formatWhitespaceTrimFrom(" \nasdf\nzxcv\n"));
    }

    @Test
    public void testFormatRecordValue() {
        assertEquals(null,                     Utility.formatRecordValue(null));
        assertEquals(null,                     Utility.formatRecordValue(""));
        assertEquals(null,                     Utility.formatRecordValue(" "));
        assertEquals(null,                     Utility.formatRecordValue("   "));
        assertEquals("asdf",                   Utility.formatRecordValue(" asdf  "));
        assertEquals("asdf",                   Utility.formatRecordValue("asdf"));
        assertEquals("asdf zxcv",              Utility.formatRecordValue("asdf zxcv"));
        assertEquals("asdf zxcv",              Utility.formatRecordValue("asdf\nzxcv"));
        assertEquals("asdf zxcv ghjk",         Utility.formatRecordValue("asdf\nzxcv\n\n\nghjk  "));
        assertEquals("asdf zxcv",              Utility.formatRecordValue(" \nasdf\nzxcv\n"));
    }

    @Test
    public void testFormatRecordValueTrimFrom() {
        assertEquals(null,                     Utility.formatRecordValueTrimFrom(null));
        assertEquals(null,                     Utility.formatRecordValueTrimFrom(""));
        assertEquals(null,                     Utility.formatRecordValueTrimFrom(" "));
        assertEquals(null,                     Utility.formatRecordValueTrimFrom("   "));
        assertEquals("asdf",                   Utility.formatRecordValueTrimFrom(" asdf  "));
        assertEquals("asdf",                   Utility.formatRecordValueTrimFrom("asdf"));
        assertEquals("asdf zxcv",              Utility.formatRecordValueTrimFrom("asdf zxcv"));
        assertEquals("asdf\nzxcv",             Utility.formatRecordValueTrimFrom("asdf\nzxcv"));
        assertEquals("asdf\nzxcv\n\n\nghjk",   Utility.formatRecordValueTrimFrom("asdf\nzxcv\n\n\nghjk  "));
        assertEquals("asdf\nzxcv",             Utility.formatRecordValueTrimFrom(" \nasdf\nzxcv\n"));
    }

}
