/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.openepics.cable.util.fbs.unit;

import org.junit.Test;
import org.openepics.cable.util.fbs.FbsClient;
import org.openepics.cable.util.fbs.FbsElement;
import org.openepics.cable.util.fbs.FbsUtil;
import org.openepics.cable.util.webservice.ResponseException;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for {@link FbsClient} class (using MockServer, details: {@link FbsServiceMock}).
 *
 * <p>
 * Class is combination of FbsClient and FbsUtil.
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 *
 * @see FbsClient
 * @see FbsUtil
 *
 * @see FbsClientIT
 * @see FbsUtilTest
 **/
public class FbsClientTest extends FbsServiceMock {

    @Test
    public void getFbsListing_readMappingsCableNameToFbsTag() {
        final List<FbsElement> fbsElements = client.getFbsListing();
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tagMappings.isEmpty());
    }

    @Test(expected = ResponseException.class)
    public void getFbsListing_readMappingsCableNameToFbsTag_fail404() {
        clean();
        createExpectationFail(Collections.emptyList());
        client.getFbsListing();
    }

    @Test
    public void getFbsListing_readMappingsESSNameToFbsTag() {
        final List<FbsElement> fbsElements = client.getFbsListing();
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListing_readMappingsFbsTagToEssName() {
        final List<FbsElement> fbsElements = client.getFbsListing();
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertFalse(fbsElements.isEmpty());
        assertFalse(tagMappings.isEmpty());
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListing_getFbsElementForCableName() {
        //		"cableName": "25B035008",
        //		"description": "From: RFQ-010:WtrC-FIS-002\nTo: RFQ-010:WtrC-FIS-002",
        //		"essName": null,
        //		"id": "ESS-1398590",
        //		"level": 6,
        //		"modified": "2019-07-10T13:10:38",
        //		"parent": "ESS-1161090",
        //		"state": "Preliminary",
        //		"tag": "=ESS.ACC.E02.W04.BF002.WG01"

        String cableName = "25B035008";

        final List<FbsElement> fbsElements = client.getFbsListing();
        FbsElement element = FbsUtil.getFbsElementForCableName(fbsElements, cableName);

        assertNotNull(fbsElements);
        assertNotNull(element);
        assertEquals(32, fbsElements.size());

        assertEquals(cableName, element.cableName);
        assertEquals(null, element.essName);
        assertEquals("ESS-1398590", element.id);
        assertEquals("=ESS.ACC.E02.W04.BF002.WG01", element.tag);
    }

    @Test
    public void getFbsListing_getFbsElementsForCableName() {
        //		"cableName": "25B035008",
        //		"description": "From: RFQ-010:WtrC-FIS-002\nTo: RFQ-010:WtrC-FIS-002",
        //		"essName": null,
        //		"id": "ESS-1398590",
        //		"level": 6,
        //		"modified": "2019-07-10T13:10:38",
        //		"parent": "ESS-1161090",
        //		"state": "Preliminary",
        //		"tag": "=ESS.ACC.E02.W04.BF002.WG01"

        String cableName = "25B035008";

        final List<FbsElement> fbsElements = client.getFbsListing();
        List<FbsElement> elements = FbsUtil.getFbsElementsForCableName(fbsElements, cableName);

        assertNotNull(fbsElements);
        assertNotNull(elements);
        assertEquals(32, fbsElements.size());
        assertEquals(1, elements.size());

        assertEquals(cableName, elements.get(0).cableName);
        assertEquals(null, elements.get(0).essName);
        assertEquals("ESS-1398590", elements.get(0).id);
        assertEquals("=ESS.ACC.E02.W04.BF002.WG01", elements.get(0).tag);
    }

    @Test
    public void getFbsListing_getFbsElementForEssName() {
        //	    "cableName": null,
        //	    "description": "MTCA AMC Board",
        //	    "essName": "Spk-070RFC:RFS-MTCA-2104AM",
        //	    "id": "ESS-0230704",
        //	    "level": 8,
        //	    "modified": "2019-12-17T08:59:50",
        //	    "parent": "ESS-0230692",
        //	    "state": "Preliminary",
        //	    "tag": "=ESS.ACC.A03.A08.E02.E01.K01.KF03"

        String essName = "Spk-070RFC:RFS-MTCA-2104AM";

        final List<FbsElement> fbsElements = client.getFbsListing();
        FbsElement element = FbsUtil.getFbsElementForEssName(fbsElements, essName);

        assertNotNull(fbsElements);
        assertNotNull(element);
        assertEquals(32, fbsElements.size());

        assertEquals(null, element.cableName);
        assertEquals(essName, element.essName);
        assertEquals("ESS-0230704", element.id);
        assertEquals("=ESS.ACC.A03.A08.E02.E01.K01.KF03", element.tag);
    }

    @Test
    public void getFbsListing_getFbsElementsForEssName() {
        //	    "cableName": null,
        //	    "description": "MTCA AMC Board",
        //	    "essName": "Spk-070RFC:RFS-MTCA-2104AM",
        //	    "id": "ESS-0230704",
        //	    "level": 8,
        //	    "modified": "2019-12-17T08:59:50",
        //	    "parent": "ESS-0230692",
        //	    "state": "Preliminary",
        //	    "tag": "=ESS.ACC.A03.A08.E02.E01.K01.KF03"

        String essName = "Spk-070RFC:RFS-MTCA-2104AM";

        final List<FbsElement> fbsElements = client.getFbsListing();
        List<FbsElement> elements = FbsUtil.getFbsElementsForEssName(fbsElements, essName);

        assertNotNull(fbsElements);
        assertNotNull(elements);
        assertEquals(32, fbsElements.size());
        assertEquals(1, elements.size());

        assertEquals(null, elements.get(0).cableName);
        assertEquals(essName, elements.get(0).essName);
        assertEquals("ESS-0230704", elements.get(0).id);
        assertEquals("=ESS.ACC.A03.A08.E02.E01.K01.KF03", elements.get(0).tag);
    }

    @Test
    public void getFbsListing_getFbsElementForId() {
        //	    "cableName": null,
        //	    "description": "MTCA AMC Board",
        //	    "essName": "Spk-070RFC:RFS-MTCA-2113MC",
        //	    "id": "ESS-0230707",
        //	    "level": 8,
        //	    "modified": "2018-10-03T12:45:53",
        //	    "parent": "ESS-0230692",
        //	    "state": "Preliminary",
        //	    "tag": "=ESS.ACC.A03.A08.E02.E01.K01.KF06"

        String id = "ESS-0230707";

        final List<FbsElement> fbsElements = client.getFbsListing();
        FbsElement element = FbsUtil.getFbsElementForId(fbsElements, id);

        assertNotNull(fbsElements);
        assertNotNull(element);
        assertEquals(32, fbsElements.size());

        assertEquals(null, element.cableName);
        assertEquals("Spk-070RFC:RFS-MTCA-2113MC", element.essName);
        assertEquals(id, element.id);
        assertEquals("=ESS.ACC.A03.A08.E02.E01.K01.KF06", element.tag);
    }

    @Test
    public void getFbsListing_getFbsElementsForId() {
        //	    "cableName": null,
        //	    "description": "MTCA AMC Board",
        //	    "essName": "Spk-070RFC:RFS-MTCA-2113MC",
        //	    "id": "ESS-0230707",
        //	    "level": 8,
        //	    "modified": "2018-10-03T12:45:53",
        //	    "parent": "ESS-0230692",
        //	    "state": "Preliminary",
        //	    "tag": "=ESS.ACC.A03.A08.E02.E01.K01.KF06"

        String id = "ESS-0230707";

        final List<FbsElement> fbsElements = client.getFbsListing();
        List<FbsElement> elements = FbsUtil.getFbsElementsForId(fbsElements, id);

        assertNotNull(fbsElements);
        assertNotNull(elements);
        assertEquals(32, fbsElements.size());
        assertEquals(1, elements.size());

        assertEquals(null, elements.get(0).cableName);
        assertEquals("Spk-070RFC:RFS-MTCA-2113MC", elements.get(0).essName);
        assertEquals(id, elements.get(0).id);
        assertEquals("=ESS.ACC.A03.A08.E02.E01.K01.KF06", elements.get(0).tag);
    }

    @Test
    public void getFbsListing_getFbsElementForTag() {
        //	    "cableName": "35A024394",
        //	    "description": "From: Spk-070RFC:RFS-Relay-210\nTo: Spk-070RFC:RFS-PP-210",
        //	    "essName": null,
        //	    "id": "ESS-1973277",
        //	    "level": 8,
        //	    "modified": "2019-12-17T09:05:07",
        //	    "parent": "ESS-0230694",
        //	    "state": "Preliminary",
        //	    "tag": "=ESS.ACC.A03.A08.E02.E01.X01.WG03"

        String tag = "=ESS.ACC.A03.A08.E02.E01.X01.WG03";

        final List<FbsElement> fbsElements = client.getFbsListing();
        FbsElement element = FbsUtil.getFbsElementForTag(fbsElements, tag);

        assertNotNull(fbsElements);
        assertNotNull(element);
        assertEquals(32, fbsElements.size());

        assertEquals("35A024394", element.cableName);
        assertEquals(null, element.essName);
        assertEquals("ESS-1973277", element.id);
        assertEquals(tag, element.tag);
    }

    @Test
    public void getFbsListing_getFbsElementsForTag() {
        //	    "cableName": "35A024394",
        //	    "description": "From: Spk-070RFC:RFS-Relay-210\nTo: Spk-070RFC:RFS-PP-210",
        //	    "essName": null,
        //	    "id": "ESS-1973277",
        //	    "level": 8,
        //	    "modified": "2019-12-17T09:05:07",
        //	    "parent": "ESS-0230694",
        //	    "state": "Preliminary",
        //	    "tag": "=ESS.ACC.A03.A08.E02.E01.X01.WG03"

        String tag = "=ESS.ACC.A03.A08.E02.E01.X01.WG03";

        final List<FbsElement> fbsElements = client.getFbsListing();
        List<FbsElement> elements = FbsUtil.getFbsElementsForTag(fbsElements, tag);

        assertNotNull(fbsElements);
        assertNotNull(elements);
        assertEquals(32, fbsElements.size());
        assertEquals(1, elements.size());

        assertEquals("35A024394", elements.get(0).cableName);
        assertEquals(null, elements.get(0).essName);
        assertEquals("ESS-1973277", elements.get(0).id);
        assertEquals(tag, elements.get(0).tag);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag() {
        // encoding of parameter handled in getFbsListing method
        String cableName = "24B035866";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("=ESS.ACC.A07.U01.U31.E01.G01.WF01", tagMappings.get(cableName));
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag2() {
        // encoding of parameter handled in getFbsListing method
        String cableName = "85C034390";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_noMatch() {
        String cableName = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_empty() {
        String cableName = "";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_empty2() {
        String cableName = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForCableName_readMappingsCableNameToFbsTag_null() {
        String cableName = null;

        final List<FbsElement> fbsElements = client.getFbsListingForCableName(cableName);
        Map<String, String> tagMappings = FbsUtil.readMappingsCableNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag() {
        // encoding of parameter handled in getFbsListing method
        String essName = "HBL-110RFC:RFS-ADR-11002";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("=ESS.ACC.A05.A12.E01.K01.K01.KF01.K02", tagMappings.get(essName));
    }

    /*
     * Duplicate tag mappings not to happen but found to happen in test system.
     */
    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_duplicates() {
        // encoding of parameter handled in getFbsListing method
        String essName = "MBL-010RFC:RFS-SIM-410";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(2, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals(
                "Ambiguous name: =ESS.ACC.A04.A02.E04.F01.F02.KF01, =ESS.ACC.A04.A02.E04.F01.F02.KF01",
                tagMappings.get(essName));
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_noMatch() {
        String essName = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNotNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_empty() {
        String essName = "";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_empty2() {
        String essName = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForEssName_readMappingsEssNameToFbsTag_null() {
        String essName = null;

        final List<FbsElement> fbsElements = client.getFbsListingForEssName(essName);
        Map<String, String> tagMappings = FbsUtil.readMappingsEssNameToFbsTag(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A03.A08.E02.E01.K01.KF06";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());
        assertEquals("Spk-070RFC:RFS-MTCA-2113MC", tagMappings.get(tag));
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_noMatch() {
        String tag = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNotNull(fbsElements);
        assertTrue(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertTrue(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_empty() {
        String tag = "";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_empty2() {
        String tag = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsFbsTagToEssName_null() {
        String tag = null;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, String> tagMappings = FbsUtil.readMappingsFbsTagToEssName(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_cableName() {
        // encoding of parameter handled in getFbsListing method
        String id = "ESS-0415608";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(id, key);
            assertEquals("21F009765", value.cableName);
            assertEquals(null, value.essName);
            assertEquals(id, value.id);
            assertEquals("=ESS.ACC.E01.W03.WB02", value.tag);
        }
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_essName() {
        // encoding of parameter handled in getFbsListing method
        String id = "ESS-0401755";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(id, key);
            assertEquals(null, value.cableName);
            assertEquals("TS2-010Row:CnPw-U-006", value.essName);
            assertEquals(id, value.id);
            assertEquals("=ESS.ACC.A06.E02.UH02", value.tag);
        }
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_noMatch() {
        String id = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertTrue(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertTrue(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_empty() {
        String id = "";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_empty2() {
        String id = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForId_readMappingsIdToFbsElement_null() {
        String id = null;

        final List<FbsElement> fbsElements = client.getFbsListingForId(id);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsIdToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_cableName() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A07.U01.U31.E01.G01.WF01";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertEquals("24B035866", value.cableName);
            assertEquals(null, value.essName);
            assertEquals("ESS-1485237", value.id);
            assertEquals(tag, value.tag);
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_essName() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A03.A08.E02.E01.K01.KF06";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertEquals(null, value.cableName);
            assertEquals("Spk-070RFC:RFS-MTCA-2113MC", value.essName);
            assertEquals("ESS-0230707", value.id);
            assertEquals(tag, value.tag);
        }
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_noMatch() {
        String tag = "noMatch";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertTrue(fbsElements.isEmpty());
        assertNotNull(tagMappings);
        assertTrue(tagMappings.isEmpty());
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_empty() {
        String tag = "";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_empty2() {
        String tag = "   ";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_null() {
        String tag = null;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tag);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNull(fbsElements);
        assertNull(tagMappings);
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readMappingsTagToFbsElement_tagSearch_wildcards() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03.T01.GF01";
        String tagSearch = "%" + tag + "%";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        Map<String, FbsElement> tagMappings = FbsUtil.readMappingsTagToFbsElement(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tagMappings);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tagMappings.size());

        for (Map.Entry<String, FbsElement> entry : tagMappings.entrySet()) {
            String key = entry.getKey();
            FbsElement value = entry.getValue();

            assertEquals(tag, key);
            assertEquals(null, value.cableName);
            assertEquals("HBL-180RFC:RFS-Circ-310", value.essName);
            assertEquals("ESS-0453597", value.id);
            assertEquals(tag, value.tag);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcardStartsWith_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = tag + "%";
        int maxResults = 5;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcards() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03.T01.GF01";
        String tagSearch = "%" + tag + "%";

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(1, fbsElements.size());
        assertEquals(1, tags.size());
        assertNotNull(tags.get(0));
        assertEquals(tag, tags.get(0));
    }

    @Test
    public void getFbsListingForTag_readTags_tagSearch_wildcards_maxResults() {
        // encoding of parameter handled in getFbsListing method
        String tag = "=ESS.ACC.A05.A19.E03";
        String tagSearch = "%" + tag + "%";
        int maxResults = 5;

        final List<FbsElement> fbsElements = client.getFbsListingForTag(tagSearch, maxResults);
        List<String> tags = FbsUtil.readTags(fbsElements);

        assertNotNull(fbsElements);
        assertNotNull(tags);
        assertEquals(maxResults, fbsElements.size());
        assertEquals(maxResults, tags.size());

        for (String value : tags) {
            assertNotNull(value);
            assertTrue(value.contains(tag));
        }
    }
}
