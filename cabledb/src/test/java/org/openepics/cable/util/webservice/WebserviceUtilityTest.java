/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Test;

/**
 * Purpose to test WebserviceUtility class.
 *
 * @author Lars Johansson
 */
public class WebserviceUtilityTest {

    /**
     * Test for returning a UriBuilder given a url.
     */
    @Test
    public void getUriBuilder_urlEmpty() {
        String url = "";

        assertNull(url, WebserviceUtility.getUriBuilder(url));
    }

    /**
     * Test for returning a UriBuilder given a url.
     */
    @Test
    public void getUriBuilder_urlEmpty2() {
        String url = "   ";

        assertNull(url, WebserviceUtility.getUriBuilder(url));
    }

    /**
     * Test for returning a UriBuilder given a url.
     */
    @Test
    public void getUriBuilder_url_noParameter() {
        String url = "kalle";

        assertEquals(url, WebserviceUtility.getUriBuilder(url).toString());
    }

    /**
     * Test for returning a UriBuilder given a url.
     */
    @Test
    public void getUriBuilder_urlNull() {
        String url = null;

        assertNull(url, WebserviceUtility.getUriBuilder(url));
    }

    /**
     * Test for returning a UriBuilder given a url and map of string values.
     */
    @Test
    public void getUriBuilder_url_oneParameter() {
        String url = "testurl";
        String paramName = "paramName";
        String paramValue = "paramValue";
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add(paramName, paramValue);

        String expected = url + "?" + paramName + "=" + paramValue;

        assertEquals(expected, WebserviceUtility.getUriBuilder(url, map).toString());
    }

    /**
     * Test for returning a UriBuilder given a url and map of string values.
     */
    @Test
    public void getUriBuilder_url_twoParameters() {
        String url = "testurl";
        String paramName = "paramName";
        String paramValue = "paramValue";
        String param2Name = "param2Name";
        String param2Value = "param2Value";
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add(paramName, paramValue);
        map.add(param2Name, param2Value);

        String expectedAlternative1 = url + "?" + paramName + "=" + paramValue + "&" + param2Name + "=" + param2Value;
        String expectedAlternative2 = url + "?" + param2Name + "=" + param2Value + "&" + paramName + "=" + paramValue;
        String actual = WebserviceUtility.getUriBuilder(url, map).toString();

        assertTrue(expectedAlternative1.equalsIgnoreCase(actual)
                || expectedAlternative2.equalsIgnoreCase(actual));
    }

}
