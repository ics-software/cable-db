/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Purpose to test EncodingUtility class.
 *
 * @author Lars Johansson
 */
public class EncodingUtilityTest {

    /**
     * Test for encoding a string.
     */
    @Test(expected = NullPointerException.class)
    public void testEncodeStringNull() {
        String s = null;
        assertEquals(s, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringEmpty() {
        String s = "";
        assertEquals(s, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringPlus() {
        String s = "+";
        String expected = "%2B";
        assertEquals(expected, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringWhitespace() {
        String s = " ";
        String expected ="+";
        assertEquals(expected, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringNumbers() {
        String s = "12345678";
        assertEquals(s, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringLetters() {
        String s = "abcdefgh";
        assertEquals(s, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringLettersPlus() {
        String s = "abc+efgh";
        String expected = "abc%2Befgh";
        assertEquals(expected, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringLettersWhitespace() {
        String s = "abcd fgh";
        String expected = "abcd+fgh";
        assertEquals(expected, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testEncodeStringLettersPlusWhitespace() {
        String s = "ab de+gh";
        String expected = "ab+de%2Bgh";
        assertEquals(expected, EncodingUtility.encode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test(expected = NullPointerException.class)
    public void testDecodeStringNull() {
        String s = null;
        assertEquals(s, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringEmpty() {
        String s = "";
        assertEquals(s, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringPlus() {
        String s = "%2B";
        String expected = "+";
        assertEquals(expected, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringWhitespace() {
        String s = "+";
        String expected = " ";
        assertEquals(expected, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringNumbers() {
        String s = "12345678";
        assertEquals(s, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringLetters() {
        String s = "abcdefgh";
        assertEquals(s, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringLettersPlus() {
        String s = "abc%2Befgh";
        String expected = "abc+efgh";
        assertEquals(expected, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringLettersWhitespace() {
        String s = "abcd+fgh";
        String expected = "abcd fgh";
        assertEquals(expected, EncodingUtility.decode(s));
    }

    /**
     * Test for encoding a string.
     */
    @Test
    public void testDecodeStringLettersPlusWhitespace() {
        String s = "ab+de%2Bgh";
        String expected = "ab de+gh";
        assertEquals(expected, EncodingUtility.decode(s));
    }

}
