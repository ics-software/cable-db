/*
 *  Copyright (c) 2018 European Spallation Source ERIC.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Purpose to test CableNumbering class.
 *
 * @author Georg Weiss
 * @author Lars Johansson
 */
public class CableNumberingTest {

    // arrays -------------------------------------------------------------------------------------

    /**
     * Test for getting cable system characters (numbers) available for cable.
     */
    @Test
    public void getSystemNumbers(){
        assertEquals(9, CableNumbering.getSystemNumbers().length);

        assertEquals("1", CableNumbering.getSystemNumbers()[0]);
        assertEquals("2", CableNumbering.getSystemNumbers()[1]);
        assertEquals("3", CableNumbering.getSystemNumbers()[2]);
        assertEquals("4", CableNumbering.getSystemNumbers()[3]);
        assertEquals("5", CableNumbering.getSystemNumbers()[4]);
        assertEquals("6", CableNumbering.getSystemNumbers()[5]);
        assertEquals("7", CableNumbering.getSystemNumbers()[6]);
        assertEquals("8", CableNumbering.getSystemNumbers()[7]);
        assertEquals("9", CableNumbering.getSystemNumbers()[8]);
    }

    /**
     * Test for getting cable subsystem characters (numbers) available for cable.
     */
    @Test
    public void getSubsystemNumbers(){
        assertEquals(24, CableNumbering.getSubsystemNumbers().length);

        assertEquals("0", CableNumbering.getSubsystemNumbers()[0]);
        assertEquals("1", CableNumbering.getSubsystemNumbers()[1]);
        assertEquals("2", CableNumbering.getSubsystemNumbers()[2]);
        assertEquals("3", CableNumbering.getSubsystemNumbers()[3]);
        assertEquals("4", CableNumbering.getSubsystemNumbers()[4]);
        assertEquals("5", CableNumbering.getSubsystemNumbers()[5]);
        assertEquals("6", CableNumbering.getSubsystemNumbers()[6]);
        assertEquals("7", CableNumbering.getSubsystemNumbers()[7]);
        assertEquals("8", CableNumbering.getSubsystemNumbers()[8]);
        assertEquals("9", CableNumbering.getSubsystemNumbers()[9]);
        assertEquals("A", CableNumbering.getSubsystemNumbers()[10]);
        assertEquals("B", CableNumbering.getSubsystemNumbers()[11]);
        assertEquals("C", CableNumbering.getSubsystemNumbers()[12]);
        assertEquals("D", CableNumbering.getSubsystemNumbers()[13]);
        assertEquals("E", CableNumbering.getSubsystemNumbers()[14]);
        assertEquals("F", CableNumbering.getSubsystemNumbers()[15]);
        assertEquals("G", CableNumbering.getSubsystemNumbers()[16]);
        assertEquals("H", CableNumbering.getSubsystemNumbers()[17]);
        assertEquals("I", CableNumbering.getSubsystemNumbers()[18]);
        assertEquals("J", CableNumbering.getSubsystemNumbers()[19]);
        assertEquals("K", CableNumbering.getSubsystemNumbers()[20]);
        assertEquals("L", CableNumbering.getSubsystemNumbers()[21]);
        assertEquals("M", CableNumbering.getSubsystemNumbers()[22]);
        assertEquals("N", CableNumbering.getSubsystemNumbers()[23]);
    }

    /**
     * Test for getting cable subsystem characters (numbers) available for cable given system character (number).
     */
    @Test
    public void getSubsystemNumbersSystem(){
        assertEquals(0,    CableNumbering.getSubsystemNumbers(null).length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("!").length);

        assertEquals(0,    CableNumbering.getSubsystemNumbers("-1").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("0").length);

        assertEquals(8,    CableNumbering.getSubsystemNumbers("1").length);
        assertEquals(9,    CableNumbering.getSubsystemNumbers("2").length);
        assertEquals(10,   CableNumbering.getSubsystemNumbers("3").length);
        assertEquals(9,    CableNumbering.getSubsystemNumbers("4").length);
        assertEquals(15,   CableNumbering.getSubsystemNumbers("5").length);
        assertEquals(24,   CableNumbering.getSubsystemNumbers("6").length);
        assertEquals(10,   CableNumbering.getSubsystemNumbers("7").length);
        assertEquals(4,    CableNumbering.getSubsystemNumbers("8").length);
        assertEquals(10,   CableNumbering.getSubsystemNumbers("9").length);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("1")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("1")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("1")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("1")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("1")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("1")[5]);
        assertEquals("6",  CableNumbering.getSubsystemNumbers("1")[6]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("1")[7]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("2")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("2")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("2")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("2")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("2")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("2")[5]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("2")[6]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("2")[7]);
        assertEquals("A",  CableNumbering.getSubsystemNumbers("2")[8]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("3")[0]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("3")[1]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("3")[2]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("3")[3]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("3")[4]);
        assertEquals("6",  CableNumbering.getSubsystemNumbers("3")[5]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("3")[6]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("3")[7]);
        assertEquals("9",  CableNumbering.getSubsystemNumbers("3")[8]);
        assertEquals("A",  CableNumbering.getSubsystemNumbers("3")[9]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("4")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("4")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("4")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("4")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("4")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("4")[5]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("4")[6]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("4")[7]);
        assertEquals("A",  CableNumbering.getSubsystemNumbers("4")[8]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("5")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("5")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("5")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("5")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("5")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("5")[5]);
        assertEquals("6",  CableNumbering.getSubsystemNumbers("5")[6]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("5")[7]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("5")[8]);
        assertEquals("9",  CableNumbering.getSubsystemNumbers("5")[9]);
        assertEquals("A",  CableNumbering.getSubsystemNumbers("5")[10]);
        assertEquals("B",  CableNumbering.getSubsystemNumbers("5")[11]);
        assertEquals("C",  CableNumbering.getSubsystemNumbers("5")[12]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("6")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("6")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("6")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("6")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("6")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("6")[5]);
        assertEquals("6",  CableNumbering.getSubsystemNumbers("6")[6]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("6")[7]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("6")[8]);
        assertEquals("9",  CableNumbering.getSubsystemNumbers("6")[9]);
        assertEquals("A",  CableNumbering.getSubsystemNumbers("6")[10]);
        assertEquals("B",  CableNumbering.getSubsystemNumbers("6")[11]);
        assertEquals("C",  CableNumbering.getSubsystemNumbers("6")[12]);
        assertEquals("D",  CableNumbering.getSubsystemNumbers("6")[13]);
        assertEquals("E",  CableNumbering.getSubsystemNumbers("6")[14]);
        assertEquals("F",  CableNumbering.getSubsystemNumbers("6")[15]);
        assertEquals("G",  CableNumbering.getSubsystemNumbers("6")[16]);
        assertEquals("H",  CableNumbering.getSubsystemNumbers("6")[17]);
        assertEquals("I",  CableNumbering.getSubsystemNumbers("6")[18]);
        assertEquals("J",  CableNumbering.getSubsystemNumbers("6")[19]);
        assertEquals("K",  CableNumbering.getSubsystemNumbers("6")[20]);
        assertEquals("L",  CableNumbering.getSubsystemNumbers("6")[21]);
        assertEquals("M",  CableNumbering.getSubsystemNumbers("6")[22]);
        assertEquals("N",  CableNumbering.getSubsystemNumbers("6")[23]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("7")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("7")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("7")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("7")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("7")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("7")[5]);
        assertEquals("6",  CableNumbering.getSubsystemNumbers("7")[6]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("7")[7]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("7")[8]);
        assertEquals("A",  CableNumbering.getSubsystemNumbers("7")[9]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("8")[0]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("8")[1]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("8")[2]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("8")[3]);

        assertEquals("0",  CableNumbering.getSubsystemNumbers("9")[0]);
        assertEquals("1",  CableNumbering.getSubsystemNumbers("9")[1]);
        assertEquals("2",  CableNumbering.getSubsystemNumbers("9")[2]);
        assertEquals("3",  CableNumbering.getSubsystemNumbers("9")[3]);
        assertEquals("4",  CableNumbering.getSubsystemNumbers("9")[4]);
        assertEquals("5",  CableNumbering.getSubsystemNumbers("9")[5]);
        assertEquals("6",  CableNumbering.getSubsystemNumbers("9")[6]);
        assertEquals("7",  CableNumbering.getSubsystemNumbers("9")[7]);
        assertEquals("8",  CableNumbering.getSubsystemNumbers("9")[8]);

        assertEquals(0,    CableNumbering.getSubsystemNumbers("a").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("b").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("c").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("d").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("e").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("f").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("g").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("h").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("i").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("j").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("k").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("l").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("m").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("n").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("o").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("p").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("q").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("r").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("s").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("t").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("u").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("v").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("w").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("x").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("y").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("z").length);

        assertEquals(0,    CableNumbering.getSubsystemNumbers("A").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("B").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("C").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("D").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("E").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("F").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("G").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("H").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("I").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("J").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("K").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("L").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("M").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("N").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("O").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("P").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("Q").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("R").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("S").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("T").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("U").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("V").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("W").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("X").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("Y").length);
        assertEquals(0,    CableNumbering.getSubsystemNumbers("Z").length);
    }

    /**
     * Test for getting cable class characters (letters) available for cable.
     */
    @Test
    public void getClassLetters(){
        assertEquals(8, CableNumbering.getClassLetters().length);

        assertEquals("A", CableNumbering.getClassLetters()[0]);
        assertEquals("B", CableNumbering.getClassLetters()[1]);
        assertEquals("C", CableNumbering.getClassLetters()[2]);
        assertEquals("D", CableNumbering.getClassLetters()[3]);
        assertEquals("E", CableNumbering.getClassLetters()[4]);
        assertEquals("F", CableNumbering.getClassLetters()[5]);
        assertEquals("G", CableNumbering.getClassLetters()[6]);
        assertEquals("H", CableNumbering.getClassLetters()[7]);
    }

    // system -------------------------------------------------------------------------------------

    /**
     * Test for getting system character (number) for cable given system label.
     */
    @Test
    public void getSystemNumber(){
        assertEquals("",  CableNumbering.getSystemNumber(null));
        assertEquals("",  CableNumbering.getSystemNumber(""));
        assertEquals("",  CableNumbering.getSystemNumber("!"));

        assertEquals("",  CableNumbering.getSystemNumber("-1"));
        assertEquals("",  CableNumbering.getSystemNumber("0"));

        assertEquals("",  CableNumbering.getSystemNumber("a"));
        assertEquals("",  CableNumbering.getSystemNumber("b"));
        assertEquals("",  CableNumbering.getSystemNumber("c"));
        assertEquals("",  CableNumbering.getSystemNumber("d"));
        assertEquals("",  CableNumbering.getSystemNumber("e"));
        assertEquals("",  CableNumbering.getSystemNumber("f"));
        assertEquals("",  CableNumbering.getSystemNumber("g"));
        assertEquals("",  CableNumbering.getSystemNumber("h"));
        assertEquals("",  CableNumbering.getSystemNumber("i"));
        assertEquals("",  CableNumbering.getSystemNumber("j"));
        assertEquals("",  CableNumbering.getSystemNumber("k"));
        assertEquals("",  CableNumbering.getSystemNumber("l"));
        assertEquals("",  CableNumbering.getSystemNumber("m"));
        assertEquals("",  CableNumbering.getSystemNumber("n"));
        assertEquals("",  CableNumbering.getSystemNumber("o"));
        assertEquals("",  CableNumbering.getSystemNumber("p"));
        assertEquals("",  CableNumbering.getSystemNumber("q"));
        assertEquals("",  CableNumbering.getSystemNumber("r"));
        assertEquals("",  CableNumbering.getSystemNumber("s"));
        assertEquals("",  CableNumbering.getSystemNumber("t"));
        assertEquals("",  CableNumbering.getSystemNumber("u"));
        assertEquals("",  CableNumbering.getSystemNumber("v"));
        assertEquals("",  CableNumbering.getSystemNumber("w"));
        assertEquals("",  CableNumbering.getSystemNumber("x"));
        assertEquals("",  CableNumbering.getSystemNumber("y"));
        assertEquals("",  CableNumbering.getSystemNumber("z"));

        assertEquals("",  CableNumbering.getSystemNumber("A"));
        assertEquals("",  CableNumbering.getSystemNumber("B"));
        assertEquals("",  CableNumbering.getSystemNumber("C"));
        assertEquals("",  CableNumbering.getSystemNumber("D"));
        assertEquals("",  CableNumbering.getSystemNumber("E"));
        assertEquals("",  CableNumbering.getSystemNumber("F"));
        assertEquals("",  CableNumbering.getSystemNumber("G"));
        assertEquals("",  CableNumbering.getSystemNumber("H"));
        assertEquals("",  CableNumbering.getSystemNumber("I"));
        assertEquals("",  CableNumbering.getSystemNumber("J"));
        assertEquals("",  CableNumbering.getSystemNumber("K"));
        assertEquals("",  CableNumbering.getSystemNumber("L"));
        assertEquals("",  CableNumbering.getSystemNumber("M"));
        assertEquals("",  CableNumbering.getSystemNumber("N"));
        assertEquals("",  CableNumbering.getSystemNumber("O"));
        assertEquals("",  CableNumbering.getSystemNumber("P"));
        assertEquals("",  CableNumbering.getSystemNumber("Q"));
        assertEquals("",  CableNumbering.getSystemNumber("R"));
        assertEquals("",  CableNumbering.getSystemNumber("S"));
        assertEquals("",  CableNumbering.getSystemNumber("T"));
        assertEquals("",  CableNumbering.getSystemNumber("U"));
        assertEquals("",  CableNumbering.getSystemNumber("V"));
        assertEquals("",  CableNumbering.getSystemNumber("W"));
        assertEquals("",  CableNumbering.getSystemNumber("X"));
        assertEquals("",  CableNumbering.getSystemNumber("Y"));
        assertEquals("",  CableNumbering.getSystemNumber("Z"));

        assertEquals("1", CableNumbering.getSystemNumber("1"));
        assertEquals("2", CableNumbering.getSystemNumber("2"));
        assertEquals("3", CableNumbering.getSystemNumber("3"));
        assertEquals("4", CableNumbering.getSystemNumber("4"));
        assertEquals("5", CableNumbering.getSystemNumber("5"));
        assertEquals("6", CableNumbering.getSystemNumber("6"));
        assertEquals("7", CableNumbering.getSystemNumber("7"));
        assertEquals("8", CableNumbering.getSystemNumber("8"));
        assertEquals("9", CableNumbering.getSystemNumber("9"));

        assertEquals("1", CableNumbering.getSystemNumber("1xxxxxx"));
        assertEquals("1", CableNumbering.getSystemNumber("1 (xxx)"));

        assertEquals("1", CableNumbering.getSystemNumber("1A"));
        assertEquals("2", CableNumbering.getSystemNumber("2BB"));
        assertEquals("3", CableNumbering.getSystemNumber("3CCC"));
        assertEquals("4", CableNumbering.getSystemNumber("4DDDD"));
        assertEquals("5", CableNumbering.getSystemNumber("5EEEEE"));
        assertEquals("6", CableNumbering.getSystemNumber("6FFFFFF"));
        assertEquals("7", CableNumbering.getSystemNumber("7GGGGGGG"));
        assertEquals("8", CableNumbering.getSystemNumber("8HHHHHHHH"));
        assertEquals("9", CableNumbering.getSystemNumber("9IIIIIIIII"));

        assertEquals("1", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL));
        assertEquals("2", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC));
        assertEquals("3", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC));
        assertEquals("4", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L));
        assertEquals("5", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS));
        assertEquals("6", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS));
        assertEquals("7",
                CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS));
        assertEquals("8", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_8_ICS));
        assertEquals("9", CableNumbering.getSystemNumber(CableNumbering.SYSTEM_LABEL_9_CRYOGENICS));
    }

    /**
     * Test for getting system label for cable given system character (number).
     */
    @Test
    public void getSystemLabel(){
        assertEquals("", CableNumbering.getSystemLabel(null));
        assertEquals("", CableNumbering.getSystemLabel(""));
        assertEquals("", CableNumbering.getSystemLabel("!"));

        assertEquals("", CableNumbering.getSystemLabel("-1"));
        assertEquals("", CableNumbering.getSystemLabel("0"));

        assertEquals(CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL,    CableNumbering.getSystemLabel("1"));
        assertEquals(CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.getSystemLabel("2"));
        assertEquals(CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC,  CableNumbering.getSystemLabel("3"));
        assertEquals(CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L,          CableNumbering.getSystemLabel("4"));
        assertEquals(CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS,          CableNumbering.getSystemLabel("5"));
        assertEquals(CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS,         CableNumbering.getSystemLabel("6"));
        assertEquals(CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                                                                            CableNumbering.getSystemLabel("7"));
        assertEquals(CableNumbering.SYSTEM_LABEL_8_ICS,                     CableNumbering.getSystemLabel("8"));
        assertEquals(CableNumbering.SYSTEM_LABEL_9_CRYOGENICS,              CableNumbering.getSystemLabel("9"));

        assertEquals("", CableNumbering.getSystemLabel("10"));

        assertEquals("", CableNumbering.getSystemLabel("a"));
        assertEquals("", CableNumbering.getSystemLabel("b"));
        assertEquals("", CableNumbering.getSystemLabel("c"));
        assertEquals("", CableNumbering.getSystemLabel("d"));
        assertEquals("", CableNumbering.getSystemLabel("e"));
        assertEquals("", CableNumbering.getSystemLabel("f"));
        assertEquals("", CableNumbering.getSystemLabel("g"));
        assertEquals("", CableNumbering.getSystemLabel("h"));
        assertEquals("", CableNumbering.getSystemLabel("i"));
        assertEquals("", CableNumbering.getSystemLabel("j"));
        assertEquals("", CableNumbering.getSystemLabel("k"));
        assertEquals("", CableNumbering.getSystemLabel("l"));
        assertEquals("", CableNumbering.getSystemLabel("m"));
        assertEquals("", CableNumbering.getSystemLabel("n"));
        assertEquals("", CableNumbering.getSystemLabel("o"));
        assertEquals("", CableNumbering.getSystemLabel("p"));
        assertEquals("", CableNumbering.getSystemLabel("q"));
        assertEquals("", CableNumbering.getSystemLabel("r"));
        assertEquals("", CableNumbering.getSystemLabel("s"));
        assertEquals("", CableNumbering.getSystemLabel("t"));
        assertEquals("", CableNumbering.getSystemLabel("u"));
        assertEquals("", CableNumbering.getSystemLabel("v"));
        assertEquals("", CableNumbering.getSystemLabel("w"));
        assertEquals("", CableNumbering.getSystemLabel("x"));
        assertEquals("", CableNumbering.getSystemLabel("c"));
        assertEquals("", CableNumbering.getSystemLabel("z"));

        assertEquals("", CableNumbering.getSystemLabel("A"));
        assertEquals("", CableNumbering.getSystemLabel("B"));
        assertEquals("", CableNumbering.getSystemLabel("C"));
        assertEquals("", CableNumbering.getSystemLabel("D"));
        assertEquals("", CableNumbering.getSystemLabel("E"));
        assertEquals("", CableNumbering.getSystemLabel("F"));
        assertEquals("", CableNumbering.getSystemLabel("G"));
        assertEquals("", CableNumbering.getSystemLabel("H"));
        assertEquals("", CableNumbering.getSystemLabel("I"));
        assertEquals("", CableNumbering.getSystemLabel("J"));
        assertEquals("", CableNumbering.getSystemLabel("K"));
        assertEquals("", CableNumbering.getSystemLabel("L"));
        assertEquals("", CableNumbering.getSystemLabel("M"));
        assertEquals("", CableNumbering.getSystemLabel("N"));
        assertEquals("", CableNumbering.getSystemLabel("O"));
        assertEquals("", CableNumbering.getSystemLabel("P"));
        assertEquals("", CableNumbering.getSystemLabel("Q"));
        assertEquals("", CableNumbering.getSystemLabel("R"));
        assertEquals("", CableNumbering.getSystemLabel("S"));
        assertEquals("", CableNumbering.getSystemLabel("T"));
        assertEquals("", CableNumbering.getSystemLabel("U"));
        assertEquals("", CableNumbering.getSystemLabel("V"));
        assertEquals("", CableNumbering.getSystemLabel("W"));
        assertEquals("", CableNumbering.getSystemLabel("X"));
        assertEquals("", CableNumbering.getSystemLabel("Y"));
        assertEquals("", CableNumbering.getSystemLabel("Z"));
    }

    /**
     * Test if given system (character (number) or label) is valid.
     */
    @Test
    public void isValidSystem(){
        assertFalse(CableNumbering.isValidSystem(null));
        assertFalse(CableNumbering.isValidSystem(""));
        assertFalse(CableNumbering.isValidSystem("!"));

        assertFalse(CableNumbering.isValidSystem("-1"));
        assertFalse(CableNumbering.isValidSystem("0"));
        assertTrue (CableNumbering.isValidSystem("1"));
        assertTrue (CableNumbering.isValidSystem("2"));
        assertTrue (CableNumbering.isValidSystem("3"));
        assertTrue (CableNumbering.isValidSystem("4"));
        assertTrue (CableNumbering.isValidSystem("5"));
        assertTrue (CableNumbering.isValidSystem("6"));
        assertTrue (CableNumbering.isValidSystem("7"));
        assertTrue (CableNumbering.isValidSystem("8"));
        assertTrue (CableNumbering.isValidSystem("9"));

        assertFalse(CableNumbering.isValidSystem("11"));
        assertFalse(CableNumbering.isValidSystem("19"));
        assertFalse(CableNumbering.isValidSystem("1a"));
        assertFalse(CableNumbering.isValidSystem("1d"));
        assertFalse(CableNumbering.isValidSystem("91"));
        assertFalse(CableNumbering.isValidSystem("99"));
        assertFalse(CableNumbering.isValidSystem("9a"));
        assertFalse(CableNumbering.isValidSystem("9d"));

        assertFalse(CableNumbering.isValidSystem("a"));
        assertFalse(CableNumbering.isValidSystem("b"));
        assertFalse(CableNumbering.isValidSystem("c"));
        assertFalse(CableNumbering.isValidSystem("d"));
        assertFalse(CableNumbering.isValidSystem("e"));
        assertFalse(CableNumbering.isValidSystem("f"));
        assertFalse(CableNumbering.isValidSystem("g"));
        assertFalse(CableNumbering.isValidSystem("h"));
        assertFalse(CableNumbering.isValidSystem("i"));
        assertFalse(CableNumbering.isValidSystem("j"));
        assertFalse(CableNumbering.isValidSystem("k"));
        assertFalse(CableNumbering.isValidSystem("l"));
        assertFalse(CableNumbering.isValidSystem("m"));
        assertFalse(CableNumbering.isValidSystem("n"));
        assertFalse(CableNumbering.isValidSystem("o"));
        assertFalse(CableNumbering.isValidSystem("p"));
        assertFalse(CableNumbering.isValidSystem("q"));
        assertFalse(CableNumbering.isValidSystem("r"));
        assertFalse(CableNumbering.isValidSystem("s"));
        assertFalse(CableNumbering.isValidSystem("t"));
        assertFalse(CableNumbering.isValidSystem("u"));
        assertFalse(CableNumbering.isValidSystem("v"));
        assertFalse(CableNumbering.isValidSystem("w"));
        assertFalse(CableNumbering.isValidSystem("x"));
        assertFalse(CableNumbering.isValidSystem("y"));
        assertFalse(CableNumbering.isValidSystem("z"));

        assertFalse(CableNumbering.isValidSystem("A"));
        assertFalse(CableNumbering.isValidSystem("B"));
        assertFalse(CableNumbering.isValidSystem("C"));
        assertFalse(CableNumbering.isValidSystem("D"));
        assertFalse(CableNumbering.isValidSystem("E"));
        assertFalse(CableNumbering.isValidSystem("F"));
        assertFalse(CableNumbering.isValidSystem("G"));
        assertFalse(CableNumbering.isValidSystem("H"));
        assertFalse(CableNumbering.isValidSystem("I"));
        assertFalse(CableNumbering.isValidSystem("J"));
        assertFalse(CableNumbering.isValidSystem("K"));
        assertFalse(CableNumbering.isValidSystem("L"));
        assertFalse(CableNumbering.isValidSystem("M"));
        assertFalse(CableNumbering.isValidSystem("N"));
        assertFalse(CableNumbering.isValidSystem("O"));
        assertFalse(CableNumbering.isValidSystem("P"));
        assertFalse(CableNumbering.isValidSystem("Q"));
        assertFalse(CableNumbering.isValidSystem("R"));
        assertFalse(CableNumbering.isValidSystem("S"));
        assertFalse(CableNumbering.isValidSystem("T"));
        assertFalse(CableNumbering.isValidSystem("U"));
        assertFalse(CableNumbering.isValidSystem("V"));
        assertFalse(CableNumbering.isValidSystem("W"));
        assertFalse(CableNumbering.isValidSystem("X"));
        assertFalse(CableNumbering.isValidSystem("Y"));
        assertFalse(CableNumbering.isValidSystem("Z"));

        assertFalse(CableNumbering.isValidSystem("32"));
        assertFalse(CableNumbering.isValidSystem("A111"));
        assertFalse(CableNumbering.isValidSystem("AB"));

        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_8_ICS));
        assertTrue (CableNumbering.isValidSystem(CableNumbering.SYSTEM_LABEL_9_CRYOGENICS));
    }

    /**
     * Test if given system character (number) is valid.
     */
    @Test
    public void isValidSystemCharacter(){
        assertFalse(CableNumbering.isValidSystemCharacter(null));
        assertFalse(CableNumbering.isValidSystemCharacter(""));
        assertFalse(CableNumbering.isValidSystemCharacter("!"));

        assertFalse(CableNumbering.isValidSystemCharacter("-1"));
        assertFalse(CableNumbering.isValidSystemCharacter("0"));
        assertTrue (CableNumbering.isValidSystemCharacter("1"));
        assertTrue (CableNumbering.isValidSystemCharacter("2"));
        assertTrue (CableNumbering.isValidSystemCharacter("3"));
        assertTrue (CableNumbering.isValidSystemCharacter("4"));
        assertTrue (CableNumbering.isValidSystemCharacter("5"));
        assertTrue (CableNumbering.isValidSystemCharacter("6"));
        assertTrue (CableNumbering.isValidSystemCharacter("7"));
        assertTrue (CableNumbering.isValidSystemCharacter("8"));
        assertTrue (CableNumbering.isValidSystemCharacter("9"));

        assertFalse(CableNumbering.isValidSystemCharacter("11"));
        assertFalse(CableNumbering.isValidSystemCharacter("19"));
        assertFalse(CableNumbering.isValidSystemCharacter("1a"));
        assertFalse(CableNumbering.isValidSystemCharacter("1d"));
        assertFalse(CableNumbering.isValidSystemCharacter("91"));
        assertFalse(CableNumbering.isValidSystemCharacter("99"));
        assertFalse(CableNumbering.isValidSystemCharacter("9a"));
        assertFalse(CableNumbering.isValidSystemCharacter("9d"));

        assertFalse(CableNumbering.isValidSystemCharacter("a"));
        assertFalse(CableNumbering.isValidSystemCharacter("b"));
        assertFalse(CableNumbering.isValidSystemCharacter("c"));
        assertFalse(CableNumbering.isValidSystemCharacter("d"));
        assertFalse(CableNumbering.isValidSystemCharacter("e"));
        assertFalse(CableNumbering.isValidSystemCharacter("f"));
        assertFalse(CableNumbering.isValidSystemCharacter("g"));
        assertFalse(CableNumbering.isValidSystemCharacter("h"));
        assertFalse(CableNumbering.isValidSystemCharacter("i"));
        assertFalse(CableNumbering.isValidSystemCharacter("j"));
        assertFalse(CableNumbering.isValidSystemCharacter("k"));
        assertFalse(CableNumbering.isValidSystemCharacter("l"));
        assertFalse(CableNumbering.isValidSystemCharacter("m"));
        assertFalse(CableNumbering.isValidSystemCharacter("n"));
        assertFalse(CableNumbering.isValidSystemCharacter("o"));
        assertFalse(CableNumbering.isValidSystemCharacter("p"));
        assertFalse(CableNumbering.isValidSystemCharacter("q"));
        assertFalse(CableNumbering.isValidSystemCharacter("r"));
        assertFalse(CableNumbering.isValidSystemCharacter("s"));
        assertFalse(CableNumbering.isValidSystemCharacter("t"));
        assertFalse(CableNumbering.isValidSystemCharacter("u"));
        assertFalse(CableNumbering.isValidSystemCharacter("v"));
        assertFalse(CableNumbering.isValidSystemCharacter("w"));
        assertFalse(CableNumbering.isValidSystemCharacter("x"));
        assertFalse(CableNumbering.isValidSystemCharacter("y"));
        assertFalse(CableNumbering.isValidSystemCharacter("z"));

        assertFalse(CableNumbering.isValidSystemCharacter("A"));
        assertFalse(CableNumbering.isValidSystemCharacter("B"));
        assertFalse(CableNumbering.isValidSystemCharacter("C"));
        assertFalse(CableNumbering.isValidSystemCharacter("D"));
        assertFalse(CableNumbering.isValidSystemCharacter("E"));
        assertFalse(CableNumbering.isValidSystemCharacter("F"));
        assertFalse(CableNumbering.isValidSystemCharacter("G"));
        assertFalse(CableNumbering.isValidSystemCharacter("H"));
        assertFalse(CableNumbering.isValidSystemCharacter("I"));
        assertFalse(CableNumbering.isValidSystemCharacter("J"));
        assertFalse(CableNumbering.isValidSystemCharacter("K"));
        assertFalse(CableNumbering.isValidSystemCharacter("L"));
        assertFalse(CableNumbering.isValidSystemCharacter("M"));
        assertFalse(CableNumbering.isValidSystemCharacter("N"));
        assertFalse(CableNumbering.isValidSystemCharacter("O"));
        assertFalse(CableNumbering.isValidSystemCharacter("P"));
        assertFalse(CableNumbering.isValidSystemCharacter("Q"));
        assertFalse(CableNumbering.isValidSystemCharacter("R"));
        assertFalse(CableNumbering.isValidSystemCharacter("S"));
        assertFalse(CableNumbering.isValidSystemCharacter("T"));
        assertFalse(CableNumbering.isValidSystemCharacter("U"));
        assertFalse(CableNumbering.isValidSystemCharacter("V"));
        assertFalse(CableNumbering.isValidSystemCharacter("W"));
        assertFalse(CableNumbering.isValidSystemCharacter("X"));
        assertFalse(CableNumbering.isValidSystemCharacter("Y"));
        assertFalse(CableNumbering.isValidSystemCharacter("Z"));

        assertFalse(CableNumbering.isValidSystemCharacter("32"));
        assertFalse(CableNumbering.isValidSystemCharacter("A111"));
        assertFalse(CableNumbering.isValidSystemCharacter("AB"));

        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS));
        assertFalse(CableNumbering.isValidSystemCharacter(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_8_ICS));
        assertFalse(CableNumbering.isValidSystemCharacter(CableNumbering.SYSTEM_LABEL_9_CRYOGENICS));
    }

    /**
     * Test if given system label is valid.
     */
    @Test
    public void isValidSystemLabel(){
        assertFalse(CableNumbering.isValidSystemLabel(null));
        assertFalse(CableNumbering.isValidSystemLabel(""));
        assertFalse(CableNumbering.isValidSystemLabel("!"));

        assertFalse(CableNumbering.isValidSystemLabel("-1"));
        assertFalse(CableNumbering.isValidSystemLabel("0"));
        assertFalse(CableNumbering.isValidSystemLabel("1"));
        assertFalse(CableNumbering.isValidSystemLabel("9"));

        assertFalse(CableNumbering.isValidSystemLabel("11"));
        assertFalse(CableNumbering.isValidSystemLabel("19"));
        assertFalse(CableNumbering.isValidSystemLabel("1a"));
        assertFalse(CableNumbering.isValidSystemLabel("1d"));
        assertFalse(CableNumbering.isValidSystemLabel("91"));
        assertFalse(CableNumbering.isValidSystemLabel("99"));
        assertFalse(CableNumbering.isValidSystemLabel("9a"));
        assertFalse(CableNumbering.isValidSystemLabel("9d"));

        assertFalse(CableNumbering.isValidSystemLabel("a"));
        assertFalse(CableNumbering.isValidSystemLabel("b"));
        assertFalse(CableNumbering.isValidSystemLabel("c"));
        assertFalse(CableNumbering.isValidSystemLabel("d"));
        assertFalse(CableNumbering.isValidSystemLabel("e"));
        assertFalse(CableNumbering.isValidSystemLabel("f"));
        assertFalse(CableNumbering.isValidSystemLabel("g"));
        assertFalse(CableNumbering.isValidSystemLabel("h"));
        assertFalse(CableNumbering.isValidSystemLabel("i"));
        assertFalse(CableNumbering.isValidSystemLabel("j"));
        assertFalse(CableNumbering.isValidSystemLabel("k"));
        assertFalse(CableNumbering.isValidSystemLabel("l"));
        assertFalse(CableNumbering.isValidSystemLabel("m"));
        assertFalse(CableNumbering.isValidSystemLabel("n"));
        assertFalse(CableNumbering.isValidSystemLabel("o"));
        assertFalse(CableNumbering.isValidSystemLabel("p"));
        assertFalse(CableNumbering.isValidSystemLabel("q"));
        assertFalse(CableNumbering.isValidSystemLabel("r"));
        assertFalse(CableNumbering.isValidSystemLabel("s"));
        assertFalse(CableNumbering.isValidSystemLabel("t"));
        assertFalse(CableNumbering.isValidSystemLabel("u"));
        assertFalse(CableNumbering.isValidSystemLabel("v"));
        assertFalse(CableNumbering.isValidSystemLabel("w"));
        assertFalse(CableNumbering.isValidSystemLabel("x"));
        assertFalse(CableNumbering.isValidSystemLabel("y"));
        assertFalse(CableNumbering.isValidSystemLabel("z"));

        assertFalse(CableNumbering.isValidSystemLabel("A"));
        assertFalse(CableNumbering.isValidSystemLabel("B"));
        assertFalse(CableNumbering.isValidSystemLabel("C"));
        assertFalse(CableNumbering.isValidSystemLabel("D"));
        assertFalse(CableNumbering.isValidSystemLabel("E"));
        assertFalse(CableNumbering.isValidSystemLabel("F"));
        assertFalse(CableNumbering.isValidSystemLabel("G"));
        assertFalse(CableNumbering.isValidSystemLabel("H"));
        assertFalse(CableNumbering.isValidSystemLabel("I"));
        assertFalse(CableNumbering.isValidSystemLabel("J"));
        assertFalse(CableNumbering.isValidSystemLabel("K"));
        assertFalse(CableNumbering.isValidSystemLabel("L"));
        assertFalse(CableNumbering.isValidSystemLabel("M"));
        assertFalse(CableNumbering.isValidSystemLabel("N"));
        assertFalse(CableNumbering.isValidSystemLabel("O"));
        assertFalse(CableNumbering.isValidSystemLabel("P"));
        assertFalse(CableNumbering.isValidSystemLabel("Q"));
        assertFalse(CableNumbering.isValidSystemLabel("R"));
        assertFalse(CableNumbering.isValidSystemLabel("S"));
        assertFalse(CableNumbering.isValidSystemLabel("T"));
        assertFalse(CableNumbering.isValidSystemLabel("U"));
        assertFalse(CableNumbering.isValidSystemLabel("V"));
        assertFalse(CableNumbering.isValidSystemLabel("W"));
        assertFalse(CableNumbering.isValidSystemLabel("X"));
        assertFalse(CableNumbering.isValidSystemLabel("Y"));
        assertFalse(CableNumbering.isValidSystemLabel("Z"));

        assertFalse(CableNumbering.isValidSystemLabel("32"));
        assertFalse(CableNumbering.isValidSystemLabel("A111"));
        assertFalse(CableNumbering.isValidSystemLabel("AB"));

        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS));
        assertTrue (CableNumbering.isValidSystemLabel(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_8_ICS));
        assertTrue (CableNumbering.isValidSystemLabel(CableNumbering.SYSTEM_LABEL_9_CRYOGENICS));
    }

    // subsystem ----------------------------------------------------------------------------------

    /**
     * Test for getting subsystem character (number) for cable given subsystem label.
     */
    @Test
    public void getSubsystemNumber(){
        assertEquals("",  CableNumbering.getSubsystemNumber(null));
        assertEquals("",  CableNumbering.getSubsystemNumber(""));
        assertEquals("",  CableNumbering.getSubsystemNumber("!"));

        assertEquals("",  CableNumbering.getSubsystemNumber("-1"));

        assertEquals("0", CableNumbering.getSubsystemNumber("0"));
        assertEquals("1", CableNumbering.getSubsystemNumber("1"));
        assertEquals("2", CableNumbering.getSubsystemNumber("2"));
        assertEquals("3", CableNumbering.getSubsystemNumber("3"));
        assertEquals("4", CableNumbering.getSubsystemNumber("4"));
        assertEquals("5", CableNumbering.getSubsystemNumber("5"));
        assertEquals("6", CableNumbering.getSubsystemNumber("6"));
        assertEquals("7", CableNumbering.getSubsystemNumber("7"));
        assertEquals("8", CableNumbering.getSubsystemNumber("8"));
        assertEquals("9", CableNumbering.getSubsystemNumber("9"));

        assertEquals("",  CableNumbering.getSubsystemNumber("a"));
        assertEquals("",  CableNumbering.getSubsystemNumber("b"));
        assertEquals("",  CableNumbering.getSubsystemNumber("c"));
        assertEquals("",  CableNumbering.getSubsystemNumber("d"));
        assertEquals("",  CableNumbering.getSubsystemNumber("e"));
        assertEquals("",  CableNumbering.getSubsystemNumber("f"));
        assertEquals("",  CableNumbering.getSubsystemNumber("g"));
        assertEquals("",  CableNumbering.getSubsystemNumber("h"));
        assertEquals("",  CableNumbering.getSubsystemNumber("i"));
        assertEquals("",  CableNumbering.getSubsystemNumber("j"));
        assertEquals("",  CableNumbering.getSubsystemNumber("k"));
        assertEquals("",  CableNumbering.getSubsystemNumber("l"));
        assertEquals("",  CableNumbering.getSubsystemNumber("m"));
        assertEquals("",  CableNumbering.getSubsystemNumber("n"));
        assertEquals("",  CableNumbering.getSubsystemNumber("o"));
        assertEquals("",  CableNumbering.getSubsystemNumber("p"));
        assertEquals("",  CableNumbering.getSubsystemNumber("q"));
        assertEquals("",  CableNumbering.getSubsystemNumber("r"));
        assertEquals("",  CableNumbering.getSubsystemNumber("s"));
        assertEquals("",  CableNumbering.getSubsystemNumber("t"));
        assertEquals("",  CableNumbering.getSubsystemNumber("u"));
        assertEquals("",  CableNumbering.getSubsystemNumber("v"));
        assertEquals("",  CableNumbering.getSubsystemNumber("w"));
        assertEquals("",  CableNumbering.getSubsystemNumber("x"));
        assertEquals("",  CableNumbering.getSubsystemNumber("y"));
        assertEquals("",  CableNumbering.getSubsystemNumber("z"));

        assertEquals("A", CableNumbering.getSubsystemNumber("A"));
        assertEquals("B", CableNumbering.getSubsystemNumber("B"));
        assertEquals("C", CableNumbering.getSubsystemNumber("C"));
        assertEquals("D", CableNumbering.getSubsystemNumber("D"));
        assertEquals("E", CableNumbering.getSubsystemNumber("E"));
        assertEquals("F", CableNumbering.getSubsystemNumber("F"));
        assertEquals("G", CableNumbering.getSubsystemNumber("G"));
        assertEquals("H", CableNumbering.getSubsystemNumber("H"));
        assertEquals("I", CableNumbering.getSubsystemNumber("I"));
        assertEquals("J", CableNumbering.getSubsystemNumber("J"));
        assertEquals("K", CableNumbering.getSubsystemNumber("K"));
        assertEquals("L", CableNumbering.getSubsystemNumber("L"));
        assertEquals("M", CableNumbering.getSubsystemNumber("M"));
        assertEquals("N", CableNumbering.getSubsystemNumber("N"));
        assertEquals("",  CableNumbering.getSubsystemNumber("O"));
        assertEquals("",  CableNumbering.getSubsystemNumber("P"));
        assertEquals("",  CableNumbering.getSubsystemNumber("Q"));
        assertEquals("",  CableNumbering.getSubsystemNumber("R"));
        assertEquals("",  CableNumbering.getSubsystemNumber("S"));
        assertEquals("",  CableNumbering.getSubsystemNumber("T"));
        assertEquals("",  CableNumbering.getSubsystemNumber("U"));
        assertEquals("",  CableNumbering.getSubsystemNumber("V"));
        assertEquals("",  CableNumbering.getSubsystemNumber("W"));
        assertEquals("",  CableNumbering.getSubsystemNumber("X"));
        assertEquals("",  CableNumbering.getSubsystemNumber("Y"));
        assertEquals("",  CableNumbering.getSubsystemNumber("Z"));

        assertEquals("1", CableNumbering.getSubsystemNumber("1xxxxxx"));
        assertEquals("1", CableNumbering.getSubsystemNumber("1 (xxx)"));

        assertEquals("1", CableNumbering.getSubsystemNumber("1A"));
        assertEquals("2", CableNumbering.getSubsystemNumber("2BB"));
        assertEquals("3", CableNumbering.getSubsystemNumber("3CCC"));
        assertEquals("4", CableNumbering.getSubsystemNumber("4DDDD"));
        assertEquals("5", CableNumbering.getSubsystemNumber("5EEEEE"));
        assertEquals("6", CableNumbering.getSubsystemNumber("6FFFFFF"));
        assertEquals("7", CableNumbering.getSubsystemNumber("7GGGGGGG"));
        assertEquals("8", CableNumbering.getSubsystemNumber("8HHHHHHHH"));
        assertEquals("9", CableNumbering.getSubsystemNumber("9IIIIIIIII"));
    }

    /**
     * Test for getting subsystem label for cable given system and subsystem characters (numbers).
     */
    @Test
    public void getSubsystemLabel(){
        assertEquals("", CableNumbering.getSubsystemLabel("2", "-1"));
        assertEquals(CableNumbering.NUMBER_2_DIAGNOSTIC,                  CableNumbering.getSubsystemLabel("2", "2"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "2"));
        assertEquals(CableNumbering.A_UPS,                                CableNumbering.getSubsystemLabel("2", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "A"));

        assertEquals("", CableNumbering.getSubsystemLabel(null, null));
        assertEquals("", CableNumbering.getSubsystemLabel("",   ""));
        assertEquals("", CableNumbering.getSubsystemLabel("!",  "!"));
        assertEquals("", CableNumbering.getSubsystemLabel("-1", "-1"));
        assertEquals("", CableNumbering.getSubsystemLabel("a",  "a"));

        assertEquals("", CableNumbering.getSubsystemLabel("0", "0"));
        assertEquals("", CableNumbering.getSubsystemLabel("0", "1"));
        assertEquals("", CableNumbering.getSubsystemLabel("0", "9"));
        assertEquals("", CableNumbering.getSubsystemLabel("0", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("0", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("0", "D"));

        assertEquals("", CableNumbering.getSubsystemLabel("1", "a"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "c"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "d"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "00"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "11"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "99"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "aa"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "cc"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "dd"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "AA"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "CC"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "DD"));

        assertEquals(CableNumbering.NUMBER_0_TSS,                         CableNumbering.getSubsystemLabel("1", "0"));
        assertEquals(CableNumbering.NUMBER_1_PSS_ARM,                     CableNumbering.getSubsystemLabel("1", "1"));
        assertEquals(CableNumbering.NUMBER_2_ODH_SYSTEM,                  CableNumbering.getSubsystemLabel("1", "2"));
        assertEquals(CableNumbering.NUMBER_3_OTHER_TARGET_SAFETY,         CableNumbering.getSubsystemLabel("1", "3"));
        assertEquals(CableNumbering.NUMBER_4_OTHER_SAFETY,                CableNumbering.getSubsystemLabel("1", "4"));
        assertEquals(
                CableNumbering.NUMBER_5_ENVIRONMENTAL_COMPLIANCE_SYSTEMS, CableNumbering.getSubsystemLabel("1", "5"));
        assertEquals(CableNumbering.NUMBER_6_MPS,                         CableNumbering.getSubsystemLabel("1", "6"));
        assertEquals(CableNumbering.NUMBER_7_REMS,                        CableNumbering.getSubsystemLabel("1", "7"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "8"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "9"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("1", "N"));

        assertEquals(CableNumbering.NUMBER_0_MAGNETS,                     CableNumbering.getSubsystemLabel("2", "0"));
        assertEquals(CableNumbering.NUMBER_1_I_SRC,                       CableNumbering.getSubsystemLabel("2", "1"));
        assertEquals(CableNumbering.NUMBER_2_DIAGNOSTIC,                  CableNumbering.getSubsystemLabel("2", "2"));
        assertEquals(CableNumbering.NUMBER_3_VACUUM,                      CableNumbering.getSubsystemLabel("2", "3"));
        assertEquals(CableNumbering.NUMBER_4_COOLING,                     CableNumbering.getSubsystemLabel("2", "4"));
        assertEquals(CableNumbering.NUMBER_5_RF,                          CableNumbering.getSubsystemLabel("2", "5"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "6"));
        assertEquals(CableNumbering.NUMBER_7_PS,                          CableNumbering.getSubsystemLabel("2", "7"));
        assertEquals(CableNumbering.NUMBER_8_CNPW,                        CableNumbering.getSubsystemLabel("2", "8"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "9"));
        assertEquals(CableNumbering.A_UPS,                                CableNumbering.getSubsystemLabel("2", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("2", "N"));

        assertEquals(CableNumbering.NUMBER_0_MAGNETS,                     CableNumbering.getSubsystemLabel("3", "0"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "1"));
        assertEquals(CableNumbering.NUMBER_2_DIAGNOSTIC,                  CableNumbering.getSubsystemLabel("3", "2"));
        assertEquals(CableNumbering.NUMBER_3_VACUUM,                      CableNumbering.getSubsystemLabel("3", "3"));
        assertEquals(CableNumbering.NUMBER_4_COOLING,                     CableNumbering.getSubsystemLabel("3", "4"));
        assertEquals(CableNumbering.NUMBER_5_RF,                          CableNumbering.getSubsystemLabel("3", "5"));
        assertEquals(CableNumbering.NUMBER_6_CRYOGENICS,                  CableNumbering.getSubsystemLabel("3", "6"));
        assertEquals(CableNumbering.NUMBER_7_PS,                          CableNumbering.getSubsystemLabel("3", "7"));
        assertEquals(CableNumbering.NUMBER_8_CNPW,                        CableNumbering.getSubsystemLabel("3", "8"));
        assertEquals(CableNumbering.NUMBER_9_SRF,                         CableNumbering.getSubsystemLabel("3", "9"));
        assertEquals(CableNumbering.A_UPS,                                CableNumbering.getSubsystemLabel("3", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("3", "N"));

        assertEquals(CableNumbering.NUMBER_0_MAGNETS,                     CableNumbering.getSubsystemLabel("4", "0"));
        assertEquals(CableNumbering.NUMBER_1_MONITORING_SYSTEMS,          CableNumbering.getSubsystemLabel("4", "1"));
        assertEquals(CableNumbering.NUMBER_2_DIAGNOSTIC,                  CableNumbering.getSubsystemLabel("4", "2"));
        assertEquals(CableNumbering.NUMBER_3_VACUUM,                      CableNumbering.getSubsystemLabel("4", "3"));
        assertEquals(CableNumbering.NUMBER_4_COOLING,                     CableNumbering.getSubsystemLabel("4", "4"));
        assertEquals(CableNumbering.NUMBER_5_RF,                          CableNumbering.getSubsystemLabel("4", "5"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "6"));
        assertEquals(CableNumbering.NUMBER_7_PS,                          CableNumbering.getSubsystemLabel("4", "7"));
        assertEquals(CableNumbering.NUMBER_8_CNPW,                        CableNumbering.getSubsystemLabel("4", "8"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "9"));
        assertEquals(CableNumbering.A_UPS,                                CableNumbering.getSubsystemLabel("4", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("4", "N"));

        assertEquals(CableNumbering.NUMBER_0_ANCILLARY_SYSTEMS,           CableNumbering.getSubsystemLabel("5", "0"));
        assertEquals(CableNumbering.NUMBER_1_HELIUM_SYSTEMS,              CableNumbering.getSubsystemLabel("5", "1"));
        assertEquals(CableNumbering.NUMBER_2_HVAC,                        CableNumbering.getSubsystemLabel("5", "2"));
        assertEquals(CableNumbering.NUMBER_3_MODERATOR_REFLECTOR_SYSTEMS, CableNumbering.getSubsystemLabel("5", "3"));
        assertEquals(CableNumbering.NUMBER_4_MONOLITH_VESSEL_SYSTEMS,     CableNumbering.getSubsystemLabel("5", "4"));
        assertEquals(CableNumbering.NUMBER_5_NEUTRON_BEAM_EXTRACTION,     CableNumbering.getSubsystemLabel("5", "5"));
        assertEquals(CableNumbering.NUMBER_6_OFF_GAS_RELIEF,              CableNumbering.getSubsystemLabel("5", "6"));
        assertEquals(CableNumbering.NUMBER_7_PROTON_BEAM_WINDOW_SYSTEMS,  CableNumbering.getSubsystemLabel("5", "7"));
        assertEquals(CableNumbering.NUMBER_8_REMOTE_HANDLING_TOOLS,       CableNumbering.getSubsystemLabel("5", "8"));
        assertEquals(CableNumbering.NUMBER_9_SHIELDINGS,                  CableNumbering.getSubsystemLabel("5", "9"));
        assertEquals(CableNumbering.A_TARGET_WHEEL_SYSTEMS,               CableNumbering.getSubsystemLabel("5", "A"));
        assertEquals(CableNumbering.B_VACUUM,                             CableNumbering.getSubsystemLabel("5", "B"));
        assertEquals(CableNumbering.C_WATER_COOLING,                      CableNumbering.getSubsystemLabel("5", "C"));
        assertEquals(CableNumbering.D_DIAGNOSTICS,                        CableNumbering.getSubsystemLabel("5", "D"));
        assertEquals(CableNumbering.E_CNPW,                               CableNumbering.getSubsystemLabel("5", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("5", "N"));

        assertEquals(CableNumbering.NUMBER_0_NON_INSTRUMENT_SPECIFIC,     CableNumbering.getSubsystemLabel("6", "0"));
        assertEquals(CableNumbering.NUMBER_1_W1_NMX,                      CableNumbering.getSubsystemLabel("6", "1"));
        assertEquals(CableNumbering.NUMBER_2_W2_BEER,                     CableNumbering.getSubsystemLabel("6", "2"));
        assertEquals(CableNumbering.NUMBER_3_W3_C_SPEC,                   CableNumbering.getSubsystemLabel("6", "3"));
        assertEquals(CableNumbering.NUMBER_4_W4_BIFROST,                  CableNumbering.getSubsystemLabel("6", "4"));
        assertEquals(CableNumbering.NUMBER_5_W5_MIRACLES,                 CableNumbering.getSubsystemLabel("6", "5"));
        assertEquals(CableNumbering.NUMBER_6_W6_MAGIC,                    CableNumbering.getSubsystemLabel("6", "6"));
        assertEquals(CableNumbering.NUMBER_7_W7_T_REX,                    CableNumbering.getSubsystemLabel("6", "7"));
        assertEquals(CableNumbering.NUMBER_8_W8_HEIMDAL,                  CableNumbering.getSubsystemLabel("6", "8"));
        assertEquals(CableNumbering.NUMBER_9_W11_TEST_BEAM,               CableNumbering.getSubsystemLabel("6", "9"));
        assertEquals(CableNumbering.NUMBER_A_N1_HR_NSE,                   CableNumbering.getSubsystemLabel("6", "A"));
        assertEquals(CableNumbering.NUMBER_B_N5_FREIA,                    CableNumbering.getSubsystemLabel("6", "B"));
        assertEquals(CableNumbering.NUMBER_C_N7_LOKI,                     CableNumbering.getSubsystemLabel("6", "C"));
        assertEquals(CableNumbering.NUMBER_D_N9_SLEIPNR,                  CableNumbering.getSubsystemLabel("6", "D"));
        assertEquals(CableNumbering.NUMBER_E_E2_ESTIA,                    CableNumbering.getSubsystemLabel("6", "E"));
        assertEquals(CableNumbering.NUMBER_F_E3_SKADI,                    CableNumbering.getSubsystemLabel("6", "F"));
        assertEquals(CableNumbering.NUMBER_G_E5_ANNI,                     CableNumbering.getSubsystemLabel("6", "G"));
        assertEquals(CableNumbering.NUMBER_H_E7_VESPA,                    CableNumbering.getSubsystemLabel("6", "H"));
        assertEquals(CableNumbering.NUMBER_I_E8_SURFACE_SC,               CableNumbering.getSubsystemLabel("6", "I"));
        assertEquals(CableNumbering.NUMBER_J_E10_SPARE,                   CableNumbering.getSubsystemLabel("6", "J"));
        assertEquals(CableNumbering.NUMBER_K_E11_VOR,                     CableNumbering.getSubsystemLabel("6", "K"));
        assertEquals(CableNumbering.NUMBER_L_S2_ODIN,                     CableNumbering.getSubsystemLabel("6", "L"));
        assertEquals(CableNumbering.NUMBER_M_S3_DREAM,                    CableNumbering.getSubsystemLabel("6", "M"));
        assertEquals(CableNumbering.NUMBER_N_S6_I22,                      CableNumbering.getSubsystemLabel("6", "N"));

        assertEquals(CableNumbering.NUMBER_0_BUNKER_W,                    CableNumbering.getSubsystemLabel("7", "0"));
        assertEquals(CableNumbering.NUMBER_1_BUNKER_N,                    CableNumbering.getSubsystemLabel("7", "1"));
        assertEquals(CableNumbering.NUMBER_2_BUNKER_E,                    CableNumbering.getSubsystemLabel("7", "2"));
        assertEquals(CableNumbering.NUMBER_3_BUNKER_S,                    CableNumbering.getSubsystemLabel("7", "3"));
        assertEquals(CableNumbering.NUMBER_4_CHOPPER_TEST_CMF_1,          CableNumbering.getSubsystemLabel("7", "4"));
        assertEquals(CableNumbering.NUMBER_5_CHOPPER_TEST_CMF_2,          CableNumbering.getSubsystemLabel("7", "5"));
        assertEquals(CableNumbering.NUMBER_6_BUNKER_NW,                   CableNumbering.getSubsystemLabel("7", "6"));
        assertEquals(CableNumbering.NUMBER_7_BUNKER_SE,                   CableNumbering.getSubsystemLabel("7", "7"));
        assertEquals(CableNumbering.NUMBER_8_CNPW,                        CableNumbering.getSubsystemLabel("7", "8"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "9"));
        assertEquals(CableNumbering.A_UPS,                                CableNumbering.getSubsystemLabel("7", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("7", "N"));

        assertEquals(CableNumbering.NUMBER_0_TIMING,                      CableNumbering.getSubsystemLabel("8", "0"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "1"));
        assertEquals(CableNumbering.NUMBER_2_NETWORK,                     CableNumbering.getSubsystemLabel("8", "2"));
        assertEquals(CableNumbering.NUMBER_3_PLC,                         CableNumbering.getSubsystemLabel("8", "3"));
        assertEquals(CableNumbering.NUMBER_4_IOC,                         CableNumbering.getSubsystemLabel("8", "4"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "5"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "6"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "7"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "8"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "9"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("8", "N"));

        assertEquals (CableNumbering.NUMBER_0_ACCP_COMPRESSOR_SYSTEM,     CableNumbering.getSubsystemLabel("9", "0"));
        assertEquals (CableNumbering.NUMBER_1_ACCP_COLDBOX_SYSTEM,        CableNumbering.getSubsystemLabel("9", "1"));
        assertEquals (CableNumbering.NUMBER_2_TICP_COMPRESSOR_SYSTEM,     CableNumbering.getSubsystemLabel("9", "2"));
        assertEquals (CableNumbering.NUMBER_3_TICP_COLDBOX_SYSTEM,        CableNumbering.getSubsystemLabel("9", "3"));
        assertEquals (CableNumbering.NUMBER_4_TMCP_COMPRESSOR_SYSTEM,     CableNumbering.getSubsystemLabel("9", "4"));
        assertEquals (CableNumbering.NUMBER_5_TMCP_COLDBOX_SYSTEM,        CableNumbering.getSubsystemLabel("9", "5"));
        assertEquals (CableNumbering.NUMBER_6_HELIUM_RECOVERY_STORAGE,    CableNumbering.getSubsystemLabel("9", "6"));
        assertEquals (CableNumbering.NUMBER_7_NITROGEN_SYSTEM,            CableNumbering.getSubsystemLabel("9", "7"));
        assertEquals (CableNumbering.NUMBER_8_CNPW,                       CableNumbering.getSubsystemLabel("9", "8"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "9"));
        assertEquals(CableNumbering.A_UPS,                                CableNumbering.getSubsystemLabel("9", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "B"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "E"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "F"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "G"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "H"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "I"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "J"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "K"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "L"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "M"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "N"));

        assertEquals("", CableNumbering.getSubsystemLabel("9", "a"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "c"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "d"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "D"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "00"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "11"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "99"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "aa"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "cc"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "dd"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "AA"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "CC"));
        assertEquals("", CableNumbering.getSubsystemLabel("9", "DD"));

        assertEquals("", CableNumbering.getSubsystemLabel("10", "0"));
        assertEquals("", CableNumbering.getSubsystemLabel("10", "1"));
        assertEquals("", CableNumbering.getSubsystemLabel("10", "9"));
        assertEquals("", CableNumbering.getSubsystemLabel("10", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("10", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("10", "D"));

        assertEquals("", CableNumbering.getSubsystemLabel("A", "0"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "1"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "9"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "A"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "C"));
        assertEquals("", CableNumbering.getSubsystemLabel("A", "D"));
    }

    /**
     * Test if given subsystem character (number) is valid.
     */
    @Test
    public void isValidSubsystemCharacter(){
        assertFalse(CableNumbering.isValidSubsystemCharacter(null));
        assertFalse(CableNumbering.isValidSubsystemCharacter(""));
        assertFalse(CableNumbering.isValidSubsystemCharacter("!"));

        assertFalse(CableNumbering.isValidSubsystemCharacter("-1"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("0"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("1"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("2"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("3"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("4"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("5"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("6"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("7"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("8"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("9"));

        assertFalse(CableNumbering.isValidSubsystemCharacter("11"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("19"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("1a"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("1d"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("91"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("99"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("9a"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("9d"));

        assertFalse(CableNumbering.isValidSubsystemCharacter("a"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("b"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("c"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("d"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("e"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("f"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("g"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("h"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("i"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("j"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("k"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("l"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("m"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("n"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("o"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("p"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("q"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("r"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("s"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("t"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("u"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("v"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("w"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("x"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("y"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("z"));

        assertTrue (CableNumbering.isValidSubsystemCharacter("A"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("B"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("C"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("D"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("E"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("F"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("G"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("H"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("I"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("J"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("K"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("L"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("M"));
        assertTrue (CableNumbering.isValidSubsystemCharacter("N"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("O"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("P"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("Q"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("R"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("S"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("T"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("U"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("V"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("W"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("X"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("Y"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("Z"));

        assertFalse(CableNumbering.isValidSubsystemCharacter("hfydudu67j"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("A111"));
        assertFalse(CableNumbering.isValidSubsystemCharacter("AB"));
    }

    /**
     * Test if system and subsystem labels are valid.
     */
    @Test
    public void areValidSystemAndSubsystemLabels(){
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels("1", "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels("1", "1"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels("1", "9"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels("1", "a"));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_0_MAGNETS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_1_PSS_ARM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_2_ODH_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_3_OTHER_TARGET_SAFETY));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_4_OTHER_SAFETY));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL,
                CableNumbering.NUMBER_5_ENVIRONMENTAL_COMPLIANCE_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_6_MPS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_7_REMS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_8_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.NUMBER_9_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.A_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_1_SAFETY_ENVIRONMENTAL, CableNumbering.C_NOT_DEFINED));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_0_MAGNETS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_1_I_SRC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_2_DIAGNOSTIC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_3_VACUUM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_4_COOLING));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_5_RF));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_6_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_7_PS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_8_CNPW));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.NUMBER_9_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.A_UPS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_2_NORMAL_CONDUCTING_LINAC, CableNumbering.C_NOT_DEFINED));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_0_MAGNETS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_1_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_2_DIAGNOSTIC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_3_VACUUM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_4_COOLING));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_5_RF));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_6_CRYOGENICS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_7_PS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_8_CNPW));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.NUMBER_9_SRF));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.A_UPS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_3_SUPER_CONDUCTING_LINAC, CableNumbering.C_NOT_DEFINED));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_0_MAGNETS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_1_MONITORING_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_2_DIAGNOSTIC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_3_VACUUM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_4_COOLING));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_5_RF));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_6_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_7_PS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_8_CNPW));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.NUMBER_9_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.A_UPS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_4_HEBT_A2T_DMP_L, CableNumbering.C_NOT_DEFINED));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_0_ANCILLARY_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_1_HELIUM_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_2_HVAC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_3_MODERATOR_REFLECTOR_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_4_MONOLITH_VESSEL_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_5_NEUTRON_BEAM_EXTRACTION));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_6_OFF_GAS_RELIEF));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_7_PROTON_BEAM_WINDOW_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_8_REMOTE_HANDLING_TOOLS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.NUMBER_9_SHIELDINGS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.A_TARGET_WHEEL_SYSTEMS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.B_VACUUM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.C_WATER_COOLING));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.D_DIAGNOSTICS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_5_TARGET_SYSTEMS, CableNumbering.E_CNPW));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_0_NON_INSTRUMENT_SPECIFIC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_1_W1_NMX));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_2_W2_BEER));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_3_W3_C_SPEC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_4_W4_BIFROST));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_5_W5_MIRACLES));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_6_W6_MAGIC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_7_W7_T_REX));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_8_W8_HEIMDAL));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_9_W11_TEST_BEAM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_A_N1_HR_NSE));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_B_N5_FREIA));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_C_N7_LOKI));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_D_N9_SLEIPNR));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_E_E2_ESTIA));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_F_E3_SKADI));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_G_E5_ANNI));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_H_E7_VESPA));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_I_E8_SURFACE_SC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_J_E10_SPARE));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_K_E11_VOR));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_L_S2_ODIN));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_M_S3_DREAM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_6_NSS_INSTRUMENTS, CableNumbering.NUMBER_N_S6_I22));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_0_BUNKER_W));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_1_BUNKER_N));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_2_BUNKER_E));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_3_BUNKER_S));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_4_CHOPPER_TEST_CMF_1));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_5_CHOPPER_TEST_CMF_2));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_6_BUNKER_NW));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_7_BUNKER_SE));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_8_CNPW));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.NUMBER_9_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.A_UPS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_7_NSS_ADDITIONAL_OR_PARTS_OF_INSTRUMENTS,
                CableNumbering.C_NOT_DEFINED));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_0_TIMING));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_1_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_2_NETWORK));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_3_PLC));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_4_IOC));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_5_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_6_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_7_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_8_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.NUMBER_9_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.A_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_8_ICS, CableNumbering.C_NOT_DEFINED));

        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, "0"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, "A"));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_0_TSS));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_0_ACCP_COMPRESSOR_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_1_ACCP_COLDBOX_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_2_TICP_COMPRESSOR_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_3_TICP_COLDBOX_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_4_TMCP_COMPRESSOR_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_5_TMCP_COLDBOX_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_6_HELIUM_RECOVERY_STORAGE));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_7_NITROGEN_SYSTEM));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_8_CNPW));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.NUMBER_9_NOT_DEFINED));
        assertTrue (CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.A_UPS));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.B_NOT_DEFINED));
        assertFalse(CableNumbering.areValidSystemAndSubsystemLabels(
                CableNumbering.SYSTEM_LABEL_9_CRYOGENICS, CableNumbering.C_NOT_DEFINED));
    }

    // class --------------------------------------------------------------------------------------

    /**
     * Test for getting class character (letter) for cable given class label.
     */
    @Test
    public void getCableClassLetter(){
        assertEquals("",  CableNumbering.getCableClassLetter(null));
        assertEquals("",  CableNumbering.getCableClassLetter(""));
        assertEquals("",  CableNumbering.getCableClassLetter("!"));

        assertEquals("",  CableNumbering.getCableClassLetter("-1"));
        assertEquals("",  CableNumbering.getCableClassLetter("0"));
        assertEquals("",  CableNumbering.getCableClassLetter("1"));
        assertEquals("",  CableNumbering.getCableClassLetter("2"));
        assertEquals("",  CableNumbering.getCableClassLetter("3"));
        assertEquals("",  CableNumbering.getCableClassLetter("4"));
        assertEquals("",  CableNumbering.getCableClassLetter("5"));
        assertEquals("",  CableNumbering.getCableClassLetter("6"));
        assertEquals("",  CableNumbering.getCableClassLetter("7"));
        assertEquals("",  CableNumbering.getCableClassLetter("8"));
        assertEquals("",  CableNumbering.getCableClassLetter("9"));

        assertEquals("",  CableNumbering.getCableClassLetter("a"));
        assertEquals("",  CableNumbering.getCableClassLetter("b"));
        assertEquals("",  CableNumbering.getCableClassLetter("c"));
        assertEquals("",  CableNumbering.getCableClassLetter("d"));
        assertEquals("",  CableNumbering.getCableClassLetter("e"));
        assertEquals("",  CableNumbering.getCableClassLetter("f"));
        assertEquals("",  CableNumbering.getCableClassLetter("g"));
        assertEquals("",  CableNumbering.getCableClassLetter("h"));
        assertEquals("",  CableNumbering.getCableClassLetter("i"));
        assertEquals("",  CableNumbering.getCableClassLetter("j"));
        assertEquals("",  CableNumbering.getCableClassLetter("k"));
        assertEquals("",  CableNumbering.getCableClassLetter("l"));
        assertEquals("",  CableNumbering.getCableClassLetter("m"));
        assertEquals("",  CableNumbering.getCableClassLetter("n"));
        assertEquals("",  CableNumbering.getCableClassLetter("o"));
        assertEquals("",  CableNumbering.getCableClassLetter("p"));
        assertEquals("",  CableNumbering.getCableClassLetter("q"));
        assertEquals("",  CableNumbering.getCableClassLetter("r"));
        assertEquals("",  CableNumbering.getCableClassLetter("s"));
        assertEquals("",  CableNumbering.getCableClassLetter("t"));
        assertEquals("",  CableNumbering.getCableClassLetter("u"));
        assertEquals("",  CableNumbering.getCableClassLetter("v"));
        assertEquals("",  CableNumbering.getCableClassLetter("w"));
        assertEquals("",  CableNumbering.getCableClassLetter("x"));
        assertEquals("",  CableNumbering.getCableClassLetter("y"));
        assertEquals("",  CableNumbering.getCableClassLetter("z"));

        assertEquals("A", CableNumbering.getCableClassLetter("A"));
        assertEquals("B", CableNumbering.getCableClassLetter("B"));
        assertEquals("C", CableNumbering.getCableClassLetter("C"));
        assertEquals("D", CableNumbering.getCableClassLetter("D"));
        assertEquals("E", CableNumbering.getCableClassLetter("E"));
        assertEquals("F", CableNumbering.getCableClassLetter("F"));
        assertEquals("G", CableNumbering.getCableClassLetter("G"));
        assertEquals("H", CableNumbering.getCableClassLetter("H"));
        assertEquals("",  CableNumbering.getCableClassLetter("I"));
        assertEquals("",  CableNumbering.getCableClassLetter("J"));
        assertEquals("",  CableNumbering.getCableClassLetter("K"));
        assertEquals("",  CableNumbering.getCableClassLetter("L"));
        assertEquals("",  CableNumbering.getCableClassLetter("M"));
        assertEquals("",  CableNumbering.getCableClassLetter("N"));
        assertEquals("",  CableNumbering.getCableClassLetter("O"));
        assertEquals("",  CableNumbering.getCableClassLetter("P"));
        assertEquals("",  CableNumbering.getCableClassLetter("Q"));
        assertEquals("",  CableNumbering.getCableClassLetter("R"));
        assertEquals("",  CableNumbering.getCableClassLetter("S"));
        assertEquals("",  CableNumbering.getCableClassLetter("T"));
        assertEquals("",  CableNumbering.getCableClassLetter("U"));
        assertEquals("",  CableNumbering.getCableClassLetter("V"));
        assertEquals("",  CableNumbering.getCableClassLetter("W"));
        assertEquals("",  CableNumbering.getCableClassLetter("Y"));
        assertEquals("",  CableNumbering.getCableClassLetter("Z"));

        assertEquals("A", CableNumbering.getCableClassLetter("A777777"));
        assertEquals("A", CableNumbering.getCableClassLetter("A (777)"));

        assertEquals("A", CableNumbering.getCableClassLetter("A1"));
        assertEquals("B", CableNumbering.getCableClassLetter("B22"));
        assertEquals("C", CableNumbering.getCableClassLetter("C333"));
        assertEquals("D", CableNumbering.getCableClassLetter("D4444"));
        assertEquals("E", CableNumbering.getCableClassLetter("E55555"));
        assertEquals("F", CableNumbering.getCableClassLetter("F666666"));
        assertEquals("G", CableNumbering.getCableClassLetter("G7777777"));
        assertEquals("H", CableNumbering.getCableClassLetter("H88888888"));

        assertEquals("A", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V));
        assertEquals("B", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V));
        assertEquals("C", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC));
        assertEquals("D", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE));
        assertEquals("E", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_E_DC_POWER));
        assertEquals("F", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE));
        assertEquals("G", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V));
        assertEquals("H", CableNumbering.getCableClassLetter(
                CableNumbering.CLASS_LABEL_H_POWER_SYSTEM_GROUNDING));
    }

    /**
     * Test for getting class label for cable given class character (letter).
     */
    @Test
    public void getClassLabel(){
        assertEquals("", CableNumbering.getClassLabel(null));
        assertEquals("", CableNumbering.getClassLabel(""));
        assertEquals("", CableNumbering.getClassLabel("!"));

        assertEquals("", CableNumbering.getClassLabel("-1"));
        assertEquals("", CableNumbering.getClassLabel("0"));
        assertEquals("", CableNumbering.getClassLabel("1"));
        assertEquals("", CableNumbering.getClassLabel("2"));
        assertEquals("", CableNumbering.getClassLabel("3"));
        assertEquals("", CableNumbering.getClassLabel("4"));
        assertEquals("", CableNumbering.getClassLabel("5"));
        assertEquals("", CableNumbering.getClassLabel("6"));
        assertEquals("", CableNumbering.getClassLabel("7"));
        assertEquals("", CableNumbering.getClassLabel("8"));
        assertEquals("", CableNumbering.getClassLabel("9"));

        assertEquals("", CableNumbering.getClassLabel("a"));
        assertEquals("", CableNumbering.getClassLabel("b"));
        assertEquals("", CableNumbering.getClassLabel("c"));
        assertEquals("", CableNumbering.getClassLabel("d"));
        assertEquals("", CableNumbering.getClassLabel("e"));
        assertEquals("", CableNumbering.getClassLabel("f"));
        assertEquals("", CableNumbering.getClassLabel("g"));
        assertEquals("", CableNumbering.getClassLabel("h"));
        assertEquals("", CableNumbering.getClassLabel("i"));
        assertEquals("", CableNumbering.getClassLabel("j"));
        assertEquals("", CableNumbering.getClassLabel("k"));
        assertEquals("", CableNumbering.getClassLabel("l"));
        assertEquals("", CableNumbering.getClassLabel("m"));
        assertEquals("", CableNumbering.getClassLabel("n"));
        assertEquals("", CableNumbering.getClassLabel("o"));
        assertEquals("", CableNumbering.getClassLabel("p"));
        assertEquals("", CableNumbering.getClassLabel("q"));
        assertEquals("", CableNumbering.getClassLabel("r"));
        assertEquals("", CableNumbering.getClassLabel("s"));
        assertEquals("", CableNumbering.getClassLabel("t"));
        assertEquals("", CableNumbering.getClassLabel("u"));
        assertEquals("", CableNumbering.getClassLabel("v"));
        assertEquals("", CableNumbering.getClassLabel("w"));
        assertEquals("", CableNumbering.getClassLabel("x"));
        assertEquals("", CableNumbering.getClassLabel("y"));
        assertEquals("", CableNumbering.getClassLabel("z"));

        assertEquals(
                CableNumbering.CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V,
                CableNumbering.getClassLabel("A"));
        assertEquals(
                CableNumbering.CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V,
                CableNumbering.getClassLabel("B"));
        assertEquals(
                CableNumbering.CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC,
                CableNumbering.getClassLabel("C"));
        assertEquals(
                CableNumbering.CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE,
                CableNumbering.getClassLabel("D"));
        assertEquals(
                CableNumbering.CLASS_LABEL_E_DC_POWER,
                CableNumbering.getClassLabel("E"));
        assertEquals(
                CableNumbering.CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE,
                CableNumbering.getClassLabel("F"));
        assertEquals(
                CableNumbering.CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V,
                CableNumbering.getClassLabel("G"));
        assertEquals(
                CableNumbering.CLASS_LABEL_H_POWER_SYSTEM_GROUNDING,
                CableNumbering.getClassLabel("H"));

        assertEquals("", CableNumbering.getClassLabel("I"));
        assertEquals("", CableNumbering.getClassLabel("J"));
        assertEquals("", CableNumbering.getClassLabel("K"));
        assertEquals("", CableNumbering.getClassLabel("L"));
        assertEquals("", CableNumbering.getClassLabel("M"));
        assertEquals("", CableNumbering.getClassLabel("N"));
        assertEquals("", CableNumbering.getClassLabel("O"));
        assertEquals("", CableNumbering.getClassLabel("P"));
        assertEquals("", CableNumbering.getClassLabel("Q"));
        assertEquals("", CableNumbering.getClassLabel("R"));
        assertEquals("", CableNumbering.getClassLabel("S"));
        assertEquals("", CableNumbering.getClassLabel("T"));
        assertEquals("", CableNumbering.getClassLabel("U"));
        assertEquals("", CableNumbering.getClassLabel("V"));
        assertEquals("", CableNumbering.getClassLabel("W"));
        assertEquals("", CableNumbering.getClassLabel("X"));
        assertEquals("", CableNumbering.getClassLabel("Y"));
        assertEquals("", CableNumbering.getClassLabel("Z"));

        assertEquals("", CableNumbering.getClassLabel("A111"));
        assertEquals("", CableNumbering.getClassLabel("AB"));
    }

    /**
     * Test if given cable class (character (letter) or label) is valid.
     */
    @Test
    public void isValidCableClass(){
        assertFalse(CableNumbering.isValidCableClass(null));
        assertFalse(CableNumbering.isValidCableClass(""));
        assertFalse(CableNumbering.isValidCableClass("!"));

        assertFalse(CableNumbering.isValidCableClass("-1"));
        assertFalse(CableNumbering.isValidCableClass("0"));
        assertFalse(CableNumbering.isValidCableClass("1"));
        assertFalse(CableNumbering.isValidCableClass("2"));
        assertFalse(CableNumbering.isValidCableClass("3"));
        assertFalse(CableNumbering.isValidCableClass("4"));
        assertFalse(CableNumbering.isValidCableClass("5"));
        assertFalse(CableNumbering.isValidCableClass("6"));
        assertFalse(CableNumbering.isValidCableClass("7"));
        assertFalse(CableNumbering.isValidCableClass("8"));
        assertFalse(CableNumbering.isValidCableClass("9"));

        assertFalse(CableNumbering.isValidCableClass("a"));
        assertFalse(CableNumbering.isValidCableClass("b"));
        assertFalse(CableNumbering.isValidCableClass("c"));
        assertFalse(CableNumbering.isValidCableClass("d"));
        assertFalse(CableNumbering.isValidCableClass("e"));
        assertFalse(CableNumbering.isValidCableClass("f"));
        assertFalse(CableNumbering.isValidCableClass("g"));
        assertFalse(CableNumbering.isValidCableClass("h"));
        assertFalse(CableNumbering.isValidCableClass("i"));
        assertFalse(CableNumbering.isValidCableClass("j"));
        assertFalse(CableNumbering.isValidCableClass("k"));
        assertFalse(CableNumbering.isValidCableClass("l"));
        assertFalse(CableNumbering.isValidCableClass("m"));
        assertFalse(CableNumbering.isValidCableClass("n"));
        assertFalse(CableNumbering.isValidCableClass("o"));
        assertFalse(CableNumbering.isValidCableClass("p"));
        assertFalse(CableNumbering.isValidCableClass("q"));
        assertFalse(CableNumbering.isValidCableClass("r"));
        assertFalse(CableNumbering.isValidCableClass("s"));
        assertFalse(CableNumbering.isValidCableClass("t"));
        assertFalse(CableNumbering.isValidCableClass("u"));
        assertFalse(CableNumbering.isValidCableClass("v"));
        assertFalse(CableNumbering.isValidCableClass("w"));
        assertFalse(CableNumbering.isValidCableClass("x"));
        assertFalse(CableNumbering.isValidCableClass("y"));
        assertFalse(CableNumbering.isValidCableClass("z"));

        assertTrue (CableNumbering.isValidCableClass("A"));
        assertTrue (CableNumbering.isValidCableClass("B"));
        assertTrue (CableNumbering.isValidCableClass("C"));
        assertTrue (CableNumbering.isValidCableClass("D"));
        assertTrue (CableNumbering.isValidCableClass("E"));
        assertTrue (CableNumbering.isValidCableClass("F"));
        assertTrue (CableNumbering.isValidCableClass("G"));
        assertTrue (CableNumbering.isValidCableClass("H"));
        assertFalse(CableNumbering.isValidCableClass("I"));
        assertFalse(CableNumbering.isValidCableClass("J"));
        assertFalse(CableNumbering.isValidCableClass("K"));
        assertFalse(CableNumbering.isValidCableClass("L"));
        assertFalse(CableNumbering.isValidCableClass("M"));
        assertFalse(CableNumbering.isValidCableClass("N"));
        assertFalse(CableNumbering.isValidCableClass("O"));
        assertFalse(CableNumbering.isValidCableClass("P"));
        assertFalse(CableNumbering.isValidCableClass("Q"));
        assertFalse(CableNumbering.isValidCableClass("R"));
        assertFalse(CableNumbering.isValidCableClass("S"));
        assertFalse(CableNumbering.isValidCableClass("T"));
        assertFalse(CableNumbering.isValidCableClass("U"));
        assertFalse(CableNumbering.isValidCableClass("V"));
        assertFalse(CableNumbering.isValidCableClass("W"));
        assertFalse(CableNumbering.isValidCableClass("X"));
        assertFalse(CableNumbering.isValidCableClass("Y"));
        assertFalse(CableNumbering.isValidCableClass("Z"));

        assertFalse(CableNumbering.isValidCableClassCharacter("A111"));
        assertFalse(CableNumbering.isValidCableClassCharacter("AB"));

        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_E_DC_POWER));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V));
        assertTrue (CableNumbering.isValidCableClass(
                CableNumbering.CLASS_LABEL_H_POWER_SYSTEM_GROUNDING));
    }

    /**
     * Test if given cable class character (letter) is valid.
     */
    @Test
    public void isValidCableClassCharacter(){
        assertFalse(CableNumbering.isValidCableClassCharacter(null));
        assertFalse(CableNumbering.isValidCableClassCharacter(""));
        assertFalse(CableNumbering.isValidCableClassCharacter("!"));

        assertFalse(CableNumbering.isValidCableClassCharacter("-1"));
        assertFalse(CableNumbering.isValidCableClassCharacter("0"));
        assertFalse(CableNumbering.isValidCableClassCharacter("1"));
        assertFalse(CableNumbering.isValidCableClassCharacter("2"));
        assertFalse(CableNumbering.isValidCableClassCharacter("3"));
        assertFalse(CableNumbering.isValidCableClassCharacter("4"));
        assertFalse(CableNumbering.isValidCableClassCharacter("5"));
        assertFalse(CableNumbering.isValidCableClassCharacter("6"));
        assertFalse(CableNumbering.isValidCableClassCharacter("7"));
        assertFalse(CableNumbering.isValidCableClassCharacter("8"));
        assertFalse(CableNumbering.isValidCableClassCharacter("9"));

        assertFalse(CableNumbering.isValidCableClassCharacter("a"));
        assertFalse(CableNumbering.isValidCableClassCharacter("b"));
        assertFalse(CableNumbering.isValidCableClassCharacter("c"));
        assertFalse(CableNumbering.isValidCableClassCharacter("d"));
        assertFalse(CableNumbering.isValidCableClassCharacter("e"));
        assertFalse(CableNumbering.isValidCableClassCharacter("f"));
        assertFalse(CableNumbering.isValidCableClassCharacter("g"));
        assertFalse(CableNumbering.isValidCableClassCharacter("h"));
        assertFalse(CableNumbering.isValidCableClassCharacter("i"));
        assertFalse(CableNumbering.isValidCableClassCharacter("j"));
        assertFalse(CableNumbering.isValidCableClassCharacter("k"));
        assertFalse(CableNumbering.isValidCableClassCharacter("l"));
        assertFalse(CableNumbering.isValidCableClassCharacter("m"));
        assertFalse(CableNumbering.isValidCableClassCharacter("n"));
        assertFalse(CableNumbering.isValidCableClassCharacter("o"));
        assertFalse(CableNumbering.isValidCableClassCharacter("p"));
        assertFalse(CableNumbering.isValidCableClassCharacter("q"));
        assertFalse(CableNumbering.isValidCableClassCharacter("r"));
        assertFalse(CableNumbering.isValidCableClassCharacter("s"));
        assertFalse(CableNumbering.isValidCableClassCharacter("t"));
        assertFalse(CableNumbering.isValidCableClassCharacter("u"));
        assertFalse(CableNumbering.isValidCableClassCharacter("v"));
        assertFalse(CableNumbering.isValidCableClassCharacter("w"));
        assertFalse(CableNumbering.isValidCableClassCharacter("x"));
        assertFalse(CableNumbering.isValidCableClassCharacter("y"));
        assertFalse(CableNumbering.isValidCableClassCharacter("z"));

        assertTrue (CableNumbering.isValidCableClassCharacter("A"));
        assertTrue (CableNumbering.isValidCableClassCharacter("B"));
        assertTrue (CableNumbering.isValidCableClassCharacter("C"));
        assertTrue (CableNumbering.isValidCableClassCharacter("D"));
        assertTrue (CableNumbering.isValidCableClassCharacter("E"));
        assertTrue (CableNumbering.isValidCableClassCharacter("F"));
        assertTrue (CableNumbering.isValidCableClassCharacter("G"));
        assertTrue (CableNumbering.isValidCableClassCharacter("H"));
        assertFalse(CableNumbering.isValidCableClassCharacter("I"));
        assertFalse(CableNumbering.isValidCableClassCharacter("J"));
        assertFalse(CableNumbering.isValidCableClassCharacter("K"));
        assertFalse(CableNumbering.isValidCableClassCharacter("L"));
        assertFalse(CableNumbering.isValidCableClassCharacter("M"));
        assertFalse(CableNumbering.isValidCableClassCharacter("N"));
        assertFalse(CableNumbering.isValidCableClassCharacter("O"));
        assertFalse(CableNumbering.isValidCableClassCharacter("P"));
        assertFalse(CableNumbering.isValidCableClassCharacter("Q"));
        assertFalse(CableNumbering.isValidCableClassCharacter("R"));
        assertFalse(CableNumbering.isValidCableClassCharacter("S"));
        assertFalse(CableNumbering.isValidCableClassCharacter("T"));
        assertFalse(CableNumbering.isValidCableClassCharacter("U"));
        assertFalse(CableNumbering.isValidCableClassCharacter("V"));
        assertFalse(CableNumbering.isValidCableClassCharacter("W"));
        assertFalse(CableNumbering.isValidCableClassCharacter("X"));
        assertFalse(CableNumbering.isValidCableClassCharacter("Y"));
        assertFalse(CableNumbering.isValidCableClassCharacter("Z"));

        assertFalse(CableNumbering.isValidCableClassCharacter("A111"));
        assertFalse(CableNumbering.isValidCableClassCharacter("AB"));

        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_E_DC_POWER));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V));
        assertFalse(CableNumbering.isValidCableClassCharacter(
                CableNumbering.CLASS_LABEL_H_POWER_SYSTEM_GROUNDING));
    }

    /**
     * Test if given cable class label is valid.
     */
    @Test
    public void isValidCableClassLabel(){
        assertFalse(CableNumbering.isValidCableClassLabel(null));
        assertFalse(CableNumbering.isValidCableClassLabel(""));
        assertFalse(CableNumbering.isValidCableClassLabel("!"));

        assertFalse(CableNumbering.isValidCableClassLabel("-1"));
        assertFalse(CableNumbering.isValidCableClassLabel("0"));
        assertFalse(CableNumbering.isValidCableClassLabel("1"));
        assertFalse(CableNumbering.isValidCableClassLabel("9"));

        assertFalse(CableNumbering.isValidCableClassLabel("a"));
        assertFalse(CableNumbering.isValidCableClassLabel("b"));
        assertFalse(CableNumbering.isValidCableClassLabel("c"));
        assertFalse(CableNumbering.isValidCableClassLabel("d"));
        assertFalse(CableNumbering.isValidCableClassLabel("e"));
        assertFalse(CableNumbering.isValidCableClassLabel("f"));
        assertFalse(CableNumbering.isValidCableClassLabel("g"));
        assertFalse(CableNumbering.isValidCableClassLabel("h"));
        assertFalse(CableNumbering.isValidCableClassLabel("i"));
        assertFalse(CableNumbering.isValidCableClassLabel("j"));
        assertFalse(CableNumbering.isValidCableClassLabel("k"));
        assertFalse(CableNumbering.isValidCableClassLabel("l"));
        assertFalse(CableNumbering.isValidCableClassLabel("m"));
        assertFalse(CableNumbering.isValidCableClassLabel("n"));
        assertFalse(CableNumbering.isValidCableClassLabel("o"));
        assertFalse(CableNumbering.isValidCableClassLabel("p"));
        assertFalse(CableNumbering.isValidCableClassLabel("q"));
        assertFalse(CableNumbering.isValidCableClassLabel("r"));
        assertFalse(CableNumbering.isValidCableClassLabel("s"));
        assertFalse(CableNumbering.isValidCableClassLabel("t"));
        assertFalse(CableNumbering.isValidCableClassLabel("u"));
        assertFalse(CableNumbering.isValidCableClassLabel("v"));
        assertFalse(CableNumbering.isValidCableClassLabel("w"));
        assertFalse(CableNumbering.isValidCableClassLabel("x"));
        assertFalse(CableNumbering.isValidCableClassLabel("y"));
        assertFalse(CableNumbering.isValidCableClassLabel("z"));

        assertFalse(CableNumbering.isValidCableClassLabel("A"));
        assertFalse(CableNumbering.isValidCableClassLabel("B"));
        assertFalse(CableNumbering.isValidCableClassLabel("C"));
        assertFalse(CableNumbering.isValidCableClassLabel("D"));
        assertFalse(CableNumbering.isValidCableClassLabel("E"));
        assertFalse(CableNumbering.isValidCableClassLabel("F"));
        assertFalse(CableNumbering.isValidCableClassLabel("G"));
        assertFalse(CableNumbering.isValidCableClassLabel("H"));
        assertFalse(CableNumbering.isValidCableClassLabel("I"));
        assertFalse(CableNumbering.isValidCableClassLabel("J"));
        assertFalse(CableNumbering.isValidCableClassLabel("K"));
        assertFalse(CableNumbering.isValidCableClassLabel("L"));
        assertFalse(CableNumbering.isValidCableClassLabel("M"));
        assertFalse(CableNumbering.isValidCableClassLabel("N"));
        assertFalse(CableNumbering.isValidCableClassLabel("O"));
        assertFalse(CableNumbering.isValidCableClassLabel("P"));
        assertFalse(CableNumbering.isValidCableClassLabel("Q"));
        assertFalse(CableNumbering.isValidCableClassLabel("R"));
        assertFalse(CableNumbering.isValidCableClassLabel("S"));
        assertFalse(CableNumbering.isValidCableClassLabel("T"));
        assertFalse(CableNumbering.isValidCableClassLabel("U"));
        assertFalse(CableNumbering.isValidCableClassLabel("V"));
        assertFalse(CableNumbering.isValidCableClassLabel("W"));
        assertFalse(CableNumbering.isValidCableClassLabel("X"));
        assertFalse(CableNumbering.isValidCableClassLabel("Y"));
        assertFalse(CableNumbering.isValidCableClassLabel("Z"));

        assertFalse(CableNumbering.isValidCableClassLabel("A111"));
        assertFalse(CableNumbering.isValidCableClassLabel("AB"));

        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_A_VERY_LOW_LEVEL_SIGNALS_10V));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_B_SIGNAL_AND_INSTRUMENTATION_10V_AND_50V));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_C_CONTROL_SIGNALS_50V_AND_230V_AC_OR_DC));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_D_LOW_POWER_LOW_VOLTAGE_AC_1K_V_AND_32A_PER_PHASE));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_E_DC_POWER));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_F_HIGH_POWER_LOW_VOLTAGE_AC_1_K_V_AND_32A_PER_PHASE));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_G_MEDIUM_VOLTAGE_AC_1_K_V_AND_50K_V));
        assertTrue (CableNumbering.isValidCableClassLabel(
                CableNumbering.CLASS_LABEL_H_POWER_SYSTEM_GROUNDING));
    }

    // name ---------------------------------------------------------------------------------------

    /**
     * Tests if given cable name is valid.
     */
    @Test
    public void isValidName() {
        assertFalse(CableNumbering.isValidCableName(null));
        assertFalse(CableNumbering.isValidCableName(""));
        assertFalse(CableNumbering.isValidCableName("12c0000014634563456"));
        assertTrue (CableNumbering.isValidCableName("12c000001"));
        assertTrue (CableNumbering.isValidCableName("16B123654"));
    }

}
