/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.webservice;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang3.StringUtils;
import org.arquillian.cube.docker.impl.client.config.Await;
import org.arquillian.cube.docker.junit.rule.ContainerDslRule;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.openepics.cable.category.DockerCategory;
import org.openepics.cable.jaxb.ManufacturerElement;
import org.openepics.cable.util.webservice.ClosableResponse;
import org.openepics.cable.util.webservice.WebserviceUtility;

/**
 * Purpose of class to have integration test for manufacturer part of REST interface of Cable application.
 *
 * @author Lars Johansson
 *
 * @see DockerCategory
 * @see CableRestServiceTestSuiteIT
 *
 * @see ClosableResponse
 * @see WebserviceUtility
 * @see org.openepics.cable.jaxb.ManufacturerElement
 * @see org.openepics.cable.jaxb.ManufacturerResource
 */
@Category(DockerCategory.class)
@RunWith(Arquillian.class)
public class ManufacturerResourceIT {

    /*
        --------------------------------------------------------------------------------
        Note
            - war file to be assembled before integration tests are run
            - setup test
                Docker containers
                    cabledb app
                    cabledb database
                Database content
                    cabledb.sql
            - ContainerDslRule
                cabledb app
                    image - "registry.esss.lu.se/ics-software/cable-db:latest"
                    build - new File("") - cabledb directory with Dockerfile
                cabledb database
                    image - "postgres:9.6.7"
            - JUnit order of execution
                @ClassRule before @BeforeClass
                https://garygregory.wordpress.com/2011/09/25/understaning-junit-method-order-execution/
        --------------------------------------------------------------------------------
        REST API paths

            /manufacturer
            /manufacturer/{name}
        --------------------------------------------------------------------------------
        REST API methods

            @GET
            @ApiOperation(value = "Finds all non-obsolete, approved manufacturers",
                    notes = "Returns a list of approved, non-obsolete manufacturer from DB",
                    response = ManufacturerElement.class,
                    responseContainer = "List")
            @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
            public List<ManufacturerElement> getAllManufacturers();

            @GET
            @ApiOperation(value = "Finds a manufacturer with specific name",
                    notes = "Returns a manufacturer by name from DB",
                    response = ManufacturerElement.class)
            @Path("{name}")
            @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
            public ManufacturerElement getManufacturer(@PathParam("name") String name);
        --------------------------------------------------------------------------------
     */

    /**
     * Docker container for cable application.
     */
    @ClassRule
    public static ContainerDslRule cabledb = new ContainerDslRule(new File(""), "cabledb")
            .withPortBinding("8081->8080/tcp")
            .withEnvironment(
                    "CABLEDB_DATABASE_URL", "jdbc:postgresql://cabledb-postgres:5432/cabledb",
                    "CABLEDB_DATABASE_USERNAME", "cabledb",
                    "CABLEDB_DATABASE_PASSWORD", "cabledb")
            .withNetworkMode("cceco_network")
            .withAwaitStrategy(getAwaitStrategySleepingSeconds(30));

    /**
     * Docker container for cable database.
     */
    @ClassRule
    public static ContainerDslRule cabledb_postgres = new ContainerDslRule("postgres:9.6.7", "cabledb-postgres")
            .withPortBinding("5433->5432/tcp")
            .withEnvironment(
                    "POSTGRES_DB", "cabledb",
                    "POSTGRES_USER", "cabledb",
                    "POSTGRES_PASSWORD", "cabledb")
            .withNetworkMode("cceco_network");

    /**
     * Setup cable database for test.
     */
    @BeforeClass
    public static void setupCabledbPostgres() {
        String url = "jdbc:postgresql://"+cabledb_postgres.getIpAddress()+":"+cabledb_postgres.getBindPort(5432)+"/cabledb";
        String user = "cabledb";
        String password = "cabledb";
        String filePath = "src/test/resources/scripts/cabledb.sql";

        //	handle each line in script as statement
        //		if line empty or starting with comment then continue with next
        //		if content then execute statement
        try (
                Connection con = DriverManager.getConnection(url, user, password);
                BufferedReader br = new BufferedReader(new FileReader(filePath))
                ) {
            String line = null;
            con.setAutoCommit(true);
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (StringUtils.isEmpty(line))
                    continue;
                else if (line.startsWith(SQL_COMMENT))
                    continue;

                con.createStatement().execute(line);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Find out ip address and port to use for test.
     */
    @BeforeClass
    public static void findOutIpAddressBindPort() {
        ipAddress = cabledb.getIpAddress();
        bindPort = cabledb.getBindPort(8080);
    }

    //	---------- Test Utility --------------------------------------------------------

    private static final String SQL_COMMENT = "--";
    private static final String SCHEME = "http";
    private static String ipAddress = "";
    private static int bindPort = -1;

    private static final Await getAwaitStrategySleepingSeconds(int seconds) {
        Await await = new Await();
        await.setStrategy("sleeping");
        await.setSleepTime(String.valueOf(seconds) + " s");
        return await;
    }

    //	--------------------------------------------------------------------------------

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_incorrectEndpoint() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacture";

        List<ManufacturerElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<List<ManufacturerElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    //	see also /manufacturer/{name}
    @Test
    public void rest_cabledb_upAndRunning_incorrectEndpoint_manufacturer() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/manufacturer";

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_incorrectEndpoint_manufacturer_manufacturer() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/Saes Getter/manufacturer";

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    //	--------------------------------------------------------------------------------

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllManufacturers() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer";

        List<ManufacturerElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<List<ManufacturerElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(9, responseData.size());
    }

    //	--------------------------------------------------------------------------------

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getManufacturer() {
        //	HUBER+SUHNER AB
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/Saes Getter";

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals("Saes Getter", responseData.getName());
    }

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getManufacturer_nonExistingValue() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/ABCDDCBA";

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getManufacturer_nonExistingValueSpace() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/9Z99zz-99ZZ+ 9Z99";

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getManufacturer_nonExistingValueTooLong() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<20; i++) {
            sb.append("abcdefghij0123456789");
        }
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/" + sb.toString();

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for manufacturer part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getManufacturer_notAValue() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/manufacturer/!#¤%&()";

        ManufacturerElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<ManufacturerElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

}
