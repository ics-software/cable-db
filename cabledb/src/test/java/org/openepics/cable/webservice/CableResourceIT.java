/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.arquillian.cube.docker.impl.client.config.Await;
import org.arquillian.cube.docker.junit.rule.ContainerDslRule;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.openepics.cable.category.DockerCategory;
import org.openepics.cable.jaxb.CableElement;
import org.openepics.cable.util.webservice.ClosableResponse;
import org.openepics.cable.util.webservice.WebserviceUtility;

/**
 * Purpose of class to have integration test for cable part of REST interface of Cable application.
 *
 * @author Lars Johansson
 *
 * @see DockerCategory
 * @see CableRestServiceTestSuiteIT
 *
 * @see ClosableResponse
 * @see WebserviceUtility
 * @see org.openepics.cable.jaxb.CableElement
 * @see org.openepics.cable.jaxb.CableResource
 */
@Category(DockerCategory.class)
@RunWith(Arquillian.class)
public class CableResourceIT {

    /*
        --------------------------------------------------------------------------------
        Note
            - war file to be assembled before integration tests are run
            - setup test
                Docker containers
                    cabledb app
                    cabledb database
                Database content
                    cabledb.sql
            - ContainerDslRule
                cabledb app
                    image - "registry.esss.lu.se/ics-software/cable-db:latest"
                    build - new File("") - cabledb directory with Dockerfile
                cabledb database
                    image - "postgres:9.6.7"
            - JUnit order of execution
                @ClassRule before @BeforeClass
                https://garygregory.wordpress.com/2011/09/25/understaning-junit-method-order-execution/
        --------------------------------------------------------------------------------
        REST API paths

            /cable
            /cable/{name}
        --------------------------------------------------------------------------------
        REST API methods

            @GET
            @ApiOperation(value = "Finds all cables",
                    notes = "Returns a list of cableElements",
                    response = CableElement.class,
                    responseContainer = "List")
            @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
            public List<CableElement> getAllCables(@QueryParam("field") List<String> field,
                    @QueryParam("regexp") String regExp);

            @GET
            @ApiOperation(value = "Searches for a specific cable name in DB",
                    notes = "Returns the details of the specific cable",
                    response = CableElement.class)
            @Path("{name}")
            @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
            public CableElement getCable(@PathParam("name") String name);
        --------------------------------------------------------------------------------
     */

    /**
     * Docker container for cable application.
     */
    @ClassRule
    public static ContainerDslRule cabledb = new ContainerDslRule(new File(""), "cabledb")
            .withPortBinding("8081->8080/tcp")
            .withEnvironment(
                    "CABLEDB_DATABASE_URL", "jdbc:postgresql://cabledb-postgres:5432/cabledb",
                    "CABLEDB_DATABASE_USERNAME", "cabledb",
                    "CABLEDB_DATABASE_PASSWORD", "cabledb")
            .withNetworkMode("cceco_network")
            .withAwaitStrategy(getAwaitStrategySleepingSeconds(30));

    /**
     * Docker container for cable database.
     */
    @ClassRule
    public static ContainerDslRule cabledb_postgres = new ContainerDslRule("postgres:9.6.7", "cabledb-postgres")
            .withPortBinding("5433->5432/tcp")
            .withEnvironment(
                    "POSTGRES_DB", "cabledb",
                    "POSTGRES_USER", "cabledb",
                    "POSTGRES_PASSWORD", "cabledb")
            .withNetworkMode("cceco_network");

    /**
     * Setup cable database for test.
     */
    @BeforeClass
    public static void setupCabledbPostgres() {
        String url = "jdbc:postgresql://"+cabledb_postgres.getIpAddress()+":"+cabledb_postgres.getBindPort(5432)+"/cabledb";
        String user = "cabledb";
        String password = "cabledb";
        String filePath = "src/test/resources/scripts/cabledb.sql";

        //	handle each line in script as statement
        //		if line empty or starting with comment then continue with next
        //		if content then execute statement
        try (
                Connection con = DriverManager.getConnection(url, user, password);
                BufferedReader br = new BufferedReader(new FileReader(filePath))
                ) {
            String line = null;
            con.setAutoCommit(true);
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (StringUtils.isEmpty(line))
                    continue;
                else if (line.startsWith(SQL_COMMENT))
                    continue;

                con.createStatement().execute(line);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Find out ip address and port to use for test.
     */
    @BeforeClass
    public static void findOutIpAddressBindPort() {
        ipAddress = cabledb.getIpAddress();
        bindPort = cabledb.getBindPort(8080);
    }

    //	---------- Test Utility --------------------------------------------------------

    private static final String SQL_COMMENT = "--";
    private static final String SCHEME = "http";
    private static String ipAddress = "";
    private static int bindPort = -1;

    private static final Await getAwaitStrategySleepingSeconds(int seconds) {
        Await await = new Await();
        await.setStrategy("sleeping");
        await.setSleepTime(String.valueOf(seconds) + " s");
        return await;
    }

    //	--------------------------------------------------------------------------------

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_incorrectEndpoint() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cabl";

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    //	see also /cable/{name}
    @Test
    public void rest_cabledb_upAndRunning_incorrectEndpoint_cable() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/cable";

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_incorrectEndpoint_cable_cable() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/35B002870/cable";

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    //	--------------------------------------------------------------------------------

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(16, responseData.size());
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables_noField_regex() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        //	field
        //		CableColumn - columnLabel
        //		no field - no condition - regex not matter
        //		fields - AND operator
        //		fields but not regex - no condition on fields
        //	regex
        //		same regex for fields

        String regExp = "35B002870";
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add("regexp", regExp);

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(16, responseData.size());
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables_nonExistingFields_noRegex() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        //	field
        //		CableColumn - columnLabel
        //		no field - no condition - regex not matter
        //		fields - AND operator
        //		fields but not regex - no condition on fields
        //	regex
        //		same regex for fields

        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add("field", "Sy");
        map.add("field", "Su");
        map.add("field", "ClCl");
        map.add("field", "NameName");

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables_fields_noRegex() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        //	field
        //		CableColumn - columnLabel
        //		no field - no condition - regex not matter
        //		fields - AND operator
        //		fields but not regex - no condition on fields
        //	regex
        //		same regex for fields

        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add("field", "Sy");
        map.add("field", "Su");
        map.add("field", "Cl");
        map.add("field", "Name");

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(16, responseData.size());
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables_field_regex() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        //	field
        //		CableColumn - columnLabel
        //		no field - no condition - regex not matter
        //		fields - AND operator
        //		fields but not regex - no condition on fields
        //	regex
        //		same regex for fields

        String regExp = "2870";
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add("field", "Name");
        map.add("regexp", regExp);

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(1, responseData.size());
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables_fields_regex() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        //	field
        //		CableColumn - columnLabel
        //		no field - no condition - regex not matter
        //		fields - AND operator
        //		fields but not regex - no condition on fields
        //	regex
        //		same regex for fields

        String regExp = "2";
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add("field", "Sy");
        map.add("field", "Su");
        map.add("regexp", regExp);

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(8, responseData.size());
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getAllCables_fields_regex_mutualExclusion() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable";

        //	field
        //		CableColumn - columnLabel
        //		no field - no condition - regex not matter
        //		fields - AND operator
        //		fields but not regex - no condition on fields
        //	regex
        //		same regex for fields

        String regExp = "35B002870";
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add("field", "Sy");
        map.add("field", "Su");
        map.add("field", "Cl");
        map.add("field", "Name");
        map.add("regexp", regExp);

        List<CableElement> responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, map)) {
            responseData = response.readEntity(new GenericType<List<CableElement>>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertEquals(0, responseData.size());
    }

    //	--------------------------------------------------------------------------------

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getCable() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/35B002870";

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNotNull(responseData);
        assertNotNull(responseData.getId());
        assertNotNull(responseData.getName());
        assertEquals("35B002870", responseData.getName());
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getCable_nonExistingValue() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/ABCDDCBA";

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getCable_nonExistingValueSpace() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/9Z99zz-99ZZ+ 9Z99";

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getCable_nonExistingValueTooLong() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<20; i++) {
            sb.append("abcdefghij0123456789");
        }
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/" + sb.toString();

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

    /**
     * Integration test for cable part of REST interface of Cable application.
     */
    @Test
    public void rest_cabledb_upAndRunning_getCable_notAValue() {
        String url = SCHEME + "://" + ipAddress + ":" + bindPort + "/rest/cable/!#¤%&()";

        CableElement responseData = null;
        try (final ClosableResponse response = WebserviceUtility.getResponseJson(url, null)) {
            responseData = response.readEntity(new GenericType<CableElement>() {});
        } catch (Exception e) {
            responseData = null;
        }

        assertNull(responseData);
    }

}
