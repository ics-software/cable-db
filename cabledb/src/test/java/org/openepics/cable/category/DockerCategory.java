package org.openepics.cable.category;

/**
 * Interface intended to be used for integration tests and may be referred to
 * in build process for control of when integration tests are run.
 *
 * @author Lars Johansson
 */
public interface DockerCategory {

}
