/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable;

import org.apache.commons.lang3.BooleanUtils;

import java.util.Properties;

/**
 * This contains properties of the cable application, such as URL of the UI and services.
 * Properties are loaded from <code>standalone.xml</code> or <tt>Docker</tt> environment.
 * All values can be overridden by setting the system properties. System properties
 * are not cached and are reflected immediately.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public final class CableProperties {

    // See Git to follow the history of the Cable application properties.
    //
    // Cable user manual. It is available as document in web app folder.
    // It is also available in Chess. Current choice is to use document
    // in web app folder. Thus no cable user manual parameter below.

    /** Base web UI application URL property name. */
    public static final String APPLICATION_BASE_URL_PROPERTY_NAME = "cable.applicationBaseURL";
    /** Cable numbering document URL property name. */
    public static final String NUMBERING_DOCUMENT_URL_PROPERTY_NAME = "cable.cableNumberingDocumentURL";
    /** CCDB URL property name. */
    public static final String CCDB_URL = "cable.ccdbURL";
    /** Chess URL property name. */
    public static final String CHESS_URL = "cable.chessURL";
    /** Cable freeze bypass usernames */
    public static final String CABLE_FREEZE_BYPASS_USERNAMES = "cable.freeze.bypass.usernames";
    /** Mail notifications flag property name. */
    public static final String MAIL_NOTIFICATIONS_ENABLED = "cable.mailNotificationsEnabled";
    /** Support mail showed to users */
    public static final String SUPPORT_EMAIL = "cable.supportEmail";
    /** Naming service URL property name. */
    public static final String NAMING_URL = "org.openepics.discs.conf.props.namingAppURL";

    /** Service URL for reading FBS (Facility Breakdown Structure) names. */
    public static final String FBS_APPLICATION_URL = "org.openepics.discs.conf.props.fbsAppURL";

    /** User name of an administrator user. */
    public static final String TEST_ADMIN_USER_PROPERTY_NAME = "cable.test.adminUser";
    /** Password of an administrator user. */
    public static final String TEST_ADMIN_PASSWORD_PROPERTY_NAME = "cable.test.adminPassword";
    /** User name of an user which is cable user. */
    public static final String TEST_CABLE_USER_PROPERTY_NAME = "cable.test.cableUser";
    /** Password of an user which is cable user. */
    public static final String TEST_CABLE_USER_PASSWORD_PROPERTY_NAME = "cable.test.cableUserPassword";
    /** The name of an existing device name in the naming service. */
    public static final String TEST_DEVICE_NAME_PROPERTY_NAME = "cable.test.deviceName";
    /** The name of an existing device name 2 in the naming service. */
    public static final String TEST_DEVICE_NAME_PROPERTY_NAME_2 = "cable.test.deviceName2";

    public static final String BATCH_NOTIFICATION_ENABLED = "cable.batch.notification.enabled";

    private static CableProperties instance;

    private final Properties properties;

    private CableProperties() {
        // Default values for system properties preferably set in standalone.xml.
        // Otherwise, values may be set in Docker environment or below.

        properties = new Properties();
        properties.setProperty(TEST_ADMIN_USER_PROPERTY_NAME, "rbactester1");
        properties.setProperty(TEST_ADMIN_PASSWORD_PROPERTY_NAME, "Changeit!");
        properties.setProperty(TEST_CABLE_USER_PROPERTY_NAME, "rbactester2");
        properties.setProperty(TEST_CABLE_USER_PASSWORD_PROPERTY_NAME, "Changeit!");
        properties.setProperty(TEST_DEVICE_NAME_PROPERTY_NAME, "Sec-Sub:Dis-Dev-0");
        properties.setProperty(TEST_DEVICE_NAME_PROPERTY_NAME_2, "Sec-Sub:Dis-Dev-1");
    }

    /**
     * Returns the singleton instance of this class.
     *
     * @return the singleton instance
     */
    public static synchronized CableProperties getInstance() {
        if (instance == null) {
            instance = new CableProperties();
        }
        return instance;
    }

    /**
     * Searches for the property with the specified name amongst system properties and loaded properties.
     *
     * @param name
     *            the property name
     *
     * @return the value of the property or null if not found
     */
    private String getProperty(String name) {
        final String systemProperty = System.getProperties().getProperty(name);
        return systemProperty != null ? systemProperty : properties.getProperty(name);
    }

    /** @return the value of the {@value #APPLICATION_BASE_URL_PROPERTY_NAME} property. */
    public String getApplicationBaseURL() {
        return getProperty(APPLICATION_BASE_URL_PROPERTY_NAME);
    }

    /** @return the value of the {@value #NUMBERING_DOCUMENT_URL_PROPERTY_NAME} property. */
    public String getCableNumberingDocumentURL() {
        return getProperty(NUMBERING_DOCUMENT_URL_PROPERTY_NAME);
    }

    /** @return the value of the {@value #CCDB_URL} property. */
    public String getCCDBURL() {
        return getProperty(CCDB_URL);
    }

    /** @return the value of the {@value #CHESS_URL} property. */
    public String getChessURL() {
        return getProperty(CHESS_URL);
    }

    /** @return the value of the {@value #CABLE_FREEZE_BYPASS_USERNAMES} property
     * (empty or commaseparated list of usernames). */
    public String getCableFreezeBypassUsernames() {
        return getProperty(CABLE_FREEZE_BYPASS_USERNAMES);
    }

    /** @return the value of the {@value #MAIL_NOTIFICATIONS_ENABLED} property. */
    public boolean isMailNotificationEnabled() {
        return getProperty(MAIL_NOTIFICATIONS_ENABLED).equalsIgnoreCase(Boolean.TRUE.toString());
    }

    /** @return the value of the {@value #SUPPORT_EMAIL} property. */
    public String getSupportEmail() {
        return getProperty(SUPPORT_EMAIL);
    }

    /** @return the value of the {@value #NAMING_URL} property. */
    public String getNamingURL() {
        return getProperty(NAMING_URL);
    }

    /** @return the value of the {@value #FBS_APPLICATION_URL} property. */
    public String getFbsApplicationURL() {
        return getProperty(FBS_APPLICATION_URL);
    }

    /** @return the value of the {@value #TEST_ADMIN_USER_PROPERTY_NAME} property. */
    public String getTestAdminUser() {
        return getProperty(TEST_ADMIN_USER_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_ADMIN_USER_PROPERTY_NAME} property. */
    public String getTestAdminPassword() {
        return getProperty(TEST_ADMIN_PASSWORD_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_CABLE_USER_PROPERTY_NAME} property. */
    public String getTestCableUser() {
        return getProperty(TEST_CABLE_USER_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_CABLE_USER_PASSWORD_PROPERTY_NAME} property. */
    public String getTestCableUserPassword() {
        return getProperty(TEST_CABLE_USER_PASSWORD_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_DEVICE_NAME_PROPERTY_NAME} property. */
    public String getTestDeviceName() {
        return getProperty(TEST_DEVICE_NAME_PROPERTY_NAME);
    }

    /** @return the value of the {@value #TEST_DEVICE_NAME_PROPERTY_NAME_2} property. */
    public String getTestDeviceName2() {
        return getProperty(TEST_DEVICE_NAME_PROPERTY_NAME_2);
    }

    public boolean getBatchNotificationEnabled() {
        return BooleanUtils.toBoolean(getProperty(BATCH_NOTIFICATION_ENABLED));
    }

}
