/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides bulk approved and specific manufacturer data.
 * 
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("manufacturer")
@Api(value = "manufacturer")
public interface ManufacturerResource {

    /** @return returns all non-obsoleted approved manufacturers in the database. */
    @GET
    @ApiOperation(value = "Finds all non-obsolete, approved manufacturers",
            notes = "Returns a list of approved, non-obsolete manufacturer from DB",
            response = ManufacturerElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<ManufacturerElement> getAllManufacturers();
    
    /**
     * Returns a specific approved manufacturer instance.
     * 
     * @param name the name of the manufacturer to retrieve
     * @return the cable type data
     */
    @GET
    @ApiOperation(value = "Finds a manufacturer with specific name",
            notes = "Returns a manufacturer by name from DB",
            response = ManufacturerElement.class)
    @Path("{name}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public ManufacturerElement getManufacturer(@PathParam("name") String name);
}
