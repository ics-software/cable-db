/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a cable endpoint for JSON and XML serialization.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@XmlRootElement(name = "endpoint")
@XmlAccessorType(XmlAccessType.FIELD)
public class EndpointElement {

    private String essName;
    private String fbsTag;
    private String lbsTag;
    private String enclosureEssName;
    private String enclosureFbsTag;
    private String connector;
    private String label;
    private String validity;

    /** Default constructor used by JAXB. */
    public EndpointElement() {/* Default constructor used by JAXB. */}

    /** @return the ESS name */
    public String getEssName() {
        return essName;
    }

    /** @return the FBS tag */
    public String getFbsTag() {
        return fbsTag;
    }

    /** @return the LBS tag */
    public String getLbsTag() {
        return lbsTag;
    }

    /** @return the enclosure ESS name */
    public String getEnclosureEssName() {
        return enclosureEssName;
    }

    /** @return the enclosure FBS tag */
    public String getEnclosureFbsTag() {
        return enclosureFbsTag;
    }

    /** @return the connector */
    public String getConnector() {
        return connector;
    }

    /** @return the label */
    public String getLabel() {
        return label;
    }

    /** @return the validity */
    public String getValidity() {
        return validity;
    }

    /**
     * @param essName the ESS name to set
     */
    public void setEssName(String essName) {
        this.essName = essName;
    }

    /**
    * @param fbsTag FBS tag to set
    */
   public void setFbsTag(String fbsTag) {
       this.fbsTag = fbsTag;
   }

    /**
     * @param lbsTag the LBS tag to set
     */
    public void setLbsTag(String lbsTag) {
        this.lbsTag = lbsTag;
    }

    /**
     * @param enclosureEssName the enclosure ESS name to set
     */
    public void setEnclosureEssName(String enclosureEssName) {
        this.enclosureEssName = enclosureEssName;
    }

    /**
     * @param enclosureFbsTag enclosure FBS tag to set
     */
    public void setEnclosureFbsTag(String enclosureFbsTag) {
        this.enclosureFbsTag = enclosureFbsTag;
    }

    /**
     * @param connector the connector to set
     */
    public void setConnector(String connector) {
        this.connector = connector;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }

}
