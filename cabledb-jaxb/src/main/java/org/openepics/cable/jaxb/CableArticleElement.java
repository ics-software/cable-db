/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.jaxb;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is data transfer object representing a cable article for JSON and XML serialization.
 *
 * @author Lars Johansson
 */
@XmlRootElement(name="cableArticle")
@XmlAccessorType(XmlAccessType.FIELD)
public class CableArticleElement {

    private String manufacturer;
    private String externalId;
    private String erpNumber;
    private String isoClass;
    private String description;
    private String longDescription;
    private String modelType;
    private Float bendingRadius;
    private Float outerDiameter;
    private Float ratedVoltage;
    private Float weightPerLength;
    private String shortName;
    private String shortNameExternalId;
    private String urlChessPart;
    private boolean active = true;
    private Date created;
    private Date modified;

    /** Default constructor used by JAXB. */
    public CableArticleElement() {/* Default constructor used by JAXB. */}

    public String getName() {
        return shortNameExternalId;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getErpNumber() {
        return erpNumber;
    }

    public void setErpNumber(String erpNumber) {
        this.erpNumber = erpNumber;
    }

    public String getIsoClass() {
        return isoClass;
    }

    public void setIsoClass(String isoClass) {
        this.isoClass = isoClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public Float getBendingRadius() {
        return bendingRadius;
    }

    public void setBendingRadius(Float bendingRadius) {
        this.bendingRadius = bendingRadius;
    }

    public Float getOuterDiameter() {
        return outerDiameter;
    }

    public void setOuterDiameter(Float outerDiameter) {
        this.outerDiameter = outerDiameter;
    }

    public Float getRatedVoltage() {
        return ratedVoltage;
    }

    public void setRatedVoltage(Float ratedVoltage) {
        this.ratedVoltage = ratedVoltage;
    }

    public Float getWeightPerLength() {
        return weightPerLength;
    }

    public void setWeightPerLength(Float weightPerLength) {
        this.weightPerLength = weightPerLength;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortNameExternalId() {
        return shortNameExternalId;
    }

    public void setShortNameExternalId(String shortNameExternalId) {
        this.shortNameExternalId = shortNameExternalId;
    }

    public String getUrlChessPart() {
        return urlChessPart;
    }

    public void setUrlChessPart(String urlChessPart) {
        this.urlChessPart = urlChessPart;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

}
