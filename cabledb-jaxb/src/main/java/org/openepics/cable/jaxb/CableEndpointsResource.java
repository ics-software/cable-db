/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package org.openepics.cable.jaxb;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * This resource provides bulk cables and specific cable data.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("cableEndpoints")
@Api(value = "/cableEndpoints")
public interface CableEndpointsResource {

    /**
     * Returns cables with minimal data. The cables are optionally filtered by matching cable properties using
     * regular expression.
     *
     * @param newerThan a timestamp to filter cables - only cables with timestamp newer than modified will be returned
     *
     * @return the cables
     */
    @GET
    @ApiOperation(value = "Finds cable-endpoints that are modified after a specific time",
            notes = "Returns a list of cable-endpoints that are modified after a specific time",
            response = CableEndpointsElement.class,
            responseContainer = "List")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<CableEndpointsElement> getAllCableEndpoints(@QueryParam("newerThan") String newerThan);

    /**
     * Returns a specific cable instance.
     *
     * @param number the number of the cable to retrieve
     * @return the cable instance data
     */
    @GET
    @ApiOperation(value = "Finds cable-endpoint with a specific number",
            notes = "Returns a cable-endpoint that has a specific number",
            response = CableEndpointsElement.class)
    @Path("{number}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CableEndpointsElement getCableEndpoints(@PathParam("number") String number);
}
