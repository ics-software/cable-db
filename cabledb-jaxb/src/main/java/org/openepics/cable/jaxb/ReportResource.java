/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.cable.jaxb;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * This resource provides reports for Cable application.
 *
 * @author Lars Johansson
 */
@Path("report")
@Api(value = "/report")
@Produces({"application/json"})
public interface ReportResource {

    /*
        Reports
            highlight cells in Excel sheet corresponding to changes and return report with cables (Excel)

        Unofficial reports
            Find cables with name and fbs tag, with different mapping in CHESS
            Find cables with name without mapping in CHESS
            Find cables for which ESS name but not FBS tag is used
            Find cables for which at least one FBS tag is without mapping in CHESS
            Find cables with invalid CHESS mapping

        Official reports
            About Cable DB
            Find cables without fbs tag
            Find cables with duplicate cable fbs tag
            Find cables with fbs tag that does not exist in CHESS
            Find cables with fbs tag that are not correct in CHESS (no linking back to same cable attribute)
            Find cables without cable article
            Find cables that are marked invalid
     */

    // ----------------------------------------------------------------------------------------------------
    // Unofficial reports
    // ----------------------------------------------------------------------------------------------------

    /**
     * Unofficial report for cables with name and fbs tag, with different mapping in CHESS.
     *
     * @param system system
     * @param subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables with name and fbs tag, with different mapping in CHESS",
                  notes = "Find cables with name and fbs tag, with different mapping in CHESS. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem, installation package) \n"
                        + "- search (owners, revision)",
                  hidden = true)
    @Path("/cable/namefbstag_differentchessmapping")
    @Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_PLAIN})
    public Response reportCablesNameFbsTagDifferentChessMapping(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Unofficial report for cables with name without mapping in CHESS.
     *
     * @param system system
     * @param subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables with name without mapping in CHESS",
                  notes = "Find cables with name without mapping in CHESS. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem, installation package) \n"
                        + "- search (owners, revision)",
                  hidden = true)
    @Path("/cable/name_nochessmapping")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesNameNoChessMapping(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Unofficial report for cables for which ESS name but not FBS tag is used.
     *
     * <p>
     * Fields
     * <ul>
     * <li>From ESS Name</li>
     * <li>From FBS Tag</li>
     * <li>To ESS Name</li>
     * <li>To FBS Tag</li>
     * </ul>
     *
     * @param system system
     * @param subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables for which ESS name but not FBS tag is used",
                  notes = "Find cables for which ESS name but not FBS tag is used. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem, installation package) \n"
                        + "- search (owners, revision)",
                  hidden = true)
    @Path("/cable/essname_nofbstag")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesEssNameNoFbsTag(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Unofficial report for cables for which at least one FBS tag is without mapping in CHESS.
     *
     * <p>
     * Fields
     * <ul>
     * <li>FBS Tag</li>
     * <li>From FBS Tag</li>
     * <li>From Enclosure FBS Tag</li>
     * <li>To FBS Tag</li>
     * <li>To Enclosure FBS Tag</li>
     * </ul>
     *
     * @param system system
     * @param subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables for which at least one FBS tag is without mapping in CHESS",
                  notes = "Find cables for which at least one FBS tag is without mapping in CHESS. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem, installation package) \n"
                        + "- search (owners, revision)",
                  hidden = true)
    @Path("/cable/fbstag_nochessmapping")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesFbsTagNoChessMapping(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Unofficial report for cables with invalid CHESS mapping.
     *
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables with invalid CHESS mapping",
                  notes = "Find cables with invalid CHESS mapping. \n\n "
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)",
                  hidden = true)
    @Path("/cable/chessmapping_invalid")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesChessInvalid(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    // ----------------------------------------------------------------------------------------------------
    // Official reports
    // ----------------------------------------------------------------------------------------------------

    /**
     * Report about Cable DB.
     *
     * @return report about Cable DB (text)
     */
    @GET
    @ApiOperation(value = "About Cable DB",
                  notes = "About Cable DB. \n\n "
                        + "Return report about Cable DB (text). \n\n"
                        + "Content \n"
                        + "- Metrics for CHESS and Cable DB \n"
                        + "-- 1) # entries - available, usage - ess names, fbs tags, lbs tags \n"
                        + "- Metrics for Cable DB \n"
                        + "-- 1) # entries - overview         - cables, cable articles, cable types, installation packages, connectors, manufacturers, logs \n"
                        + "-- 2) # entries - usage, overview  - cable, system, system subsystem, system subsystem class, class, owner, cable article, cable type, installation package, connector \n"
                        + "-- 3) # entries - usage, details   - in use     (of active) - system, system subsystem, system subsystem class, class, owner, cable article, cable type, installation package, connector \n"
                        + "-- 4) # entries - usage, details   - not in use (of active) - cable article, cable type, installation package, connector \n"
                        + "-- 5) # entries - usage, details   - in use (invalid)       - cable, cable article, cable type, installation package, connector")
    @Path("/about")
    @Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_PLAIN})
    public String reportAbout();

    /**
     * Report for cables without fbs tag.
     *
     * @param cableFbsTag cable fbs tag
     * @param endpointFbsTag from/to endpoint fbs tag
     * @param enclosureFbsTag from/to enclosure fbs tag
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return
     */
    @GET
    @ApiOperation(value = "Find cables without fbs tag",
                  notes = "Find cables without fbs tag. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- true/false (cable fbs tag, from/to endpoint fbs tag, from/to enclosure fbs tag) \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)")
    @Path("/cable/1_fbstag_without")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesFbsTagWithout(
            @DefaultValue(value="true") @QueryParam("cableFbsTag") Boolean cableFbsTag,
            @DefaultValue(value="true") @QueryParam("endpointFbsTag") Boolean endpointFbsTag,
            @DefaultValue(value="true") @QueryParam("enclosureFbsTag") Boolean enclosureFbsTag,
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Report for cables with duplicate fbs tag.
     *
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables with duplicate cable fbs tag",
                  notes = "Find cables with duplicate cable fbs tag. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)")
    @Path("/cable/2_fbstag_duplicate")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesFbsTagDuplicate(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Report for cables with fbs tag that does not exist in CHESS.
     *
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables with fbs tag that does not exist in CHESS",
                  notes = "Find cables with fbs tag that does not exist in CHESS. \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)")
    @Path("/cable/3_fbstag_not_exist_in_chess")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesFbsTagNotExistInChess(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Report for cables with fbs tag that are not correct in CHESS (no linking back to same cable attribute).
     *
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables with fbs tag that are not correct in CHESS (no linking back to same cable attribute)",
                  notes = "Find cables with fbs tag that are not correct in CHESS (no linking back to same cable attribute). \n\n"
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)")
    @Path("/cable/4_fbstag_not_correct_in_chess")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesFbsTagNotCorrectInChess(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Report for cables without cable article.
     *
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables without cable article",
                  notes = "Find cables without cable article. \n\n "
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)")
    @Path("/cable/5_cablearticle_without")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesCableArticleWithout(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

    /**
     * Report for cables that are marked invalid.
     *
     * @param system system
     * @param subsystem subsystem subsystem
     * @param installationPackage installation package
     * @param owners owners
     * @param revision revision
     * @return report with cables (Excel)
     */
    @GET
    @ApiOperation(value = "Find cables that are marked invalid",
                  notes = "Find cables that are marked invalid. \n\n "
                        + "Return report with cables (Excel). Relevant cells and values are highlighted. Reasons for invalid cables are available as comments in highlighted cells. \n\n"
                        + "How to use parameters \n"
                        + "- may be empty \n"
                        + "- case sensitive \n"
                        + "- exact match (system, subsystem) \n"
                        + "- search (installation package, owners, revision)")
    @Path("/cable/invalid")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response reportCablesInvalid(
            @QueryParam("system") String system,
            @QueryParam("subsystem") String subsystem,
            @QueryParam("installationPackage") String installationPackage,
            @QueryParam("owners") String owners,
            @QueryParam("revision") String revision);

}
