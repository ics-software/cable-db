About scripts
----------------------------------------------------------------------------------------------------

scripts
    1) delete_orphaned_unused_artifacts.sql
    2) remove_rows_from_history_content_no_change.sql
    3) set_fields_to_null_where_fields_empty.sql

1) used in Jira ticket CD-626
       purpose
           more clean database
           remove orphaned and unused artifacts and reduce size of database
           remove 130 out of 722 artifacts

2) used in Jira ticket CD-625
       purpose
           more clean database
           remove rows with text "Changes:: (no change)" from history table
           remove about 700000 out of 1400000 rows

3) used in Jira ticket CD-625
       purpose
           more clean database
           give better sorting on columns in UI
           in combination with adjustments in application
